<!DOCTYPE html>
<html lang="en" xmlns:th="http://thymeleaf.org"
      xmlns="http://www.w3.org/1999/xhtml"
      xmlns:svg="http://www.w3.org/2000/svg"
      xmlns:xlink="http://www.w3.org/1999/xlink">
<head>

    <meta charset="utf-8">

    <title> Eleven Kings </title>
    <meta name="description" content="">

    <!-- Template Basic Images Start -->
    {{--<link rel="apple-touch-icon" sizes="57x57" href="{{ url('static/img/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ url('static/img/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ url('static/img/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('static/img/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ url('static/img/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ url('static/img/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ url('static/img/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ url('static/img/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('static/img/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ url('static/img/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('static/img/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ url('static/img/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('static/img/favicon/favicon-16x16.png') }}">
    <meta name="msapplication-TileImage" content="{{ url('static/img/favicon/ms-icon-144x144.png') }}">--}}
{{--    <link rel="icon" type="image/png" href="{{ url('/client/img/favicon/favicon.ico') }}">--}}
    <link rel="icon" type="image/png" href="{{ url('/static/img/favicon/fav.png') }}">
    <meta name="csrf" content="{{ csrf_token() }}">
    <!-- <link rel="manifest" href="/manifest.json"> -->
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta property="og:image" content="{{ url('static/img/og/img.png') }}">
    <!-- Template Basic Images End -->

    <!-- Custom Browsers Color Start -->
    <meta name="theme-color" content="#000">
    <!-- Custom Browsers Color End -->

    <link rel="stylesheet" href="{{ url('static/bootstrap/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('static/css/app.min.css') }}?v=1">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-148120535-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-148120535-1');
    </script>

</head>
<body>

<?php
$langs = config('app.locales');
foreach ($langs as $lang)
    'href='.route('change_language', ['code' => $lang])
?>

@yield('content')


<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
<script src="{{ url('static/js/libs.min.js') }}"></script>
<script src="{{ url('static/js/home.js') }}?v=<? php echo true() ?>"></script>
<script src="{{ url('static/bootstrap/js/bootstrap.js') }}"></script>
<script src="{{ url('static/js/common.js') }}?v=<? php echo true() ?>"></script>
@stack('js')
</body>
</html>
