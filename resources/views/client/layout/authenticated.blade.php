<!DOCTYPE html>
<html lang="en" xmlns:th="http://thymeleaf.org"
      xmlns="http://www.w3.org/1999/xhtml"
      xmlns:svg="http://www.w3.org/2000/svg"
      xmlns:xlink="http://www.w3.org/1999/xlink">
<head>

    <meta charset="utf-8">

    <title> Eleven Kings </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf" content="{{ csrf_token() }}">

    <!-- Template hsic Images Start -->
    {{--<link rel="apple-touch-icon" sizes="57x57" href="{{ url('static/img/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ url('static/img/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ url('static/img/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('static/img/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ url('static/img/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ url('static/img/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ url('static/img/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ url('static/img/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('static/img/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ url('static/img/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('static/img/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ url('static/img/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('static/img/favicon/favicon-16x16.png') }}">--}}
    <!-- <link rel="manifest" href="/manifest.json"> -->
    <link rel="icon" type="image/png" href="{{ url('/static/img/favicon/fav.png') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ url('static/img/favicon/ms-icon-144x144.png') }}">
    <meta property="og:image" content="{{ url('static/img/og/img.png') }}">
    <!-- Template Basic Images End -->

    <!-- Custom Browsers Color Start -->
    <meta name="theme-color" content="#000">
    <!-- Custom Browsers Color End -->

    <link rel="stylesheet" href="{{ url('static/css/app.min.css') }}">

</head>
<body>
<!-- =========================
    Section -  Wrapper
============================== -->
<div class="ek--wrapper">
    <!-- =========================
        Section -  Header
    ============================== -->
    <header class="ek--header">
        <!-- EK Header logo -->
        <div class="ek--header--logo">
            <a  href="{{ route("home") }}">
                <img src="{{url("/static/img/Logo/Ek--logo.svg")}}" alt="ek--logo">
            </a>
            <div class="ek--header--logo--close">
                <svg class="icon icon-ek--close--icon fill--navy--200"><use xlink:href="{{url("/static/img/icons.svg#icon-ek--close--icon")}}"></use></svg>
            </div>
        </div>
        <!-- End EK Header logo -->
        <!-- EK Header Navbar -->
        <ul class="ek--header--nav">
            <li class="ek--header--item"><a class="ek--header--link {{ (request()->is('home')) ? 'ek--header--link--active' : '' }}"  href="{{ route("home") }}"><svg class="icon icon-ek--overwiev"><use xlink:href="{{url("/static/img/icons.svg#icon-ek--overwiev")}}"></use></svg> {{  __('translation.menu_home') }}</a></li>
        </ul>

        <ul class="ek--header--nav">
            <li class="ek--header--item"><a class="ek--header--link {{ (request()->is('squad')) ? 'ek--header--link--active' : '' }}"  href="{{ route("squad") }}"><svg class="icon icon-ek--squad"><use xlink:href="{{url("/static/img/icons.svg#icon-ek--squad")}}"></use></svg> {{  __('translation.menu_squad') }}</a></li>
            <li class="ek--header--item"><a class="ek--header--link {{ (request()->is('lineup')) ? 'ek--header--link--active' : '' }}"  href="{{ route("lineup") }}"><svg class="icon icon-ek--lineup"><use xlink:href="{{url("/static/img/icons.svg#icon-ek--lineup")}}"></use></svg> {{  __('translation.menu_lineup') }}</a></li>
            <li class="ek--header--item"><a class="ek--header--link {{ (request()->is('daily')) ? 'ek--header--link--active' : '' }}"  href="{{ route("daily") }}"><svg class="icon icon-ek--daily"><use xlink:href="{{url("/static/img/icons.svg#icon-ek--daily")}}"></use></svg> {{  __('translation.menu_training') }}</a></li>
            <li class="ek--header--item"><a class="ek--header--link {{ (request()->is('training')) ? 'ek--header--link--active' : '' }}"  href="{{ route("training") }}"><svg class="icon icon-ek--camp"><use xlink:href="{{url("/static/img/icons.svg#icon-ek--camp")}}"></use></svg> {{  __('translation.menu_camp') }}</a></li>
            <li class="ek--header--item"><a class="ek--header--link {{ (request()->is('transfer')) ? 'ek--header--link--active' : '' }}"  href="{{ route("transfer") }}"> <svg class="icon icon-ek--transfer"><use xlink:href="{{url("/static/img/icons.svg#icon-ek--transfer")}}"></use></svg> {{  __('translation.menu_transfer') }}</a></li>
            <li class="ek--header--item"><a class="ek--header--link {{ (request()->is('stadium')) ? 'ek--header--link--active' : '' }}"  href="{{ route("stadium") }}"><svg class="icon icon-stadium"><use xlink:href="{{url("/static/img/icons.svg#icon-stadium")}}"></use></svg>{{  __('translation.menu_stadium') }}</a></li>
{{--            <li class="ek--header--item"><a class="ek--header--link {{ (request()->is('fan_shop')) ? 'ek--header--link--active' : '' }}"  href=""><svg class="icon icon-stadium"><use xlink:href="/static/img/icons.svg#icon-stadium"></use></svg>{{  __('translation.menu_fan_shop') }}</a></li>--}}
            <li class="ek--header--item"><a class="ek--header--link {{ (request()->is('statistics')) ? 'ek--header--link--active' : '' }}"  href="{{ route("statistics") }}"><svg class="icon icon-ek--statistics"><use xlink:href="{{url("/static/img/icons.svg#icon-ek--statistics")}}"></use></svg> {{  __('translation.menu_statistics') }}</a></li>
        </ul>

        <ul class="ek--header--nav">
{{--            <li class="ek--header--item"><a class="ek--header--link"  href="{{ route("leagueMatch") }}"><svg class="icon icon-ek--league"><use xlink:href="/static/img/icons.svg#icon-ek--league"></use></svg> {{  __('translation.menu_league') }}</a></li>--}}
{{--            <li class="ek--header--item"><a class="ek--header--link"  href="{{ route("leaugeCup") }}"><svg class="icon icon-ek--cup"><use xlink:href="/static/img/icons.svg#icon-ek--cup"></use></svg> {{  __('translation.menu_cup') }}</a></li>--}}
            <li class="ek--header--item"><a class="ek--header--link {{ (request()->is('tournament*','tournaments*')) ? 'ek--header--link--active' : '' }}"  href="{{ route("tournaments") }}"><svg class="icon icon-ek--challenge"><use xlink:href="{{url("/static/img/icons.svg#icon-ek--challenge")}}"></use></svg> {{  __('translation.menu_tournaments') }}</a></li>
            <li class="ek--header--item"><a class="ek--header--link {{ (request()->is('friendly*')) ? 'ek--header--link--active' : '' }}"  href="{{ route("friendly") }}"> <svg class="icon icon-ek--friendly"><use xlink:href="{{url("/static/img/icons.svg#icon-ek--friendly")}}"></use></svg> {{  __('translation.menu_friendly') }}</a></li>
        </ul>

        <ul class="ek--header--nav">
            <li class="ek--header--item"><a class="ek--header--link {{ (request()->is('coins')) ? 'ek--header--link--active' : '' }}"  href="{{ route("coins") }}"><svg class="icon icon-ek--coins"><use xlink:href="{{url("/static/img/icons.svg#icon-ek--coins")}}"></use></svg> {{  __('translation.menu_coins') }}</a></li>
            <li class="ek--header--item"><a class="ek--header--link {{ (request()->is('budget')) ? 'ek--header--link--active' : '' }}"  href="{{ route("budget") }}"><svg class="icon icon-ek--budget"><use xlink:href="{{url("/static/img/icons.svg#icon-ek--budget")}}"></use></svg> {{  __('translation.menu_budget') }}</a></li>
            <li class="ek--header--item"><a class="ek--header--link {{ (request()->is('marketing')) ? 'ek--header--link--active' : '' }}"  href="{{ route("marketing") }}"><svg class="icon icon-ek--marketing"><use xlink:href="{{url("/static/img/icons.svg#icon-ek--marketing")}}"></use></svg> {{  __('translation.menu_marketing') }}</a></li>
            <li class="ek--header--item"><a class="ek--header--link {{ (request()->is('global-ranking')) ? 'ek--header--link--active' : '' }}"  href="{{ route("gRanking") }}"><svg class="icon icon-ek--marketing"><use xlink:href="{{url("/static/img/icons.svg#icon-ranking")}}"></use></svg>{{  __('translation.top_rank_global') }}</a></li>
            <li class="ek--header--item"><a class="ek--header--link {{ (request()->is('local-ranking')) ? 'ek--header--link--active' : '' }}"  href="{{ route("lRanking") }}"><svg class="icon icon-ek--marketing"><use xlink:href="{{url("/static/img/icons.svg#icon-ranking")}}"></use></svg>{{  __('translation.top_rank_local') }}</a></li>
            <li class="ek--header--item"><a class="ek--header--link {{ (request()->is('promotions')) ? 'ek--header--link--active' : '' }}"  href=""><svg class="icon icon-stadium"><use xlink:href="{{url("/static/img/icons.svg#icon-stadium")}}"></use></svg>{{  __('translation.menu_promotions') }}</a></li>


        </ul>

        <ul class="ek--header--nav">
            <li class="ek--header--item"><a class="ek--header--link {{ (request()->is('settings')) ? 'ek--header--link--active' : '' }}"  href="{{ route("settings") }}"><svg class="icon icon-ek--settings"><use xlink:href="{{url("/static/img/icons.svg#icon-ek--settings")}}"></use></svg> Settings</a></li>
        </ul>
        <!-- End EK Header Navbar -->
    </header>
    <!-- =========================
        End Section -  Header
    ============================== -->
    <!-- =========================
        Section -  Main
    ============================== -->
    <main class="ek--body">

        <!-- EK Body Head -->
        <section class="ek--body--head">
            <!-- EK Body Head Left -->
            <div class="ek--body--head--left">
                <!-- EK Body Mobile Menu -->
                <div class="ek--body--head--mobile--icon">
                    <svg class="icon icon-ek--mobile--menu fill--green"><use xlink:href="{{ url("static/img/icons.svg#icon-ek--mobile--menu") }}"></use></svg>
                </div>
                <!-- End EK Body Mobile Menu -->
                <!-- EK Body Head Club -->
                <div class="ek--body--head--club">
                    <!-- EK Body Head Club Rank -->
                    <div class="ek--body--head--club--rank text--navy--900 ek--size--16--500">{{ number_format(Session::get("club")["ability"],0) }}</div>
                    <!-- End EK Body Head Club Rank -->
                    <!-- EK Body Head Club Name -->
                    <p class="text--white ek--size--20">{{Session::get("club")["clubName"]}}</p>
                    <!-- End EK Body Head Club Name -->
                </div>
                <!-- End EK Body Head Club -->
                <!-- EK Body Head Left Match -->
                @if(Session::get("next_match")[0] != null && !isset($challangesPageActive))
                <div id="matchReady" class="ek--body--head--left--match bg--navy--700">
                    <!-- EK Body Head Left Match Start -->
                    <div class="ek--body--head--left--match--start">
                        <svg class="icon icon-ek--time fill--navy--300"><use xlink:href="{{url("static/img/icons.svg#icon-ek--time")}}"></use></svg>
                        <p class="text--white ek--size--16">{{  __('translation.match_ready') }}</p>
                    </div>
                    <!-- End EK Body Head Left Match Start -->
                    <!-- EK Body Head Left Match Table -->

                    <div class="ek--body--head--left--match--table">
                        <!-- EK Body Head Left Match Table Title -->
                        <h3 class="text--navy--100 ek--size--16 text--center">{{  __('translation.match_ready') }}</h3>
                        <!-- End EK Body Head Left Match Table Title -->
                        <!-- EK Body Head Left Match Table Clubs -->
                        <div class="ek--body--head--left--match--table--clubs">
                            <!-- EK Body Head Left Match Table Clubs Left -->
                            <div class="ek--body--head--left--match--table--left">
                                <div id="homePower" class="ek--body--head--left--match--table--rank text--navy--900 ek--size--16--500">{{ number_format(Session::get("next_match")[0]["homeTeam"]["ability"],0) }}</div>
                                <h4 id="homeTeam" class="text--white">{{ Session::get("next_match")[0]["homeTeam"]["clubName"] }}</h4>
                            </div>
                            <!-- End EK Body Head Left Match Table Clubs Left -->
                            <!-- EK Body Head Left Match Table Clubs Center -->
                            <div class="ek--body--head--left--match--table--center text--navy--300 ek--size--20 text--center">vs</div>
                            <!-- End EK Body Head Left Match Table Clubs Center -->
                            <!-- EK Body Head Left Match Table Clubs Right -->
                            <div class="ek--body--head--left--match--table--right">
                                <div id="awayPower" class="ek--body--head--left--match--table--rank text--navy--900 ek--size--16--500">{{ number_format(Session::get("next_match")[0]["awayTeam"]["ability"],0) }}</div>
                                <h4 id="awayTeam" class="text--white">{{ Session::get("next_match")[0]["awayTeam"]["clubName"] }}</h4>
                            </div>
                            <!-- End EK Body Head Left Match Table Clubs Right -->
                        </div>
                        <!-- End EK Body Head Left Match Table Clubs -->
                        <!-- Ek Body Head Left Start Button -->
                        <div class="ek--body--head--left--match--button">
                            @php
                                $isDisabled = Session::get("next_match")[0]["waitingTime"] != 0;
                            @endphp
                                @if( Session::get("next_match")[1] == "league" )

                                        <a class="button--green @if($isDisabled)button--gray btn-is-disabled @endif" href="{{route("playMatch")}}">{{  __('translation.start_match') }}</a>

                                    @elseif( Session::get("next_match")[1] == "friendly" )

                                        <a class="button--cyan @if($isDisabled)button--gray btn-is-disabled @endif" href="{{route("friendlyMatch")}}">{{  __('translation.start_match') }}</a>

                                    @elseif( Session::get("next_match")[1] == "tournament" )

                                        @if(Session::get("next_match")[0]["waitingTime"] == 0)
                                            <a class="button--yellow @if($isDisabled)button--gray btn-is-disabled @endif" data-id="{{ Session::get("next_match")[0]["waitingTime"] }}" href="{{route("tournamentMatch",  ['tournamentId'=>Session::get("next_match")[2]])}}">{{  __('translation.start_match') }}</a>
                                        @else
                                            <a class="button--yellow @if($isDisabled)button--gray btn-is-disabled @endif" data-id="{{ Session::get("next_match")[0]["waitingTime"] }}" style="display:block;background:lightgray; color:white; opacity: .7; cursor: not-allowed">{{  __('translation.start_match') }}</a>
                                    @endif

                                @endif
                        </div>
                        @if(Session::get("next_match")[0]["waitingTime"] > 0)
                            <p id="time" style="text-align: center; color:white; margin-top:10px" data-time="{{ Session::get("next_match")[0]["waitingTime"]  }}">00:00</p>
                        @endif
                        <!-- End Ek Body Head Left Start Button -->
                    </div>
                     <!-- End EK Body Head Left Match Table -->
                </div>
                @endif
            <!-- End EK Body Head Left Match -->

            </div>
            <!-- EnD EK Body Head Left -->
            <div class="ek--body--head--center">
                <a href="{{ route("home") }}">
                    <img src="{{url("static/img/Logo/Ek--logo.svg")}}" alt="ek--logo">
                </a>
            </div>
            <!-- EK Body Head Right -->
            <div class="ek--body--head--right">
                <!-- Ek Body Head Money -->
                <div class="ek--body--head--money">
                    <!-- Ek Body Head Money Dollar -->
                    <a href="{{ route("budget") }}" class="ek--body--head--money--dollar bg--navy--700">
                        <svg class="icon icon-ek--budget fill--green"><use xlink:href="{{url("static/img/icons.svg#icon-ek--budget")}}"></use></svg>

                        @if(Session::get("finance")["money"] < 1000)

                            <p class="ek--body--inner--club--amount text--white"><span class="ek--body--inner--club--amount text--white">{{ Session::get("finance")["money"] }}</span></p>

                        @elseif(Session::get("finance")["money"] >= 1000 && Session::get("finance")["money"] < 1000000)
                            <p class="ek--body--inner--club--amount text--white"><span class="ek--body--inner--club--amount text--white">{{ number_format(Session::get("finance")["money"]/1000,1) }}</span><span class="ek--body--inner--club--amount text--white">K</span></p>

                        @elseif(Session::get("finance")["money"] >= 1000000 && Session::get("finance")["money"] < 1000000000)
                            <p class="ek--body--inner--club--amount text--white"><span class="ek--body--inner--club--amount text--white">{{ number_format(Session::get("finance")["money"]/1000000,1) }}</span><span class="ek--body--inner--club--amount text--white">M</span></p>

                        @elseif(Session::get("finance")["money"] >= 1000000000)
                            <p class="ek--body--inner--club--amount text--white"><span class="ek--body--inner--club--amount text--white">{{ number_format(Session::get("finance")["money"]/1000000000,1) }}</span><span class="ek--body--inner--club--amount text--white">B</span></p>

                        @endif

                    </a>
                    <!-- End Ek Body Head Money Dollar -->
                    <!-- Ek Body Head Money Dollar -->
                    <a href="{{ route("coins") }}" class="ek--body--head--money--coins bg--navy--700">
                        <svg class="icon icon-ek--coins fill--yellow"><use xlink:href="{{url("static/img/icons.svg#icon-ek--coins")}}"></use></svg>
                        <p class="text--white ek--size--16">{{Session::get("finance")["coins"]}}</p>
                    </a>
                    <!-- End Ek Body Head Money Dollar -->
                </div>
                <!-- End Ek Body Head Money -->
                <!-- EK Body Head User -->
                <div class="ek--body--head--user">
                    <!-- EK Body Head User Inner -->
                    <div class="ek--body--head--inner">
                        <!-- EK Body Head User Image -->
                        <div class="ek--body--head--image">
                            <img src="{{ url("static/img/Other/ek--null--image.png") }}" alt="ek--user">
                        </div>
                        <!-- End EK Body Head User Image -->
                        <!-- EK Body Head User Name and Level -->
                        <div class="ek--body--head--name">
                            <!-- EK Body Head User Name Title -->
                            <h1 class="ek--body--head--name--title text--white" >{{Session::get('user')["fullName"]}}</h1>
                            <!-- End EK Body Head User Name Title -->
                            <!-- EK Body Head User Levet -->
                            <p class="ek--body--head--name--level text--yellow">{{  __('translation.profile_level') }} {{ Session::get('level') }}</p>
                            <!-- End EK Body Head User Levet -->
                        </div>
                        <!-- End EK Body Head User Name and Level -->
                        <!-- EK Body Down and Up Icons -->
                        <div class="ek--body--head--icons">
                            <svg class="icon icon-ek--arrow--up"><use xlink:href="{{url("static/img/icons.svg#icon-ek--arrow--up")}}"></use></svg>
                            <svg class="icon icon-ek--arrow--down"><use xlink:href="{{url("static/img/icons.svg#icon-ek--arrow--down")}}"></use></svg>
                        </div>
                        <!-- End EK Body Down and Up Icons -->
                        <!-- Ek Body Profil Dropdown  -->
                        <div class="ek--body--user">
                            <!-- Ek Body Profil Dropdown Menu  -->
                            <div class="ek--body--user--menu">
                                <a href="{{ route("myProfile") }}">My profile</a>
                                <a href="{{ route("edit") }}">Profile settings</a>
                            </div>
                            <!-- End Ek Body Profil Dropdown Menu  -->
                            <!-- Ek Body Profil Dropdown Logout  -->
                            <div class="ek--body--user--logout">
                                <a href="{{route("client.logout")}}">Log out</a>
                            </div>
                            <!-- End Ek Body Profil Dropdown Logout  -->
                        </div>
                        <!-- End Ek Body Profil Dropdown  -->
                    </div>
                    <!-- End EK Body Head User Inner -->
                </div>
                <!-- End EK Body Head User -->
            </div>
            <!-- EnD EK Body Head Right -->
        </section>
        <!-- End EK Body Head -->
        @yield('content')

        <section class="ek--mobile--play">
            <div class="container d-flex justify-content-between alig">
                <div class="ek--body--head--money">
                    <!-- Ek Body Head Money Dollar -->
                    <a th:href="@{/budget}" class="ek--body--head--money--dollar bg--navy--700">
                        <svg class="icon icon-ek--budget fill--green"><use xlink:href="{{ url("static/img/icons.svg#icon-ek--budget") }}"></use></svg>

                        @if(Session::get("finance")["money"] < 1000)
                            <p class="text--white ek--size--16"><span class="text--white ek--size--16">{{ Session::get("finance")["money"] }}</span></p>
                        @elseif(Session::get("finance")["money"] >= 1000 && Session::get("finance")["money"] < 1000000)
                            <p class="text--white ek--size--16"><span class="text--white ek--size--16">{{ number_format(Session::get("finance")["money"]/1000,1) }}</span><span class="text--white ek--size--16">K</span></p>
                        @elseif(Session::get("finance")["money"] >= 1000000 && Session::get("finance")["money"] < 1000000000)
                            <p class="text--white ek--size--16"><span class="text--white ek--size--16">{{ number_format(Session::get("finance")["money"]/1000000,1) }}</span><span class="text--white ek--size--16">M</span></p>
                        @elseif(Session::get("finance")["money"] >= 1000000000)
                            <p class="text--white ek--size--16"><span class="text--white ek--size--16">{{ number_format(Session::get("finance")["money"]/1000000000,1) }}</span><span class="text--white ek--size--16">B</span></p>
                        @endif
                    </a>
                    <!--End Ek Body Head Money Dollar-->
                    <!--Ek Body Head Money Dollar-->
                    <a th:href="@{/coins}" class="ek--body--head--money--coins bg--navy--700">
                        <svg class="icon icon-ek--coins fill--yellow"><use xlink:href="{{ url("static/img/icons.svg#icon-ek--coins") }}"></use></svg>
                        <p class="text--white ek--size--16">{{ Session::get("finance")["coins"] }}</p>
                    </a>
                    <!-- End Ek Body Head Money Dollar -->

                </div>
                <a class="ek--mobile--play--next button--cyan" th:href="@{/friendly-match}">Next match</a>
            </div>
        </section>

    </main>

<!-- =========================
    EndSection -  Main
============================== -->
</div>
<!-- =========================
    End Section -  Wrapper
============================== -->



<div id="ek--notification" class="ek--notification bg--navy--700 modal">
    <div class="ek--notification--body">
        <!-- Ek Body Squad Message Notification Modal Left -->
        <div class="ek--notification--left bg--green--dark--two"></div>
        <!-- End Ek Body Squad Message Notification Modal left -->
        <!-- Ek Body Squad Message Notification Modal Right -->
        <div class="ek--notification--right">
            <div class="ek--notification--right--head">
                <div class="ek--notification--right--image bg--green--dark--two">
                    <svg class="icon icon-ek--correct fill--white"><use xlink:href="{{url("/static/img/icons.svg#icon-ek--correct")}}"></use></svg>
                </div>
                <div class="ek--notification--right--info">
                    <h4 class="text--white ek--size--20">Success!</h4>
                    <p class="text--navy--100 ek--size--18" >Changes saved successfully!</p>
                </div>
            </div>
            <div class="ek--notification--right--cancel">
                <a class="text--white bg--navy--600 ek--size--16--500" href="#" rel="modal:close">Close</a>
            </div>
        </div>
        <!-- End Ek Body Squad Message Notification Modal Right -->
    </div>
</div>

@yield("appendix")

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
<script src="{{ url('static/js/libs.min.js') }}"></script>
<script src="{{ url('static/js/common.js') }}"></script>
@yield('javascript')
</body>
</html>
