@extends('client.layout.authenticated')
@section('content')
    <!-- EK Setting -->
    <div class="ek--setting">
        <!-- EK Setting Body -->
        <div class="ek--setting--body bg--navy--700">
            <!-- EK Setting Body Head -->
            <div class="ek--setting--body--head text-white ek--size--20">Settings</div>
            <!-- End EK Setting Body Head -->
            <!-- EK Setting Body Game -->
            <div class="ek--setting--game">
                <!-- EK Setting Body Game Language -->

                <div class="ek--setting--game--language bg--navy--600">
                    <!-- EK Setting Body Game Language Left -->
                    <div class="ek--setting--game--language--left">
                        <!-- EK Setting Body Game Language Left Title -->
                        <h3 class="ek--size--18 text--white">Game language</h3>
                        <!-- End EK Setting Body Game Language Left Title -->
                        <!-- EK Setting Body Game Language Left Paragraph -->
                        <p class="text--navy--200 ek--size--16">
                            Use here to change the general language of the game.
                        </p>
                        <!-- End EK Setting Body Game Language Left Paragraph -->
                    </div>
                    <!-- End EK Setting Body Game Language Left -->
                    <!-- EK Setting Body Game Language Right -->
                    <div class="ek--setting--game--language--right">
                        <!-- EK Setting Body Game Language Right Select -->
                        <div class="ek--setting--game--language--select bg--navy--700">
                            <!-- EK Setting Body Game Language Right Select Country -->
                            <img class="ek--setting--game--language--image" src="{{ url("static/img/Other/langs/".Session::get("locale").".svg") }}" alt="ek--{{Session::get("locale")}}" /><span
                                class="text--navy--200 ek--size--16">{{Session::get("lang_name")}}</span>
                            <!-- EK Setting Body Game Language Right Select Country -->
                            <!-- EK Setting Body Game Language Right Select Content -->
                            <div class="ek--setting--game--language--content bg--navy--700">
                                <!-- EK Setting Body Game Language Link -->
                                <a class="ek--setting--game--language--link" href="{{ url("lang/en", 0) }}">
                                    <img src="{{ url("static/img/Other/english.svg") }}" alt="english" /> English</a>
                                <a class="ek--setting--game--language--link" href="{{ url("lang/es", 1) }}">
                                    <img src="{{ url("static/img/Other/ek--espanyol.svg") }}" alt="spanish" />
                                    Espanyol</a>
                                <a class="ek--setting--game--language--link" href="{{ url("lang/tr", 2) }}">
                                    <img src="{{ url("static/img/Other/ek--turkey.svg") }}" alt="turkish" />
                                    Türkçe</a>
                                <a class="ek--setting--game--language--link" href="{{ url("lang/az", 3) }}">
                                    <img src="{{ url("static/img/Other/ek--azerbaijan.svg") }}" alt="azerbaijani" />
                                    Azərbaycan dili</a>
                                <a class="ek--setting--game--language--link" href="{{ url("lang/pt", 4) }}">
                                    <img src="{{ url("static/img/Other/ek--portegues.svg") }}" alt="portugese" />
                                    Portuguese</a>
                                <a class="ek--setting--game--language--link" href="{{ url("lang/ar", 5) }}">
                                    <img src="{{ url("static/img/Flags/SA.svg") }}" alt="arabic" />
                                    عربى</a>
                                <a class="ek--setting--game--language--link" href="{{ url("lang/ru", 6) }}">
                                    <img src="{{ url("static/img/Other/ek--russian.svg") }}" alt="russian" />
                                    Pусский</a>

                                <!-- End EK Setting Body Game Language Link -->
                            </div>
                            <!-- End EK Setting Body Game Language Right Select Content -->
                        </div>
                        <!-- End EK Setting Body Game Language Right Select -->
                    </div>
                    <!-- End EK Setting Body Game Language Right -->
                </div>

                <!-- End EK Setting Body Game Language -->
                <!-- EK Setting Body Game Sound -->
                <div class="ek--setting--game--sound bg--navy--600">
                    <!-- EK Setting Body Game Sound Left -->
                    <div class="ek--setting--game--sound--left">
                        <h3 class="text--white ek--size--18">Game sounds</h3>
                        <p class="text--navy--200 ek--size--16">Match time, use to turn on or off the incoming sounds when you press the buttons.</p>
                    </div>
                    <!-- End EK Setting Body Game Sound Left -->
                    <!-- EK Setting Body Game Sound Right -->
                    <div class="ek--setting--game--sound--right">
                        <form action="#">
                            <label class="toggle-control">
                                <input type="checkbox">
                                <span class="control"></span>
                            </label>
                            <span class="text--navy--300 ek--size--16 ek--setting--game--sound--disabled">Disabled</span>
                        </form>
                    </div>
                    <!-- End EK Setting Body Game Sound Right -->
                </div>
                <!-- End EK Setting Body Game Sound -->
            </div>
            <!-- End EK Setting Body Game -->
        </div>
        <!-- End EK Setting Body -->
    </div>
    <!-- End EK Setting -->
@endsection

