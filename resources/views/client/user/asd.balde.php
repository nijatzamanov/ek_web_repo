<script src="https://www.gstatic.com/firebasejs/7.17.2/firebase-app.js"></script>

<script src="https://www.gstatic.com/firebasejs/7.17.2/firebase-analytics.js"></script>

<script>

  var firebaseConfig = {
    apiKey: "AIzaSyAGJNXqXR0IJ_8WSNa_8KuIqn1yAHNCadU",
    authDomain: "eleven-kings-pro.firebaseapp.com",
    databaseURL: "https://eleven-kings-pro.firebaseio.com",
    projectId: "eleven-kings-pro",
    storageBucket: "eleven-kings-pro.appspot.com",
    messagingSenderId: "420735125105",
    appId: "1:420735125105:web:54acf691e83c0cd02d3dd0",
    measurementId: "G-7YYLJFTNFG"
  };

  firebase.initializeApp(firebaseConfig);
  firebase.analytics();
</script>
