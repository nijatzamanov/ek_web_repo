@extends('client.layout.authenticated')
@section('content')
    <!-- EK Edit -->
    <div class="ek--edit">
        <!-- EK Edit Body -->
        <div class="ek--edit--body bg--navy--700">
            <!-- EK Edit Body Head -->
            <div class="ek--edit--body--head">
                <h3 class="ek--size--20 text--white">Profile settings</h3>
            </div>
            <!-- End EK Edit Body Head -->
            <!-- EK Edit Body Avatar -->
            <div class="ek--edit--body--avatar bg--navy--600">
                <!-- EK Edit Body Avatar Left -->
                <div class="ek--edit--body--avatar--left">
                    <!-- EK Edit Body Avatar Left Title -->
                    <h3 class="text--white ek--size--18">Avatar</h3>
                    <!-- End EK Edit Body Avatar Left Title -->
                    <!-- EK Edit Body Avatar Left Paragraph -->
                    <p class="text--navy--200 ek--size--16">If your club is popular, it will appear on the home page</p>
                    <!-- End EK Edit Body Avatar Left Paragraph -->
                </div>
                <!-- End EK Edit Body Avatar Left -->
                <!-- EK Edit Body Avatar Right -->
                <div class="ek--edit--body--avatar--right">
                    <!-- EK Edit Body Avatar Right Image -->
                    <dir class="ek--edit--body--avatar--right--image">
                        <?php
                            $avatarName = '/uploads/'.\Illuminate\Support\Str::slug(session()->get('user')['fullName'])
                        ?>
{{--                            <label for="userAvatar">--}}
                                <img src="{{ is_file($avatarName)?$avatarName:'/static/img/Other/ek--null--image.png' }}" alt="ek--user" style="cursor: pointer">
{{--                            </label>--}}
                    </dir>
                </div>
                <!-- End EK Edit Body Avatar Right -->
            </div>
            @if(isset($errors) && $errors->any())
                @php
                //dd($errors->all())
                    @endphp
            @endif
            <!-- End EK Edit Body Avatar -->
            <!-- EK Edit Body Personal Details -->
            <div class="ek--edit--body--personal bg--navy--600">
                <!-- EK Edit Body Personal Details Left -->
                <div class="ek--edit--body--personal--left">
                    <h3 class="text--white ek--size--18">Personal details</h3>
                    <p class="text--navy--200 ek--size--16">All your important information is here. You can change the password here.</p>
                </div>
                <!-- End EK Edit Body Personal Details Left -->
                <!-- EK Edit Body Personal Details Right -->
                <div class="ek--edit--body--personal--right">
                    <!-- EK Edit Body Personal Form -->
                    <form class="ek--edit--form" action="{{ route('edit') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="avatar" style="display: none" id="userAvatar">
                        <!-- EK Edit Body Personal Form Group -->
                        <div class="ek--edit--form--group">
                            <!-- EK Edit Body Personal Form Label -->
                            <label class="ek--edit--form--group--label" for="ek--edit--name">Manager Name</label>
                            <!-- End EK Edit Body Personal Form Label -->
                            <!-- EK Edit Body Personal Form Input -->
                            <input class="ek--edit--form--group--input" type="text" value="{{ session()->get('user')['fullName'] }}" name="fullName" id="ek--edit--name" required>
                            <!-- End EK Edit Body Personal Form Input -->
                        </div>
                        <!-- End EK Edit Body Personal Form Group -->
                        <!-- EK Edit Body Personal Form Group -->
                        <div class="ek--edit--form--group">
                            <!-- EK Edit Body Personal Form Label -->
                            <label class="ek--edit--form--group--label" for="ek--edit--teamname">Team Name</label>
                            <!-- End EK Edit Body Personal Form Label -->
                            <!-- EK Edit Body Personal Form Input -->
                            <input class="ek--edit--form--group--input" type="text" id="ek--edit--teamname" disabled value="{{ session()->get('club')['clubName'] }}">
                            <!-- End EK Edit Body Personal Form Input -->
                        </div>
                        <!-- End EK Edit Body Personal Form Group -->
                        <!-- EK Edit Body Personal Form Group -->
                        <div class="ek--edit--form--group">
                            <!-- EK Edit Body Personal Form Label -->
                            <label class="ek--edit--form--group--label" for="ek--edit--password">Current password</label>
                            <!-- End EK Edit Body Personal Form Label -->
                            <!-- EK Edit Body Personal Form Input -->
                            <input class="ek--edit--form--group--input" type="password" id="ek--edit--password" placeholder="Current password" name="password">
                            <!-- End EK Edit Body Personal Form Input -->
                        </div>
                        <!-- End EK Edit Body Personal Form Group -->
                        <!-- EK Edit Body Personal Form Group -->
                        <div class="ek--edit--form--group">
                            <!-- EK Edit Body Personal Form Label -->
                            <label class="ek--edit--form--group--label" for="ek--edit--newpassword">New password</label>
                            <!-- End EK Edit Body Personal Form Label -->
                            <!-- EK Edit Body Personal Form Input -->
                            <input class="ek--edit--form--group--input" name="newPassword" type="password" id="ek--edit--newpassword" placeholder="New password">
                            <!-- End EK Edit Body Personal Form Input -->
                            <small   class="ek--validation text--red">
                                <svg class="icon icon-ek--warning ek--svg--size fill--red"><use xlink:href="img/icons.svg#icon-ek--warning"></use></svg>
                                <span id="message"></span>
                            </small>

                        </div>
                        <!-- End EK Edit Body Personal Form Group -->
                        <div class="ek--edit--body--save">
                            <button  id="editProfile">
                                <img src="/static/img/Dashboard/loading2.png" style="display:none;width:25px;height:25px;"><span> Save changes</span></button>
                        </div>
                    </form>
                    <!-- End EK Edit Body Personal Form -->
                </div>
                <!-- End EK Edit Body Personal Details Right -->
            </div>
            <!-- End EK Edit Body Personal Details -->
            <!-- EK Edit Body Personal Save -->

            <!-- End EK Edit Body Personal Save -->
        </div>
        <!-- End EK Edit Body -->
    </div>
    <!-- End EK Edit -->

@endsection



@section('appendix')
<div id="ek--notification" class="ek--notification bg--navy--700 modal">
    <div class="ek--notification--body">
        <!-- Ek Body Squad Message Notification Modal Left -->
        <div class="ek--notification--left bg--green--dark--two"></div>
        <!-- End Ek Body Squad Message Notification Modal left -->
        <!-- Ek Body Squad Message Notification Modal Right -->
        <div class="ek--notification--right">
            <div class="ek--notification--right--head">
                <div class="ek--notification--right--image bg--green--dark--two">
                    <svg class="icon icon-ek--correct fill--white"><use xlink:href="img/icons.svg#icon-ek--correct"></use></svg>
                </div>
                <div class="ek--notification--right--info">
                    <h4 class="text--white ek--size--20">Success!</h4>
                    <p class="text--navy--100 ek--size--18" >Changes saved successfully!</p>
                </div>
            </div>
            <div class="ek--notification--right--cancel">
                <a class="text--white bg--navy--600 ek--size--16--500" href="#" rel="modal:close">Close</a>
            </div>
        </div>
        <!-- End Ek Body Squad Message Notification Modal Right -->
    </div>
</div>
@endsection

@section('javascript')
    <script src="{{url("section/js/edit.js")}}"></script>
@endsection

