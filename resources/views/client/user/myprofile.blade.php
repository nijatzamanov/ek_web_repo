@extends('client.layout.authenticated')
@section('content')

    <!-- EK Profile -->
    <div classs="ek--profile">
        <!-- EK Profile Body -->
        <div class="ek--profile--body">
            <!-- EK Profile Body Left -->
            <div class="ek--profile--body--left bg--navy--700">
                <!-- EK Profile Body Left Image -->
                <div class="ek--profile--body--left--image bg--navy--600">
                    @if( Session::get("user")["avatarLink"] == null )
                        <img src="static/img/Other/ek--null--image.png" alt="">
                        @else
                        <img src="static/{{Session::get("user")["avatarLink"]}}" alt="">
                    @endif
                </div>
                <!-- End EK Profile Body Left Image -->
                <!-- EK Profile Body Name -->
                <h3 class="text--white ek--size--20">{{ Session::get("user")["fullName"] }}</h3>
                <!-- End EK Profile Body Name -->
                <!-- EK Profile Body Edit Profile Button -->
                <div class="ek--profile--body--left--edit">
                    <a class="ek--profile--body--left--edit--button" href="{{ route("edit") }}"><svg class="icon icon-ek--settings"><use xlink:href="static/img/icons.svg#icon-ek--settings"></use></svg>  Edit Profile</a>
                </div>
                <!-- End EK Profile Body Edit Profile Button -->
                <!-- EK Profile Body Manages Club -->
                <div class="ek--profile--body--left--manages">
                    <span class="text--navy--200 ek--size--16">Manages club</span>
                    <h4 class="text--white ek--size--16">{{ Session::get("club")["clubName"] }}</h4>
                </div>
                <!-- End EK Profile Body Manages Club -->
                <!-- EK Profile Body Manages Country -->
                <div class="ek--profile--body--left--country">
                    <span class="text--navy--200 ek--size--16">Country</span>
                    <div class="ek--profile--body--left--country--flag">
{{--                        <img th:src="@{${'img/Flags/'+ session.club.country.code +'.svg'}}" alt="">--}}
                        <img src="{{ url("static/img/Flags/".Session::get("club")["country"]["code"].".svg") }}">
                        <h4 class="text--white ek--size--16">{{ Session::get("club")["country"]["code"] }}</h4>
                    </div>
                </div>
                <!-- End EK Profile Body Manages Country -->
            </div>
            <!-- End EK Profile Body Left -->
            <!-- EK Profile Body Right -->
            <div class="ek--profile--body--right bg--navy--700">
                <!-- EK Profile Tab -->
                <div class="ek--profile--tab">
                    <!-- EK Profile Tab Head -->
                    <div class="ek--profile--tab--head">
                        <!-- EK Profile Tab Head Buttons -->
                        <button id="ek--profile--tab--info" class="ek--profile--tab--head--button button--active">Info</button>
                        <button id="ek--profile--tab--statistics" class="ek--profile--tab--head--button">Statistics</button>
                        <button id="ek--profile--tab--trophies" class="ek--profile--tab--head--button">Trophies</button>
                        <!-- End EK Profile Tab Head Buttons -->
                    </div>
                    <!-- EK Profile Tab Head -->
                    <!-- EK Profile Tab Content -->
                    <div class="ek--profile--tab--content">
                        <!-- EK Profile Tab Info -->
                        <div class="ek--profile--tab--info ek--profile--tab--table">
                            <!-- EK Profile Tab Inner -->
                            <div class="ek--profile--tab--inner bg--navy--600">
                                <!-- EK Profile Tab Inner Left -->
                                <div class="ek--profile--tab--inner--left">
                                    <div class="ek--profile--tab--inner--dot"></div>
                                    <span class="ek--size--16 text--white">Registered</span>
                                </div>
                                <!-- End EK Profile Tab Inner Left -->
                                <!-- EK Profile Tab Inner Right -->
                                <div class="ek--profile--tab--inner--right">
                                    <p class="text--white ek--size--16"> {{ date("d M Y", strtotime(Session::get("user")["createdAt"])) }} </p>
                                </div>
                                <!-- End EK Profile Tab Inner Right -->
                            </div>
                            <!-- End EK Profile Tab Inner -->
                            <!-- EK Profile Tab Inner -->
                            <div class="ek--profile--tab--inner bg--navy--600">
                                <!-- EK Profile Tab Inner Left -->
                                <div class="ek--profile--tab--inner--left">
                                    <div class="ek--profile--tab--inner--dot"></div>
                                    <span class="ek--size--16 text--white">Global Rank</span>
                                </div>
                                <!-- End EK Profile Tab Inner Left -->
                                <!-- EK Profile Tab Inner Right -->
                                <div class="ek--profile--tab--inner--right">
                                    <p class="text--white ek--size--16">#{{ Session::get("profile_data")["allGlobalRanking"] }}</p>
                                </div>
                                <!-- End EK Profile Tab Inner Right -->
                            </div>
                            <!-- End EK Profile Tab Inner -->
                            <!-- EK Profile Tab Inner -->
                            <div class="ek--profile--tab--inner bg--navy--600">
                                <!-- EK Profile Tab Inner Left -->
                                <div class="ek--profile--tab--inner--left">
                                    <div class="ek--profile--tab--inner--dot"></div>
                                    <span class="ek--size--16 text--white">Local Rank</span>
                                </div>
                                <!-- End EK Profile Tab Inner Left -->
                                <!-- EK Profile Tab Inner Right -->
                                <div class="ek--profile--tab--inner--right">
                                    <p class="text--white ek--size--16">#{{ Session::get("profile_data")["allLocalRanking"] }}</p>
                                </div>
                                <!-- End EK Profile Tab Inner Right -->
                            </div>
                            <!-- End EK Profile Tab Inner -->
                            <!-- EK Profile Tab Inner -->
                            <div class="ek--profile--tab--inner bg--navy--600">
                                <!-- EK Profile Tab Inner Left -->
                                <div class="ek--profile--tab--inner--left">
                                    <div class="ek--profile--tab--inner--dot"></div>
                                    <span class="ek--size--16 text--white">Manager Points</span>
                                </div>
                                <!-- End EK Profile Tab Inner Left -->
                                <!-- EK Profile Tab Inner Right -->
                                <div class="ek--profile--tab--inner--right">
                                    <p class="text--white ek--size--16">{{ Session::get("profile_data")["managerPoints"] }}</p>
                                </div>
                                <!-- End EK Profile Tab Inner Right -->
                            </div>
                            <!-- End EK Profile Tab Inner -->
                            <!-- EK Profile Tab Inner -->
                            <div class="ek--profile--tab--inner bg--navy--600">
                                <!-- EK Profile Tab Inner Left -->
                                <div class="ek--profile--tab--inner--left">
                                    <div class="ek--profile--tab--inner--dot"></div>
                                    <span class="ek--size--16 text--white">Manager Level</span>
                                </div>
                                <!-- End EK Profile Tab Inner Left -->
                                <!-- EK Profile Tab Inner Right -->
                                <div class="ek--profile--tab--inner--right">
                                    <p class="text--white ek--size--16">{{ Session::get("profile_data")["managerLevel"] }}</p>
                                </div>
                                <!-- End EK Profile Tab Inner Right -->
                            </div>
                            <!-- End EK Profile Tab Inner -->
                        </div>
                        <!-- EK Profile Tab Info -->
                        <!-- EK Profile Tab Statistics -->
                        <div class="ek--profile--tab--statistics ek--profile--tab--table">
                             <!-- EK Profile Tab Inner -->


{{--                             <div class="ek--profile--tab--inner bg--navy--600">--}}
{{--                                    <!-- EK Profile Tab Inner Left -->--}}
{{--                                    <div class="ek--profile--tab--inner--left">--}}
{{--                                        <div class="ek--profile--tab--inner--dot"></div>--}}
{{--                                        <span class="ek--size--16 text--white">Played</span>--}}
{{--                                    </div>--}}
{{--                                    <!-- End EK Profile Tab Inner Left -->--}}
{{--                                    <!-- EK Profile Tab Inner Right -->--}}
{{--                                    <div class="ek--profile--tab--inner--right">--}}
{{--                                        <p class="text--white ek--size--16" th:text="${#strings.substring(session.profileData.statistics.played,0,10)}">22.11.2017</p>--}}
{{--                                    </div>--}}
{{--                                    <!-- End EK Profile Tab Inner Right -->--}}
{{--                            </div>--}}



                            <!-- End EK Profile Tab Inner -->
                            <!-- EK Profile Tab Inner -->
                            <div class="ek--profile--tab--inner bg--navy--600">
                                <!-- EK Profile Tab Inner Left -->
                                <div class="ek--profile--tab--inner--left">
                                    <div class="ek--profile--tab--inner--dot"></div>
                                    <span class="ek--size--16 text--white">Won</span>
                                </div>
                                <!-- End EK Profile Tab Inner Left -->
                                <!-- EK Profile Tab Inner Right -->
                                <div class="ek--profile--tab--inner--right">
                                    <p class="text--white ek--size--16">{{ Session::get("profile_data")["won"] }} matches</p>
                                </div>
                                <!-- End EK Profile Tab Inner Right -->
                            </div>
                            <!-- End EK Profile Tab Inner -->
                            <!-- EK Profile Tab Inner -->
                            <div class="ek--profile--tab--inner bg--navy--600">
                                <!-- EK Profile Tab Inner Left -->
                                <div class="ek--profile--tab--inner--left">
                                    <div class="ek--profile--tab--inner--dot"></div>
                                    <span class="ek--size--16 text--white">Draw</span>
                                </div>
                                <!-- End EK Profile Tab Inner Left -->
                                <!-- EK Profile Tab Inner Right -->
                                <div class="ek--profile--tab--inner--right">
                                    <p class="text--white ek--size--16">{{ Session::get("profile_data")["draw"] }} matches</p>
                                </div>
                                <!-- End EK Profile Tab Inner Right -->
                            </div>
                            <!-- End EK Profile Tab Inner -->
                            <!-- EK Profile Tab Inner -->
                            <div class="ek--profile--tab--inner bg--navy--600">
                                <!-- EK Profile Tab Inner Left -->
                                <div class="ek--profile--tab--inner--left">
                                    <div class="ek--profile--tab--inner--dot"></div>
                                    <span class="ek--size--16 text--white">Lost</span>
                                </div>
                                <!-- End EK Profile Tab Inner Left -->
                                <!-- EK Profile Tab Inner Right -->
                                <div class="ek--profile--tab--inner--right">
                                    <p class="text--white ek--size--16">{{ Session::get("profile_data")["lost"] }} matches</p>
                                </div>
                                <!-- End EK Profile Tab Inner Right -->
                            </div>
                            <!-- End EK Profile Tab Inner -->
                            <!-- EK Profile Tab Inner -->
                            <div class="ek--profile--tab--inner bg--navy--600">
                                <!-- EK Profile Tab Inner Left -->
                                <div class="ek--profile--tab--inner--left">
                                    <div class="ek--profile--tab--inner--dot"></div>
                                    <span class="ek--size--16 text--white">Goal F/A</span>
                                </div>
                                <!-- End EK Profile Tab Inner Left -->
                                <!-- EK Profile Tab Inner Right -->
                                <div class="ek--profile--tab--inner--right">
                                    <p class="text--white ek--size--16">{{ Session::get("profile_data")["ga"] }} matches</p>
                                </div>
                                <!-- End EK Profile Tab Inner Right -->
                            </div>
                            <!-- End EK Profile Tab Inner -->
                        </div>
                        <!-- EK Profile Tab Statistics -->
                        <!-- EK Profile Tab Trophies -->
                        <div class="ek--profile--tab--trophies ek--profile--tab--table">
                            <!-- EK Profile Tab Inner -->
                            <div class="ek--profile--tab--inner bg--navy--600">
                                    <!-- EK Profile Tab Inner Left -->
                                    <div class="ek--profile--tab--inner--left">
                                        <div class="ek--profile--tab--inner--dot"></div>
                                        <span class="ek--size--16 text--white">Leagues Won</span>
                                    </div>
                                    <!-- End EK Profile Tab Inner Left -->
                                    <!-- EK Profile Tab Inner Right -->
                                    <div class="ek--profile--tab--inner--right">
                                        <p class="text--white ek--size--16">{{ Session::get("profile_data")["leaguesWon"] }}</p>
                                    </div>
                                    <!-- End EK Profile Tab Inner Right -->
                            </div>
                            <!-- End EK Profile Tab Inner -->
                            <!-- EK Profile Tab Inner -->
                            <div class="ek--profile--tab--inner bg--navy--600">
                                <!-- EK Profile Tab Inner Left -->
                                <div class="ek--profile--tab--inner--left">
                                    <div class="ek--profile--tab--inner--dot"></div>
                                    <span class="ek--size--16 text--white">Cups WonCups Won</span>
                                </div>
                                <!-- End EK Profile Tab Inner Left -->
                                <!-- EK Profile Tab Inner Right -->
                                <div class="ek--profile--tab--inner--right">
                                    <p class="text--white ek--size--16">{{ Session::get("profile_data")["cupsWon"] }}</p>
                                </div>
                                <!-- End EK Profile Tab Inner Right -->
                            </div>
                            <!-- End EK Profile Tab Inner -->
                            <!-- EK Profile Tab Inner -->
                            <div class="ek--profile--tab--inner bg--navy--600">
                                <!-- EK Profile Tab Inner Left -->
                                <div class="ek--profile--tab--inner--left">
                                    <div class="ek--profile--tab--inner--dot"></div>
                                    <span class="ek--size--16 text--white">Challenges Won</span>
                                </div>
                                <!-- End EK Profile Tab Inner Left -->
                                <!-- EK Profile Tab Inner Right -->
                                <div class="ek--profile--tab--inner--right">
                                    <p class="text--white ek--size--16">{{ Session::get("profile_data")["challengesWon"] }}</p>
                                </div>
                                <!-- End EK Profile Tab Inner Right -->
                            </div>
                            <!-- End EK Profile Tab Inner -->
                            <!-- EK Profile Tab Inner -->
                            <!--<div class="ek&#45;&#45;profile&#45;&#45;tab&#45;&#45;inner bg&#45;&#45;navy&#45;&#45;600">-->
                                <!--&lt;!&ndash; EK Profile Tab Inner Left &ndash;&gt;-->
                                <!--<div class="ek&#45;&#45;profile&#45;&#45;tab&#45;&#45;inner&#45;&#45;left">-->
                                    <!--<div class="ek&#45;&#45;profile&#45;&#45;tab&#45;&#45;inner&#45;&#45;dot"></div>-->
                                    <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">Friendlies Won</span>-->
                                <!--</div>-->
                                <!--&lt;!&ndash; End EK Profile Tab Inner Left &ndash;&gt;-->
                                <!--&lt;!&ndash; EK Profile Tab Inner Right &ndash;&gt;-->
                                <!--<div class="ek&#45;&#45;profile&#45;&#45;tab&#45;&#45;inner&#45;&#45;right">-->
                                    <!--<p class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16">8</p>-->
                                <!--</div>-->
                                <!--&lt;!&ndash; End EK Profile Tab Inner Right &ndash;&gt;-->
                            <!--</div>-->
                            <!-- End EK Profile Tab Inner -->
                        </div>
                        <!-- EK Profile Tab Trophies -->
                    </div>
                    <!-- End EK Profile Tab Content -->
                </div>
                <!-- End EK Profile Tab -->
            </div>
            <!-- End EK Profile Body Right -->
        </div>
        <!-- End EK Profile Body -->
    </div>
    <!-- End EK Profile -->
@endsection
