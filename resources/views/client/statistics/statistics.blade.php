@extends('client.layout.authenticated')
@section('content')

    <!-- EK Body Statistics -->
    <section class="ek--statistics">
        <!-- EK Body Statistics Body -->
        <div class="ek--statistics--body">
            <!-- EK Body Statistics Body Head -->
            <div class="ek--statistics--body--head bg--navy--700">
                <h3 class="text--white ek--size--20">Statistics</h3>
            </div>
            <!-- End EK Body Statistics Body Head -->
            <!-- EK Body Statistics Tabs -->
            <div class="ek--statistics--tabs bg--navy--700">
                <!-- EK Body Statistics Tabs Head -->
                <div class="ek--statistics--tabs--head">
                    <!-- EK Body Statistics Tabs Head Buttons -->
                    <button id="ek--statistics--tabs--value" class="ek--statistics--tabs--button button--active">Most Value Players</button>
                    <button id="ek--statistics--tabs--topscores" class="ek--statistics--tabs--button">Topscores</button>
                    <button id="ek--statistics--tabs--player" class="ek--statistics--tabs--button">Most Player</button>
                    <button id="ek--statistics--tabs--abl" class="ek--statistics--tabs--button">Best Abl</button>
                    <!-- End EK Body Statistics Tabs Head Buttons -->
                </div>
                <!-- End EK Body Statistics Tabs Head -->
                <!-- EK Body Statistics Tabs Content -->
                <div class="ek--statistics--tabs--content">
                    <!-- End EK Body Statistics Tabs Content Value -->
                    <div class="ek--statistics--tabs--value ek--statistics--tabs--table">
                        <!-- EK Body Statistics Tabs Content Value Head -->
                        <div class="ek--statistics--tabs--value--head">
                            <div class="ek--statistics--tabs--value--head--jn text--navy--200 ek--size--16">JN</div>
                            <div class="ek--statistics--tabs--value--head--player text--navy--200 ek--size--16">Player name</div>
                            <div class="ek--statistics--tabs--value--head--amount text--navy--200 ek--size--16">Price</div>
                        </div>
                        <!-- End EK Body Statistics Tabs Content Value Head -->
                        <!-- EK Body Statistics Tabs Content Value List -->
                        <div class="ek--statistics--tabs--value--list" style="width:auto !important;">
                            <!-- EK Body Statistics Tabs Inner -->
                            @foreach( Session::get("stats")["mostValues"] as $stat )
                                <div class="ek--statistics--tabs--inner bg--navy--600">
                                    <!-- EK Body Statistics Tabs Inner JN -->
                                    <div class="ek--statistics--tabs--inner--jn text--white ek--size--16">{{ $loop->index+1 }}</div>
                                    <!-- End EK Body Statistics Tabs Inner JN -->
                                    <!-- EK Body Statistics Tabs Inner Player -->
                                    <div class="ek--statistics--tabs--inner--player text--white ek--size--16" >
                                        <!--<img src="img/Flags/ek&#45;&#45;aze.svg"  alt="ek&#45;&#45;aze"/>-->
                                        <!--<img th:src="@{${'img/Flags/'+ player.country.code +'.svg'}}" alt="">-->
                                        <span class="text--white ek--size--16">{{ $stat["name"] }}</span>
                                    </div>
                                    <!-- End EK Body Statistics Tabs Inner Player -->
                                    <!-- EK Body Statistics Tabs Inner Amount -->
                                    <div class="ek--statistics--tabs--inner--amount text--white ek--size--16">
                                        <span >$</span>

                                        @if($stat["value"] < 1000)
                                            <span  th:if="${value.value < 1000}" th:text="${value.value}">{{ $stat["value"] }}</span>
                                        @endif

                                        @if($stat["value"] >= 1000 && $stat["value"] < 1000000)
                                            <span  th:if="${value.value >= 1000 and value.value< 1000000}" th:with="result=${value.value/1000}"><span class="ek--statistics--tabs--inner--amount text--white ek--size--16" th:text="${result}">{{ number_format($stat["value"]/1000,1) }}</span><span class="ek--statistics--tabs--inner--amount text--white ek--size--16">K</span></span>
                                        @endif

                                        @if($stat["value"] >= 1000000 && $stat["value"] < 1000000000)
                                            <span  th:if="${value.value >= 1000000 and value.value< 1000000000}" th:with="result=${value.value/1000000}"><span class="ek--statistics--tabs--inner--amount text--white ek--size--16" th:text="${result}">{{ number_format($stat["value"]/1000000,1) }}</span><span class="ek--statistics--tabs--inner--amount text--white ek--size--16">M</span></span>
                                        @endif

                                        @if($stat["value"] >= 1000000000)
                                            <span  th:if="${value.value >= 1000000000 }" th:with="result=${value.value/1000000000}"><span class="ek--statistics--tabs--inner--amount text--white ek--size--16" th:text="${result}">{{ number_format($stat["value"]/1000000000,1) }}</span><span class="ek--statistics--tabs--inner--amount text--white ek--size--16">B</span></span>
                                        @endif

                                    </div>
                                    <!-- End EK Body Statistics Tabs Inner Amount -->
                                </div>
                            @endforeach
                            <!-- End EK Body Statistics Tabs Inner -->
                        </div>
                        <!-- End EK Body Statistics Tabs Content Value List -->
                    </div>
                    <!-- End End EK Body Statistics Tabs Content Value -->
                    <!-- End EK Body Statistics Tabs Content Top Scores -->
                    <div class="ek--statistics--tabs--topscores ek--statistics--tabs--table">
                        <!-- EK Body Statistics Tabs Content Top Scores Head -->
                        <div class="ek--statistics--tabs--topscores--head">
                            <div class="ek--statistics--tabs--topscores--head--jn text--navy--200 ek--size--16">JN</div>
                            <div class="ek--statistics--tabs--topscores--head--player text--navy--200 ek--size--16">Player name</div>
                            <div class="ek--statistics--tabs--topscores--head--amount text--navy--200 ek--size--16">Topscore</div>
                        </div>
                        <!-- End EK Body Statistics Tabs Content Top Scores Head -->
                        <!-- EK Body Statistics Tabs Content Top Scores List -->
                        <div class="ek--statistics--tabs--topscores--list" style="width:auto !important;">
                            <!-- EK Body Statistics Tabs Inner -->


                            @foreach( Session::get("stats")["topScores"] as $stat )
                                <div class="ek--statistics--tabs--inner bg--navy--600">
                                    <!-- EK Body Statistics Tabs Inner JN -->
                                    <div class="ek--statistics--tabs--inner--jn text--white ek--size--16">{{ $loop->index+1 }}</div>
                                    <!-- End EK Body Statistics Tabs Inner JN -->
                                    <!-- EK Body Statistics Tabs Inner Player -->
                                    <div class="ek--statistics--tabs--inner--player text--white ek--size--16" >
                                        <!--<img src="img/Flags/ek&#45;&#45;aze.svg"  alt="ek&#45;&#45;aze"/>-->
                                        <!--<img th:src="@{${'img/Flags/'+ player.country.code +'.svg'}}" alt="">-->
                                        <span class="text--white ek--size--16">{{ $stat["name"] }}</span>
                                    </div>
                                    <!-- End EK Body Statistics Tabs Inner Player -->
                                    <!-- EK Body Statistics Tabs Inner Amount -->
                                    <div class="ek--statistics--tabs--inner--amount text--white ek--size--16">
                                        @if($stat["value"] < 1000)
                                            <span>{{ $stat["value"] }}</span>
                                        @endif

                                        @if($stat["value"] >= 1000 && $stat["value"] < 1000000)
                                            <span><span class="ek--statistics--tabs--inner--amount text--white ek--size--16>{{ number_format($stat["value"]/1000,1) }}</span><span class="ek--statistics--tabs--inner--amount text--white ek--size--16">K</span></span>
                                        @endif

                                        @if($stat["value"] >= 1000000 && $stat["value"] < 1000000000)
                                            <span><span class="ek--statistics--tabs--inner--amount text--white ek--size--16">{{ number_format($stat["value"]/1000000,1) }}</span><span class="ek--statistics--tabs--inner--amount text--white ek--size--16">M</span></span>
                                        @endif

                                        @if($stat["value"] >= 1000000000)
                                            <span><span class="ek--statistics--tabs--inner--amount text--white ek--size--16">{{ number_format($stat["value"]/1000000000,1) }}</span><span class="ek--statistics--tabs--inner--amount text--white ek--size--16">B</span></span>
                                        @endif

                                    </div>
                                    <!-- End EK Body Statistics Tabs Inner Amount -->
                                </div>
                            @endforeach

                            <!-- End EK Body Statistics Tabs Inner -->
                        </div>
                        <!-- End EK Body Statistics Tabs Content Top Scores List -->
                    </div>
                    <!-- End End EK Body Statistics Tabs Content Top Scores -->
                    <!-- End EK Body Statistics Tabs Content Player -->
                    <div class="ek--statistics--tabs--player ek--statistics--tabs--table">
                        <!-- EK Body Statistics Tabs Content Player Head -->
                        <div class="ek--statistics--tabs--player--head">
                            <div class="ek--statistics--tabs--player--head--jn text--navy--200 ek--size--16">JN</div>
                            <div class="ek--statistics--tabs--player--head--player text--navy--200 ek--size--16">Player name</div>
                            <div class="ek--statistics--tabs--player--head--amount text--navy--200 ek--size--16">Played</div>
                        </div>
                        <!-- End EK Body Statistics Tabs Content Player Head -->
                        <!-- EK Body Statistics Tabs Content Player List -->
                        <div class="ek--statistics--tabs--player--list" th:with="roww=0" style="width:auto !important;">
                            <!-- EK Body Statistics Tabs Inner -->


                            @foreach( Session::get("stats")["mostPlays"] as $stat )
                                <div class="ek--statistics--tabs--inner bg--navy--600">
                                    <!-- EK Body Statistics Tabs Inner JN -->
                                    <div class="ek--statistics--tabs--inner--jn text--white ek--size--16">{{ $loop->index+1 }}</div>
                                    <!-- End EK Body Statistics Tabs Inner JN -->
                                    <!-- EK Body Statistics Tabs Inner Player -->
                                    <div class="ek--statistics--tabs--inner--player text--white ek--size--16" >
                                        <!--<img src="img/Flags/ek&#45;&#45;aze.svg"  alt="ek&#45;&#45;aze"/>-->
                                        <!--<img th:src="@{${'img/Flags/'+ player.country.code +'.svg'}}" alt="">-->
                                        <span class="text--white ek--size--16">{{ $stat["name"] }}</span>
                                    </div>
                                    <!-- End EK Body Statistics Tabs Inner Player -->
                                    <!-- EK Body Statistics Tabs Inner Amount -->
                                    <div class="ek--statistics--tabs--inner--amount text--white ek--size--16">
                                        @if($stat["value"] < 1000)
                                            <span  th:if="${value.value < 1000}" th:text="${value.value}">{{ $stat["value"] }}</span>
                                        @endif

                                        @if($stat["value"] >= 1000 && $stat["value"] < 1000000)
                                            <span  th:if="${value.value >= 1000 and value.value< 1000000}" th:with="result=${value.value/1000}"><span class="ek--statistics--tabs--inner--amount text--white ek--size--16" th:text="${result}">{{ number_format($stat["value"]/1000,1) }}</span><span class="ek--statistics--tabs--inner--amount text--white ek--size--16">K</span></span>
                                        @endif

                                        @if($stat["value"] >= 1000000 && $stat["value"] < 1000000000)
                                            <span  th:if="${value.value >= 1000000 and value.value< 1000000000}" th:with="result=${value.value/1000000}"><span class="ek--statistics--tabs--inner--amount text--white ek--size--16" th:text="${result}">{{ number_format($stat["value"]/1000000,1) }}</span><span class="ek--statistics--tabs--inner--amount text--white ek--size--16">M</span></span>
                                        @endif

                                        @if($stat["value"] >= 1000000000)
                                            <span  th:if="${value.value >= 1000000000 }" th:with="result=${value.value/1000000000}"><span class="ek--statistics--tabs--inner--amount text--white ek--size--16" th:text="${result}">{{ number_format($stat["value"]/1000000000,1) }}</span><span class="ek--statistics--tabs--inner--amount text--white ek--size--16">B</span></span>
                                        @endif

                                    </div>
                                    <!-- End EK Body Statistics Tabs Inner Amount -->
                                </div>
                            @endforeach

                            <!-- End EK Body Statistics Tabs Inner -->
                        </div>
                        <!-- End EK Body Statistics Tabs Content Player List -->
                    </div>
                    <!-- End End EK Body Statistics Tabs Content Player -->
                    <!-- End EK Body Statistics Tabs Content Abl -->
                    <div class="ek--statistics--tabs--abl ek--statistics--tabs--table">
                        <!-- EK Body Statistics Tabs Content Abl Head -->
                        <div class="ek--statistics--tabs--abl--head">
                            <div class="ek--statistics--tabs--abl--head--jn text--navy--200 ek--size--16">JN</div>
                            <div class="ek--statistics--tabs--abl--head--player text--navy--200 ek--size--16">Player name</div>
                            <div class="ek--statistics--tabs--abl--head--amount text--navy--200 ek--size--16">Abl</div>
                        </div>
                        <!-- End EK Body Statistics Tabs Content Abl Head -->
                        <!-- EK Body Statistics Tabs Content Abl List -->
                        <div class="ek--statistics--tabs--abl--list" style="width:auto !important;">



                            <!-- EK Body Statistics Tabs Inner -->
                            @foreach( Session::get("stats")["bestAbilities"] as $stat )
                                <div class="ek--statistics--tabs--inner bg--navy--600">
                                    <!-- EK Body Statistics Tabs Inner JN -->
                                    <div class="ek--statistics--tabs--inner--jn text--white ek--size--16">{{ $loop->index+1 }}</div>
                                    <!-- End EK Body Statistics Tabs Inner JN -->
                                    <!-- EK Body Statistics Tabs Inner Player -->
                                    <div class="ek--statistics--tabs--inner--player text--white ek--size--16" >
                                        <!--<img src="img/Flags/ek&#45;&#45;aze.svg"  alt="ek&#45;&#45;aze"/>-->
                                        <!--<img th:src="@{${'img/Flags/'+ player.country.code +'.svg'}}" alt="">-->
                                        <span class="text--white ek--size--16">{{ $stat["name"] }}</span>
                                    </div>
                                    <!-- End EK Body Statistics Tabs Inner Player -->
                                    <!-- EK Body Statistics Tabs Inner Amount -->
                                    <div class="ek--statistics--tabs--inner--amount text--white ek--size--16">
                                        @if($stat["value"] < 1000)
                                            <span  th:if="${value.value < 1000}" th:text="${value.value}">{{ $stat["value"] }}</span>
                                        @endif

                                        @if($stat["value"] >= 1000 && $stat["value"] < 1000000)
                                            <span  th:if="${value.value >= 1000 and value.value< 1000000}" th:with="result=${value.value/1000}"><span class="ek--statistics--tabs--inner--amount text--white ek--size--16" th:text="${result}">{{ number_format($stat["value"]/1000,1) }}</span><span class="ek--statistics--tabs--inner--amount text--white ek--size--16">K</span></span>
                                        @endif

                                        @if($stat["value"] >= 1000000 && $stat["value"] < 1000000000)
                                            <span  th:if="${value.value >= 1000000 and value.value< 1000000000}" th:with="result=${value.value/1000000}"><span class="ek--statistics--tabs--inner--amount text--white ek--size--16" th:text="${result}">{{ number_format($stat["value"]/1000000,1) }}</span><span class="ek--statistics--tabs--inner--amount text--white ek--size--16">M</span></span>
                                        @endif

                                        @if($stat["value"] >= 1000000000)
                                            <span  th:if="${value.value >= 1000000000 }" th:with="result=${value.value/1000000000}"><span class="ek--statistics--tabs--inner--amount text--white ek--size--16" th:text="${result}">{{ number_format($stat["value"]/1000000000,1) }}</span><span class="ek--statistics--tabs--inner--amount text--white ek--size--16">B</span></span>
                                        @endif

                                    </div>
                                    <!-- End EK Body Statistics Tabs Inner Amount -->
                                </div>
                            @endforeach

                            <!-- End EK Body Statistics Tabs Inner -->
                        </div>
                        <!-- End EK Body Statistics Tabs Content Abl List -->
                    </div>
                    <!-- End End EK Body Statistics Tabs Content Abl -->
                </div>
                 <!-- EK Body Statistics Tabs Content -->
            </div>
            <!-- End EK Body Statistics Tabs -->
        </div>
        <!-- End EK Body Statistics Body -->
    </section>
    <!-- End EK Body Statistics -->

@endsection





