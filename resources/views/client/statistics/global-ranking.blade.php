@extends('client.layout.authenticated')
@section('content')


    <div class="ek--ranking">

        <div class="ek--ranking--body">

            <div class="ek--ranking--table bg--navy--700">

                <div class="ek--ranking--table--head">
                    <button id="ek--ranking--table--global" class="ek--ranking--table--button active">Current Rank</button>
                    <button id="ek--ranking--table--local" class="ek--ranking--table--button">All time Rank</button>
                </div>

                <div class="ek--ranking--table--body">

                    <div class="ek--ranking--table--global ek--ranking--table--content" >

                        <div class="ek--ranking--table--inner">
                            <div class="ek--ranking--table--inner--club">
                                <span class="ek--ranking--table--inner--rank">#</span>
                                <span class="ek--ranking--table--inner--name">Club name</span>
                            </div>
                            <div class="ek--ranking--table--inner--country">
                                <span>Country name</span>
                            </div>

                            <div class="ek--ranking--table--inner--score">Score</div>

                        </div>

                        @foreach(Session::get("global_ranks")["currentRanks"] as $ranking)
                            @if($loop->index < 50 && $loop->index+1 != Session::get("global_ranks")["myCurrentRank"]["rank"])
                                <div class="ek--ranking--table--inner">
                                    <div class="ek--ranking--table--inner--club">
                                        <span class="ek--ranking--table--inner--rank">{{ $loop->index+1 }}</span>
                                        <span class="ek--ranking--table--inner--name">{{ $ranking["clubName"] }}</span>
                                    </div>
                                    <div class="ek--ranking--table--inner--country" style="justify-content: flex-start">
                                        <span>{{ $ranking["country"]["name"] }}</span>
                                        <img src="{{ url("static/img/Flags/".$ranking["country"]["name"].".svg") }}">
                                    </div>

                                    <div class="ek--ranking--table--inner--score">{{ $ranking["points"] }}</div>


                                </div>
                                @else
                                <div class="ek--ranking--table--inner">
                                    <div class="ek--ranking--table--inner--club">
                                        <span class="ek--ranking--table--inner--rank text--yellow">{{ $loop->index+1 }}</span>
                                        <span class="ek--ranking--table--inner--name text--yellow">{{ $ranking["clubName"] }}</span>
                                    </div>
                                    <div class="ek--ranking--table--inner--country" style="justify-content: flex-start">
                                        <span class="text--yellow">{{ $ranking["country"]["name"] }}</span>
                                        <img src="{{ url("static/img/Flags/".$ranking["country"]["name"].".svg") }}">
                                    </div>

                                    <div class="ek--ranking--table--inner--score text--yellow">{{ $ranking["points"] }}</div>


                                </div>
                            @endif
                        @endforeach

                        @if(Session::get("global_ranks")["myCurrentRank"]["rank"] > 50)
                        <block>

                            @foreach(Session::get("global_ranks")["currentRanks"] as $ranking)

                                @if($ranking["clubId"] == Session::get("club")["id"])
                                    <div class="ek--ranking--table--inner own" style="display: flex">

                                        <div class="ek--ranking--table--inner--club" >
                                            <span class="ek--ranking--table--inner--rank">{{ $loop->index+1 }}</span>
                                            <span class="ek--ranking--table--inner--name">{{ $ranking["clubName"] }}</span>
                                        </div>

                                        <div class="ek--ranking--table--inner--country">
                                            <span th:text="${ranking.countryDao.name}">{{ $ranking["country"]["name"] }}</span>
                                            <img src="{{ url("static/img/Flags/".$ranking["country"]["name"].".svg") }}">

                                        </div>

                                        <div class="ek--ranking--table--inner--score">{{ $ranking["points"] }}</div>

                                    </div>
                                @endif
                            @endforeach

                        </block>
                        @endif
                    </div>

                    <div class="ek--ranking--table--local ek--ranking--table--content">
                        <div class="ek--ranking--table--inner">
                            <div class="ek--ranking--table--inner--club">
                                <span class="ek--ranking--table--inner--rank">#</span>
                                <span class="ek--ranking--table--inner--name">Club name</span>
                            </div>
                            <div class="ek--ranking--table--inner--country">
                                <span>Country name</span>
                            </div>

                            <div class="ek--ranking--table--inner--score">Score</div>

                        </div>
                        @foreach(Session::get("global_ranks")["allRanks"] as $ranking)
                            @if($loop->index < 50  && $loop->index+1 != Session::get("global_ranks")["myAllRank"]["rank"])
                                <div class="ek--ranking--table--inner">
                                    <div class="ek--ranking--table--inner--club">
                                        <span class="ek--ranking--table--inner--rank">{{ $loop->index+1 }}</span>
                                        <span class="ek--ranking--table--inner--name">{{ $ranking["clubName"] }}</span>
                                    </div>
                                    <div class="ek--ranking--table--inner--country" style="justify-content: flex-start">
                                        <span>{{ $ranking["country"]["name"] }}</span>
                                        <img src="{{ url("static/img/Flags/".$ranking["country"]["name"].".svg") }}">
                                    </div>

                                    <div class="ek--ranking--table--inner--score">{{ $ranking["points"] }}</div>


                                </div>
                                @else
                                <div class="ek--ranking--table--inner">
                                    <div class="ek--ranking--table--inner--club">
                                        <span class="ek--ranking--table--inner--rank text--yellow">{{ $loop->index+1 }}</span>
                                        <span class="ek--ranking--table--inner--name text--yellow">{{ $ranking["clubName"] }}</span>
                                    </div>
                                    <div class="ek--ranking--table--inner--country" style="justify-content: flex-start">
                                        <span class="text--yellow">{{ $ranking["country"]["name"] }}</span>
                                        <img src="{{ url("static/img/Flags/".$ranking["country"]["name"].".svg") }}">
                                    </div>

                                    <div class="ek--ranking--table--inner--score text--yellow">{{ $ranking["points"] }}</div>


                                </div>
                            @endif
                        @endforeach

                            @if(Session::get("global_ranks")["myAllRank"]["rank"] > 50)
                            <block>

                                @foreach(Session::get("global_ranks")["allRanks"] as $ranking)

                                    @if($ranking["clubId"] == Session::get("club")["id"])
                                        <div class="ek--ranking--table--inner own" style="display: flex">

                                            <div class="ek--ranking--table--inner--club" >
                                                <span class="ek--ranking--table--inner--rank">{{ $loop->index+1 }}</span>
                                                <span class="ek--ranking--table--inner--name">{{ $ranking["clubName"] }}</span>
                                            </div>

                                            <div class="ek--ranking--table--inner--country" style="justify-content: flex-start">
                                                <span>{{ $ranking["country"]["name"] }}</span>
                                                <img src="{{ url("static/img/Flags/".$ranking["country"]["name"].".svg") }}">

                                            </div>

                                            <div class="ek--ranking--table--inner--score">{{ $ranking["points"] }}</div>

                                        </div>
                                    @endif
                                @endforeach

                            </block>
                            @endif
                    </div>

                </div>

            </div>

        </div>

    </div>


@endsection
