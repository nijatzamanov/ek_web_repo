@extends('client.layout.authenticated')
@section('content')

    <!-- Ek Match -->
    <section class="ek--match">
        <!-- Ek Match Head -->
        <div class="ek--match--head bg--navy--700">
            <!-- Ek Match Head Info -->
            <div class="ek--match--head--info bg--success text--green"><span class="text--green"></span><span class="text--green">{{ Session::get("next_match")[0]["tournament"] }}, Round &nbsp #</span><span class="text--green">{{ Session::get("next_match")[0]["round"] }}</span></div>
            <!-- End Ek Match Head Info -->
            <!-- Ek Match Head Clubs -->


            <div class="ek--match--head--clubs">

                    <div id="cards-left" style="display:flex">

                    </div>

                    <div class="ek--body--matches--inner--clubs--left">
                        <div class="ek--body--matches--inner--clubs--name text--right">
                            <h3 class="text--white" id="homeTeam">{{ Session::get("next_match")[0]["homeTeam"]["clubName"] }}</h3>
                        </div>
                        <div class="ek--body--inner--club--hexagon text--navy--900" id="homeScore">{{ number_format(Session::get("next_match")[0]["homeTeam"]["ability"],0) }}</div>
                    </div>

                    <div class="ek--match--head--vs bg--navy--900 text--white">
                            <span class="ek--match--head--vs--span">VS</span>
                            <p class="ek--match--head--vs--soccer">
                                <!-- First Team Goal -->
                                <span id="span1">0</span>
                                <!-- First Team Goal -->
                                -
                                <!-- First Team Goal -->
                                <span id="span2" >0</span>
                                <!-- First Team Goal -->
                            </p>
                    </div>

                    <div class="ek--body--matches--inner--clubs--right">
                        <div class="ek--body--inner--club--hexagon text--navy--900" id="awayScore">{{ number_format(Session::get("next_match")[0]["awayTeam"]["ability"],0) }}</div>
                        <div class="ek--body--matches--inner--clubs--name text--start">
                            <h3 class="text--white" id="awayTeam">{{ Session::get("next_match")[0]["awayTeam"]["clubName"] }}</h3>
                        </div>
                    </div>

                    <div id="cards-right" style="display:flex">

                    </div>

            </div>
             <!-- End Ek Match Head Clubs -->
             <!-- Ek Match Head Ovals -->
             <div class="ek--match--head--ovals">
                 <div class="ek--match--head--ovals--div bg--navy--600">
                     <div class="ek--match--head--ovals--inner bg--navy--500"></div>
                 </div>
                 <div class="ek--match--head--ovals--div bg--navy--600">
                    <div class="ek--match--head--ovals--inner bg--navy--500"></div>
                </div>
                <div class="ek--match--head--ovals--div bg--navy--600">
                    <div class="ek--match--head--ovals--inner bg--navy--500"></div>
                </div>
                <div class="ek--match--head--ovals--div bg--navy--600">
                    <div class="ek--match--head--ovals--inner bg--navy--500"></div>
                </div>
                <div class="ek--match--head--ovals--div bg--navy--600">
                    <div class="ek--match--head--ovals--inner bg--navy--500"></div>
                </div>
             </div>
             <!-- End Ek Match Head Ovals -->
            <!-- Ek Match Head Loading -->
            <div class="ek--match--head--loading">
                <div class="ek--match--head--loading--container">
                    <ul>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
            </div>
            <!-- End Ek Match Head Loading -->
        </div>
        <!-- End Ek Match Head -->

        <!-- Ek Match Body -->
        <div class="ek--match--body">
            <!-- Ek Match Body Waiting  -->
            <div class="ek--match--body--waiting bg--navy--700">
                <div class="ek--match--body--waiting--fade">


                    <!-- Ek Match Body Waiting Ad  -->
{{--                    <div class="ek--match--body--ad bg--blue">--}}
{{--                        <!-- Ek Match Body Waiting Ad  Image -->--}}
{{--                        <img src="{{ url("static/img/Logo/ek--adidas@2x.png") }}" alt="">--}}
{{--                        <!-- End Ek Match Body Waiting Ad  Image -->--}}
{{--                        <!-- Ek Match Body Waiting Ad  Paragraph -->--}}
{{--                        <p class="text--white ek--size--16">With the sponsorship of <strong>Adidas</strong>, the exciting game begins!</p>--}}
{{--                        <!-- End Ek Match Body Waiting Ad  Paragraph -->--}}
{{--                    </div>--}}
                    <!-- End Ek Match Body Waiting Ad  -->



                    <!-- Ek Match Body Waiting Ball Loading  -->
                    <div class="ek--match--body--ball">
                        <!-- Ek Match Body Waiting Ball Loading Image -->
                        <img class="ek--match--body--ball--image" src="{{ url("static/img/Other/ek--ball.svg") }}" alt="ek--ball">
                        <!-- End Ek Match Body Waiting Ball Loading Image -->
                        <!-- Ek Match Body Waiting Ball Loading Image -->
                        <!-- <img class="ek--match--body--ball--shadow" src="img/Other/ek--ball--shadow.svg" /> -->
                        <div class="ek--match--body--ball--shadow"></div>
                        <!-- End Ek Match Body Waiting Ball Loading Image -->
                    </div>
                    <!-- End Ek Match Body Waiting Ball Loading  -->
                    <!-- Ek Match Body Lucky -->
                    <div class="ek--match--body--lucky">
                        <h3 class="text--white ek--size-20">The big game starts in <span>5</span> seconds</h3>
                        <p class="text--navy--200 ek--size-16">Players are now ready to play. Good luck!</p>
                    </div>
                    <!-- End Ek Match Body Lucky -->
                </div>
                <!-- Ek Match Body Game -->
                <div class="ek--match--body--game bg--navy--700">
                    <!-- Ek Match Body Game Time -->
                    <div class="ek--match--body--game--time">
                        <!-- Ek Match Body Game Time Badge -->
                        <div class="ek--match--body--game--time--badge text--white bg--red">
                            <svg class="icon icon-ek--oval fill--white">
                                <use xlink:href="static/img/icons.svg#icon-ek--oval"></use>
                            </svg>
                            <span class="ek--size--16" id="countdown">1</span>
                        </div>
                        <!-- End Ek Match Body Game Time Badge -->
                        <!-- Ek Match Body Game Time Full  -->
                        <div id="ek--match--body--game--time--half" class="ek--match--body--game--time--full bg--yellow text--navy--900 ek--size--16">Half time</div>

                        <div id="ek--match--body--game--time--full" class="ek--match--body--game--time--full bg--green text--navy--900 ek--size--16">Full time</div>
                        <!-- End Ek Match Body Game Time Full  -->
                    </div>
                    <!-- End Ek Match Body Game Time -->

                    <!-- Ek Match Body Game Play -->
                    <div class="ek--match--body--game--play">
                        <!-- Ek Match Body Game Play Time -->
                        <div class="ek--match--body--game--play--inner" id="divvv">

                        </div>
                        <!-- Ek Match Body Game Play Time -->
                        <!-- EK Match Body Play Gradient -->
                        <div class="ek--match--body--game--play--gradient"></div>
                        <!-- End EK Match Body Play Gradient -->
                    </div>
                    <!-- End Ek Match Body Game Play -->
                    <!-- Ek Match Body Game Skip Details -->
                    <div class="ek--match--body--game--skip">
                        <a class="bg--navy--600 text--white ek--size--16" id="skip1" href="#skip">Skip this details</a>
                    </div>

                    <div class="ek--match--body--game--media">
                        <audio id="ek--start" src="{{ url("static/media/sound_start.mpeg") }}"></audio>
                        <audio id="ek--finish" src="{{ url("static/media/sound_finish.mpeg") }}"></audio>
                        <audio id="ek--goal" src="{{ url("static/media/sound_goal.wav") }}"></audio>
                        <audio id="ek--card" src="{{ url("static/media/sound_card.wav") }}"></audio>
                        <audio id="ek--live" src="{{ url("static/media/sound_live.mp3") }}"></audio>
                    </div>



                    <div class="ek--match--goal--modal" style="display: none">
                        <div class="ek--match--goal--modal--body">
                            <div class="ek--match--goal--content">
                                <h4 class="ek--match--goal--title">Gooooooaaaaal</h4>
                                <div class="ek--match--goal--content--image">
                                    <img src="{{ url("static/img/Other/gif_goal.gif") }}">
                                    <img class="ek--match--goal--content--player" src="{{ url("static/img/Other/player.svg") }}">
                                    <audio src=""></audio>
                                </div>
                                <p class="ek--match--goal--info">GOAL! Superb finish by </p>
                                <span class="ek--match--goal--player" id="homePlayerName"></span>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- End Ek Match Body Game -->
                <!-- Ek Match Body Stats -->
                <div class="ek--match--body--stats bg--navy--700">
                    <!-- Ek Match Body Stats Tabs -->
                    <div class="ek--match--body--stats--tabs">
                        <!-- Ek Match Body Stats Tabs Head -->
                        <div class="ek--match--body--stats--tabs--head">
                            <!-- Ek Match Body Stats Tabs Head Buttons -->
                            <div id="ek--match--body--stats--tabs--matchstats" class="ek--match--body--stats--tabs--head--button button--active ek--size--16">Match stats</div>
                            <div id="ek--match--body--stats--tabs--matchinfo" class="ek--match--body--stats--tabs--head--button ek--size--16">Match Info</div>
                            <div id="ek--match--body--stats--tabs--matchreward" class="ek--match--body--stats--tabs--head--button ek--size--16">Match reward</div>
                            <!-- Ek Match Body Stats Tabs Head Buttons -->
                        </div>
                        <!-- End Ek Match Body Stats Tabs Head -->
                        <!-- Ek Match Body Stats Tabs Table -->
                        <!-- Ek Match Body Stats Tabs Table Stats -->
                        <div class="ek--match--body--stats--tabs--matchstats ek--match--body--stats--tabs--filter">
                            <!-- Ek Match Body Stats Tabs Table Inner -->
                            <div class="ek--match--body--stats--tabs--matchstats--inner bg--navy--600">
                                <!-- Ek Match Body Stats Tabs Table Inner Left -->
                                <div class="ek--match--body--stats--tabs--matchstats--left">
                                    <!-- Ek Match Body Stats Tabs Table Inner Left Amount -->
                                    <span class="text--white ek--size--16--500" id="homePosession" ></span>
                                    <!-- End Ek Match Body Stats Tabs Table Inner Left Amount -->
                                    <!-- Ek Match Body Stats Tabs Table Inner Left Progress -->
                                    <div class="ek--match--body--stats--tabs--matchstats--progress">
                                        <div id="homePosessionProgress" class="progress">
                                            <!--<div class="progress-bar" role="progressbar" style="width:90%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>-->
                                        </div>
                                    </div>
                                    <!-- End Ek Match Body Stats Tabs Table Inner Left Progress -->
                                </div>
                                <!-- End Ek Match Body Stats Tabs Table Inner Left -->
                                <!-- Ek Match Body Stats Tabs Table Inner Center -->
                                <div class="ek--match--body--stats--tabs--matchstats--center ek--size--16 text--white">Posession</div>
                                <!-- End Ek Match Body Stats Tabs Table Inner Center -->
                                <!-- Ek Match Body Stats Tabs Table Inner Right -->
                                <div class="ek--match--body--stats--tabs--matchstats--right">
                                    <!-- Ek Match Body Stats Tabs Table Inner Right Progress -->
                                    <div class="ek--match--body--stats--tabs--matchstats--progress">
                                        <div id="awayPosessionProgress" class="progress">
                                            <!--<div class="progress-bar" role="progressbar" style="width:90%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>-->
                                        </div>
                                    </div>
                                    <!-- End Ek Match Body Stats Tabs Table Inner Right Progress -->
                                    <!-- Ek Match Body Stats Tabs Table Inner Right Amount -->
                                    <span class="text--white ek--size--16--500" id="awayPosession"  ></span>
                                    <!-- End Ek Match Body Stats Tabs Table Inner Right Amount -->
                                </div>
                                <!-- End Ek Match Body Stats Tabs Table Inner Right -->
                            </div>
                            <!-- End Ek Match Body Stats Tabs Table Inner -->
                            <!-- Ek Match Body Stats Tabs Table Inner -->
                            <div class="ek--match--body--stats--tabs--matchstats--inner bg--navy--600">
                                <!-- Ek Match Body Stats Tabs Table Inner Left -->
                                <div class="ek--match--body--stats--tabs--matchstats--left">
                                    <!-- Ek Match Body Stats Tabs Table Inner Left Amount -->
                                    <span class="text--white ek--size--16--500" id="homeTotalShoots" ></span>
                                    <!-- End Ek Match Body Stats Tabs Table Inner Left Amount -->
                                    <!-- Ek Match Body Stats Tabs Table Inner Left Progress -->
                                    <div class="ek--match--body--stats--tabs--matchstats--progress">
                                        <div id="homeTotalShootsProgress"  class="progress">
                                            <!--<div class="progress-bar" role="progressbar" style="width:90%" aria-valuenow="17" aria-valuemin="0" aria-valuemax="100"></div>-->
                                        </div>
                                    </div>
                                    <!-- End Ek Match Body Stats Tabs Table Inner Left Progress -->
                                </div>
                                <!-- End Ek Match Body Stats Tabs Table Inner Left -->
                                <!-- Ek Match Body Stats Tabs Table Inner Center -->
                                <div class="ek--match--body--stats--tabs--matchstats--center ek--size--16 text--white">Total shots</div>
                                <!-- End Ek Match Body Stats Tabs Table Inner Center -->
                                <!-- Ek Match Body Stats Tabs Table Inner Right -->
                                <div class="ek--match--body--stats--tabs--matchstats--right">
                                    <!-- Ek Match Body Stats Tabs Table Inner Right Progress -->
                                    <div class="ek--match--body--stats--tabs--matchstats--progress">
                                        <div id="awayTotalShootsProgress"  class="progress">
                                            <!--<div class="progress-bar" role="progressbar" style="width: 90%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>-->
                                        </div>
                                    </div>
                                    <!-- End Ek Match Body Stats Tabs Table Inner Right Progress -->
                                    <!-- Ek Match Body Stats Tabs Table Inner Right Amount -->
                                    <span class="text--white ek--size--16--500" id="awayTotalShoots" ></span>
                                    <!-- End Ek Match Body Stats Tabs Table Inner Right Amount -->
                                </div>
                                <!-- End Ek Match Body Stats Tabs Table Inner Right -->
                            </div>
                            <!-- End Ek Match Body Stats Tabs Table Inner -->
                            <!-- Ek Match Body Stats Tabs Table Inner -->
                            <div class="ek--match--body--stats--tabs--matchstats--inner bg--navy--600">
                                <!-- Ek Match Body Stats Tabs Table Inner Left -->
                                <div class="ek--match--body--stats--tabs--matchstats--left">
                                    <!-- Ek Match Body Stats Tabs Table Inner Left Amount -->
                                    <span class="text--white ek--size--16--500" id="homeShootsTarget" ></span>
                                    <!-- End Ek Match Body Stats Tabs Table Inner Left Amount -->
                                    <!-- Ek Match Body Stats Tabs Table Inner Left Progress -->
                                    <div class="ek--match--body--stats--tabs--matchstats--progress">
                                        <div id="homeShootsTargetProgress" class="progress">
                                            <!--<div class="progress-bar" role="progressbar" style="width:90%" aria-valuenow="8" aria-valuemin="0" aria-valuemax="100"></div>-->
                                        </div>
                                    </div>
                                    <!-- End Ek Match Body Stats Tabs Table Inner Left Progress -->
                                </div>
                                <!-- End Ek Match Body Stats Tabs Table Inner Left -->
                                <!-- Ek Match Body Stats Tabs Table Inner Center -->
                                <div class="ek--match--body--stats--tabs--matchstats--center ek--size--16 text--white">Shots on target</div>
                                <!-- End Ek Match Body Stats Tabs Table Inner Center -->
                                <!-- Ek Match Body Stats Tabs Table Inner Right -->
                                <div class="ek--match--body--stats--tabs--matchstats--right">
                                    <!-- Ek Match Body Stats Tabs Table Inner Right Progress -->
                                    <div class="ek--match--body--stats--tabs--matchstats--progress">
                                        <div id="awayShootsTargetProgress" class="progress">
                                            <!--<div class="progress-bar" role="progressbar" style="width:90%" aria-valuenow="6" aria-valuemin="0" aria-valuemax="100"></div>-->
                                        </div>
                                    </div>
                                    <!-- End Ek Match Body Stats Tabs Table Inner Right Progress -->
                                    <!-- Ek Match Body Stats Tabs Table Inner Right Amount -->
                                    <span class="text--white ek--size--16--500" id="awayShootsTarget"></span>
                                    <!-- End Ek Match Body Stats Tabs Table Inner Right Amount -->
                                </div>
                                <!-- End Ek Match Body Stats Tabs Table Inner Right -->
                            </div>
                            <!-- End Ek Match Body Stats Tabs Table Inner -->
                            <!-- Ek Match Body Stats Tabs Table Inner -->
                            <div class="ek--match--body--stats--tabs--matchstats--inner bg--navy--600">
                                <!-- Ek Match Body Stats Tabs Table Inner Left -->
                                <div class="ek--match--body--stats--tabs--matchstats--left">
                                    <!-- Ek Match Body Stats Tabs Table Inner Left Amount -->
                                    <span class="text--white ek--size--16--500" id="homeYellowCard"></span>
                                    <!-- End Ek Match Body Stats Tabs Table Inner Left Amount -->
                                    <!-- Ek Match Body Stats Tabs Table Inner Left Progress -->
                                    <div class="ek--match--body--stats--tabs--matchstats--progress">
                                        <div id="homeYellowCardProgress" class="progress">
                                            <!--<div class="progress-bar" role="progressbar" style="width:90%" aria-valuenow="1" aria-valuemin="0" aria-valuemax="100"></div>-->
                                        </div>
                                    </div>
                                    <!-- End Ek Match Body Stats Tabs Table Inner Left Progress -->
                                </div>
                                <!-- End Ek Match Body Stats Tabs Table Inner Left -->
                                <!-- Ek Match Body Stats Tabs Table Inner Center -->
                                <div class="ek--match--body--stats--tabs--matchstats--center ek--size--16 text--white">Yellow card</div>
                                <!-- End Ek Match Body Stats Tabs Table Inner Center -->
                                <!-- Ek Match Body Stats Tabs Table Inner Right -->
                                <div class="ek--match--body--stats--tabs--matchstats--right">
                                    <!-- Ek Match Body Stats Tabs Table Inner Right Progress -->
                                    <div class="ek--match--body--stats--tabs--matchstats--progress">
                                        <div id="awayYellowCardProgress" class="progress">
                                            <!--<div class="progress-bar" role="progressbar" style="width:90%" aria-valuenow="3" aria-valuemin="0" aria-valuemax="100"></div>-->
                                        </div>
                                    </div>
                                    <!-- End Ek Match Body Stats Tabs Table Inner Right Progress -->
                                    <!-- Ek Match Body Stats Tabs Table Inner Right Amount -->
                                    <span class="text--white ek--size--16--500" id="awayYellowCard"></span>
                                    <!-- End Ek Match Body Stats Tabs Table Inner Right Amount -->
                                </div>
                                <!-- End Ek Match Body Stats Tabs Table Inner Right -->
                            </div>
                            <!-- End Ek Match Body Stats Tabs Table Inner -->
                            <!-- Ek Match Body Stats Tabs Table Inner -->
                            <div class="ek--match--body--stats--tabs--matchstats--inner bg--navy--600">
                                <!-- Ek Match Body Stats Tabs Table Inner Left -->
                                <div class="ek--match--body--stats--tabs--matchstats--left">
                                    <!-- Ek Match Body Stats Tabs Table Inner Left Amount -->
                                    <span class="text--white ek--size--16--500" id="homeRedCard"></span>
                                    <!-- End Ek Match Body Stats Tabs Table Inner Left Amount -->
                                    <!-- Ek Match Body Stats Tabs Table Inner Left Progress -->
                                    <div class="ek--match--body--stats--tabs--matchstats--progress">
                                        <div id="homeRedCardProgress" class="progress">
                                            <!--<div class="progress-bar" role="progressbar" style="width:90%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>-->
                                        </div>
                                    </div>
                                    <!-- End Ek Match Body Stats Tabs Table Inner Left Progress -->
                                </div>
                                <!-- End Ek Match Body Stats Tabs Table Inner Left -->
                                <!-- Ek Match Body Stats Tabs Table Inner Center -->
                                <div class="ek--match--body--stats--tabs--matchstats--center ek--size--16 text--white">Red card</div>
                                <!-- End Ek Match Body Stats Tabs Table Inner Center -->
                                <!-- Ek Match Body Stats Tabs Table Inner Right -->
                                <div class="ek--match--body--stats--tabs--matchstats--right">
                                    <!-- Ek Match Body Stats Tabs Table Inner Right Progress -->
                                    <div class="ek--match--body--stats--tabs--matchstats--progress">
                                        <div id="awayRedCardProgress" class="progress">
                                            <!--<div class="progress-bar" role="progressbar"  style="width:90%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>-->
                                        </div>
                                    </div>
                                    <!-- End Ek Match Body Stats Tabs Table Inner Right Progress -->
                                    <!-- Ek Match Body Stats Tabs Table Inner Right Amount -->
                                    <span class="text--white ek--size--16--500" id="awayRedCard" ></span>
                                    <!-- End Ek Match Body Stats Tabs Table Inner Right Amount -->
                                </div>
                                <!-- End Ek Match Body Stats Tabs Table Inner Right -->
                            </div>
                            <!-- End Ek Match Body Stats Tabs Table Inner -->
                        </div>
                        <!-- End Ek Match Body Stats Tabs Table Stats -->
                        <!-- Ek Match Body Stats Tabs Table Info -->
                        <div class="ek--match--body--stats--tabs--matchinfo ek--match--body--stats--tabs--filter" id="actions">





                        </div>
                        <!-- End Ek Match Body Stats Tabs Table Info -->
                        <!-- Ek Match Body Stats Tabs Table Reward -->
                        <div class="ek--match--body--stats--tabs--matchreward ek--match--body--stats--tabs--filter">
                            <!-- Ek Match Body Stats Tabs Table Reward Inner -->
                            <div class="ek--match--body--stats--tabs--matchreward--inner bg--navy--600">
                                <!-- Ek Match Body Stats Tabs Table Reward Inner Left -->
                                <div class="ek--match--body--stats--tabs--matchreward--left">
                                    <!-- Ek Match Body Stats Tabs Table Reward Inner Left Game Bonus -->
                                    <span class="ek--size--16 text--white">Games bonus</span>
                                    <!-- End Ek Match Body Stats Tabs Table Reward Inner Left Game Bonus -->
                                </div>
                                <!-- End Ek Match Body Stats Tabs Table Reward Inner Left -->
                                <!-- Ek Match Body Stats Tabs Table Reward Inner Right -->
                                <div class="ek--match--body--stats--tabs--matchreward--right">
                                    <!-- Ek Match Body Stats Tabs Table Reward Inner Right Amount -->

                                    <span class="ek--size--16 text--white" id="earnedMoney"></span>
                                    <!-- End Ek Match Body Stats Tabs Table Reward Inner Right Amount -->
                                    <!-- Ek Match Body Stats Tabs Table Reward Inner Right SVG Icon -->
                                    <svg class="icon icon-ek--budget"><use xlink:href="static/img/icons.svg#icon-ek--budget"></use></svg>
                                    <!-- End Ek Match Body Stats Tabs Table Reward Inner Right SVG Icon -->
                                </div>
                                <!-- End Ek Match Body Stats Tabs Table Reward Inner Right -->
                            </div>
                            <!-- End Ek Match Body Stats Tabs Table Reward Inner -->
                            <!-- Ek Match Body Stats Tabs Table Reward Inner -->
                            <div class="ek--match--body--stats--tabs--matchreward--inner bg--navy--600">
                                <!-- Ek Match Body Stats Tabs Table Reward Inner Left -->
                                <div class="ek--match--body--stats--tabs--matchreward--left">
                                    <!-- Ek Match Body Stats Tabs Table Reward Inner Left Game Bonus -->
                                    <span class="ek--size--16 text--white">EK Coins earned</span>
                                    <!-- End Ek Match Body Stats Tabs Table Reward Inner Left Game Bonus -->
                                </div>
                                <!-- End Ek Match Body Stats Tabs Table Reward Inner Left -->
                                <!-- Ek Match Body Stats Tabs Table Reward Inner Right -->
                                <div class="ek--match--body--stats--tabs--matchreward--right">
                                    <!-- Ek Match Body Stats Tabs Table Reward Inner Right Amount -->
                                    <span class="ek--size--16 text--white" id="earnedCoins" ></span>
                                    <!-- End Ek Match Body Stats Tabs Table Reward Inner Right Amount -->
                                    <!-- Ek Match Body Stats Tabs Table Reward Inner Right SVG Icon -->
                                    <svg class="icon icon-ek--coins"><use xlink:href="static/img/icons.svg#icon-ek--coins"></use></svg>
                                    <!-- End Ek Match Body Stats Tabs Table Reward Inner Right SVG Icon -->
                                </div>
                                <!-- End Ek Match Body Stats Tabs Table Reward Inner Right -->
                            </div>
                            <!-- End Ek Match Body Stats Tabs Table Reward Inner -->
                        </div>
                        <!-- End Ek Match Body Stats Tabs Table Reward -->
                        <!-- End Ek Match Body Stats Tabs Table -->
                    </div>
                    <!-- End Ek Match Body Stats -->
                </div>
                <!-- End Ek Match Body Stats -->
            </div>
            <!-- End Ek Match Body Waiting -->

        </div>
        <!-- End Ek Match Body -->
    </section>
    <!-- End Ek Match -->
@endsection
<div class="ek--match--background"></div>
@section('appendix')
    <div id="game" class="ek--modal ek--modal--border--left--orange bg--navy--700 modal">
        <div class="ek--modal--body">
            <div class="ek--modal--body--left">
                <div class="ek--modal--body--left--image bg--orange--light">
                    <svg class="icon icon-ek--warning fill--white"><use xlink:href="static/img/icons.svg#icon-ek--warning"></use></svg>
                </div>
            </div>
            <div class="ek--modal--body--right">
                <p id="no-energy" class="text--navy--100 ek--size--18">Player energy of club is low than limit</p>
                <p id="waiting-time" class="text--navy--100 ek--size--18">Wait untill the next match</p>

                <div class="ek--modal--body--buttons">
                    <a class="ek--modal--body--buttons--cancel text--white ek--size--16" href="{{ route("lineup") }}" >Cancel</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{url("static/js/text.js")}}"></script>
    <script src="{{url("static/js/play-match.js?v=".time())}}"></script>
    <script src="{{url("static/js/finance.js")}}"></script>
@endsection

