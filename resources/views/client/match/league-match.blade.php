@extends('client.layout.authenticated')
@section('content')

    <!-- Ek Match -->
    <section class="ek--match ek--league">
        <!-- Ek Match Head -->
        <div class="ek--match--head bg--navy--700">
            <!-- Ek League Season  -->
            <div class="ek--league--season">
                <div class="ek--league--season--info bg--navy--900">
                    <h3 class="ek--size--20--400 text--white">League</h3>
                    <p class="text--green">Season 5</p>
                    <img src="{{ url("static/img/Other/ek--league--image.png") }}" alt="ek--league--image">
                </div>
            </div>
            <!-- End Ek League Season  -->
            <!-- Ek Match Head Ovals -->
            <div class="ek--match--head--ovals">
                <div class="ek--match--head--ovals--div bg--navy--600">
                    <div class="ek--match--head--ovals--inner bg--navy--500"></div>
                </div>
                <div class="ek--match--head--ovals--div bg--navy--600">
                <div class="ek--match--head--ovals--inner bg--navy--500"></div>
            </div>
            <div class="ek--match--head--ovals--div bg--navy--600">
                <div class="ek--match--head--ovals--inner bg--navy--500"></div>
            </div>
            <div class="ek--match--head--ovals--div bg--navy--600">
                <div class="ek--match--head--ovals--inner bg--navy--500"></div>
            </div>
            <div class="ek--match--head--ovals--div bg--navy--600">
                <div class="ek--match--head--ovals--inner bg--navy--500"></div>
            </div>
            </div>
            <!-- End Ek Match Head Ovals -->
            <!-- Ek Match Head Loading -->
            <div class="ek--match--head--loading">
                <div class="ek--match--head--loading--container">
                    <ul>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
            </div>
            <!-- End Ek Match Head Loading -->
        </div>
        <!-- End Ek Match Head -->
        <!-- Ek League Tab -->
        <div class="ek--league--tab bg--navy--700">
            <!-- Ek League Tab Head -->
            <div class="ek--league--tab--head">
                <button id="ek--league--tab--standing" class="ek--league--tab--button button--active">Standing</button>
                <button id="ek--league--tab--results" class="ek--league--tab--button">Results</button>
                <button id="ek--league--tab--fixture" class="ek--league--tab--button">Fixture</button>
                <button id="ek--league--tab--topscores" class="ek--league--tab--button">Topscores</button>
            </div>
            <!-- End Ek League Tab Head -->
            <!-- Ek League Tab Content -->
            <div class="ek--league--tab--content">
                <!-- Ek League Tab Standing -->
                <div class="ek--league--tab--standing ek--league--tab--table">
                    <!-- Ek League Tab Standing Head -->
                    <div class="ek--league--tab--standing--head">
                        <div class="ek--league--tab--standing--head--jn text--navy--200 ek--size--16">JN</div>
                        <div class="ek--league--tab--standing--head--name text--navy--200 ek--size--16">Club name</div>
                        <div class="ek--league--tab--standing--head--gp text--navy--200 ek--size--16">GP</div>
                        <div class="ek--league--tab--standing--head--w text--navy--200 ek--size--16">W</div>
                        <div class="ek--league--tab--standing--head--d text--navy--200 ek--size--16">D</div>
                        <div class="ek--league--tab--standing--head--l text--navy--200 ek--size--16">L</div>
                        <div class="ek--league--tab--standing--head--fa text--navy--200 ek--size--16">G F/A</div>
                        <div class="ek--league--tab--standing--head--p text--navy--200 ek--size--16">P</div>
                    </div>
                     <!-- End Ek League Tab Standing Head -->
                     <!-- Ek League Tab List -->
                     <div class="ek--league--tab--list">

                         @php
                             $clubs = Session::get("league_standing");
                             function sortByPoints($a, $b) {
                                 return $a['point'] < $b['point'];
                             }

                             function sortByWins($a, $b){
                                 return $a['gf'] < $b['gf'];
                             }

                             usort($clubs, 'sortByPoints');
                             usort($clubs, 'sortByWins');
                         @endphp
                        <!-- Ek League Tab Inner -->
                        @foreach( $clubs as $standing )
                        <div class="ek--league--tab--inner bg--navy--600">
                            @if( $standing["clubName"] != Session::get("club")["clubName"] )
                                <div class="ek--league--tab--inner--jn text--white ek--size--16--500">{{ $loop->index+1 }}</div>

                                <div class="ek--league--tab--inner--name">
                                    <span class="text--white ek--size--16--500">{{ $standing["clubName"] }}</span></div>
                                    <div class="ek--league--tab--inner--gp text--white ek--size--16--500">{{ $standing["gp"] }}</div>
                                    <div class="ek--league--tab--inner--w text--white ek--size--16--500">{{ $standing["w"] }}</div>
                                    <div class="ek--league--tab--inner--d text--white ek--size--16--500">{{ $standing["d"] }}</div>
                                    <div class="ek--league--tab--inner--l text--white ek--size--16--500">{{ $standing["l"] }}</div>
                                    <div class="ek--league--tab--inner--fa text--white ek--size--16--500"><span class="text--white ek--size--16--500">{{ $standing["gf"] }} </span><span class="text--white ek--size--16--500">-</span><span class="text--white ek--size--16--500">{{ $standing["ga"] }}</span></div>
                                    <div class="ek--league--tab--inner--p text--white ek--size--16--500">{{ $standing["point"] }}</div>
                                </div>
                                @else
                                 <div class="ek--league--tab--inner--jn text--yellow ek--size--16--500">{{ $loop->index+1 }}</div>

                                 <div class="ek--league--tab--inner--name">
                                     <span class="text--yellow ek--size--16--500">{{ $standing["clubName"] }}</span></div>
                                     <div class="ek--league--tab--inner--gp text--yellow ek--size--16--500">{{ $standing["gp"] }}</div>
                                     <div class="ek--league--tab--inner--w text--yellow ek--size--16--500">{{ $standing["w"] }}</div>
                                     <div class="ek--league--tab--inner--d text--yellow ek--size--16--500">{{ $standing["d"] }}</div>
                                     <div class="ek--league--tab--inner--l text--yellow ek--size--16--500">{{ $standing["l"] }}</div>
                                     <div class="ek--league--tab--inner--fa text--yellow ek--size--16--500"><span class="text--yellow ek--size--16--500">{{ $standing["gf"] }} </span><span class="text--yellow ek--size--16--500">-</span><span class="text--yellow ek--size--16--500">{{ $standing["ga"] }}</span></div>
                                     <div class="ek--league--tab--inner--p text--yellow ek--size--16--500">{{ $standing["point"] }}</div>
                                </div>
                            @endif
                        @endforeach
                        <!-- End Ek League Tab Inner -->


                     </div>
                     <!-- End Ek League Tab Standing List -->
                </div>
                <!-- End Ek League Tab Standing -->
                <!-- Ek League Tab Results -->
                <div class="ek--league--tab--results ek--league--tab--table">
                    <!-- Ek League Tab List -->
                    <div class="ek--league--tab--list">


                    <!-- Ek League Tab Inner -->
                    @foreach( Session::get("results_and_fixtures") as $result )
                        @if($result["played"] == true)
                            <div class="ek--league--tab--inner bg--navy--600">
                                <!-- Ek League Tab Inner First Win -->
                                @if( $result["homeScore"] >= $result["awayScore"] )
                                    <div class="ek--league--tab--inner--first text--white ek--size-16 @if($result["homeClubName"] == Session::get("club")["clubName"])text--yellow @else text--white @endif">{{ $result["homeClubName"] }}</div>
                                    @else
                                    <div class="ek--league--tab--inner--first ek--size-16 @if($result["homeClubName"] == Session::get("club")["clubName"])text--yellow @else text--white @endif">{{ $result["homeClubName"] }}</div>
                                @endif

                                <!-- End Ek League Tab Inner First Win -->
                                <!-- End Ek League Tab Inner Soccer -->
                                <div class="ek--league--tab--inner--soccer">
                                    <!-- End Ek League Tab Inner Soccer Cup -->
                                    <div class="ek--league--tab--inner--soccer--cup" ></div>
                                    <div class="ek--league--tab--inner--soccer--result text--white ek--size--16" ><span class="text--white ek--size--16">{{ $result["homeScore"] }}</span><span class="text--white ek--size--16">-</span><span class="text--white ek--size--16">{{ $result["awayScore"] }}</span></div>
                                    <div class="ek--league--tab--inner--soccer--cup"></div>
                                    <!-- End End Ek League Tab Inner Soccer Cup -->
                                </div>

                                @if( $result["homeScore"] <= $result["awayScore"] )
                                    <div class="ek--league--tab--inner--second text--white ek--size-16 @if($result["awayClubName"] == Session::get("club")["clubName"])text--yellow @else text--white @endif">{{ $result["awayClubName"] }}</div>
                                @else
                                    <div class="ek--league--tab--inner--second ek--size-16 @if($result["awayClubName"] == Session::get("club")["clubName"])text--yellow @else text--white @endif">{{ $result["awayClubName"] }}</div>
                                @endif
                                <!-- End Ek League Tab Inner First Win -->
                            </div>
                       @endif
                    @endforeach
                    <!-- End Ek League Tab Inner -->


                </div>
                <!-- End Ek League Tab Results -->
                </div>


                <!-- Ek League Tab Fixture -->
                <div class="ek--league--tab--fixture ek--league--tab--table">
                <!-- Ek League Tab List -->
                <div class="ek--league--tab--list">


                    <!-- Ek League Tab Inner -->
                    @foreach( Session::get("results_and_fixtures") as $fixture )
                        @if($fixture['played'] == false)
                            <div class="ek--league--tab--inner bg--navy--600">
                                <div class="ek--league--tab--inner--first  ek--size-16 text--white @if($fixture["homeClubName"] == Session::get("club")["clubName"])text--yellow @endif">{{ $fixture["homeClubName"] }}</div>
                                <div class="ek--league--tab--inner--soccer text--navy--200 ek--size--16 j-center">vs</div>
                                <div  class="ek--league--tab--inner--second  ek--size--16 text--white @if($fixture["awayClubName"] == Session::get("club")["clubName"])text--yellow @endif">{{ $fixture["awayClubName"] }}</div>
                            </div>
                        @endif
                    @endforeach
                    <!-- End Ek League Tab Inner -->

                </div>
                <!-- End Ek League Tab List -->
                </div>
                <!-- End Ek League Tab Fixture -->
                <!-- Ek League Tab Topscores -->
                <div class="ek--league--tab--topscores ek--league--tab--table">
                     <!-- Ek League Tab Standing Head -->
                    <div class="ek--league--tab--topscores--head">
                            <div class="ek--league--tab--topscores--head--jn text--navy--200 ek--size--16">JN</div>
                            <div class="ek--league--tab--topscores--head--name text--navy--200 ek--size--16">Club name</div>
                            <div class="ek--league--tab--topscores--head--goals text--navy--200 ek--size--16">Goals</div>
                    </div>
                    <!-- End Ek League Tab Standing Head -->
                    <!-- Ek League Tab List -->
                    <div class="ek--league--tab--list">


                    <!-- Ek League Tab Inner -->
                    @foreach( Session::get("scorers") as $scorer )
                            @if($scorer["clubName"] != Session::get("club")["clubName"])
                                <div class="ek--league--tab--inner bg--navy--600">
                                    <div class="ek--league--tab--inner--jn text--white ek--size--16">{{ $scorer["num"] }}</div>
                                    <div class="ek--league--tab--inner--name">
                                            <span class="text--white ek--size--16">{{ $scorer["fullName"] }}</span>
                                    </div>
                                    <div class="ek--league--tab--inner--goals text--white ek--size--16">{{ $scorer["goals"] }}</div>
                                </div>
                            @else
                                <div class="ek--league--tab--inner bg--navy--600">
                                    <div class="ek--league--tab--inner--jn text--white ek--size--16 text--yellow">{{ $scorer["num"] }}</div>
                                    <div class="ek--league--tab--inner--name">
                                        <span class="text--white ek--size--16 text--yellow">{{ $scorer["fullName"] }}</span>
                                    </div>
                                    <div class="ek--league--tab--inner--goals text--white ek--size--16 text--yellow">{{ $scorer["goals"] }}</div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <!-- End Ek League Tab Inner -->

                    </div>
                    <!-- End Ek League Tab Standing List -->
                </div>
                <!-- End Ek League Tab Topscores -->

            <!-- End Ek League Tab Content -->
        </div>
        <!-- End Ek League Tab -->
        </div>
    </section>
@endsection

