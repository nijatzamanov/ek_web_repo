@extends('client.layout.authenticated')
@section('content')
        <!-- EK Stadion -->
        <section class="ek--stadium">
            <div class="ek--stadium--container">
                <div class="ek--stadium--left bg--navy--700">
                    <div class="ek--stadium--left--head">
                        <h3 class="ek--stadium--left--title">{{ Session::get("stadium")["name"] }}</h3>
                        <span class="ek--stadium--left--level">Level {{ Session::get("stadium")["level"] }}</span>
                    </div>

                    <div class="ek--stadium--left--center">
                        <img src="{{ url("/static/img/Stadium/stadium-1.svg") }}" alt="" />
                    </div>

                    <div class="ek--stadium--left--bottom">
                        <div class="ek--stadium--left--bottom--info">
                            <svg class="icon icon-full-icon">
                                <use xlink:href="static/img/icons.svg#icon-full-icon"></use>
                            </svg>
                            <span>Max. Capacity</span>
                        </div>

                        <div class="ek--stadium--left--bottom--capacity">{{ Session::get("stadium")["capacity"] }}</div>
                    </div>
                </div>
                <div class="ek--stadium--right bg--navy--700">
                    <div class="ek--stadium--right--head">
                        <button id="ek--stadium--right--stadiums" class="active">
                            Stadiums
                        </button>
                        <button id="ek--stadium--right--statistics">Statistics</button>
                    </div>

                    <div class="ek--stadium--right--tabs">
                        <div class="ek--stadium--right--stadiums ek--stadium--right--table">
                            @foreach(Session::get("stadiums") as $stadium)

                            <div class="ek--stadium--right--inner bg--navy--600 @if($stadium["level"] == 1 || $stadium["selected"] == true)open-stadium @elseif(Session::get("stadiums")[$loop->index-1]["selected"]==true)buy-stadium @else disabled-stadium @endif">
                                <div class="ek--stadium--right--inner--image">
                                    <img src="{{ url("/static/img/Stadium/stadium-".$stadium["level"].".svg") }}" alt="">
                                    <div class="ek--stadium--right--inner--opacity"></div>
                                    <div class="ek--stadium--right--inner--lock">
                                        <svg class="icon icon-lock2">
                                            <use xlink:href="static/img/icons.svg#icon-lock2"></use>
                                        </svg>
                                    </div>
                                </div>

                                @php
                                    if( $stadium["price"] < 1000 ){
                                        $price = $stadium["price"];
                                    }else if($stadium["price"] >= 1000 && $stadium["price"] < 1000000){
                                        $price = number_format($stadium["price"]/1000)."K";
                                    }else if($stadium["price"] >= 1000000 && $stadium["price"] < 1000000000){
                                        $price = number_format($stadium["price"]/1000000)."M";
                                    }
                                    else if($stadium["price"] >= 1000000000){
                                        $price = number_format($stadium["price"]/1000000000)."B";
                                    }

                                @endphp

                                <div class="ek--stadium--right--inner--about">
                                    <span class="ek--stadium--right--inner--level">Level {{ $stadium["level"] }}</span>
                                    <button class="ek--stadium--right--inner--buy">
                                        <span>{{ $price }}</span>
                                        <svg class="icon icon-ek--budget">
                                            <use xlink:href="static/img/icons.svg#icon-ek--budget"></use>
                                        </svg>
                                    </button>
                                    <span class="ek--stadium--right--inner--required">Required<br />Level {{ $stadium["level"]-1 }}</span>

                                    @if($loop->index != 0)
                                        @if(Session::get("stadiums")[$loop->index-1]["selected"]==true)
                                        <!-- Modals -->
                                        <div class="ek--stadium--right--modal">
                                            <div class="ek--stadium--right--modal--body">
                                                <div class="ek--stadium--modal--close">
                                                    <svg class="icon icon-ek--close--icon">
                                                        <use xlink:href="static/img/icons.svg#icon-ek--close--icon"></use>
                                                    </svg>
                                                </div>

                                                <div class="ek--stadium--modal">
                                                    <div class="ek--stadium--modal--top">
                                                        <h3 class="ek--stadium--modal--title">
                                                            {{ $stadium["name"] }}
                                                        </h3>
                                                        <div class="ek--stadium--modal--level">
                                                            Level <span>{{$stadium["level"]}}</span>
                                                        </div>
                                                    </div>

                                                    <div class="ek--stadium--modal--center">
                                                        <div class="ek--stadium--modal--shadow">
                                                            <div class="ek--stadium--modal--image">
                                                                <img src="{{ url("/static/img/Stadium/stadium-".$stadium["level"].".svg") }}" alt="">
                                                                <div class="ek--stadium--modal--opacity"></div>
                                                                <div class="ek--stadium--modal--lock">
                                                                    <svg class="icon icon-lock2">
                                                                        <use xlink:href="static/img/icons.svg#icon-lock2"></use>
                                                                    </svg>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="ek--stadium--modal--bottom">
                                                        <div class="ek--stadium--left--bottom">
                                                            <div class="ek--stadium--left--bottom--info">
                                                                <svg class="icon icon-full-icon">
                                                                    <use xlink:href="static/img/icons.svg#icon-full-icon"></use>
                                                                </svg>
                                                                <span>Capacity</span>
                                                            </div>

                                                            <div class="ek--stadium--left--bottom--capacity">
                                                                {{ $stadium["capacity"] }}
                                                            </div>
                                                        </div>
                                                        <div class="ek--stadium--left--bottom">
                                                            <div class="ek--stadium--left--bottom--info">
                                                                <svg class="icon icon-ek--budget">
                                                                    <use xlink:href="static/img/icons.svg#icon-ek--budget"></use>
                                                                </svg>
                                                                <span>Cost</span>
                                                            </div>

                                                            <div class="ek--stadium--left--bottom--capacity">
                                                                {{ $price }}
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <button class="ek--stadium--modal--button">
                                                        Upgrade stadium
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="ek--stadium--right--upgrade">
                                            <div class="ek--stadium--right--upgrade--body">
                                                <div class="ek--stadium--upgrade">
                                                    <div class="ek--stadium--upgrade--container">
                                                        <div class="ek--stadium--upgrade--left">
                                                            <svg class="icon icon-stadium">
                                                                <use xlink:href="static/img/icons.svg#icon-stadium"></use>
                                                            </svg>
                                                        </div>

                                                        <div class="ek--stadium--upgrade--right">
                                                            @if(Session::get("finance")["money"] >= $stadium["price"])
                                                                <h4 class="ek--stadium--upgrade--title">
                                                                        Upgrade Stadium
                                                                    </h4>
                                                                    <p class="ek--stadium--upgrade--info">
                                                                        Do you want to upgrade
                                                                        <strong>Level <span>{{ $stadium["level"] }}</span>?</strong>
                                                                    </p>

                                                                    <div class="ek--stadium--upgrade--money">

                                                                        <span class="ek--stadium--upgrade--money--info">It will cost you</span>
                                                                        <span class="ek--stadium--upgrade--money--amount"><span>{{ $price }}</span>
                                                                    <svg class="icon icon-ek--budget">
                                                                      <use xlink:href="static/img/icons.svg#icon-ek--budget"></use>
                                                                    </svg></span>
                                                                    </div>

                                                                    <div class="ek--stadium--upgrade--buttons">
                                                                        <button class="ek--stadium--upgrade--cancel">
                                                                            Cancel
                                                                        </button>
                                                                        <button class="ek--stadium--upgrade--accept" data-id="{{ $stadium["level"] }}">
                                                                        Accept
                                                                    </button>
                                                                </div>
                                                            @else
                                                                <p class="text--navy--100 ek--size--18">Your money is not enough for upgrade</p>
                                                                <div class="ek--stadium--upgrade--buttons">
                                                                    <button class="ek--stadium--upgrade--cancel">
                                                                        Cancel
                                                                    </button>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- End Modals -->
                                        @endif
                                    @endif

                                </div>
                            </div>
                            @endforeach
                        </div>

                        <div class="ek--stadium--right--statistics ek--stadium--right--table">
                              <span class="ek--stadium--right--statistics--inner bg--navy--600">
                                <span class="ek--stadium--right--statistics--info">Total Attendance</span>
                                <span class="ek--stadium--right--statistics--amount">
                                  <span>{{ Session::get("stats")["totalAttendance"] }}</span>
                                </span>
                              </span>
                            @foreach( Session::get("stats") as $key=>$value )
                                @if($key != "totalAttendance")
                                @php
                                    if( $value < 1000 ){
                                        $price = $value;
                                    }else if($value >= 1000 && $value < 1000000){
                                        $price = number_format($value/1000,1)."K";
                                    }else if($value >= 1000000 && $value < 1000000000){
                                        $price = number_format($value/1000000,1)."M";
                                    }
                                    else if($value >= 1000000000){
                                        $price = number_format($value/1000000000,1)."B";
                                    }

                                @endphp
                                <span class="ek--stadium--right--statistics--inner bg--navy--600">
                                    <span class="ek--stadium--right--statistics--info">{{ ucwords(implode(' ', preg_split('/(?=[A-Z])/', $key))) }}</span>
                                    <span class="ek--stadium--right--statistics--amount">
                                      <span>{{ $price }}</span>
                                      <svg class="icon icon-ek--budget">
                                        <use xlink:href="static/img/icons.svg#icon-ek--budget"></use>
                                      </svg>
                                    </span>
                                </span>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End EK Stadion -->
@endsection


@section("javascript")
    <script src="{{ url('static/js/stadium.js') }}"></script>
@endsection
