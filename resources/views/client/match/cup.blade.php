@extends('client.layout.authenticated')
@section('content')
    <!-- Ek Challenge -->
    <div class="ek--challenge">
        <!-- Ek Challenge Body -->
        <div class="ek--challenge--body ek--cup">
            <!-- Ek Challenge Body Head -->
            <div class="ek--challenge--body--head bg--navy--700">
                <h3 class="text--white ek--size--20">Cup</h3>
            </div>
            <!-- End Ek Challenge Body Head -->
            <!-- Ek Challenge Body Tabs -->
            <div class="ek--challenge--tabs bg--navy--700">
                <!-- Ek Challenge Body Tabs Head -->
                <div class="ek--challenge--tabs--head">
                    <!-- Ek Challenge Body Tabs Button -->
                    <button id="ek--challenge--tabs--eight" class="ek--challenge--tabs--button button--active">1/8</button>
                    <button id="ek--challenge--tabs--four" class="ek--challenge--tabs--button">1/4</button>
                    <button id="ek--challenge--tabs--semi" class="ek--challenge--tabs--button">Semi final</button>
                    <button id="ek--challenge--tabs--final" class="ek--challenge--tabs--button">Final</button>
                    <!-- End Ek Challenge Body Tabs Button -->
                </div>
                <!-- End Ek Challenge Body Tabs Head -->

                <!-- Ek Challenge Body Tabs Content -->
                <div class="ek--challenge--tabs--content">
                    <!-- Ek Challenge Body Tabs Table Eight -->
                    <div class="ek--challenge--tabs--eight ek--challenge--tabs--table">
                        <!-- Ek Challenge Body Tabs Table Ordinal -->
                        <div class="ek--challenge--tabs--ordinal">
                            <!-- Ek Challenge Body Tabs Table Ordinal League -->
                            <h3 class="text--white ek--size--16" th:if="${session.size > 0}">1st League</h3>
                            <!-- End Ek Challenge Body Tabs Table Ordinal League -->
                            <!-- Ek Challenge Body Tabs Inner -->


                            <div class="ek--league--tab--inner bg--navy--600" th:each="cup:${session.cup}" th:if="${cup.round==1}">
                                <!-- Ek League Tab Inner First Win -->
                                <div class="ek--league--tab--inner--first text--white ek--size-16" th:if="${ cup.homeTeamScore >= cup.awayTeamScore}" th:text="${cup.homeTeam}">Karabakh</div>
                                <div class="ek--league--tab--inner--first text--navy--200 ek--size-16" th:if="${cup.homeTeamScore < cup.awayTeamScore}" th:text="${cup.homeTeam}"></div>
                                <!-- End Ek League Tab Inner First Win -->
                                <!-- End Ek League Tab Inner Soccer -->
                                <div class="ek--league--tab--inner--soccer">
                                    <!-- End Ek League Tab Inner Soccer Cup -->
                                    <div class="ek--league--tab--inner--soccer--cup" ></div>
                                    <div class="ek--league--tab--inner--soccer--result text--white ek--size--16" ><span class="text--white ek--size--16" th:text="${cup.homeTeamScore}">3</span><span class="text--white ek--size--16">-</span><span class="text--white ek--size--16" th:text="${cup.awayTeamScore}">2</span></div>
                                    <div class="ek--league--tab--inner--soccer--cup"></div>
                                    <!-- End End Ek League Tab Inner Soccer Cup -->
                                </div>

                                <!-- End End Ek League Tab Inner Soccer -->
                                <div class="ek--league--tab--inner--second text--white ek--size--16" th:if="${cup.homeTeamScore <= cup.awayTeamScore}" th:text="${cup.awayTeam}">Manchester Unt</div>
                                <div class="ek--league--tab--inner--second text--navy--200 ek--size--16" th:if="${ cup.homeTeamScore > cup.awayTeamScore}" th:text="${cup.awayTeam}"></div>
                                <!-- End Ek League Tab Inner First Win -->
                            </div>

                            <!-- End Ek Challenge Body Tabs Inner -->

                        </div>
                        <!-- End Ek Challenge Body Tabs Table Ordinal -->
                        <!-- Ek Challenge Body Tabs Table Ordinal -->
                        <div class="ek--challenge--tabs--ordinal">
                            <!-- Ek Challenge Body Tabs Table Ordinal League -->
                            <h3 class="text--white ek--size--16" th:if="${session.size > 8}">2nd League</h3>
                            <!-- End Ek Challenge Body Tabs Table Ordinal League -->
                            <!-- Ek Challenge Body Tabs Inner -->
                            <div class="ek--league--tab--inner bg--navy--600" th:each="cup:${session.cup}" th:if="${cup.round==2}">
                                <!-- Ek League Tab Inner First Win -->
                                <div class="ek--league--tab--inner--first text--white ek--size-16" th:if="${ cup.homeTeamScore >= cup.awayTeamScore}" th:text="${cup.homeTeam}">Karabakh</div>
                                <div class="ek--league--tab--inner--first text--navy--200 ek--size-16" th:if="${cup.homeTeamScore < cup.awayTeamScore}" th:text="${cup.homeTeam}"></div>
                                <!-- End Ek League Tab Inner First Win -->
                                <!-- End Ek League Tab Inner Soccer -->
                                <div class="ek--league--tab--inner--soccer">
                                    <!-- End Ek League Tab Inner Soccer Cup -->
                                    <div class="ek--league--tab--inner--soccer--cup" ></div>
                                    <div class="ek--league--tab--inner--soccer--result text--white ek--size--16" ><span class="text--white ek--size--16" th:text="${cup.homeTeamScore}">3</span><span class="text--white ek--size--16">-</span><span class="text--white ek--size--16" th:text="${cup.awayTeamScore}">2</span></div>
                                    <div class="ek--league--tab--inner--soccer--cup"></div>
                                    <!-- End End Ek League Tab Inner Soccer Cup -->
                                </div>

                                <!-- End End Ek League Tab Inner Soccer -->
                                <div class="ek--league--tab--inner--second text--white ek--size--16" th:if="${cup.homeTeamScore <= cup.awayTeamScore}" th:text="${cup.awayTeam}">Manchester Unt</div>
                                <div class="ek--league--tab--inner--second text--navy--200 ek--size--16" th:if="${ cup.homeTeamScore > cup.awayTeamScore}" th:text="${cup.awayTeam}"></div>
                                <!-- End Ek League Tab Inner First Win -->
                            </div>


                        </div>
                        <!-- End Ek Challenge Body Tabs Table Ordinal -->
                    </div>
                    <!-- End Ek Challenge Body Tabs Table Eight -->
                    <!-- Ek Challenge Body Tabs Table Four -->
                    <div class="ek--challenge--tabs--four ek--challenge--tabs--table">
                        <!-- Ek Challenge Body Tabs Table Ordinal -->
                        <div class="ek--challenge--tabs--ordinal">
                                <!-- Ek Challenge Body Tabs Table Ordinal League -->
                                <h3 class="text--white ek--size--16" th:if="${session.size > 16}">1st League</h3>
                                <!-- End Ek Challenge Body Tabs Table Ordinal League -->
                                <!-- Ek Challenge Body Tabs Inner -->
                            <div class="ek--league--tab--inner bg--navy--600" th:each="cup:${session.cup}" th:if="${cup.round==3}">
                                <!-- Ek League Tab Inner First Win -->
                                <div class="ek--league--tab--inner--first text--white ek--size-16" th:if="${cup.homeTeamScore >= cup.awayTeamScore}" th:text="${cup.homeTeam}">Karabakh</div>
                                <div class="ek--league--tab--inner--first text--navy--200 ek--size-16" th:if="${cup.homeTeamScore < cup.awayTeamScore}" th:text="${cup.homeTeam}"></div>
                                <!-- End Ek League Tab Inner First Win -->
                                <!-- End Ek League Tab Inner Soccer -->
                                <div class="ek--league--tab--inner--soccer">
                                    <!-- End Ek League Tab Inner Soccer Cup -->
                                    <div class="ek--league--tab--inner--soccer--cup" ></div>
                                    <div class="ek--league--tab--inner--soccer--result text--white ek--size--16" ><span class="text--white ek--size--16" th:text="${cup.homeTeamScore}">3</span><span class="text--white ek--size--16">-</span><span class="text--white ek--size--16" th:text="${cup.awayTeamScore}">2</span></div>
                                    <div class="ek--league--tab--inner--soccer--cup"></div>
                                    <!-- End End Ek League Tab Inner Soccer Cup -->
                                </div>

                                <!-- End End Ek League Tab Inner Soccer -->
                                <div class="ek--league--tab--inner--second text--white ek--size--16" th:if="${cup.homeTeamScore <= cup.awayTeamScore}" th:text="${cup.awayTeam}">Manchester Unt</div>
                                <div class="ek--league--tab--inner--second text--navy--200 ek--size--16" th:if="${ cup.homeTeamScore > cup.awayTeamScore}" th:text="${cup.awayTeam}"></div>
                                <!-- End Ek League Tab Inner First Win -->
                            </div>

                        </div>
                        <!-- End Ek Challenge Body Tabs Table Ordinal -->
                        <!-- Ek Challenge Body Tabs Table Ordinal -->
                        <div class="ek--challenge--tabs--ordinal">
                            <!-- Ek Challenge Body Tabs Table Ordinal League -->
                            <h3 class="text--white ek--size--16" th:if="${session.size > 24}">2nd League</h3>
                            <!-- End Ek Challenge Body Tabs Table Ordinal League -->
                            <!-- Ek Challenge Body Tabs Inner -->
                            <div class="ek--league--tab--inner bg--navy--600" th:each="cup: ${session.cup}" th:if="${cup.round==4}">
                                <!-- Ek League Tab Inner First Win -->
                                <div class="ek--league--tab--inner--first text--white ek--size-16" th:if="${cup.homeTeamScore >= cup.awayTeamScore}" th:text="${cup.homeTeam}">Karabakh</div>
                                <div class="ek--league--tab--inner--first text--navy--200 ek--size-16" th:if="${cup.homeTeamScore < cup.awayTeamScore}" th:text="${cup.homeTeam}"></div>
                                <!-- End Ek League Tab Inner First Win -->
                                <!-- End Ek League Tab Inner Soccer -->
                                <div class="ek--league--tab--inner--soccer">
                                    <!-- End Ek League Tab Inner Soccer Cup -->
                                    <div class="ek--league--tab--inner--soccer--cup" ></div>
                                    <div class="ek--league--tab--inner--soccer--result text--white ek--size--16" ><span class="text--white ek--size--16" th:text="${cup.homeTeamScore}">3</span>
                                        <span class="text--white ek--size--16">-</span><span class="text--white ek--size--16" th:text="${cup.awayTeamScore}">2</span></div>
                                    <div class="ek--league--tab--inner--soccer--cup"></div>
                                    <!-- End End Ek League Tab Inner Soccer Cup -->
                                </div>

                                <!-- End End Ek League Tab Inner Soccer -->
                                <div class="ek--league--tab--inner--second text--white ek--size--16" th:if="${cup.homeTeamScore <= cup.awayTeamScore}" th:text="${cup.awayTeam}">Manchester Unt</div>
                                <div class="ek--league--tab--inner--second text--navy--200 ek--size--16" th:if="${cup.homeTeamScore > cup.awayTeamScore}" th:text="${cup.awayTeam}"></div>
                                <!-- End Ek League Tab Inner First Win -->
                            </div>
>
                            <!-- End Ek Challenge Body Tabs Inner -->

                        </div>
                        <!-- End Ek Challenge Body Tabs Table Ordinal -->

                    <!-- End Ek Challenge Body Tabs Table Four -->
                    <!-- Ek Challenge Body Tabs Table Semi --></div>
                    <div class="ek--challenge--tabs--semi ek--challenge--tabs--table">
                        <!-- Ek Challenge Body Tabs Table Ordinal -->
                        <div class="ek--challenge--tabs--ordinal">
                            <!-- Ek Challenge Body Tabs Table Ordinal League -->
                            <h3 class="text--white ek--size--16" th:if="${session.size > 32}">1st League</h3>
                            <!-- End Ek Challenge Body Tabs Table Ordinal League -->
                            <!-- Ek Challenge Body Tabs Inner -->
                            <div class="ek--league--tab--inner bg--navy--600" th:each="cup:${session.cup}" th:if="${cup.round==5}">
                                <!-- Ek League Tab Inner First Win -->
                                <div class="ek--league--tab--inner--first text--white ek--size-16" th:if="${ cup.homeTeamScore >= cup.awayTeamScore}" th:text="${cup.homeTeam}">Karabakh</div>
                                <div class="ek--league--tab--inner--first text--navy--200 ek--size-16" th:if="${cup.homeTeamScore < cup.awayTeamScore}" th:text="${cup.homeTeam}"></div>
                                <!-- End Ek League Tab Inner First Win -->
                                <!-- End Ek League Tab Inner Soccer -->
                                <div class="ek--league--tab--inner--soccer">
                                    <!-- End Ek League Tab Inner Soccer Cup -->
                                    <div class="ek--league--tab--inner--soccer--cup" ></div>
                                    <div class="ek--league--tab--inner--soccer--result text--white ek--size--16" ><span class="text--white ek--size--16" th:text="${cup.homeTeamScore}">3</span><span class="text--white ek--size--16">-</span><span class="text--white ek--size--16" th:text="${cup.awayTeamScore}">2</span></div>
                                    <div class="ek--league--tab--inner--soccer--cup"></div>
                                    <!-- End End Ek League Tab Inner Soccer Cup -->
                                </div>

                                <!-- End End Ek League Tab Inner Soccer -->
                                <div class="ek--league--tab--inner--second text--white ek--size--16" th:if="${cup.homeTeamScore <= cup.awayTeamScore}" th:text="${cup.awayTeam}">Manchester Unt</div>
                                <div class="ek--league--tab--inner--second text--navy--200 ek--size--16" th:if="${ cup.homeTeamScore > cup.awayTeamScore}" th:text="${cup.awayTeam}"></div>
                                <!-- End Ek League Tab Inner First Win -->
                            </div>

                        </div>
                        <!-- End Ek Challenge Body Tabs Table Ordinal -->
                        <!-- Ek Challenge Body Tabs Table Ordinal -->
                        <div class="ek--challenge--tabs--ordinal">
                            <!-- Ek Challenge Body Tabs Table Ordinal League -->
                            <h3 class="text--white ek--size--16" th:if="${session.size > 40}">2nd League</h3>
                            <!-- End Ek Challenge Body Tabs Table Ordinal League -->
                            <!-- Ek Challenge Body Tabs Inner -->
                            <div class="ek--league--tab--inner bg--navy--600" th:each="cup:${session.cup}" th:if="${cup.round==6}">
                                <!-- Ek League Tab Inner First Win -->
                                <div class="ek--league--tab--inner--first text--white ek--size-16" th:if="${ cup.homeTeamScore >= cup.awayTeamScore}" th:text="${cup.homeTeam}">Karabakh</div>
                                <div class="ek--league--tab--inner--first text--navy--200 ek--size-16" th:if="${cup.homeTeamScore < cup.awayTeamScore}" th:text="${cup.homeTeam}"></div>
                                <!-- End Ek League Tab Inner First Win -->
                                <!-- End Ek League Tab Inner Soccer -->
                                <div class="ek--league--tab--inner--soccer">
                                    <!-- End Ek League Tab Inner Soccer Cup -->
                                    <div class="ek--league--tab--inner--soccer--cup" ></div>
                                    <div class="ek--league--tab--inner--soccer--result text--white ek--size--16" ><span class="text--white ek--size--16" th:text="${cup.homeTeamScore}">3</span><span class="text--white ek--size--16">-</span><span class="text--white ek--size--16" th:text="${cup.awayTeamScore}">2</span></div>
                                    <div class="ek--league--tab--inner--soccer--cup"></div>
                                    <!-- End End Ek League Tab Inner Soccer Cup -->
                                </div>

                                <!-- End End Ek League Tab Inner Soccer -->
                                <div class="ek--league--tab--inner--second text--white ek--size--16" th:if="${cup.homeTeamScore <= cup.awayTeamScore}" th:text="${cup.awayTeam}">Manchester Unt</div>
                                <div class="ek--league--tab--inner--second text--navy--200 ek--size--16" th:if="${ cup.homeTeamScore > cup.awayTeamScore}" th:text="${cup.awayTeam}"></div>
                                <!-- End Ek League Tab Inner First Win -->
                            </div>

                        </div>
                        <!-- End Ek Challenge Body Tabs Table Ordinal -->
                    </div>
                    <!-- End Ek Challenge Body Tabs Table Semi -->
                    <!-- Ek Challenge Body Tabs Table Final -->
                    <div class="ek--challenge--tabs--final ek--challenge--tabs--table">
                        <!-- Ek Challenge Body Tabs Table Ordinal -->
                        <div class="ek--challenge--tabs--ordinal">
                                <!-- Ek Challenge Body Tabs Table Ordinal League -->
                                <h3 class="text--white ek--size--16" th:if="${session.size > 48}">1st League</h3>
                                <!-- End Ek Challenge Body Tabs Table Ordinal League -->
                                <!-- Ek Challenge Body Tabs Inner -->
                            <div class="ek--league--tab--inner bg--navy--600" th:each="cup:${session.cup}" th:if="${cup.round==7}">
                                <!-- Ek League Tab Inner First Win -->
                                <div class="ek--league--tab--inner--first text--white ek--size-16" th:if="${ cup.homeTeamScore >= cup.awayTeamScore}" th:text="${cup.homeTeam}">Karabakh</div>
                                <div class="ek--league--tab--inner--first text--navy--200 ek--size-16" th:if="${cup.homeTeamScore < cup.awayTeamScore}" th:text="${cup.homeTeam}"></div>
                                <!-- End Ek League Tab Inner First Win -->
                                <!-- End Ek League Tab Inner Soccer -->
                                <div class="ek--league--tab--inner--soccer">
                                    <!-- End Ek League Tab Inner Soccer Cup -->
                                    <div class="ek--league--tab--inner--soccer--cup" ></div>
                                    <div class="ek--league--tab--inner--soccer--result text--white ek--size--16" ><span class="text--white ek--size--16" th:text="${cup.homeTeamScore}">3</span><span class="text--white ek--size--16">-</span><span class="text--white ek--size--16" th:text="${cup.awayTeamScore}">2</span></div>
                                    <div class="ek--league--tab--inner--soccer--cup"></div>
                                    <!-- End End Ek League Tab Inner Soccer Cup -->
                                </div>

                                <!-- End End Ek League Tab Inner Soccer -->
                                <div class="ek--league--tab--inner--second text--white ek--size--16" th:if="${cup.homeTeamScore <= cup.awayTeamScore}" th:text="${cup.awayTeam}">Manchester Unt</div>
                                <div class="ek--league--tab--inner--second text--navy--200 ek--size--16" th:if="${ cup.homeTeamScore > cup.awayTeamScore}" th:text="${cup.awayTeam}"></div>
                                <!-- End Ek League Tab Inner First Win -->
                            </div>
                            </div>
                            <!-- End Ek Challenge Body Tabs Table Ordinal -->
                            <!-- Ek Challenge Body Tabs Table Ordinal -->
                            <!--<div class="ek&#45;&#45;challenge&#45;&#45;tabs&#45;&#45;ordinal">-->
                                <!--&lt;!&ndash; Ek Challenge Body Tabs Table Ordinal League &ndash;&gt;-->
                                <!--<h3 class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16">2nd League</h3>-->
                                <!--&lt;!&ndash; End Ek Challenge Body Tabs Table Ordinal League &ndash;&gt;-->
                                <!--&lt;!&ndash; Ek Challenge Body Tabs Inner &ndash;&gt;-->
                                <!--<div class="ek&#45;&#45;challenge&#45;&#45;tabs&#45;&#45;inner bg&#45;&#45;navy&#45;&#45;600">-->
                                    <!--&lt;!&ndash; Ek Challenge Body Tabs Inner Left &ndash;&gt;-->
                                    <!--<div class="ek&#45;&#45;challenge&#45;&#45;tabs&#45;&#45;inner&#45;&#45;left ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">As Monaco FC</div>-->
                                    <!--&lt;!&ndash; Ek Challenge Body Tabs Inner Left &ndash;&gt;-->
                                    <!--&lt;!&ndash; Ek Challenge Body Tabs Inner Center &ndash;&gt;-->
                                    <!--<div class="ek&#45;&#45;challenge&#45;&#45;tabs&#45;&#45;inner&#45;&#45;center ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">vs</div>-->
                                    <!--&lt;!&ndash; End Ek Challenge Body Tabs Inner Center &ndash;&gt;-->
                                    <!--&lt;!&ndash; Ek Challenge Body Tabs Inner Right &ndash;&gt;-->
                                    <!--<div class="ek&#45;&#45;challenge&#45;&#45;tabs&#45;&#45;inner&#45;&#45;right ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">Karabakh</div>-->
                                    <!--&lt;!&ndash; End Ek Challenge Body Tabs Inner Right &ndash;&gt;-->
                                <!--</div>-->
                                <!--&lt;!&ndash; End Ek Challenge Body Tabs Inner &ndash;&gt;-->
                            <!--</div>-->
                            <!-- End Ek Challenge Body Tabs Table Ordinal -->
                    </div>
                    <!-- End Ek Challenge Body Tabs Table Final -->
                </div>
                <!-- End Ek Challenge Body Tabs Content -->
            </div>
            <!-- End Ek Challenge Body Tabs -->
        </div>
        <!-- End Ek Challenge Body -->
    </div>

@endsection
