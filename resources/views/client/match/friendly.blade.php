@extends('client.layout.authenticated')
@section('content')
    <!-- Ek Challenge -->
    <section class="ek--challenge">
        <!-- Ek Challenge Body -->
        <div class="ek--challenge--body">
            <!-- Ek Challenge Body Head -->
            <div class="ek--challenge--body--head bg--navy--700">
                <h3 class="text--white ek--size--20">Friendly</h3>
            </div>
            <!-- End Ek Challenge Body Head -->
            <!-- Ek Challenge Body Tabs -->
            <div class="ek--challenge--tabs bg--navy--700">
                <!-- Ek Challenge Body Tabs Head -->
                <div class="ek--challenge--tabs--head ek--challenge--tabs--friendly--head">
                    <!-- Ek Challenge Body Tabs Button -->
                    <button id="ek--challenge--tabs--semi" class="ek--challenge--tabs--button  button--active">Arrange</button>
                    <button id="ek--challenge--tabs--eight" class="ek--challenge--tabs--button ">Fixture</button>
                    <button id="ek--challenge--tabs--four" class="ek--challenge--tabs--button">Results</button>

                    <!-- End Ek Challenge Body Tabs Button -->
                </div>
                <!-- End Ek Challenge Body Tabs Head -->

                <!-- Ek Challenge Body Tabs Content -->
                <div class="ek--challenge--tabs--content">
                    <!-- Ek Challenge Body Tabs Table Eight -->
                    <div class="ek--challenge--tabs--eight ek--challenge--tabs--table" style="display: none;">
                        <!-- Ek Challenge Body Tabs Table Ordinal -->
                        <div id="fixture" class="ek--challenge--tabs--ordinal">


                            <!-- Ek Challenge Body Tabs Inner -->
                            <div  class="ek--challenge--tabs--inner bg--navy--600" th:each="fixture : ${session.friendlyResultsAndFixtures}" th:if="${fixture.played == false}">
                                <!-- Ek Challenge Body Tabs Inner Left -->
                                <div th:classappend="${fixture.homeClubId == session.club.id} ? 'text--cyan' : 'text--white' " class="ek--challenge--tabs--inner--left ek--size--16" th:text="${fixture.homeTeam}"></div>
                                <!-- Ek Challenge Body Tabs Inner Left -->
                                <!-- Ek Challenge Body Tabs Inner Center -->
                                <div class="ek--challenge--tabs--inner--center ek--size--16 text--navy--200">vs</div>
                                <!-- End Ek Challenge Body Tabs Inner Center -->
                                <!-- Ek Challenge Body Tabs Inner Right -->
                                <div th:classappend="${fixture.awayClubId == session.club.id} ? 'text--cyan' : 'text--white' " class="ek--challenge--tabs--inner--right ek--size--16" th:text="${fixture.awayTeam}"></div>
                                <!-- End Ek Challenge Body Tabs Inner Right -->
                            </div>
                            <!-- End Ek Challenge Body Tabs Inner -->

                        </div>
                        <!-- End Ek Challenge Body Tabs Table Ordinal -->
                    </div>
                    <!-- End Ek Challenge Body Tabs Table Eight -->
                    <!-- Ek Challenge Body Tabs Table Four -->
                    <div class="ek--challenge--tabs--four ek--challenge--tabs--table">
                        <!-- Ek Challenge Body Tabs Table Ordinal -->
                        <div class="ek--challenge--tabs--ordinal">

                                <!-- Ek Challenge Body Tabs Inner -->
                                @foreach(Session::get("friendly_matches") as $match)
                                    @if($match["played"])
                                        <div class="ek--challenge--tabs--inner bg--navy--600">
                                            <!-- Ek Challenge Body Tabs Inner Left -->
                                            <div class="ek--challenge--tabs--inner--left ek--size--16 @if($match["homeTeam"] == Session::get("club")["clubName"])text--yellow @else text--white @endif" th:text="${result.homeTeam}">{{ $match["homeTeam"] }}</div>
                                            <!-- Ek Challenge Body Tabs Inner Left -->
                                            <!-- Ek Challenge Body Tabs Inner Center -->
                                            <div class="ek--challenge--tabs--inner--center ek--size--16 text--navy--200">
                                                <div class="ek--challenge--tabs--inner--center--win">
                                                    @if($match["homeScore"] > $match["awayScore"])
                                                        <svg class="icon icon-ek--cup" th:if="${result.homeTeamScore> result.awayTeamScore}"><use xlink:href="static/img/icons.svg#icon-ek--cup"></use></svg>
                                                    @endif
                                                </div>
                                                <div class="ek--challenge--tabs--inner--center--soccar text--white">
                                                    <span class="ek--challenge--tabs--inner--center--soccar text--white" th:text="${result.homeTeamScore}">{{ $match["homeScore"] }}</span>
                                                    <span class="ek--challenge--tabs--inner--center--soccar text--white">-</span>
                                                    <span class="ek--challenge--tabs--inner--center--soccar text--white" th:text="${result.awayTeamScore}">{{ $match["awayScore"] }}</span></div>
                                                <div class="ek--challenge--tabs--inner--center--win">
                                                    @if($match["homeScore"] < $match["awayScore"])
                                                        <svg class="icon icon-ek--cup" th:if="${result.homeTeamScore< result.awayTeamScore}"><use xlink:href="static/img/icons.svg#icon-ek--cup"></use></svg>
                                                    @endif
                                                </div>
                                            </div>
                                            <!-- End Ek Challenge Body Tabs Inner Center -->
                                            <!-- Ek Challenge Body Tabs Inner Right -->
                                            <div class="ek--challenge--tabs--inner--right ek--size--16 @if($match["awayTeam"] == Session::get("club")["clubName"])text--yellow @else text--white @endif" th:text="${result.awayTeam}">{{ $match["awayTeam"] }}</div>
                                            <!-- End Ek Challenge Body Tabs Inner Right -->
                                        </div>
                                    @endif
                                @endforeach
                                <!-- End Ek Challenge Body Tabs Inner -->

                        </div>
                        <!-- End Ek Challenge Body Tabs Table Ordinal -->
                    </div>
                    <!-- End Ek Challenge Body Tabs Table Four -->
                    <!-- Ek Challenge Body Tabs Table Semi -->
                    <div class="ek--challenge--tabs--semi ek--challenge--tabs--table" style="display: block !important;">
                        <!-- Ek Friendly Arrange -->
                        <div class="ek--challenge--friendly--arrange" >
                            <!-- Ek Friendly Arrange Head -->
                            <div class="ek--challenge--friendly--arrange--head">
                                <h3 class="text--white ek--size--20">Arrange a friendly matches</h3>
                                <p class="text--navy--100 ek--size--18">By choosing the league we will find the best friendly club for you.</p>
                            </div>
                            <!-- End Ek Friendly Arrange Head -->
                            <!-- Ek Friendly Arrange Form -->
                            <form class="ek--challenge--friendly--arrange--select" >
                                <!-- Ek Friendly Arrange Form Input -->
                                <span class="ek--challenge--friendly--arrange--arrow">
                                    <label for="ek--challenge--friendly--arrange--input">Opponent Level</label>
                                    <input type="text" class="ek--challenge--friendly--arrange--input bg--navy--800" value="" placeholder="Choose Opponent Level" readonly>
                                </span>
                                <!-- End Ek Friendly Arrange Form Input -->
                                <!-- Ek Friendly Arrange Form Content -->
                                <div class="ek--challenge--friendly--arrange--content bg--navy--800">
                                    <!-- Ek Friendly Arrange Form Content Items -->
                                    <ul class="ek--challenge--friendly--arrange--list">
                                        <li class="ek--challenge--friendly--arrange--item" id="random" data-id="1">
                                            <span class="ek--challenge--friendly--arrange--league text--white ek--size--16"  name="level" >League 1</span>
                                            <p class="ek--challenge--friendly--arrange--power text--navy--200"><small>Power</small> <strong class="ek--size--16 text--white">85-100</strong></p>
                                        </li>
                                        <li class="ek--challenge--friendly--arrange--item"  id="random" data-id="2">
                                            <span class="ek--challenge--friendly--arrange--league text--white ek--size--16"  name="level" >League 2</span>
                                            <p class="ek--challenge--friendly--arrange--power text--navy--200"><small>Power</small> <strong class="ek--size--16 text--white">65-85</strong></p>
                                        </li>
                                        <li class="ek--challenge--friendly--arrange--item"  id="random" data-id="3">
                                            <span class="ek--challenge--friendly--arrange--league text--white ek--size--16"  name="level">League 3</span>
                                            <p class="ek--challenge--friendly--arrange--power text--navy--200"><small>Power</small> <strong class="ek--size--16 text--white">50-75</strong></p>
                                        </li>
                                        <li class="ek--challenge--friendly--arrange--item"  id="random" data-id="4">
                                            <span class="ek--challenge--friendly--arrange--league text--white ek--size--16"  name="level" >League 4</span>
                                            <p class="ek--challenge--friendly--arrange--power text--navy--200"><small>Power</small> <strong class="ek--size--16 text--white">35-65</strong></p>
                                        </li>
                                        <li class="ek--challenge--friendly--arrange--item"  id="random" data-id="5">
                                            <span class="ek--challenge--friendly--arrange--league text--white ek--size--16"  name="level" >League 5</span>
                                            <p class="ek--challenge--friendly--arrange--power text--navy--200"><small>Power</small> <strong class="ek--size--16 text--white">20-50</strong></p>
                                        </li>
                                    </ul>
                                    <!-- Endz Ek Friendly Arrange Form Content Items -->
                                </div>
                                <!-- End Ek Friendly Arrange Form Content -->
                                <!-- Ek Friendly Arrange Opponent -->
                                <div class="ek--challenge--friendly--opponent " id="randomClub">
                                    <!-- Ek Friendly Arrange Opponent Head -->
                                    <div class="ek--challenge--friendly--opponent--head bg--navy--600">
                                        <h3 class="text--navy--100 ek--size--16">Your opponent</h3>
                                        <div class="ek--challenge--friendly--opponent--power">
                                            <div class="ek--challenge--friendly--opponent--left">
                                                <div class="ek--challenge--friendly--opponent--power--image text--navy--900 ek--size--16--500" id="powerr"></div>
                                                <p class="ek--size--20 text--white" id="club"></p>
                                            </div>
                                            <div class="ek--challenge--friendly--opponent--right">
                                                <small class="text--navy--200">Power</small>
                                                <strong class="ek--size--16 text--white" id="power"></strong>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Ek Friendly Arrange Opponent Head -->

                                    <!-- Ek Friendly Arrange Opponent Buttons -->
                                    <div class="ek--challenge--friendly--opponent--buttons">
                                        <button type="submit" class="ek--challenge--friendly--opponent--decline text--white ek--size--16" id="removeButton"  >Decline</button>
                                        <button type="submit" class="ek--challenge--friendly--opponent--accept text--navy--900 bg--cyan ek--size--16--500" href="{{ route("friendlyMatch") }}" id="accept">Accept</button>
                                    </div>
                                    <!-- End Ek Friendly Arrange Opponent Buttons -->
                                </div>
                                <!-- End Ek Friendly Arrange Opponent -->
                            </form>
                            <!-- Ek Friendly Arrange Form -->
                        </div>
                        <!-- End Ek Friendly Arrange -->
                    </div>
                    <!-- End Ek Challenge Body Tabs Table Semi -->
                </div>
                <!-- End Ek Challenge Body Tabs Content -->
            </div>
            <!-- End Ek Challenge Body Tabs -->
        </div>
        <!-- End Ek Challenge Body -->
    </section>
    <!-- End Ek Challenge -->

@endsection
@section('javascript')
    <script src="{{url("static/js/friendly.js")}}"></script>
@endsection
