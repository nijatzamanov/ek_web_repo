@extends('client.layout.authenticated')
@section('content')

    <!-- Ek Challenge -->
    <div class="ek--challenge">
        <!-- Ek Challenge Body -->
        <div class="ek--challenge--body">
            <!-- Ek Challenge Body Head -->
            <div class="ek--challenge--body--head bg--navy--700">
                <h3 class="text--white ek--size--20">Tournament</h3>
            </div>
            @if(count(Session::get("challenge_matches")) != 0 && Session::get("challenge_matches") != null && Session::get("next_match")[0] != null)
                <!-- End Ek Challenge Body Head -->
                        <!-- Ek Challenge Body Tabs -->
                        <div  class="ek--challenge--tabs bg--navy--700">
                        <!-- Ek Challenge Body Tabs Head -->
                        <div class="ek--challenge--tabs--head">
                            <!-- Ek Challenge Body Tabs Button -->
                            <button id="ek--challenge--tabs--eight" class="ek--challenge--tabs--button button--active">1/8</button>
                            <button id="ek--challenge--tabs--four" class="ek--challenge--tabs--button">1/4</button>
                            <button id="ek--challenge--tabs--semi" class="ek--challenge--tabs--button">Semi final</button>
                            <button id="ek--challenge--tabs--final" class="ek--challenge--tabs--button">Final</button>
                            <!-- End Ek Challenge Body Tabs Button -->
                        </div>
                        <!-- End Ek Challenge Body Tabs Head -->

                        <!-- Ek Challenge Body Tabs Content -->
                        <div class="ek--challenge--tabs--content">
                            <!-- Ek Challenge Body Tabs Table Eight -->
                            <div class="ek--challenge--tabs--eight ek--challenge--tabs--table">
                                <!-- Ek Challenge Body Tabs Table Ordinal -->
                                <div class="ek--challenge--tabs--ordinal">
                                    <!-- Ek Challenge Body Tabs Table Ordinal League -->
                                    @if(count(Session::get("challenge_matches")) > 0)
                                        <h3 class="text--white ek--size--16">1st League</h3>
                                        @else
                                        <div class="ek--matches--empty bg--navy--700" >
                                            <div class="ek--matches--empty--content">
                                                <div class="ek--matches--empty--about">
                                                    <p class="text--navy--300">You haven't reach to 1/8 yet</p>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <!-- End Ek Challenge Body Tabs Table Ordinal League -->
                                    <!-- Ek Challenge Body Tabs Inner -->
                                    @foreach(Session::get("challenge_matches") as $match)
                                        @if($match["round"] == 1)
                                            <div class="ek--challenge--tabs--inner bg--navy--600">
                                                <!-- Ek Challenge Body Tabs Inner Left -->
                                                <div class="ek--challenge--tabs--inner--left ek--size--16 @if($match["homeClubName"] == Session::get("club")["clubName"])text--yellow @else text--white @endif">{{ $match["homeClubName"] }}</div>
                                                <!-- Ek Challenge Body Tabs Inner Left -->
                                                <!-- Ek Challenge Body Tabs Inner Center -->
                                                @if($match["played"] == true)
                                                    <div class="ek--challenge--tabs--inner--center ek--size--16 text--navy--200">{{ $match["homeScore"] }} - {{ $match["awayScore"] }}</div>
                                                @else
                                                    <div class="ek--challenge--tabs--inner--center ek--size--16 text--navy--200">vs</div>
                                                @endif
                                                <!-- End Ek Challenge Body Tabs Inner Center -->
                                                <!-- Ek Challenge Body Tabs Inner Right -->
                                                <div class="ek--challenge--tabs--inner--right ek--size--16 @if($match["awayClubName"] == Session::get("club")["clubName"])text--yellow @else text--white @endif">{{ $match["awayClubName"] }}</div>
                                                <!-- End Ek Challenge Body Tabs Inner Right -->

                                            </div>
                                        @endif
                                    @endforeach
                                    <!-- End Ek Challenge Body Tabs Inner -->

                                </div>
                                <!-- End Ek Challenge Body Tabs Table Ordinal -->
                                <!-- Ek Challenge Body Tabs Table Ordinal -->
                                <div class="ek--challenge--tabs--ordinal">
                                    <!-- Ek Challenge Body Tabs Table Ordinal League -->
                                    @if(count(Session::get("challenge_matches")) > 8)
                                        <h3 class="text--white ek--size--16">2nd League</h3>
                                    @endif
                                    <!-- End Ek Challenge Body Tabs Table Ordinal League -->
                                    <!-- Ek Challenge Body Tabs Inner -->
                                    @foreach(Session::get("challenge_matches") as $match)
                                        @if($match["round"] == 2)
                                            <div class="ek--challenge--tabs--inner bg--navy--600">
                                                <!-- Ek Challenge Body Tabs Inner Left -->
                                                <div class="ek--challenge--tabs--inner--left ek--size--16 @if($match["homeClubName"] == Session::get("club")["clubName"])text--yellow @else text--white @endif">{{ $match["homeClubName"] }}</div>
                                                <!-- Ek Challenge Body Tabs Inner Left -->
                                                <!-- Ek Challenge Body Tabs Inner Center -->
                                                @if($match["played"] == true)
                                                    <div class="ek--challenge--tabs--inner--center ek--size--16 text--navy--200">{{ $match["homeScore"] }} - {{ $match["awayScore"] }}</div>
                                                @else
                                                    <div class="ek--challenge--tabs--inner--center ek--size--16 text--navy--200">vs</div>
                                                @endif
                                                <!-- End Ek Challenge Body Tabs Inner Center -->
                                                <!-- Ek Challenge Body Tabs Inner Right -->
                                                <div class="ek--challenge--tabs--inner--right ek--size--16 @if($match["awayClubName"] == Session::get("club")["clubName"])text--yellow @else text--white @endif">{{ $match["awayClubName"] }}</div>
                                                <!-- End Ek Challenge Body Tabs Inner Right -->

                                            </div>
                                        @endif
                                    @endforeach
                                    <!-- End Ek Challenge Body Tabs Inner -->

                                </div>
                                <!-- End Ek Challenge Body Tabs Table Ordinal -->
                            </div>
                            <!-- End Ek Challenge Body Tabs Table Eight -->
                            <!-- Ek Challenge Body Tabs Table Four -->
                            <div class="ek--challenge--tabs--four ek--challenge--tabs--table">
                                <!-- Ek Challenge Body Tabs Table Ordinal -->
                                <div class="ek--challenge--tabs--ordinal">
                                        <!-- Ek Challenge Body Tabs Table Ordinal League -->
                                        @if(count(Session::get("challenge_matches")) > 16)
                                            <h3 class="text--white ek--size--16">1st League</h3>
                                            @else
                                            <div class="ek--matches--empty bg--navy--700" >
                                                <div class="ek--matches--empty--content">
                                                    <div class="ek--matches--empty--about">
                                                        <h3 class="text--white">You haven't reach to 1/4 yet</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        <!-- End Ek Challenge Body Tabs Table Ordinal League -->
                                        <!-- Ek Challenge Body Tabs Inner -->
                                        @foreach(Session::get("challenge_matches") as $match)
                                            @if($match["round"] == 3)
                                                <div class="ek--challenge--tabs--inner bg--navy--600">
                                                    <!-- Ek Challenge Body Tabs Inner Left -->
                                                    <div class="ek--challenge--tabs--inner--left ek--size--16 @if($match["homeClubName"] == Session::get("club")["clubName"])text--yellow @else text--white @endif">{{ $match["homeClubName"] }}</div>
                                                    <!-- Ek Challenge Body Tabs Inner Left -->
                                                    <!-- Ek Challenge Body Tabs Inner Center -->
                                                    @if($match["played"] == true)
                                                        <div class="ek--challenge--tabs--inner--center ek--size--16 text--navy--200">{{ $match["homeScore"] }} - {{ $match["awayScore"] }}</div>
                                                    @else
                                                        <div class="ek--challenge--tabs--inner--center ek--size--16 text--navy--200">vs</div>
                                                    @endif
                                                    <!-- End Ek Challenge Body Tabs Inner Center -->
                                                    <!-- Ek Challenge Body Tabs Inner Right -->
                                                    <div class="ek--challenge--tabs--inner--right ek--size--16 @if($match["awayClubName"] == Session::get("club")["clubName"])text--yellow @else text--white @endif">{{ $match["awayClubName"] }}</div>
                                                    <!-- End Ek Challenge Body Tabs Inner Right -->

                                                </div>
                                            @endif
                                        @endforeach
                                        <!-- End Ek Challenge Body Tabs Inner -->

                                </div>
                                <!-- End Ek Challenge Body Tabs Table Ordinal -->
                                <!-- Ek Challenge Body Tabs Table Ordinal -->
                                <div class="ek--challenge--tabs--ordinal">
                                    <!-- Ek Challenge Body Tabs Table Ordinal League -->
                                    @if(count(Session::get("challenge_matches")) > 20)
                                        <h3 class="text--white ek--size--16">2nd League</h3>
                                    @endif
                                    <!-- End Ek Challenge Body Tabs Table Ordinal League -->
                                    <!-- Ek Challenge Body Tabs Inner -->
                                    @foreach(Session::get("challenge_matches") as $match)
                                        @if($match["round"] == 4)
                                            <div class="ek--challenge--tabs--inner bg--navy--600">
                                                <!-- Ek Challenge Body Tabs Inner Left -->
                                                <div class="ek--challenge--tabs--inner--left ek--size--16 @if($match["homeClubName"] == Session::get("club")["clubName"])text--yellow @else text--white @endif">{{ $match["homeClubName"] }}</div>
                                                <!-- Ek Challenge Body Tabs Inner Left -->
                                                <!-- Ek Challenge Body Tabs Inner Center -->
                                                @if($match["played"] == true)
                                                    <div class="ek--challenge--tabs--inner--center ek--size--16 text--navy--200">{{ $match["homeScore"] }} - {{ $match["awayScore"] }}</div>
                                                @else
                                                    <div class="ek--challenge--tabs--inner--center ek--size--16 text--navy--200">vs</div>
                                                @endif
                                                <!-- End Ek Challenge Body Tabs Inner Center -->
                                                <!-- Ek Challenge Body Tabs Inner Right -->
                                                <div class="ek--challenge--tabs--inner--right ek--size--16 @if($match["awayClubName"] == Session::get("club")["clubName"])text--yellow @else text--white @endif">{{ $match["awayClubName"] }}</div>
                                                <!-- End Ek Challenge Body Tabs Inner Right -->

                                            </div>
                                        @endif
                                    @endforeach
                                    <!-- End Ek Challenge Body Tabs Inner -->

                                </div>
                                <!-- End Ek Challenge Body Tabs Table Ordinal -->
                            </div>
                            <!-- End Ek Challenge Body Tabs Table Four -->
                            <!-- Ek Challenge Body Tabs Table Semi -->
                            <div class="ek--challenge--tabs--semi ek--challenge--tabs--table">
                                <!-- Ek Challenge Body Tabs Table Ordinal -->
                                <div class="ek--challenge--tabs--ordinal">
                                    <!-- Ek Challenge Body Tabs Table Ordinal League -->
                                    @if(count(Session::get("challenge_matches")) > 24)
                                        <h3 class="text--white ek--size--16">1st League</h3>
                                        @else
                                        <div class="ek--matches--empty bg--navy--700" >
                                            <div class="ek--matches--empty--content">
                                                <div class="ek--matches--empty--about">
                                                    <h3 class="text--white">You haven't reach to Semi final yet</h3>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <!-- End Ek Challenge Body Tabs Table Ordinal League -->
                                    <!-- Ek Challenge Body Tabs Inner -->
                                    @foreach(Session::get("challenge_matches") as $match)
                                        @if($match["round"] == 5)
                                            <div class="ek--challenge--tabs--inner bg--navy--600">
                                                <!-- Ek Challenge Body Tabs Inner Left -->
                                                <div class="ek--challenge--tabs--inner--left ek--size--16 @if($match["homeClubName"] == Session::get("club")["clubName"])text--yellow @else text--white @endif">{{ $match["homeClubName"] }}</div>
                                                <!-- Ek Challenge Body Tabs Inner Left -->
                                                <!-- Ek Challenge Body Tabs Inner Center -->
                                                @if($match["played"] == true)
                                                    <div class="ek--challenge--tabs--inner--center ek--size--16 text--navy--200">{{ $match["homeScore"] }} - {{ $match["awayScore"] }}</div>
                                                @else
                                                    <div class="ek--challenge--tabs--inner--center ek--size--16 text--navy--200">vs</div>
                                                @endif
                                                <!-- End Ek Challenge Body Tabs Inner Center -->
                                                <!-- Ek Challenge Body Tabs Inner Right -->
                                                <div class="ek--challenge--tabs--inner--right ek--size--16 @if($match["awayClubName"] == Session::get("club")["clubName"])text--yellow @else text--white @endif">{{ $match["awayClubName"] }}</div>
                                                <!-- End Ek Challenge Body Tabs Inner Right -->

                                            </div>
                                        @endif
                                    @endforeach
                                    <!-- End Ek Challenge Body Tabs Inner -->

                                </div>
                                <!-- End Ek Challenge Body Tabs Table Ordinal -->
                                <!-- Ek Challenge Body Tabs Table Ordinal -->
                                <div class="ek--challenge--tabs--ordinal">
                                    <!-- Ek Challenge Body Tabs Table Ordinal League -->
                                    @if(count(Session::get("challenge_matches")) > 26)
                                        <h3 class="text--white ek--size--16">2nd League</h3>
                                    @endif
                                    <!-- End Ek Challenge Body Tabs Table Ordinal League -->
                                    <!-- Ek Challenge Body Tabs Inner -->
                                    @foreach(Session::get("challenge_matches") as $match)
                                        @if($match["round"] == 6)
                                            <div class="ek--challenge--tabs--inner bg--navy--600">
                                                <!-- Ek Challenge Body Tabs Inner Left -->
                                                <div class="ek--challenge--tabs--inner--left ek--size--16 @if($match["homeClubName"] == Session::get("club")["clubName"])text--yellow @else text--white @endif">{{ $match["homeClubName"] }}</div>
                                                <!-- Ek Challenge Body Tabs Inner Left -->
                                                <!-- Ek Challenge Body Tabs Inner Center -->
                                                @if($match["played"] == true)
                                                    <div class="ek--challenge--tabs--inner--center ek--size--16 text--navy--200">{{ $match["homeScore"] }} - {{ $match["awayScore"] }}</div>
                                                @else
                                                    <div class="ek--challenge--tabs--inner--center ek--size--16 text--navy--200">vs</div>
                                                @endif
                                                <!-- End Ek Challenge Body Tabs Inner Center -->
                                                <!-- Ek Challenge Body Tabs Inner Right -->
                                                <div class="ek--challenge--tabs--inner--right ek--size--16 @if($match["awayClubName"] == Session::get("club")["clubName"])text--yellow @else text--white @endif">{{ $match["awayClubName"] }}</div>
                                                <!-- End Ek Challenge Body Tabs Inner Right -->

                                            </div>
                                        @endif
                                    @endforeach
                                    <!-- End Ek Challenge Body Tabs Inner -->

                                </div>
                                <!-- End Ek Challenge Body Tabs Table Ordinal -->
                            </div>
                            <!-- End Ek Challenge Body Tabs Table Semi -->
                            <!-- Ek Challenge Body Tabs Table Final -->
                            <div class="ek--challenge--tabs--final ek--challenge--tabs--table">
                                <!-- Ek Challenge Body Tabs Table Ordinal -->
                                <div class="ek--challenge--tabs--ordinal">
                                        <!-- Ek Challenge Body Tabs Table Ordinal League -->
                                        @if(count(Session::get("challenge_matches")) > 27)
                                            <h3 class="text--white ek--size--16">1st League</h3>
                                            @else
                                            <div class="ek--matches--empty bg--navy--700" >
                                                <div class="ek--matches--empty--content">
                                                    <div class="ek--matches--empty--about">
                                                        <h3 class="text--white">You haven't reach to Final yet</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        <!-- End Ek Challenge Body Tabs Table Ordinal League -->
                                        <!-- Ek Challenge Body Tabs Inner -->
                                        @foreach(Session::get("challenge_matches") as $match)
                                            @if($match["round"] == 7)
                                                <div class="ek--challenge--tabs--inner bg--navy--600">
                                                    <!-- Ek Challenge Body Tabs Inner Left -->
                                                    <div class="ek--challenge--tabs--inner--left ek--size--16 @if($match["homeClubName"] == Session::get("club")["clubName"])text--yellow @else text--white @endif">{{ $match["homeClubName"] }}</div>
                                                    <!-- Ek Challenge Body Tabs Inner Left -->
                                                    <!-- Ek Challenge Body Tabs Inner Center -->
                                                    @if($match["played"] == true)
                                                        <div class="ek--challenge--tabs--inner--center ek--size--16 text--navy--200">{{ $match["homeScore"] }} - {{ $match["awayScore"] }}</div>
                                                    @else
                                                        <div class="ek--challenge--tabs--inner--center ek--size--16 text--navy--200">vs</div>
                                                    @endif
                                                    <!-- End Ek Challenge Body Tabs Inner Center -->
                                                    <!-- Ek Challenge Body Tabs Inner Right -->
                                                    <div class="ek--challenge--tabs--inner--right ek--size--16 @if($match["awayClubName"] == Session::get("club")["clubName"])text--yellow @else text--white @endif">{{ $match["awayClubName"] }}</div>
                                                    <!-- End Ek Challenge Body Tabs Inner Right -->

                                                </div>
                                            @endif
                                        @endforeach
                                        <!-- End Ek Challenge Body Tabs Inner -->
                                    </div>
                                    <!-- End Ek Challenge Body Tabs Table Ordinal -->

                            </div>
                            <!-- End Ek Challenge Body Tabs Table Final -->
                        </div>
                        <!-- End Ek Challenge Body Tabs Content -->
                    </div>
                @else
                    <!-- Challenge Empty  -->

                    <div id="newSeason" class="ek--matches--empty bg--navy--700" >
                        <div class="ek--matches--empty--content">
                            <div class="ek--matches--empty--about">
                                <h3 class="text--white">Create new tournament</h3>
                                <p class="text--navy--300">You have never created a tournament before.Please click the button below to create new and wait for games</p>
                                <button class="button--yellow text--navy--1000" id="newSeasonBtn">Create tournament</button>
                            </div>
                        </div>
                    </div>
                @endif
                <!-- End Challenge Empty -->
            <!-- End Ek Challenge Body Tabs -->




        </div>
        <!-- End Ek Challenge Body -->
    </div>
@endsection


@section('appendix')
    <div id="loading" class="ek--modal--loading">
        <div class="ek--modal--loading--body">
            <div class="ek--modal--loading--body--inner">
                <div class="ek--modal--loading--body--image bg--navy--700">
                    <img src="{{ url("static/img/Dashboard/ek--loading@2x.png") }}" />
                </div>
                <p class="text--navy--100 ek--size--16"></p>
            </div>
        </div>
    </div>

    <input type="hidden" id="tournamentId" value="{{$id}}">
@endsection
@section('javascript')
    <script src="{{url("static/js/challenge.js")}}"></script>
@endsection
