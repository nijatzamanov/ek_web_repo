@php
    $challangesPageActive = true;
@endphp

@extends('client.layout.authenticated')
@section('content')


    <!-- Ek Challenge -->
    <div class="ek--match ek--league">
        <!-- Ek Challenge Body -->
        <div class="ek--league--tab bg--navy--700">
            <!-- Ek Challenge Body Head -->
            <div class="ek--challenge--body--head bg--navy--700">
                <h3 class="text--white ek--size--20">Tournaments</h3>
            </div>
            <!-- End Ek Challenge Body Head -->

            <div class="ek--league--tab--content">

                <div class="ek--league--tab--standing ek--league--tab--table">
                    <!-- EK Body Squad Table Head -->
                    <div class="ek--league--tab--standing--head">
                        <div class="ek--league--tab--standing--head--name text--navy--200 ek--size--16">
                            Tournament Name
                        </div>
                        <div class="ek--league--tab--standing--head--name text--navy--200 ek--size--16">
                            Winning award
                        </div>
                        <div class="ek--league--tab--standing--head--fa text--navy--200 ek--size--16">
                            Time
                        </div>
                        <div class="ek--league--tab--standing--head--fa text--navy--200 ek--size--16">
                            Opponents
                        </div>
                        <div class="ek--league--tab--standing--head--w text--navy--200 ek--size--16">
                            Entry fee
                        </div>
                    </div>
                    <!-- End EK Body Squad Table Head -->

                    <!-- EK Body Squad Table body -->
                    <div class="ek--league--tab--list">
                        @foreach(Session::get("active_challenges") as $challenge)
                        @if($challenge["activeLevel"] <= Session::get("level"))
                        <!-- EK Body Squad Table Body Inner -->
                        <a href="{{ url("/tournament",$challenge["id"]) }}">
                            <div class="ek--league--tab--inner bg--navy--600">
                            <div class="ek--league--tab--inner--name">
                                <span class="text--white ek--size--16--500">{{ $challenge["name"] }}</span>
                            </div>
                            <div class="ek--league--tab--inner--name text--white ek--size--16--500">

                                <span class="text--white ek--size--16--500 ml-0">
                                    <ul>
                                        @if($challenge["winPoints"] != 0)
                                        <li style="display: inline-block">

                                                {{ $challenge["winPoints"] }}
                                                <svg style="fill:#2979ff !important;" class="icon icon-ek--played mr-2">
                                                    <use xlink:href="/static/img/icons.svg#icon-ek--played"></use>
                                                </svg>
                                        </li>
                                        @endif

                                        @if($challenge["winMoney"] != 0)
                                        <li style="display: inline-block">
                                                @if($challenge["winMoney"] < 1000)
                                                    <span class="ek--body--inner--club--amount text--white ml-0">
                                                <span class="ek--body--inner--club--amount text--white ml-0">
                                                    {{ $challenge["winMoney"] }}
                                                </span>
                                            </span>
                                                @elseif($challenge["winMoney"] >= 1000 && $challenge["winMoney"] < 1000000)
                                                    <span class="ek--body--inner--club--amount text--white ml-0">
                                                        <span class="ek--body--inner--club--amount text--white ml-0">
                                                            {{ number_format($challenge["winMoney"]/1000,1) }}
                                                        </span>
                                                        <span class="ek--body--inner--club--amount text--white ml-0">K</span>
                                                    </span>
                                                        @elseif($challenge["winMoney"] >= 1000000 && $challenge["winMoney"] < 1000000000)
                                                            <span class="ek--body--inner--club--amount text--white ml-0">
                                                        <span class="ek--body--inner--club--amount text--white ml-0">
                                                            {{ number_format($challenge["winMoney"]/1000000,1) }}
                                                        </span>
                                                        <span class="ek--body--inner--club--amount text--white ml-0">M</span>
                                                    </span>
                                                        @elseif($challenge["winMoney"] >= 1000000000)
                                                            <span class="ek--body--inner--club--amount text--white ml-0">
                                                        <span class="ek--body--inner--club--amount text--white ml-0">
                                                            {{ number_format($challenge["winMoney"]/1000000000,1) }}
                                                        </span>
                                                        <span class="ek--body--inner--club--amount text--white ml-0">B</span>
                                                    </span>
                                                 @endif
                                                    <svg class="icon icon-ek--budget mr-2">
                                                        <use xlink:href="/static/img/icons.svg#icon-ek--budget"></use>
                                                    </svg>
                                        </li>
                                        @endif

                                        @if($challenge["winCoins"] != 0)
                                        <li style="display: inline-block">
                                                {{ $challenge["winCoins"] }}
                                                <svg style="fill:#ffc107 !important; --color-1:#ffc107 !important;--color-2:#ffc107 !important;" class="icon icon-ek--coins mr-2">
                                                    <use xlink:href="/static/img/icons.svg#icon-ek--coins"></use>
                                                </svg>
                                        </li>
                                        @endif
                                    </ul>

                                </span>

                            </div>
                            <div class="ek--league--tab--inner--fa text--white ek--size--16--500">
                                @if($challenge["perMatchSeconds"] < 60)
                                    {{ $challenge["perMatchSeconds"] }}s
                                    @elseif(($challenge["perMatchSeconds"]/60)/60)
                                    {{ ($challenge["perMatchSeconds"]/60)/60 }}h
                                @endif
                            </div>
                            <div class="ek--league--tab--inner--fa text--white ek--size--16--500">
                                {{ $challenge["ablRangStart"].'-'.$challenge["ablRangEnd"] }}
                            </div>
                            <div class="ek--league--tab--inner--w text--white ek--size--16--500">
                                {{ $challenge["freeCoins"] }}
                            </div>
                        </div>
                        </a>
                        <!-- End EK Body Squad Table Body Inner -->
                        @else
                        <a style="display:block;color:white; opacity: .6; cursor: not-allowed">
                            <div class="ek--league--tab--inner bg--navy--600">
                                        <div class="ek--league--tab--inner--name">
                                            <span class="text--white ek--size--16--500">{{ $challenge["name"] }}</span>
                                        </div>
                                        <div class="ek--league--tab--inner--name text--white ek--size--16--500">

                                <span class="text--white ek--size--16--500 ml-0">
                                    {{ $challenge["winPoints"] }}
                                    <svg style="fill:#2979ff !important;" class="icon icon-ek--played mr-2">
                                        <use xlink:href="/static/img/icons.svg#icon-ek--played"></use>
                                    </svg>

                                    @if($challenge["winMoney"] < 1000)
                                        <span class="ek--body--inner--club--amount text--white ml-0">
                                            <span class="ek--body--inner--club--amount text--white ml-0">
                                                {{ $challenge["winMoney"] }}
                                            </span>
                                        </span>
                                    @elseif($challenge["winMoney"] >= 1000 && $challenge["winMoney"] < 1000000)
                                        <span class="ek--body--inner--club--amount text--white ml-0">
                                            <span class="ek--body--inner--club--amount text--white ml-0">
                                                {{ number_format($challenge["winMoney"]/1000,1) }}
                                            </span>
                                            <span class="ek--body--inner--club--amount text--white ml-0">K</span>
                                        </span>
                                    @elseif($challenge["winMoney"] >= 1000000 && $challenge["winMoney"] < 1000000000)
                                        <span class="ek--body--inner--club--amount text--white ml-0">
                                            <span class="ek--body--inner--club--amount text--white ml-0">
                                                {{ number_format($challenge["winMoney"]/1000000,1) }}
                                            </span>
                                            <span class="ek--body--inner--club--amount text--white ml-0">M</span>
                                        </span>
                                    @elseif($challenge["winMoney"] >= 1000000000)
                                        <span class="ek--body--inner--club--amount text--white ml-0">
                                            <span class="ek--body--inner--club--amount text--white ml-0">
                                                {{ number_format($challenge["winMoney"]/1000000000,1) }}
                                            </span>
                                            <span class="ek--body--inner--club--amount text--white ml-0">B</span>
                                        </span>
                                    @endif
                                    <svg class="icon icon-ek--budget mr-2">
                                        <use xlink:href="/static/img/icons.svg#icon-ek--budget"></use>
                                    </svg>

                                    {{ $challenge["winCoins"] }}
                                    <svg class="icon icon-ek--coins" style="fill:#ffc107 !important">
                                        <use xlink:href="/static/img/icons.svg#icon-ek--coins"></use>
                                    </svg>
                                </span>

                                        </div>
                                        <div class="ek--league--tab--inner--fa text--white ek--size--16--500">
                                            @if($challenge["perMatchSeconds"] < 60)
                                                {{ $challenge["perMatchSeconds"] }}s
                                            @elseif(($challenge["perMatchSeconds"]/60)/60)
                                                {{ ($challenge["perMatchSeconds"]/60)/60 }}h
                                            @endif
                                        </div>
                                        <div class="ek--league--tab--inner--fa text--white ek--size--16--500">
                                            {{ $challenge["ablRangStart"].'-'.$challenge["ablRangEnd"] }}
                                        </div>
                                        <div class="ek--league--tab--inner--w text--white ek--size--16--500">
                                            {{ $challenge["freeCoins"] }}
                                        </div>
                                    </div>
                        </a>
                        @endif
                        @endforeach
                    </div>
                    <!-- End EK Body Squad Table Body -->
                </div>
            </div>


        </div>
        <!-- End Ek Challenge Body -->
    </div>
@endsection


@section('appendix')
    <div id="loading" class="ek--modal--loading">
        <div class="ek--modal--loading--body">
            <div class="ek--modal--loading--body--inner">
                <div class="ek--modal--loading--body--image bg--navy--700">
                    <img src="{{ url("static/img/Dashboard/ek--loading@2x.png") }}" />
                </div>
                <p class="text--navy--100 ek--size--16"></p>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script src="{{url("static/js/challenge.js")}}"></script>
@endsection
