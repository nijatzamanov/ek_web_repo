@extends('client.layout.layout')

@section('endScripts')
    <script>
        $(document).on("click", ".ek--password--eye", function (e) {
            let input = $(this).parents(".ek--password").find(".ek--manager--password");
            let type = $(this)
                .parents(".ek--password")
                .find(".ek--manager--password")
                .attr("type");
            if (type === "password") {
                input.attr("type", "text");
                $(this).find(".icon-eye").hide();
                $(this).find(".icon-eye-2").show();
            } else {
                input.attr("type", "password");
                $(this).find(".icon-eye").show();
                $(this).find(".icon-eye-2").hide();
            }
        });
    </script>
@endsection

@section('content')
    <!-- =========================
    Section -  Wrapper
============================== -->
    <div class="ek--wrapper">
        <!-- =========================
      Section -  Header
  ============================== -->
        <header class="ek--header">
            <!-- EK Header logo -->
            <div class="ek--header--logo">
                <a href="{{ route('overview') }}"><img src="/static/img/Logo/Ek--logo.svg" alt="ek--logo"/></a>
                <div class="ek--header--logo--close">
                    <svg class="icon icon-ek--close--icon fill--navy--200">
                        <use xlink:href="/static/img/icons.svg#icon-ek--close--icon"></use>
                    </svg>
                </div>
            </div>
            <!-- End EK Header logo -->
            <!-- EK Header Navbar -->
            <ul class="ek--header--nav">
                <li class="ek--header--item">
                    <a
                        class="ek--header--link ek--header--link--active"
                        href="{{ route('overview') }}"
                    >
                        <svg class="icon icon-ek--overwiev">
                            <use xlink:href="/static/img/icons.svg#icon-ek--overwiev"></use>
                        </svg>
                        Overview</a
                    >
                </li>
            </ul>

            <ul class="ek--header--nav">
                <li class="ek--header--item">
                    <a class="ek--header--link" href="@{/squad}"
                    >
                        <svg class="icon icon-ek--squad">
                            <use xlink:href="/static/img/icons.svg#icon-ek--squad"></use>
                        </svg>
                        Squad</a
                    >
                </li>
                <li class="ek--header--item">
                    <a class="ek--header--link" href="@{/lineup}"
                    >
                        <svg class="icon icon-ek--lineup">
                            <use xlink:href="/static/img/icons.svg#icon-ek--lineup"></use>
                        </svg>
                        Lineup & Tactics</a
                    >
                </li>
                <li class="ek--header--item">
                    <a class="ek--header--link" href="@{/daily}"
                    >
                        <svg class="icon icon-ek--daily">
                            <use xlink:href="/static/img/icons.svg#icon-ek--daily"></use>
                        </svg>
                        Daily Training</a
                    >
                </li>
                <li class="ek--header--item">
                    <a class="ek--header--link" href="@{/training}"
                    >
                        <svg class="icon icon-ek--camp">
                            <use xlink:href="/static/img/icons.svg#icon-ek--camp"></use>
                        </svg>
                        Training Camp</a
                    >
                </li>
                <li class="ek--header--item">
                    <a class="ek--header--link" href="@{/transfer}">
                        <svg class="icon icon-ek--transfer">
                            <use xlink:href="/static/img/icons.svg#icon-ek--transfer"></use>
                        </svg>
                        Transfer</a
                    >
                </li>
                <li class="ek--header--item">
                    <a class="ek--header--link" href="@{/statistics}"
                    >
                        <svg class="icon icon-ek--statistics">
                            <use xlink:href="/static/img/icons.svg#icon-ek--statistics"></use>
                        </svg>
                        Statistics</a
                    >
                </li>
            </ul>

            <ul class="ek--header--nav">
                <li class="ek--header--item">
                    <a class="ek--header--link" href="@{/league-match}"
                    >
                        <svg class="icon icon-ek--league">
                            <use xlink:href="/static/img/icons.svg#icon-ek--league"></use>
                        </svg>
                        League</a
                    >
                </li>
                <li class="ek--header--item">
                    <a class="ek--header--link" href="@{/cup}"
                    >
                        <svg class="icon icon-ek--cup">
                            <use xlink:href="/static/img/icons.svg#icon-ek--cup"></use>
                        </svg>
                        Cup</a
                    >
                </li>
                <li class="ek--header--item">
                    <a class="ek--header--link" href="@{/challenge}"
                    >
                        <svg class="icon icon-ek--challenge">
                            <use xlink:href="/static/img/icons.svg#icon-ek--challenge"></use>
                        </svg>
                        Challenge</a
                    >
                </li>
                <li class="ek--header--item">
                    <a class="ek--header--link" href="@{/friendly}">
                        <svg class="icon icon-ek--friendly">
                            <use xlink:href="/static/img/icons.svg#icon-ek--friendly"></use>
                        </svg>
                        Friendly</a
                    >
                </li>
            </ul>

            <ul class="ek--header--nav">
                <li class="ek--header--item">
                    <a class="ek--header--link" href="@{/coins}"
                    >
                        <svg class="icon icon-ek--coins">
                            <use xlink:href="/static/img/icons.svg#icon-ek--coins"></use>
                        </svg>
                        EK Coins</a
                    >
                </li>
                <li class="ek--header--item">
                    <a class="ek--header--link" href="@{/budget}"
                    >
                        <svg class="icon icon-ek--budget">
                            <use xlink:href="/static/img/icons.svg#icon-ek--budget"></use>
                        </svg>
                        Budget</a
                    >
                </li>
                <li class="ek--header--item">
                    <a class="ek--header--link" href="@{/marketing}"
                    >
                        <svg class="icon icon-ek--marketing">
                            <use xlink:href="/static/img/icons.svg#icon-ek--marketing"></use>
                        </svg>
                        Marketing</a
                    >
                </li>
            </ul>

            <ul class="ek--header--nav">
                <li class="ek--header--item">
                    <a class="ek--header--link" href="@{/setting}"
                    >
                        <svg class="icon icon-ek--settings">
                            <use xlink:href="/static/img/icons.svg#icon-ek--settings"></use>
                        </svg>
                        Settings</a
                    >
                </li>
            </ul>
            <!-- End EK Header Navbar -->
        </header>
        <!-- =========================
      End Section -  Header
  ============================== -->
        <!-- =========================
      Section -  Main
  ============================== -->
        <main class="ek--body">
            <!-- EK Body Head -->
            <section class="ek--body--head">
                <!-- EK Body Head Left -->
                <div class="ek--body--head--left">
                    <!-- EK Body Mobile Menu -->
                    <div class="ek--body--head--mobile--icon">
                        <svg class="icon icon-ek--mobile--menu fill--green">
                            <use xlink:href="/static/img/icons.svg#icon-ek--mobile--menu"></use>
                        </svg>
                    </div>
                    <!-- End EK Body Mobile Menu -->
                </div>
                <!-- EnD EK Body Head Left -->
                <div class="ek--body--head--center">
                    <a href="{{ route('overview') }}">
                        <img src="/static/img/Logo/Ek--logo.svg" alt="ek--logo"/>
                    </a>
                </div>
                <!-- EK Body Head Right -->
                <div class="ek--body--head--right">
                    <!-- Ek Body Head Money -->
                    <div class="ek--body--head--money">
                        <!-- Ek Body Head Money Dollar -->
                        <div class="ek--body--head--money--dollar bg--navy--700">
                            <svg class="icon icon-ek--budget fill--green">
                                <use xlink:href="/static/img/icons.svg#icon-ek--budget"></use>
                            </svg>
                            <p class="text--white ek--size--16">14,2 M</p>
                        </div>
                        <!-- End Ek Body Head Money Dollar -->
                        <!-- Ek Body Head Money Dollar -->
                        <div class="ek--body--head--money--coins bg--navy--700">
                            <svg class="icon icon-ek--coins fill--yellow">
                                <use xlink:href="/static/img/icons.svg#icon-ek--coins"></use>
                            </svg>
                            <p class="text--white ek--size--16">70,290</p>
                        </div>
                        <!-- End Ek Body Head Money Dollar -->
                    </div>
                    <!-- End Ek Body Head Money -->
                    <!-- EK Body Head User -->
                    <div class="ek--body--head--user">
                        <!-- EK Body Head User Inner -->
                        <div class="ek--body--head--inner">
                            <!-- EK Body Head User Image -->
                            <div class="ek--body--head--image">
                                <img src="/static/img/Other/ek--null--image.png" alt="ek--user"/>
                            </div>
                            <!-- End EK Body Head User Image -->
                            <!-- EK Body Head User Name and Level -->
                            <div class="ek--body--head--name">
                                <!-- EK Body Head User Name Title -->
                                <h1 class="ek--body--head--name--title text--white">
                                    Kanan Hashimov
                                </h1>
                                <!-- End EK Body Head User Name Title -->
                                <!-- EK Body Head User Levet -->
                                <p class="ek--body--head--name--level text--yellow">
                                    Level 2
                                </p>
                                <!-- End EK Body Head User Levet -->
                            </div>
                            <!-- End EK Body Head User Name and Level -->
                            <!-- EK Body Down and Up Icons -->
                            <div class="ek--body--head--icons">
                                <svg class="icon icon-ek--arrow--up">
                                    <use xlink:href="/static/img/icons.svg#icon-ek--arrow--up"></use>
                                </svg>
                                <svg class="icon icon-ek--arrow--down">
                                    <use xlink:href="/static/img/icons.svg#icon-ek--arrow--down"></use>
                                </svg>
                            </div>
                            <!-- End EK Body Down and Up Icons -->
                            <!-- Ek Body Profil Dropdown  -->
                            <div class="ek--body--user">
                                <!-- Ek Body Profil Dropdown Menu  -->
                                <div class="ek--body--user--menu">
                                    <a href="@{/myprofile}">My profile</a>
                                    <a href="@{/edit}">Profile settings</a>
                                </div>
                                <!-- End Ek Body Profil Dropdown Menu  -->
                                <!-- Ek Body Profil Dropdown Logout  -->
                                <div class="ek--body--user--logout">
                                    <a href="#">Log out</a>
                                </div>
                                <!-- End Ek Body Profil Dropdown Logout  -->
                            </div>
                            <!-- End Ek Body Profil Dropdown  -->
                        </div>
                        <!-- End EK Body Head User Inner -->
                    </div>
                    <!-- End EK Body Head User -->
                </div>
                <!-- EnD EK Body Head Right -->
            </section>
            <!-- End EK Body Head -->



            <!-- EK Body Inner -->
            <section class="ek--body--inner">
                <!-- EK Body Inner Team Tools -->
                <div class="ek--body--inner--team">
                    <!-- EK Body Inner Club -->
                    <div class="ek--body--inner--club bg--navy--700">
                        <!-- EK Body Inner Club Left -->

                        <a  href="{{ route("squad") }}" class="ek--body--inner--club--left">
                            <div class="ek--body--inner--club--hexagon text--navy--900">72</div>
                            <div class="ek--body--inner--club--flex home--flex">
                                <h2 class="ek--body--inner--club--name text--white">Karabakh</h2>
                                <p class="ek--body--inner--club--paragraph text--navy--200">
                                    <span class="text--navy--200" >League 5, #6 place</span>
                                </span>
                                </p>
                            </div>
                        </a>
                        <!-- End EK Body Inner Club Left -->

                        <!-- EK Body Inner Club Right -->
                        <div class="ek--body--inner--club--right">
                            <!-- EK Body Inner Club Right Tool -->
                            <a  href="{{ route("budget") }}" class="ek--body--inner--club--tool bg--navy--600">
                                <!-- EK Body Inner Club Right Tool Title-->
                                <h3 class="ek--body--inner--club--title text--navy--200">{{  __('translation.balance') }}</h3>
                                <!-- End EK Body Inner Club Right Tool Title-->

                                <!-- EK Body Inner Club Right Tool Balance-->
                                <div  class="ek--body--inner--club--balance" >
                                    <svg class="icon icon-ek--budget">
                                        <use xlink:href="/static/img/icons.svg#icon-ek--budget"></use>
                                    </svg>
                                    <span class="text--white">14,2M</span>
                                </div>
                                <!-- End EK Body Inner Club Right Tool Balance-->
                            </a>
                            <a  href="{{ route("coins") }}" class="ek--body--inner--club--tool bg--navy--600">
                                <!-- EK Body Inner Club Right Tool Title-->
                                <h3 class="ek--body--inner--club--title text--navy--200">{{  __('translation.ek_coin') }}</h3>
                                <!-- End EK Body Inner Club Right Tool Title-->
                                <!-- EK Body Inner Club Right Tool Balance-->
                                <div  class="ek--body--inner--club--balance">
                                    <svg class="icon icon-ek--coins">
                                        <use xlink:href="/static/img/icons.svg#icon-ek--coins"></use>
                                    </svg>
                                    <p class="ek--body--inner--club--amount text--white">70,290</p>
                                </div>
                                <!-- End EK Body Inner Club Right Tool Balance-->
                            </a>
                            <a  href="{{ route("myProfile") }}" class="ek--body--inner--club--tool bg--navy--600">
                                <!-- EK Body Inner Club Right Tool Title-->
                                <h3 class="ek--body--inner--club--title text--navy--200">{{  __('translation.points') }}</h3>
                                <!-- End EK Body Inner Club Right Tool Title-->

                                <!-- EK Body Inner Club Right Tool Balance-->
                                <div  class="ek--body--inner--club--balance">
                                    <svg class="icon icon-ek--played">
                                        <use xlink:href="/static/img/icons.svg#icon-ek--played"></use>
                                    </svg>
                                    <p class="ek--body--inner--club--amount text--white">1530</p>
                                </div>
                                <!-- End EK Body Inner Club Right Tool Balance-->
                            </a>
                            <!-- End EK Body Inner Club Right Tool -->
                        </div>
                        <!-- End EK Body Inner Club Right -->
                    </div>
                    <!-- End EK Body Inner Club -->
                </div>
                <!-- End EK Body Inner Team Tools -->



                <!--geri qaytar yuxarini-->

                <!-- EK Body Inner Matches -->
                <div class="ek--body--matches"  >
                    <div class="ek--body--matches--left bg--navy--700"  >
                        <div class="ek--body--matches--head">
                            <h3 class="ek--body--matches--head--title text--white">Next match</h3>
                            <a class="ek--body--matches--head--fixtures text--green" href="#">Fixture
                                <svg class="icon icon-right">
                                    <use xlink:href="/static/img/icons.svg#icon-right"></use>
                                </svg>
                            </a>
                        </div>


                            <div class="ek--body--matches--inner bg--navy--600">

                                <div class="ek--body--matches--inner--head">
                                    <span class="text--green"></span>
                                    <span class="text--green">
                                    <span class="text--green">Kings League 5, Round #</span>
                                </span>
                                    <span class="text--green">9</span>
                                </div>

                                <div class="ek--body--matches--inner--clubs"  >

                                    <div class="ek--body--matches--inner--clubs--left"  >
                                        <div class="ek--body--matches--inner--clubs--name text--right" >
                                            <h3 class="text--white">Karabakh</h3>
                                            <div class="ek--body--matches--inner--clubs--country d--flex--end">
                                                <span class="text--navy--200">Azerbaijan</span>
                                                <img src="{{ url("static/img/Flags/AZ.svg") }}">
                                            </div>
                                        </div>
                                        <div class="ek--body--inner--club--hexagon text--navy--900">72</div>
                                    </div >

                                    <div class="ek--body--matches--inner--clubs--center text--navy--300">VS</div>

                                    <div class="ek--body--matches--inner--clubs--right" >
                                        <div class="ek--body--inner--club--hexagon text--navy--900">99</div>
                                        <div class="ek--body--matches--inner--clubs--name text--start">
                                            <h3 class="text--white">Real Madrid</h3>
                                            <div class="ek--body--matches--inner--clubs--country d--flex--start">
                                                <img src="{{ url("static/img/Flags/US.svg") }}">
                                                <span class="text--navy--200">USA</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a class="ek--body--matches--inner--play text--navy--900 button--green" href="#">{{  __('translation.home_play_match') }}</a>
                            </div>

                    </div>

                    <div class="ek--body--matches--right--own">
                        <div class="ek--body--matches--right bg--navy--700">
                            <div class="ek--body--matches--head">
                                <h3 class="ek--body--matches--head--title text--white ">
                                    League
                                    # 5</h3>
                                <a class="ek--body--matches--head--fixtures text--green"  href="#">{{  __('translation.league_standing') }}
                                    <svg class="icon icon-right">
                                        <use xlink:href="/static/img/icons.svg#icon-right"></use>
                                    </svg>
                                </a>
                            </div>

                            <div class="ek--body--matches--table">
                                <div class="ek--body--matches--table--head">
                                    <div
                                        class="ek--body--matches--table--head--number ek--size--16 text--navy--200"
                                    >
                                        #
                                    </div>
                                    <div
                                        class="ek--body--matches--table--head--club ek--size--16 text--navy--200"
                                    >
                                        Club Name
                                    </div>
                                    <div
                                        class="ek--body--matches--table--head--played ek--size--16 text--center text--navy--200"
                                    >
                                        Played
                                    </div>
                                    <div
                                        class="ek--body--matches--table--head--won ek--size--16 text--center text--navy--200"
                                    >
                                        Won
                                    </div>
                                </div>

                                <div class="ek--body--matches--table--body">
                                    <div
                                        class="ek--body--matches--table--body--inner bg--navy--600"
                                    >
                                        <div
                                            class="ek--body--matches--table--body--number text--white ek--size--16"
                                        >
                                            <span>4</span>
                                            <svg class="icon icon-ek--arrow--up fill--green">
                                                <use
                                                    xlink:href="/static/img/icons.svg#icon-ek--arrow--up"
                                                ></use>
                                            </svg>
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--club text--white ek--size--16"
                                        >
                                            RM CF
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                        >
                                            18
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                        >
                                            12
                                        </div>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--inner bg--navy--600"
                                    >
                                        <div
                                            class="ek--body--matches--table--body--number text--white ek--size--16"
                                        >
                                            <span>5</span>
                                            <svg class="icon icon-ek--arrow--down fill--red">
                                                <use
                                                    xlink:href="/static/img/icons.svg#icon-ek--arrow--down"
                                                ></use>
                                            </svg>
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--club text--white ek--size--16"
                                        >
                                            Burnies
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                        >
                                            18
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                        >
                                            12
                                        </div>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--inner bg--navy--600"
                                    >
                                        <div
                                            class="ek--body--matches--table--body--number text--white ek--size--16"
                                        >
                                            <span>6</span>
                                            <svg class="icon icon-ek--arrow--up fill--green">
                                                <use
                                                    xlink:href="/static/img/icons.svg#icon-ek--arrow--up"
                                                ></use>
                                            </svg>
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--club text--white ek--size--16"
                                        >
                                            Manchester
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                        >
                                            18
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                        >
                                            12
                                        </div>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--inner bg--navy--600"
                                    >
                                        <div
                                            class="ek--body--matches--table--body--number text--white ek--size--16"
                                        >
                                            <span>3</span>
                                            <svg class="icon icon-ek--arrow--down fill--red">
                                                <use
                                                    xlink:href="/static/img/icons.svg#icon-ek--arrow--down"
                                                ></use>
                                            </svg>
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--club text--white ek--size--16"
                                        >
                                            Karabakh FC
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                        >
                                            18
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                        >
                                            12
                                        </div>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--inner bg--navy--600"
                                    >
                                        <div
                                            class="ek--body--matches--table--body--number text--white ek--size--16"
                                        >
                      <span>5</span
                      >
                                            <svg class="icon icon-ek--oval fill--navy--300">
                                                <use xlink:href="/static/img/icons.svg#icon-ek--oval"></use>
                                            </svg>
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--club text--white ek--size--16"
                                        >
                                            Burnies
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                        >
                                            18
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                        >
                                            12
                                        </div>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--inner bg--navy--600"
                                    >
                                        <div
                                            class="ek--body--matches--table--body--number text--white ek--size--16"
                                        >
                                            <span>4</span>
                                            <svg class="icon icon-ek--arrow--up fill--green">
                                                <use
                                                    xlink:href="/static/img/icons.svg#icon-ek--arrow--up"
                                                ></use>
                                            </svg>
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--club text--white ek--size--16"
                                        >
                                            RM CF
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                        >
                                            18
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                        >
                                            12
                                        </div>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--inner bg--navy--600"
                                    >
                                        <div
                                            class="ek--body--matches--table--body--number text--white ek--size--16"
                                        >
                                            <span>3</span>
                                            <svg class="icon icon-ek--arrow--down fill--red">
                                                <use
                                                    xlink:href="/static/img/icons.svg#icon-ek--arrow--down"
                                                ></use>
                                            </svg>
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--club text--white ek--size--16"
                                        >
                                            Karabakh FC
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                        >
                                            18
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                        >
                                            12
                                        </div>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--inner bg--navy--600"
                                    >
                                        <div
                                            class="ek--body--matches--table--body--number text--white ek--size--16"
                                        >
                                            <span>6</span>
                                            <svg class="icon icon-ek--arrow--up fill--green">
                                                <use
                                                    xlink:href="/static/img/icons.svg#icon-ek--arrow--up"
                                                ></use>
                                            </svg>
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--club text--white ek--size--16"
                                        >
                                            Manchester
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                        >
                                            18
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                        >
                                            12
                                        </div>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--inner bg--navy--600"
                                    >
                                        <div
                                            class="ek--body--matches--table--body--number text--white ek--size--16"
                                        >
                                            <span>4</span>
                                            <svg class="icon icon-ek--arrow--up fill--green">
                                                <use
                                                    xlink:href="/static/img/icons.svg#icon-ek--arrow--up"
                                                ></use>
                                            </svg>
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--club text--white ek--size--16"
                                        >
                                            RM CF
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                        >
                                            18
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                        >
                                            12
                                        </div>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--inner bg--navy--600"
                                    >
                                        <div
                                            class="ek--body--matches--table--body--number text--white ek--size--16"
                                        >
                      <span>5</span
                      >
                                            <svg class="icon icon-ek--oval fill--navy--300">
                                                <use xlink:href="/static/img/icons.svg#icon-ek--oval"></use>
                                            </svg>
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--club text--white ek--size--16"
                                        >
                                            Burnies
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                        >
                                            18
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                        >
                                            12
                                        </div>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--inner bg--navy--600"
                                    >
                                        <div
                                            class="ek--body--matches--table--body--number text--white ek--size--16"
                                        >
                                            <span>4</span>
                                            <svg class="icon icon-ek--arrow--up fill--green">
                                                <use
                                                    xlink:href="/static/img/icons.svg#icon-ek--arrow--up"
                                                ></use>
                                            </svg>
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--club text--white ek--size--16"
                                        >
                                            RM CF
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                        >
                                            18
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                        >
                                            12
                                        </div>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--inner bg--navy--600"
                                    >
                                        <div
                                            class="ek--body--matches--table--body--number text--white ek--size--16"
                                        >
                      <span>5</span
                      >
                                            <svg class="icon icon-ek--oval fill--navy--300">
                                                <use xlink:href="/static/img/icons.svg#icon-ek--oval"></use>
                                            </svg>
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--club text--white ek--size--16"
                                        >
                                            Burnies
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                        >
                                            18
                                        </div>
                                        <div
                                            class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                        >
                                            12
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ek--body--matches--ranking bg--navy--700">
                            <div class="ek--body--matches--head">
                                <h3 class="ek--body--matches--head--title text--white">{{  __('translation.home_ranking') }}</h3>
                                <a class="ek--body--matches--head--fixtures text--green" href="#}">{{  __('translation.home_ranking') }}
                                    <svg class="icon icon-right">
                                        <use xlink:href="/static/img/icons.svg#icon-right"></use>
                                    </svg>
                                </a>
                            </div>
                            <div class="ek--body--matches--ranking--body">
                                <div class="ek--body--matches--ranking--own">
                                    <h4 class="ek--body--matches--ranking--country">Azerbaijan</h4>
                                        <block>
                                            <strong class="ek--body--matches--ranking--number" style="display: flex">1</strong>
                                        </block>
                                    <div class="ek--body--matches--ranking--icons">
                                        <svg class="icon icon-ek--arrow--up fill--green">
                                            <use xlink:href="/static/img/icons.svg#icon-ek--arrow--up"></use>
                                        </svg>
                                    </div>
                                </div>
                                <div class="ek--body--matches--ranking--own">
                                    <h4 class="ek--body--matches--ranking--country">{{  __('translation.home_global') }}</h4>
                                        <block>
                                            <strong class="ek--body--matches--ranking--number" style="display: flex">1</strong>
                                        </block>
                                    <div class="ek--body--matches--ranking--icons">
                                        <svg class="icon icon-ek--arrow--down fill--red">
                                            <use xlink:href="/static/img/icons.svg#icon-ek--arrow--down"></use>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="ek--body--inner--team ek--body--inner--4--section">

                    <!-- EK Body Inner Line Up -->
                    <a href="#" class="ek--body--inner--lineup bg--navy--700">
                        <svg class="icon icon-ek--lineup">
                            <use xlink:href="{{ url("/static/img/icons.svg#icon-ek--lineup") }}"></use>
                        </svg>
                        <h3 class="ek--body--inner--lineup--title text--navy--200">{{  __('translation.menu_lineup') }}</h3>
                    </a>
                    <!-- End EK Body Inner Line Up -->

                    <!-- EK Body Inner Training -->
                    <a href="#" class="ek--body--inner--training bg--navy--700" >
                        <svg class="icon icon-ek--daily">
                            <use xlink:href="/static/img/icons.svg#icon-ek--daily"></use>
                        </svg>
                        <h3 class="ek--body--inner--training--title text--navy--200">{{  __('translation.menu_training') }}</h3>
                    </a>
                    <!-- End EK Body Inner Training -->

                    <!-- EK Body Inner Stadium -->
                    <a href="#" class="ek--body--inner--stadium bg--navy--700">
                        <img src="{{ url("/static/img/stadium-green.png") }}" alt="">
                        <h3 class="ek--body--inner--lineup--title text--navy--200">{{  __('translation.menu_stadium') }}</h3>
                    </a>
                    <!-- End EK Body Inner Stadium -->

                    <!-- EK Body Inner Tournaments -->
                    <a href="#" class="ek--body--inner--tournaments bg--navy--700" >
                        <svg class="icon icon-ek--challenge">
                            <use xlink:href="/static/img/icons.svg#icon-ek--challenge"></use>
                        </svg>
                        <h3 class="ek--body--inner--training--title text--navy--200">{{  __('translation.menu_tournaments') }}</h3>
                    </a>
                    <!-- End EK Body Inner Tournaments -->

                </div>
                <!-- End EK Body Inner Matches -->
            </section>



            <section class="ek--mobile--play">
                <div class="container d-flex justify-content-between alig">
                    <div class="ek--body--head--money">
                        <!-- Ek Body Head Money Dollar -->
                        <div class="ek--body--head--money--dollar bg--navy--700">
                            <svg class="icon icon-ek--budget fill--green">
                                <use xlink:href="/static/img/icons.svg#icon-ek--budget"></use>
                            </svg>
                            <p class="text--white ek--size--16">14,2 M</p>
                        </div>
                        <!-- End Ek Body Head Money Dollar -->
                        <!-- Ek Body Head Money Dollar -->
                        <div class="ek--body--head--money--coins bg--navy--700">
                            <svg class="icon icon-ek--coins fill--yellow">
                                <use xlink:href="/static/img/icons.svg#icon-ek--coins"></use>
                            </svg>
                            <p class="text--white ek--size--16">70,290</p>
                        </div>
                        <!-- End Ek Body Head Money Dollar -->
                    </div>
                    <a class="ek--mobile--play--next bg--green" href="@{/play-match}"
                    >Next match</a
                    >
                </div>
            </section>
            <!-- Ek Register Form -->
            <section class="ek--register">
                <div class="ek--register--body">
                    <!-- EK Landing Create Club Modal -->
                    <!-- Create Cluba ek--create--div classsini elave etdim -->
                    <div
                        id="create-club"
                        class="bg--navy--700 ek--create ek--parent ek--create--div"
                        style="${param.check==null ? 'display:block;' : 'display:none;'}"
                    >
                        <!-- EK Landing Create Club Modal Head -->
                        <div class="ek--create--head bg--navy--700">
                            <img src="/static/img/Other/ek--create--uniform.svg" alt=""/>
                        </div>
                        <!-- End EK Landing Create Club Modal Head -->
                        <!-- EK Landing Create Club Modal Title -->
                        <h4 class="ek--create--title text--white">Create a club</h4>
                        <!-- End EK Landing Create Club Modal Title -->
                        <!-- EK Landing Create Club Modal Preface -->
                        <p class="ek--create--preface text--navy--200">
                            Create a club to play the Eleven Kings
                        </p>
                        <!-- Ends EK Landing Create Club Modal Preface -->
                        <!-- EK Landing Create Club Form  -->
                        <form class="ek--create--form" onsubmit="return false;">
                            <!-- EK Landing Create Club Form  Club Name -->
                            <div class="ek--footer--body--form--group">
                                <label for="ek--create--input" class="ek--footer--body--form--label text--navy--300">Choose club name</label>
                                <input id="clubName" type="text" class="ek--create--input bg--navy--1000 text--white" required/>
                            </div>
                            <!-- End EK Landing Create Club Form  Club Name -->
                            <!-- EK Landing Create Club Form  Club Country -->
                            <div class="ek--footer--body--form--group">
                                <!-- EK Landing Create Club Form  Club Country Input -->

                                <label for="ek--create--select" class="ek--create--label text--navy--300" >Country / Region</label>
                                <input type="text" id="selectCountry" data-id="1" value="Azerbaijan" class="ek--create--select bg--navy--1000 text--white" readonly required />

                                <!-- End EK Landing Create Club Form  Club Country Input -->
                                <!-- EK Landing Create Club Form  Club Country Content -->
                                <div class="ek--create--icon">
                                    <svg class="icon icon-ek--arrow--up">
                                        <use xlink:href="/static/img/icons.svg#icon-ek--arrow--up"></use>
                                    </svg>
                                    <svg class="icon icon-ek--arrow--down">
                                        <use
                                            xlink:href="/static/img/icons.svg#icon-ek--arrow--down"
                                        ></use>
                                    </svg>
                                </div>
                                <div class="ek--create--content bg--navy--800">
                                    <input type="text" class="ek--create--search text--white" id="selectedCountry" placeholder="Search country" />

                                    <ul id="ek--create--content--nav" class="ek--create--content--nav countries" >
                                        @foreach($countries["data"] as $country)
                                        <li class="ek--create--content--item" >
                                            <a class="text--navy--200" href="javascript:void(0)">
                                                <span id="countryId" class="ek--create--content--link text--navy--200" data-id="{{$country["id"]}}" text="{{$country["name"]}}">{{$country["name"]}}</span>
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <!-- End EK Landing Create Club Form  Club Country Content -->
                            </div>
                            <!-- End EK Landing Create Club Form  Club Country -->
                            <button
                                type="submit"
                                class="ek--create--button ek--form--button"
                            >
                                <a
                                    class="button--navy--500 text--navy--300"
                                    href="#ek--manager"
                                >Create a Club</a
                                >
                            </button>
                        </form>
                        <!-- End EK Landing Create Club Form  -->
                        <!-- <a href="#" rel="modal:close">Close</a> -->
                        <div class="ek--create--signin">
                            <p class="text--navy--400">You already have a club?</p>
                            <a href="{{ route('login',['lang' =>app()->getLocale()]) }}" class="text--green ek--sign--button">Sign in</a>
                        </div>
                    </div>
                    <!-- End EK Landing Create Club Modal -->
                    <!-- EK Landing Create Manage Account -->
                    <div id="ek--manager" class="ek--parent bg--navy--700 ek--manager">
                        <!-- EK Landing Create Manage Account  Modal Head -->
                        <div class="ek--create--head bg--navy--700">
                            <img src="/static/img/Other/ek--player.svg" alt=""/>
                        </div>
                        <!-- End EK Landing Create Manage Account  Modal Head -->
                        <!-- EK Landing Create Manage Account Modal Back -->
                        <a class="ek--manager--back bg--navy--700" href="#create-club"
                        ><img src="/static/img/Other/ek--left--arrow.svg" alt=""
                            /></a>
                        <!-- End EK Landing Create Manage Account Modal Back -->
                        <!-- EK Landing Manage Account Title -->
                        <h4 class="ek--create--title text--white">
                            Create manager account
                        </h4>
                        <!-- End EK Landing Manage Account Title -->
                        <!-- EK Landing Manage Account Form -->
{{--                        <form action="">--}}
{{--                            <!-- EK Landing Create Manage Account Social Account -->--}}
{{--                            <!--<button id="registerFacebook" class="ek&#45;&#45;social bg&#45;&#45;facebook"><img src="/static/img/Other/ek&#45;&#45;facebook.svg" alt=""> <span class="text&#45;&#45;white">Play with Facebook</span></button>-->--}}
{{--                            <a href="/signUpWithFacebook" class="ek--social bg--facebook"--}}
{{--                            ><img src="/static/img/Other/ek--facebook.svg" alt=""/>--}}
{{--                                <span class="text--white">Play with Facebook</span></a--}}
{{--                            >--}}

{{--                            <!--<button class="ek&#45;&#45;social bg&#45;&#45;google"><img src="/static/img/Other/ek&#45;&#45;google.svg" alt=""> <span class="text&#45;&#45;white">Play with Google</span></button>-->--}}
{{--                            <!--href="user"-->--}}
{{--                            <a class="ek--social bg--google"--}}
{{--                            ><img src="/static/img/Other/ek--google.svg" alt=""/>--}}
{{--                                <span class="text--white">Play with Google</span></a--}}
{{--                            >--}}

{{--                            <!-- EK Landing Create Manage Account Social Account -->--}}
{{--                        </form>--}}
                        <!-- EK Landing Create Manage Account Caption -->
                        <div class="ek--manager--caption">
                            <div class="ek--manager--caption--hr bg--navy--600"></div>
                            <p class="ek--manager--caption--email text--navy--200">
                                Register with Email
                            </p>
                            <div class="ek--manager--caption--hr bg--navy--600"></div>
                        </div>
                        <!-- End EK Landing Create Manage Account Caption -->
                        <form onsubmit="return false;">
                            <!-- EK Landing Create Manage Account Name Email Password -->
                            <div class="ek--footer--body--form--group">
                                <label for="ek--manager--input" class="ek--footer--body--form--label text--navy--300" >Manager Name</label>
                                <input id="fullName" type="text" class="ek--manager--input bg--navy--1000 text--white" required />
                            </div>
                            <div class="ek--footer--body--form--group">
                                <label for="ek--manager--email" class="ek--footer--body--form--label text--navy--300" >Email</label>
                                <input type="email" id="email" class="ek--manager--email bg--navy--1000 text--white" required />
                            </div>
                            <!-- Password Eye  -->
                            <div class="ek--footer--body--form--group ek--password">
                                <label for="ek--manager--password" class="ek--footer--body--form--label text--navy--300" >Password</label>
                                <input type="password" id="password" class="ek--manager--password bg--navy--1000 text--white ek--password" required />
                                <small id="regMessage" class="ek--validation text--red" >
                                    <svg class="icon icon-ek--warning ek--svg--size fill--red">
                                        <use xlink:href="/static/img/icons.svg#icon-ek--warning">

                                        </use>
                                    </svg>
                                    <span></span>
                                </small>
                                <!-- Password Show Hide  -->
                                <div class="ek--password--eye">
                                    <svg class="icon icon-eye">
                                        <use xlink:href="/static/img/icons.svg#icon-eye"></use>
                                    </svg>
                                    <svg class="icon icon-eye-2">
                                        <use xlink:href="/static/img/icons.svg#icon-eye-2"></use>
                                    </svg>
                                </div>
                            </div>
                            <!-- End EK Landing Create Manage Account Name Email Password -->
                            <button
                                type="submit"
                                class="ek--create--button--play button--navy--500 text--navy--300 ek--form--button"
                                id="register"
                            >
                                <img
                                    src="/static/img/Dashboard/loading2.png"
                                    style="display: none; wid  25px; height: 25px;"
                                /><span>Play Game</span>
                            </button>
                        </form>
                        <!-- End EK Landing Manage Account Form -->
                        <div class="ek--create--signin">
                            <p class="text--navy--400">You already have a club?</p>
                            <a href="{{ route('login', ['lang' =>app()->getLocale()]) }}" class="text--green ek--sign--button">Sign in</a>
                        </div>
                    </div>
                    <!-- End EK Landing Create Manage Account -->
{{--                    <div--}}
{{--                        id="create-club-social"--}}
{{--                        class="ek--parent bg--navy--700 ek--create"--}}
{{--                        if="${param.check!=null}"--}}
{{--                        style="${param.check==null ? 'display:none;' : 'display:block;'}"--}}
{{--                    >--}}
{{--                        <!-- EK Landing Create Club Modal Head -->--}}
{{--                        <div class="ek--create--head bg--navy--700">--}}
{{--                            <img src="/static/img/Other/ek--create--uniform.svg" alt=""/>--}}
{{--                        </div>--}}
{{--                        <!-- End EK Landing Create Club Modal Head -->--}}
{{--                        <!-- Ek Social Name -->--}}
{{--                        <div class="ek--create--social--name">--}}
{{--                            <div class="ek--create--social--image">--}}
{{--                                <img src="/static/img/ek--user.png" alt=""/>--}}
{{--                            </div>--}}
{{--                            <h4 class="text--white ek--size--28">--}}
{{--                                Hello,--}}
{{--                                <span--}}
{{--                                    class="text--white ek--size--28"--}}
{{--                                    text="${param.name}"--}}
{{--                                ></span>--}}
{{--                            </h4>--}}
{{--                        </div>--}}
{{--                        <!-- End Ek Social Name -->--}}
{{--                        <!-- EK Landing Create Club Modal Preface -->--}}
{{--                        <p class="ek--create--preface text--navy--200">--}}
{{--                            Please create a club--}}
{{--                        </p>--}}
{{--                        <!-- Ends EK Landing Create Club Modal Preface -->--}}
{{--                        <!-- EK Landing Create Club Form  -->--}}
{{--                        <form action="" class="ek--create--form">--}}
{{--                            <!-- EK Landing Create Club Form  Club Name -->--}}

{{--                            <div class="ek--footer--body--form--group">--}}
{{--                                <label--}}
{{--                                    for="ek--create--input"--}}
{{--                                    class="ek--footer--body--form--label text--navy--300"--}}
{{--                                >Choose club name</label--}}
{{--                                >--}}
{{--                                <input--}}
{{--                                    id="socialClubName"--}}
{{--                                    type="text"--}}
{{--                                    class="ek--create--input bg--navy--1000 text--white"--}}
{{--                                    required--}}
{{--                                />--}}
{{--                            </div>--}}
{{--                            <!-- End EK Landing Create Club Form  Club Name -->--}}
{{--                            <!-- EK Landing Create Club Form  Club Country -->--}}
{{--                            <div class="ek--footer--body--form--group">--}}
{{--                                <!-- EK Landing Create Club Form  Club Country Input -->--}}
{{--                                <input--}}
{{--                                    id="socialEmail"--}}
{{--                                    type="hidden"--}}
{{--                                    value="${param.email}"--}}
{{--                                    style="display: none;"--}}
{{--                                />--}}
{{--                                <input--}}
{{--                                    id="socialName"--}}
{{--                                    type="hidden"--}}
{{--                                    value="${param.name}"--}}
{{--                                    style="display: none;"--}}
{{--                                />--}}
{{--                                <input--}}
{{--                                    id="socialPass"--}}
{{--                                    type="hidden"--}}
{{--                                    value="${param.password}"--}}
{{--                                    style="display: none;"--}}
{{--                                />--}}
{{--                                <label--}}
{{--                                    for="ek--create--select"--}}
{{--                                    class="ek--create--label text--navy--300"--}}
{{--                                >Country / Region</label--}}
{{--                                >--}}
{{--                                <input--}}
{{--                                    type="text"--}}
{{--                                    id="socialCountryId"--}}
{{--                                    data-id="1"--}}
{{--                                    value="Azerbaijan"--}}
{{--                                    class="ek--create--select bg--navy--1000 text--white"--}}
{{--                                    required--}}
{{--                                />--}}
{{--                                <!-- End EK Landing Create Club Form  Club Country Input -->--}}
{{--                                <!-- EK Landing Create Club Form  Club Country Content -->--}}
{{--                                <div class="ek--create--icon">--}}
{{--                                    <svg class="icon icon-ek--arrow--up">--}}
{{--                                        <use xlink:href="/static/img/icons.svg#icon-ek--arrow--up"></use>--}}
{{--                                    </svg>--}}
{{--                                    <svg class="icon icon-ek--arrow--down">--}}
{{--                                        <use--}}
{{--                                            xlink:href="/static/img/icons.svg#icon-ek--arrow--down"--}}
{{--                                        ></use>--}}
{{--                                    </svg>--}}
{{--                                </div>--}}
{{--                                <div class="ek--create--content bg--navy--800">--}}
{{--                                    <input--}}
{{--                                        type="text"--}}
{{--                                        class="ek--create--search text--white"--}}
{{--                                        placeholder="Search country"--}}
{{--                                    />--}}

{{--                                    <ul--}}
{{--                                        id="ek--create--content--nav"--}}
{{--                                        class="ek--create--content--nav"--}}
{{--                                    >--}}
{{--                                        <li--}}
{{--                                            class="ek--create--content--item"--}}
{{--                                            each="country : ${countries}"--}}
{{--                                        >--}}
{{--                                            <a class="text--navy--200" href="javascript:void(0)"--}}
{{--                                            ><span--}}
{{--                                                    id="countryId"--}}
{{--                                                    class="ek--create--content--link text--navy--200"--}}
{{--                                                    data-id="${country.id}"--}}
{{--                                                    text="${country.name}"--}}
{{--                                                ></span--}}
{{--                                                ></a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                                <!-- End EK Landing Create Club Form  Club Country Content -->--}}
{{--                            </div>--}}
{{--                            <!-- End EK Landing Create Club Form  Club Country -->--}}
{{--                            <button--}}
{{--                                if="${param.check!=null}"--}}
{{--                                style="${param.check==1 ? 'display:none;' : 'display:block;'}"--}}
{{--                                type="submit"--}}
{{--                                class="ek--create--button--form ek--form--button"--}}
{{--                            >--}}
{{--                                <a--}}
{{--                                    class="button--navy--500 text--navy--300"--}}
{{--                                    href="#ek--manager"--}}
{{--                                    id="signUpFacebook"--}}
{{--                                >Create a Club</a--}}
{{--                                >--}}
{{--                            </button>--}}
{{--                            <button--}}
{{--                                if="${param.check!=null}"--}}
{{--                                style="${param.check==1 ? 'display:block;' : 'display:none;'}"--}}
{{--                                type="submit"--}}
{{--                                class="ek--create--button--form ek--form--button"--}}
{{--                            >--}}
{{--                                <a--}}
{{--                                    class="button--navy--500 text--navy--300"--}}
{{--                                    href="#ek--manager"--}}
{{--                                    id="signUpWithGoogle"--}}
{{--                                >Create a Club</a--}}
{{--                                >--}}
{{--                            </button>--}}
{{--                            <small--}}
{{--                                id="registerFaceBookMessage"--}}
{{--                                class="ek--validation text--red"--}}
{{--                            >--}}
{{--                                <svg class="icon icon-ek--warning ek--svg--size fill--red">--}}
{{--                                    <use xlink:href="/static/img/icons.svg#icon-ek--warning"></use>--}}
{{--                                </svg--}}
{{--                                >--}}
{{--                                <span></span--}}
{{--                                ></small>--}}
{{--                        </form>--}}
{{--                        <!-- End EK Landing Create Club Form  -->--}}
{{--                        <!-- <a href="#" rel="modal:close">Close</a> -->--}}
{{--                        <div class="ek--create--signin">--}}
{{--                            <p class="text--navy--400">You already have a club?</p>--}}
{{--                            <a href="{{ route('login') }}" class="text--green ek--sign--button"--}}
{{--                            >Sign in</a--}}
{{--                            >--}}
{{--                        </div>--}}
{{--                    </div>--}}


                    <div
                        id="ek--create--verifycode"
                        class="bg--navy--700 ek--create--sign ek--manager ek--parent"
                        style="display: none;"
                    >
                        <!-- EK Landing Create Manage Account  Modal Head -->
                        <div class="ek--create--head bg--navy--700">
                            <img src="/static/img/Other/ek--lock.svg" alt=""/>
                        </div>
                        <!-- End EK Landing Create Manage Account  Modal Head -->
                        <!-- EK Landing Manage Account Title -->
                        <a
                            id="registerBack"
                            class="ek--manager--back bg--navy--700"
                            href="#create-club"
                        ><img src="/static/img/Other/ek--left--arrow.svg" alt=""
                            /></a>
                        <h4 class="ek--create--title text--white">Verification</h4>
                        <!-- End EK Landing Manage Account Title -->
                        <!-- EK Landing Create Club Modal Preface -->
                        <p class="ek--create--preface text--navy--200">
                            You have sent a code to verify
                            <span
                                class="ek--create--preface text--navy--200"
                                id="verifyEmail"
                            ></span>
                        </p>
                        <!-- Ends EK Landing Create Club Modal Preface -->
                        <small id="regVerifyMessage" class="ek--validation text--red"
                        >
                            <svg class="icon icon-ek--warning ek--svg--size fill--red">
                                <use xlink:href="/static/img/icons.svg#icon-ek--warning"></use>
                            </svg
                            >
                            <span></span
                            ></small>

                        <form action="" onsubmit="return false;">
                            <!-- EK Landing Create Manage Account Name Email Password -->
                            <div class="ek--create--verify">
                                <div class="ek--footer--body--form--group">
                                    <input
                                        id="one"
                                        type="number"
                                        min="0"
                                        max="9"
                                        maxlength="1"
                                        class="quantity ek--manager--email bg--navy--1000 text--white"
                                        style="-webkit-appearance: none;"
                                    />
                                </div>
                                <div class="ek--footer--body--form--group">
                                    <input
                                        id="two"
                                        type="number"
                                        min="0"
                                        max="9"
                                        maxlength="1"
                                        class="quantity ek--manager--email bg--navy--1000 text--white"
                                        style="-webkit-appearance: none;"
                                    />
                                </div>
                                <div class="ek--footer--body--form--group">
                                    <input
                                        id="three"
                                        type="number"
                                        min="0"
                                        max="9"
                                        maxlength="1"
                                        class="quantity ek--manager--email bg--navy--1000 text--white"
                                        style="-webkit-appearance: none;"
                                    />
                                </div>
                                <div class="ek--footer--body--form--group">
                                    <input
                                        id="four"
                                        type="number"
                                        min="0"
                                        max="9"
                                        maxlength="1"
                                        class="quantity ek--manager--email bg--navy--1000 text--white"
                                        style="-webkit-appearance: none;"
                                    />
                                </div>
                                <div class="ek--footer--body--form--group">
                                    <input
                                        id="five"
                                        type="number"
                                        min="0"
                                        max="9"
                                        maxlength="1"
                                        class="quantity ek--manager--email bg--navy--1000 text--white"
                                        style="-webkit-appearance: none;"
                                    />
                                </div>
                                <div class="ek--footer--body--form--group">
                                    <input
                                        id="six"
                                        type="number"
                                        min="0"
                                        max="9"
                                        maxlength="1"
                                        class="quantity ek--manager--email bg--navy--1000 text--white"
                                        style="-webkit-appearance: none;"
                                    />
                                </div>
                            </div>
                            <!-- End EK Landing Create Manage Account Name Email Password -->
                            <button
                                type="submit"
                                id="confirmCode"
                                class="ek--form--button ek--create--button--play button--navy--500 text--navy--300"
                            >
                                <img
                                    src="/static/img/Dashboard/loading2.png"
                                    style="wid  25px; leng  25px; display: none;"
                                />
                                <span>Confirm</span>
                            </button>

                            <a
                                class="ek--create--button--resend"
                                href="#"
                                id="resendCodeReg"
                            >Resend Code</a
                            >
                        </form>
                        <!-- End EK Landing Manage Account Form -->
                    </div>

                    <div id="ek--notification" class="ek--notification bg--navy--700 modal">
                        <div class="ek--notification--body">
                            <!-- Ek Body Squad Message Notification Modal Left -->
                            <div class="ek--notification--left bg--green--dark--two"></div>
                            <!-- End Ek Body Squad Message Notification Modal left -->
                            <!-- Ek Body Squad Message Notification Modal Right -->
                            <div class="ek--notification--right">
                                <div class="ek--notification--right--head">
                                    <div
                                        class="ek--notification--right--image bg--green--dark--two"
                                    >
                                        <svg class="icon icon-ek--correct fill--white">
                                            <use xlink:href="/static/img/icons.svg#icon-ek--correct"></use>
                                        </svg>
                                    </div>
                                    <div class="ek--notification--right--info">
                                        <h4 class="text--white ek--size--20">
                                            Successfully registered!
                                        </h4>
                                        <p class="text--navy--100 ek--size--18">
                                            You have successfully registered.
                                        </p>
                                    </div>
                                </div>
                                <div class="ek--notification--right--cancel">
                                    <a class="text--white bg--navy--600 ek--size--16--500" href="#" id="close" rel="modal:close">Close</a>
                                </div>
                            </div>
                            <!-- End Ek Body Squad Message Notification Modal Right -->
                        </div>
                    </div>
                    <!-- End Ek Body Squad Message Notification Modall -->
                </div>
            </section>
            <!-- End Ek Register Form -->
        </main>
        <!-- =========================
      EndSection -  Main
  ============================== -->
    </div>
@endsection

@push('js')
    <script src="{{ url('static/js/register.js') }}"></script>
@endpush


