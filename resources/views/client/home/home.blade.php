@extends('client.layout.authenticated')
@section('content')

        @php
            $clubs = Session::get("league_standing");
            function sortByPoints($a, $b) {
                return $a['point'] < $b['point'];
            }

            usort($clubs, 'sortByPoints');
        @endphp

        <!-- EK Body Inner -->
        <section class="ek--body--inner">
            <!-- EK Body Inner Team Tools -->
            <div class="ek--body--inner--team">
                <!-- EK Body Inner Club -->
                <div class="ek--body--inner--club bg--navy--700">
                    <!-- EK Body Inner Club Left -->

                    <a  href="{{ route("squad") }}" class="ek--body--inner--club--left">
                        <div class="ek--body--inner--club--hexagon text--navy--900">{{ number_format(Session::get("club")["ability"],0) }}</div>
                        <div class="ek--body--inner--club--flex home--flex">
                            <h2 class="ek--body--inner--club--name text--white">{{ Session::get("club")["clubName"] }} </h2>
                            <p class="ek--body--inner--club--paragraph text--navy--200">
                                <span class="text--navy--200" >League #{{ Session::get("club")["tournamentId"] }}</span>
                                <span>,&nbsp;#
                                    @foreach( $clubs as $count )
                                        <block>
                                            @if(Session::get("club")["clubName"] == $count["clubName"])
                                                <span  class=" text--navy--200">{{ $loop->index+1 }}</span>
                                            @endif
                                        </block>
                                    @endforeach

                                    <span  class=" text--navy--200">{{  __('translation.place') }}</span>
                                </span>
                            </p>
                        </div>
                    </a>
                    <!-- End EK Body Inner Club Left -->

                    <!-- EK Body Inner Club Right -->
                    <div class="ek--body--inner--club--right">
                        <!-- EK Body Inner Club Right Tool -->
                        <a  href="{{ route("budget") }}" class="ek--body--inner--club--tool bg--navy--600">
                            <!-- EK Body Inner Club Right Tool Title-->
                            <h3 class="ek--body--inner--club--title text--navy--200">{{  __('translation.balance') }}</h3>
                            <!-- End EK Body Inner Club Right Tool Title-->

                            <!-- EK Body Inner Club Right Tool Balance-->
                            <div  class="ek--body--inner--club--balance" >
                                <svg class="icon icon-ek--budget">
                                    <use xlink:href="/static/img/icons.svg#icon-ek--budget"></use>
                                </svg>

                                @if(Session::get("finance")["money"] < 1000)

                                    <p class="ek--body--inner--club--amount text--white">
                                        <span class="ek--body--inner--club--amount text--white">{{ Session::get("finance")["money"] }}</span>
                                    </p>

                                    @elseif(Session::get("finance")["money"] >= 1000 && Session::get("finance")["money"] < 1000000)
                                        <p class="ek--body--inner--club--amount text--white">
                                            <span class="ek--body--inner--club--amount text--white">{{ number_format(Session::get("finance")["money"]/1000,1) }}</span>
                                            <span class="ek--body--inner--club--amount text--white">K</span>
                                        </p>

                                    @elseif(Session::get("finance")["money"] >= 1000000 && Session::get("finance")["money"] < 1000000000)
                                        <p class="ek--body--inner--club--amount text--white">
                                            <span class="ek--body--inner--club--amount text--white">{{ number_format(Session::get("finance")["money"]/1000000,1) }}</span>
                                            <span class="ek--body--inner--club--amount text--white">M</span>
                                        </p>

                                    @elseif(Session::get("finance")["money"] >= 1000000000)
                                        <p class="ek--body--inner--club--amount text--white">
                                            <span class="ek--body--inner--club--amount text--white">{{ number_format(Session::get("finance")["money"]/1000000000,1) }}</span>
                                            <span class="ek--body--inner--club--amount text--white">B</span>
                                        </p>

                                @endif
                            </div>
                            <!-- End EK Body Inner Club Right Tool Balance-->
                        </a>
                        <a  href="{{ route("coins") }}" class="ek--body--inner--club--tool bg--navy--600">
                            <!-- EK Body Inner Club Right Tool Title-->
                            <h3 class="ek--body--inner--club--title text--navy--200">{{  __('translation.ek_coin') }}</h3>
                            <!-- End EK Body Inner Club Right Tool Title-->
                            <!-- EK Body Inner Club Right Tool Balance-->
                            <div  class="ek--body--inner--club--balance">
                                <svg class="icon icon-ek--coins">
                                    <use xlink:href="/static/img/icons.svg#icon-ek--coins"></use>
                                </svg>
                                <p class="ek--body--inner--club--amount text--white">{{Session::get("finance")["coins"]}}</p>
                            </div>
                            <!-- End EK Body Inner Club Right Tool Balance-->
                        </a>
                        <a  href="{{ route("myProfile") }}" class="ek--body--inner--club--tool bg--navy--600">
                            <!-- EK Body Inner Club Right Tool Title-->
                            <h3 class="ek--body--inner--club--title text--navy--200">{{  __('translation.points') }}</h3>
                            <!-- End EK Body Inner Club Right Tool Title-->

                            <!-- EK Body Inner Club Right Tool Balance-->
                            <div  class="ek--body--inner--club--balance">
                                <svg class="icon icon-ek--played">
                                    <use xlink:href="/static/img/icons.svg#icon-ek--played"></use>
                                </svg>
                                <p class="ek--body--inner--club--amount text--white">{{ Session::get("finance")["points"] }}</p>
                            </div>
                            <!-- End EK Body Inner Club Right Tool Balance-->
                        </a>
                        <!-- End EK Body Inner Club Right Tool -->
                    </div>
                    <!-- End EK Body Inner Club Right -->
                </div>
                <!-- End EK Body Inner Club -->
            </div>
            <!-- End EK Body Inner Team Tools -->



            <!--geri qaytar yuxarini-->

            <!-- EK Body Inner Matches -->
            <div class="ek--body--matches"  >
                <div class="ek--body--matches--left bg--navy--700"  >
                    <div class="ek--body--matches--head">
                        <h3 class="ek--body--matches--head--title text--white">{{  __('translation.home_next_match') }}</h3>
                        <a class="ek--body--matches--head--fixtures text--green" href="{{ route("leagueMatch") }}">{{  __('translation.league_fixture') }}
                            <svg class="icon icon-right">
                                <use xlink:href="/static/img/icons.svg#icon-right"></use>
                            </svg>
                        </a>
                    </div>


                    @if( Session::get("next_match")[0] != null )
                        <div class="ek--body--matches--inner bg--navy--600">

                            <div class="ek--body--matches--inner--head">
                                <span class="text--green"></span>
                                <span class="text--green">
                                    <span class="text--green">{{ Session::get("next_match")[0]["tournament"]  }},&nbsp;{{  __('translation.raund') }} #</span>
                                </span>
                                <span class="text--green">{{ Session::get("next_match")[0]["round"] }}</span>
                            </div>

                            <div class="ek--body--matches--inner--clubs"  >

                                <div class="ek--body--matches--inner--clubs--left"  >
                                    <div class="ek--body--matches--inner--clubs--name text--right" >
                                        <h3 class="text--white">{{ Session::get("next_match")[0]["homeTeam"]["clubName"] }}</h3>
                                        <div class="ek--body--matches--inner--clubs--country d--flex--end">
                                            <span class="text--navy--200">{{ Session::get("next_match")[0]["homeTeam"]["country"]["name"] }}</span>
                                            <img src="{{ url("static/img/Flags/".Session::get("next_match")[0]["homeTeam"]["country"]["code"].".svg") }}">
                                        </div>
                                    </div>
                                    <div class="ek--body--inner--club--hexagon text--navy--900">{{ number_format(Session::get("next_match")[0]["homeTeam"]["ability"],0) }}</div>
                                </div >

                                <div class="ek--body--matches--inner--clubs--center text--navy--300">VS</div>

                                <div class="ek--body--matches--inner--clubs--right" >
                                    <div class="ek--body--inner--club--hexagon text--navy--900">{{ number_format(Session::get("next_match")[0]["awayTeam"]["ability"],0) }}</div>
                                    <div class="ek--body--matches--inner--clubs--name text--start">
                                        <h3 class="text--white">{{ Session::get("next_match")[0]["awayTeam"]["clubName"] }} </h3>
                                        <div class="ek--body--matches--inner--clubs--country d--flex--start">
                                            <img src="{{ url("static/img/Flags/".Session::get("next_match")[0]["awayTeam"]["country"]["code"].".svg") }}">
                                            <span class="text--navy--200">{{ Session::get("next_match")[0]["awayTeam"]["country"]["name"] }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a class="ek--body--matches--inner--play text--navy--900 button--green" href="{{ route("playMatch") }}">{{  __('translation.home_play_match') }}</a>
                        </div>

                        @else
                        <div class="ek--body--matches--empty" id="newSeasonModal">
                            <div class="ek--body--matches--empty--body bg--navy--600">
                                <div class="ek--body--matches--empty--contnet">
                                    <h3 class="ek--body--matches--empty--title">{{  __('translation.home_start_new_season') }}</h3>
                                    <p class="ek--body--matches--empty--info text--navy--200">{{  __('translation.home_next_season_text') }}</p>
                                    <button class="ek--body--matches--empty--start text--navy--900 button--green" id="startNewSeason">Start</button>
                                </div>
                            </div>
                        </div>
                    @endif

                </div>

                <div class="ek--body--matches--right--own">
                    <div class="ek--body--matches--right bg--navy--700">
                        <div class="ek--body--matches--head">
                            <h3 class="ek--body--matches--head--title text--white ">
                                League
                                #{{ Session::get("club")["tournamentId"] }}</h3>
                            <a class="ek--body--matches--head--fixtures text--green"  href="{{ route("leagueMatch") }}">{{  __('translation.league_standing') }}
                                <svg class="icon icon-right">
                                    <use xlink:href="/static/img/icons.svg#icon-right"></use>
                                </svg>
                            </a>
                        </div>

                        <div class="ek--body--matches--table">
                            <div class="ek--body--matches--table--head">
                                <div class="ek--body--matches--table--head--number ek--size--16 text--navy--200">#</div>
                                <div class="ek--body--matches--table--head--club ek--size--16 text--navy--200">{{  __('translation.league_standing_team_name') }}</div>
                                <div class="ek--body--matches--table--head--played ek--size--16 text--center text--navy--200">{{  __('translation.profile_statistic_played') }}</div>
                                <div class="ek--body--matches--table--head--won ek--size--16 text--center text--navy--200">{{  __('translation.points') }}</div>
                            </div>

                            <div class="ek--body--matches--table--body js--leagueStanding" id="leagueStanding">

                                @foreach( $clubs as $count)
                                    @if($count["clubName"] != Session::get("club")["clubName"])

                                        <div class="ek--body--matches--table--body--inner bg--navy--600">
                                            <div class="ek--body--matches--table--body--number text--white ek--size--16">
                                                <span>{{ $loop->index+1 }}</span>
                                            </div>
                                            <div class="ek--body--matches--table--body--club text--white ek--size--16">{{ $count["clubName"] }}</div>
                                            <div class="ek--body--matches--table--body--played text--white ek--size--16 text--center">{{ $count["gp"] }}</div>
                                            <div class="ek--body--matches--table--body--won text--white ek--size--16 text--center">{{ $count["point"] }}</div>
                                        </div>

                                    @else

                                        <div class="ek--body--matches--table--body--inner bg--navy--600 active--line js--currentCommand">
                                            <div class="ek--body--matches--table--body--number text--yellow ek--size--16">
                                                <span>{{ $loop->index+1 }}</span>
                                            </div>
                                            <div class="ek--body--matches--table--body--club text--yellow ek--size--16">{{ $count["clubName"] }}</div>
                                            <div class="ek--body--matches--table--body--played text--yellow ek--size--16 text--center">{{ $count["gp"] }}</div>
                                            <div class="ek--body--matches--table--body--won text--yellow ek--size--16 text--center">{{ $count["point"] }}</div>
                                        </div>

                                    @endif
                                @endforeach

                            </div>
                        </div>
                    </div>
                    <div class="ek--body--matches--ranking bg--navy--700">
                        <div class="ek--body--matches--head">
                            <h3 class="ek--body--matches--head--title text--white">{{  __('translation.home_ranking') }}</h3>
                            <a class="ek--body--matches--head--fixtures text--green" href="{{ route("gRanking") }}">{{  __('translation.home_ranking') }}
                                <svg class="icon icon-right">
                                    <use xlink:href="/static/img/icons.svg#icon-right"></use>
                                </svg>
                            </a>
                        </div>
                        <div class="ek--body--matches--ranking--body">
                            <div class="ek--body--matches--ranking--own">
                                <h4 class="ek--body--matches--ranking--country">{{ Session::get("club")["country"]["name"] }}</h4>

                                @foreach( Session::get("local_ranks")["currentRanks"] as $count )
                                    <block>
                                        @if( Session::get("club")["id"] == $count["clubId"] )
                                            <strong class="ek--body--matches--ranking--number" style="display: flex">{{ $loop->index+1 }}</strong>
                                        @endif
                                    </block>
                                @endforeach
                                <div class="ek--body--matches--ranking--icons">
                                    <svg class="icon icon-ek--arrow--up fill--green">
                                        <use xlink:href="/static/img/icons.svg#icon-ek--arrow--up"></use>
                                    </svg>
                                </div>
                            </div>
                            <div class="ek--body--matches--ranking--own">
                                <h4 class="ek--body--matches--ranking--country">{{  __('translation.home_global') }}</h4>
                                @foreach( Session::get("global_ranks")["currentRanks"] as $count )
                                    <block>
                                        @if( Session::get("club")["id"] == $count["clubId"] )
                                            <strong class="ek--body--matches--ranking--number" style="display: flex">{{ $loop->index+1 }}</strong>
                                        @endif
                                    </block>
                                @endforeach
                                <div class="ek--body--matches--ranking--icons">
                                    <svg class="icon icon-ek--arrow--down fill--red">
                                        <use xlink:href="/static/img/icons.svg#icon-ek--arrow--down"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="ek--body--inner--team ek--body--inner--4--section">

                <!-- EK Body Inner Line Up -->
                <a href="{{ route("lineup") }}" class="ek--body--inner--lineup bg--navy--700">
                    <svg class="icon icon-ek--lineup">
                        <use xlink:href="/static/img/icons.svg#icon-ek--lineup"></use>
                    </svg>
                    <h3 class="ek--body--inner--lineup--title text--navy--200">{{  __('translation.menu_lineup') }}</h3>
                </a>
                <!-- End EK Body Inner Line Up -->

                <!-- EK Body Inner Training -->
                <a href="{{ route("training") }}" class="ek--body--inner--training bg--navy--700" >
                    <svg class="icon icon-ek--daily">
                        <use xlink:href="/static/img/icons.svg#icon-ek--daily"></use>
                    </svg>
                    <h3 class="ek--body--inner--training--title text--navy--200">{{  __('translation.menu_training') }}</h3>
                </a>
                <!-- End EK Body Inner Training -->

                <!-- EK Body Inner Stadium -->
                <a href="{{ route("stadium") }}" class="ek--body--inner--stadium bg--navy--700">
                    <img src="{{ url("/static/img/stadium-green.png") }}" alt="">
                    <h3 class="ek--body--inner--lineup--title text--navy--200">{{  __('translation.menu_stadium') }}</h3>
                </a>
                <!-- End EK Body Inner Stadium -->

                <!-- EK Body Inner Tournaments -->
                <a href="{{ route("tournaments") }}" class="ek--body--inner--tournaments bg--navy--700" >
                    <svg class="icon icon-ek--challenge">
                        <use xlink:href="/static/img/icons.svg#icon-ek--challenge"></use>
                    </svg>
                    <h3 class="ek--body--inner--training--title text--navy--200">{{  __('translation.menu_tournaments') }}</h3>
                </a>
                <!-- End EK Body Inner Tournaments -->

            </div>
            <!-- End EK Body Inner Matches -->
        </section>

@endsection

@section('javascript')
    <script src="{{ url('static/js/home.js?v=1') }}"></script>
@endsection
