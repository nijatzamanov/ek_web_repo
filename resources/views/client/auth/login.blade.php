<!DOCTYPE html>
<html lang="en" xmlns:th="http://thymeleaf.org" xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta charset="utf-8">

    <title> Eleven Kings </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf" content="{{ csrf_token() }}">

    <!-- Template Basic Images Start -->
    {{--<link rel="apple-touch-icon" sizes="57x57" href="/static/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/static/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/static/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/static/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/static/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/static/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/static/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/static/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/static/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/static/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/static/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/static/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/static/img/favicon/favicon-16x16.png">--}}
    <!-- <link rel="manifest" href="/manifest.json"> -->
    <link rel="icon" type="image/png" href="{{ url('/static/img/favicon/fav.png') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/static/img/favicon/ms-icon-144x144.png">
    <meta property="og:image" content="/static/img/og/img.png">
    <!-- Template Basic Images End -->

    <!-- Custom Browsers Color Start -->
    <meta name="theme-color" content="#000">
    <!-- Custom Browsers Color End -->

    <link rel="stylesheet" href="/static/css/app.min.css">

</head>
<body>







<!-- =========================
    Section -  Wrapper
============================== -->
<div class="ek--wrapper">
    <!-- =========================
        Section -  Header
    ============================== -->
    <header class="ek--header">
        <!-- EK Header logo -->
        <div class="ek--header--logo">
            <a href="{{ route('overview') }}"><img src="/static/img/Logo/Ek--logo.svg" alt="ek--logo"></a>
            <div class="ek--header--logo--close">
                <svg class="icon icon-ek--close--icon fill--navy--200"><use xlink:href="/static/img/icons.svg#icon-ek--close--icon"></use></svg>
            </div>
        </div>
        <!-- End EK Header logo -->
        <!-- EK Header Navbar -->
        <ul class="ek--header--nav">
            <li class="ek--header--item"><a class="ek--header--link ek--header--link--active"  href="{{ route('overview') }}"><svg class="icon icon-ek--overwiev"><use xlink:href="/static/img/icons.svg#icon-ek--overwiev"></use></svg> Overview</a></li>
        </ul>

        <ul class="ek--header--nav">
            <li class="ek--header--item"><a class="ek--header--link"  href="@{/squad}"><svg class="icon icon-ek--squad"><use xlink:href="/static/img/icons.svg#icon-ek--squad"></use></svg> Squad</a></li>
            <li class="ek--header--item"><a class="ek--header--link"  href="@{/lineup}"><svg class="icon icon-ek--lineup"><use xlink:href="/static/img/icons.svg#icon-ek--lineup"></use></svg> Lineup & Tactics</a></li>
            <li class="ek--header--item"><a class="ek--header--link "  href="@{/daily}"><svg class="icon icon-ek--daily"><use xlink:href="/static/img/icons.svg#icon-ek--daily"></use></svg> Daily Training</a></li>
            <li class="ek--header--item"><a class="ek--header--link"  href="@{/training}"><svg class="icon icon-ek--camp"><use xlink:href="/static/img/icons.svg#icon-ek--camp"></use></svg> Training Camp</a></li>
            <li class="ek--header--item"><a class="ek--header--link"  href="@{/transfer}"> <svg class="icon icon-ek--transfer"><use xlink:href="/static/img/icons.svg#icon-ek--transfer"></use></svg> Transfer</a></li>
            <li class="ek--header--item"><a class="ek--header--link"  href="@{/statistics}"><svg class="icon icon-ek--statistics"><use xlink:href="/static/img/icons.svg#icon-ek--statistics"></use></svg> Statistics</a></li>
        </ul>

        <ul class="ek--header--nav">
            <li class="ek--header--item"><a class="ek--header--link"  href="@{/league-match}"><svg class="icon icon-ek--league"><use xlink:href="/static/img/icons.svg#icon-ek--league"></use></svg> League</a></li>
            <li class="ek--header--item"><a class="ek--header--link"  href="@{/cup}"><svg class="icon icon-ek--cup"><use xlink:href="/static/img/icons.svg#icon-ek--cup"></use></svg> Cup</a></li>
            <li class="ek--header--item"><a class="ek--header--link"  href="@{/challenge}"><svg class="icon icon-ek--challenge"><use xlink:href="/static/img/icons.svg#icon-ek--challenge"></use></svg> Challenge</a></li>
            <li class="ek--header--item"><a class="ek--header--link"  href="@{/friendly}"> <svg class="icon icon-ek--friendly"><use xlink:href="/static/img/icons.svg#icon-ek--friendly"></use></svg> Friendly</a></li>
        </ul>

        <ul class="ek--header--nav">
            <li class="ek--header--item"><a class="ek--header--link"  href="@{/coins}"><svg class="icon icon-ek--coins"><use xlink:href="/static/img/icons.svg#icon-ek--coins"></use></svg> EK Coins</a></li>
            <li class="ek--header--item"><a class="ek--header--link"  href="@{/budget}"><svg class="icon icon-ek--budget"><use xlink:href="/static/img/icons.svg#icon-ek--budget"></use></svg> Budget</a></li>
            <li class="ek--header--item"><a class="ek--header--link"  href="@{/marketing}"><svg class="icon icon-ek--marketing"><use xlink:href="/static/img/icons.svg#icon-ek--marketing"></use></svg> Marketing</a></li>
        </ul>

        <ul class="ek--header--nav">
            <li class="ek--header--item"><a class="ek--header--link"  href="@{/setting}"><svg class="icon icon-ek--settings"><use xlink:href="/static/img/icons.svg#icon-ek--settings"></use></svg> Settings</a></li>
        </ul>
        <!-- End EK Header Navbar -->
    </header>
    <!-- =========================
        End Section -  Header
    ============================== -->
    <!-- =========================
        Section -  Main
    ============================== -->
    <main class="ek--body">
        <!-- EK Body Head -->
        <section class="ek--body--head">
            <!-- EK Body Head Left -->
            <div class="ek--body--head--left">

                <!-- EK Body Mobile Menu -->
                <div class="ek--body--head--mobile--icon">
                    <svg class="icon icon-ek--mobile--menu fill--green"><use xlink:href="/static/img/icons.svg#icon-ek--mobile--menu"></use></svg>
                </div>
                <!-- End EK Body Mobile Menu -->
            </div>
            <!-- EnD EK Body Head Left -->
            <div class="ek--body--head--center">
                <a href="{{ route('overview') }}">
                    <img src="/static/img/Logo/Ek--logo.svg" alt="ek--logo">
                </a>
            </div>
            <!-- EK Body Head Right -->
            <div class="ek--body--head--right">
                <!-- Ek Body Head Money -->
                <div class="ek--body--head--money">
                    <!-- Ek Body Head Money Dollar -->
                    <div class="ek--body--head--money--dollar bg--navy--700">
                        <svg class="icon icon-ek--budget fill--green"><use xlink:href="/static/img/icons.svg#icon-ek--budget"></use></svg>
                        <p class="text--white ek--size--16">14,2 M</p>
                    </div>
                    <!-- End Ek Body Head Money Dollar -->
                    <!-- Ek Body Head Money Dollar -->
                    <div class="ek--body--head--money--coins bg--navy--700">
                        <svg class="icon icon-ek--coins fill--yellow"><use xlink:href="/static/img/icons.svg#icon-ek--coins"></use></svg>
                        <p class="text--white ek--size--16">70,290</p>
                    </div>
                    <!-- End Ek Body Head Money Dollar -->
                </div>
                <!-- End Ek Body Head Money -->
                <!-- EK Body Head User -->
                <div class="ek--body--head--user">
                    <!-- EK Body Head User Inner -->
                    <div class="ek--body--head--inner">
                        <!-- EK Body Head User Image -->
                        <div class="ek--body--head--image">
                            <img src="/static/img/Other/ek--null--image.png" alt="ek--user">
                        </div>
                        <!-- End EK Body Head User Image -->
                        <!-- EK Body Head User Name and Level -->
                        <div class="ek--body--head--name">
                            <!-- EK Body Head User Name Title -->
                            <h1 class="ek--body--head--name--title text--white">Kanan Hashimov</h1>
                            <!-- End EK Body Head User Name Title -->
                            <!-- EK Body Head User Levet -->
                            <p class="ek--body--head--name--level text--yellow">Level 2</p>
                            <!-- End EK Body Head User Levet -->
                        </div>
                        <!-- End EK Body Head User Name and Level -->
                        <!-- EK Body Down and Up Icons -->
                        <div class="ek--body--head--icons">
                            <svg class="icon icon-ek--arrow--up"><use xlink:href="/static/img/icons.svg#icon-ek--arrow--up"></use></svg>
                            <svg class="icon icon-ek--arrow--down"><use xlink:href="/static/img/icons.svg#icon-ek--arrow--down"></use></svg>
                        </div>
                        <!-- End EK Body Down and Up Icons -->
                        <!-- Ek Body Profil Dropdown  -->
                        <div class="ek--body--user">
                            <!-- Ek Body Profil Dropdown Menu  -->
                            <div class="ek--body--user--menu">
                                <a  href="@{/myprofile}">My profile</a>
                                <a  href="@{/edit}">Profile settings</a>
                            </div>
                            <!-- End Ek Body Profil Dropdown Menu  -->
                            <!-- Ek Body Profil Dropdown Logout  -->
                            <div class="ek--body--user--logout">
                                <a href="#">Log out</a>
                            </div>
                            <!-- End Ek Body Profil Dropdown Logout  -->
                        </div>
                        <!-- End Ek Body Profil Dropdown  -->
                    </div>
                    <!-- End EK Body Head User Inner -->
                </div>
                <!-- End EK Body Head User -->
            </div>
            <!-- EnD EK Body Head Right -->
        </section>
        <!-- End EK Body Head -->


        <!-- EK Body Inner -->
        <section class="ek--body--inner">
            <!-- EK Body Inner Team Tools -->
            <div class="ek--body--inner--team">
                <!-- EK Body Inner Club -->
                <div class="ek--body--inner--club bg--navy--700">
                    <!-- EK Body Inner Club Left -->

                    <a  href="{{ route("squad") }}" class="ek--body--inner--club--left">
                        <div class="ek--body--inner--club--hexagon text--navy--900">72</div>
                        <div class="ek--body--inner--club--flex home--flex">
                            <h2 class="ek--body--inner--club--name text--white">Karabakh</h2>
                            <p class="ek--body--inner--club--paragraph text--navy--200">
                                <span class="text--navy--200" >League 5, #6 place</span>
                                </span>
                            </p>
                        </div>
                    </a>
                    <!-- End EK Body Inner Club Left -->

                    <!-- EK Body Inner Club Right -->
                    <div class="ek--body--inner--club--right">
                        <!-- EK Body Inner Club Right Tool -->
                        <a  href="{{ route("budget") }}" class="ek--body--inner--club--tool bg--navy--600">
                            <!-- EK Body Inner Club Right Tool Title-->
                            <h3 class="ek--body--inner--club--title text--navy--200">{{  __('translation.balance') }}</h3>
                            <!-- End EK Body Inner Club Right Tool Title-->

                            <!-- EK Body Inner Club Right Tool Balance-->
                            <div  class="ek--body--inner--club--balance" >
                                <svg class="icon icon-ek--budget">
                                    <use xlink:href="/static/img/icons.svg#icon-ek--budget"></use>
                                </svg>
                                <span class="text--white">14,2M</span>
                            </div>
                            <!-- End EK Body Inner Club Right Tool Balance-->
                        </a>
                        <a  href="{{ route("coins") }}" class="ek--body--inner--club--tool bg--navy--600">
                            <!-- EK Body Inner Club Right Tool Title-->
                            <h3 class="ek--body--inner--club--title text--navy--200">{{  __('translation.ek_coin') }}</h3>
                            <!-- End EK Body Inner Club Right Tool Title-->
                            <!-- EK Body Inner Club Right Tool Balance-->
                            <div  class="ek--body--inner--club--balance">
                                <svg class="icon icon-ek--coins">
                                    <use xlink:href="/static/img/icons.svg#icon-ek--coins"></use>
                                </svg>
                                <p class="ek--body--inner--club--amount text--white">70,290</p>
                            </div>
                            <!-- End EK Body Inner Club Right Tool Balance-->
                        </a>
                        <a  href="{{ route("myProfile") }}" class="ek--body--inner--club--tool bg--navy--600">
                            <!-- EK Body Inner Club Right Tool Title-->
                            <h3 class="ek--body--inner--club--title text--navy--200">{{  __('translation.points') }}</h3>
                            <!-- End EK Body Inner Club Right Tool Title-->

                            <!-- EK Body Inner Club Right Tool Balance-->
                            <div  class="ek--body--inner--club--balance">
                                <svg class="icon icon-ek--played">
                                    <use xlink:href="/static/img/icons.svg#icon-ek--played"></use>
                                </svg>
                                <p class="ek--body--inner--club--amount text--white">1530</p>
                            </div>
                            <!-- End EK Body Inner Club Right Tool Balance-->
                        </a>
                        <!-- End EK Body Inner Club Right Tool -->
                    </div>
                    <!-- End EK Body Inner Club Right -->
                </div>
                <!-- End EK Body Inner Club -->
            </div>
            <!-- End EK Body Inner Team Tools -->



            <!--geri qaytar yuxarini-->

            <!-- EK Body Inner Matches -->
            <div class="ek--body--matches"  >
                <div class="ek--body--matches--left bg--navy--700"  >
                    <div class="ek--body--matches--head">
                        <h3 class="ek--body--matches--head--title text--white">Next match</h3>
                        <a class="ek--body--matches--head--fixtures text--green" href="#">Fixture
                            <svg class="icon icon-right">
                                <use xlink:href="/static/img/icons.svg#icon-right"></use>
                            </svg>
                        </a>
                    </div>


                    <div class="ek--body--matches--inner bg--navy--600">

                        <div class="ek--body--matches--inner--head">
                            <span class="text--green"></span>
                            <span class="text--green">
                                    <span class="text--green">Kings League 5, Round #</span>
                                </span>
                            <span class="text--green">9</span>
                        </div>

                        <div class="ek--body--matches--inner--clubs"  >

                            <div class="ek--body--matches--inner--clubs--left"  >
                                <div class="ek--body--matches--inner--clubs--name text--right" >
                                    <h3 class="text--white">Karabakh</h3>
                                    <div class="ek--body--matches--inner--clubs--country d--flex--end">
                                        <span class="text--navy--200">Azerbaijan</span>
                                        <img src="{{ url("static/img/Flags/AZ.svg") }}">
                                    </div>
                                </div>
                                <div class="ek--body--inner--club--hexagon text--navy--900">72</div>
                            </div >

                            <div class="ek--body--matches--inner--clubs--center text--navy--300">VS</div>

                            <div class="ek--body--matches--inner--clubs--right" >
                                <div class="ek--body--inner--club--hexagon text--navy--900">99</div>
                                <div class="ek--body--matches--inner--clubs--name text--start">
                                    <h3 class="text--white">Real Madrid</h3>
                                    <div class="ek--body--matches--inner--clubs--country d--flex--start">
                                        <img src="{{ url("static/img/Flags/US.svg") }}">
                                        <span class="text--navy--200">USA</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a class="ek--body--matches--inner--play text--navy--900 button--green" href="#">{{  __('translation.home_play_match') }}</a>
                    </div>

                </div>

                <div class="ek--body--matches--right--own">
                    <div class="ek--body--matches--right bg--navy--700">
                        <div class="ek--body--matches--head">
                            <h3 class="ek--body--matches--head--title text--white ">
                                League
                                # 5</h3>
                            <a class="ek--body--matches--head--fixtures text--green"  href="#">{{  __('translation.league_standing') }}
                                <svg class="icon icon-right">
                                    <use xlink:href="/static/img/icons.svg#icon-right"></use>
                                </svg>
                            </a>
                        </div>

                        <div class="ek--body--matches--table">
                            <div class="ek--body--matches--table--head">
                                <div
                                    class="ek--body--matches--table--head--number ek--size--16 text--navy--200"
                                >
                                    #
                                </div>
                                <div
                                    class="ek--body--matches--table--head--club ek--size--16 text--navy--200"
                                >
                                    Club Name
                                </div>
                                <div
                                    class="ek--body--matches--table--head--played ek--size--16 text--center text--navy--200"
                                >
                                    Played
                                </div>
                                <div
                                    class="ek--body--matches--table--head--won ek--size--16 text--center text--navy--200"
                                >
                                    Won
                                </div>
                            </div>

                            <div class="ek--body--matches--table--body">
                                <div
                                    class="ek--body--matches--table--body--inner bg--navy--600"
                                >
                                    <div
                                        class="ek--body--matches--table--body--number text--white ek--size--16"
                                    >
                                        <span>4</span>
                                        <svg class="icon icon-ek--arrow--up fill--green">
                                            <use
                                                xlink:href="/static/img/icons.svg#icon-ek--arrow--up"
                                            ></use>
                                        </svg>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--club text--white ek--size--16"
                                    >
                                        RM CF
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                    >
                                        18
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                    >
                                        12
                                    </div>
                                </div>
                                <div
                                    class="ek--body--matches--table--body--inner bg--navy--600"
                                >
                                    <div
                                        class="ek--body--matches--table--body--number text--white ek--size--16"
                                    >
                                        <span>5</span>
                                        <svg class="icon icon-ek--arrow--down fill--red">
                                            <use
                                                xlink:href="/static/img/icons.svg#icon-ek--arrow--down"
                                            ></use>
                                        </svg>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--club text--white ek--size--16"
                                    >
                                        Burnies
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                    >
                                        18
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                    >
                                        12
                                    </div>
                                </div>
                                <div
                                    class="ek--body--matches--table--body--inner bg--navy--600"
                                >
                                    <div
                                        class="ek--body--matches--table--body--number text--white ek--size--16"
                                    >
                                        <span>6</span>
                                        <svg class="icon icon-ek--arrow--up fill--green">
                                            <use
                                                xlink:href="/static/img/icons.svg#icon-ek--arrow--up"
                                            ></use>
                                        </svg>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--club text--white ek--size--16"
                                    >
                                        Manchester
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                    >
                                        18
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                    >
                                        12
                                    </div>
                                </div>
                                <div
                                    class="ek--body--matches--table--body--inner bg--navy--600"
                                >
                                    <div
                                        class="ek--body--matches--table--body--number text--white ek--size--16"
                                    >
                                        <span>3</span>
                                        <svg class="icon icon-ek--arrow--down fill--red">
                                            <use
                                                xlink:href="/static/img/icons.svg#icon-ek--arrow--down"
                                            ></use>
                                        </svg>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--club text--white ek--size--16"
                                    >
                                        Karabakh FC
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                    >
                                        18
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                    >
                                        12
                                    </div>
                                </div>
                                <div
                                    class="ek--body--matches--table--body--inner bg--navy--600"
                                >
                                    <div
                                        class="ek--body--matches--table--body--number text--white ek--size--16"
                                    >
                      <span>5</span
                      >
                                        <svg class="icon icon-ek--oval fill--navy--300">
                                            <use xlink:href="/static/img/icons.svg#icon-ek--oval"></use>
                                        </svg>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--club text--white ek--size--16"
                                    >
                                        Burnies
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                    >
                                        18
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                    >
                                        12
                                    </div>
                                </div>
                                <div
                                    class="ek--body--matches--table--body--inner bg--navy--600"
                                >
                                    <div
                                        class="ek--body--matches--table--body--number text--white ek--size--16"
                                    >
                                        <span>4</span>
                                        <svg class="icon icon-ek--arrow--up fill--green">
                                            <use
                                                xlink:href="/static/img/icons.svg#icon-ek--arrow--up"
                                            ></use>
                                        </svg>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--club text--white ek--size--16"
                                    >
                                        RM CF
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                    >
                                        18
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                    >
                                        12
                                    </div>
                                </div>
                                <div
                                    class="ek--body--matches--table--body--inner bg--navy--600"
                                >
                                    <div
                                        class="ek--body--matches--table--body--number text--white ek--size--16"
                                    >
                                        <span>3</span>
                                        <svg class="icon icon-ek--arrow--down fill--red">
                                            <use
                                                xlink:href="/static/img/icons.svg#icon-ek--arrow--down"
                                            ></use>
                                        </svg>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--club text--white ek--size--16"
                                    >
                                        Karabakh FC
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                    >
                                        18
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                    >
                                        12
                                    </div>
                                </div>
                                <div
                                    class="ek--body--matches--table--body--inner bg--navy--600"
                                >
                                    <div
                                        class="ek--body--matches--table--body--number text--white ek--size--16"
                                    >
                                        <span>6</span>
                                        <svg class="icon icon-ek--arrow--up fill--green">
                                            <use
                                                xlink:href="/static/img/icons.svg#icon-ek--arrow--up"
                                            ></use>
                                        </svg>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--club text--white ek--size--16"
                                    >
                                        Manchester
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                    >
                                        18
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                    >
                                        12
                                    </div>
                                </div>
                                <div
                                    class="ek--body--matches--table--body--inner bg--navy--600"
                                >
                                    <div
                                        class="ek--body--matches--table--body--number text--white ek--size--16"
                                    >
                                        <span>4</span>
                                        <svg class="icon icon-ek--arrow--up fill--green">
                                            <use
                                                xlink:href="/static/img/icons.svg#icon-ek--arrow--up"
                                            ></use>
                                        </svg>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--club text--white ek--size--16"
                                    >
                                        RM CF
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                    >
                                        18
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                    >
                                        12
                                    </div>
                                </div>
                                <div
                                    class="ek--body--matches--table--body--inner bg--navy--600"
                                >
                                    <div
                                        class="ek--body--matches--table--body--number text--white ek--size--16"
                                    >
                      <span>5</span
                      >
                                        <svg class="icon icon-ek--oval fill--navy--300">
                                            <use xlink:href="/static/img/icons.svg#icon-ek--oval"></use>
                                        </svg>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--club text--white ek--size--16"
                                    >
                                        Burnies
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                    >
                                        18
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                    >
                                        12
                                    </div>
                                </div>
                                <div
                                    class="ek--body--matches--table--body--inner bg--navy--600"
                                >
                                    <div
                                        class="ek--body--matches--table--body--number text--white ek--size--16"
                                    >
                                        <span>4</span>
                                        <svg class="icon icon-ek--arrow--up fill--green">
                                            <use
                                                xlink:href="/static/img/icons.svg#icon-ek--arrow--up"
                                            ></use>
                                        </svg>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--club text--white ek--size--16"
                                    >
                                        RM CF
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                    >
                                        18
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                    >
                                        12
                                    </div>
                                </div>
                                <div
                                    class="ek--body--matches--table--body--inner bg--navy--600"
                                >
                                    <div
                                        class="ek--body--matches--table--body--number text--white ek--size--16"
                                    >
                      <span>5</span
                      >
                                        <svg class="icon icon-ek--oval fill--navy--300">
                                            <use xlink:href="/static/img/icons.svg#icon-ek--oval"></use>
                                        </svg>
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--club text--white ek--size--16"
                                    >
                                        Burnies
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--played text--white ek--size--16 text--center"
                                    >
                                        18
                                    </div>
                                    <div
                                        class="ek--body--matches--table--body--won text--white ek--size--16 text--center"
                                    >
                                        12
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ek--body--matches--ranking bg--navy--700">
                        <div class="ek--body--matches--head">
                            <h3 class="ek--body--matches--head--title text--white">{{  __('translation.home_ranking') }}</h3>
                            <a class="ek--body--matches--head--fixtures text--green" href="#}">{{  __('translation.home_ranking') }}
                                <svg class="icon icon-right">
                                    <use xlink:href="/static/img/icons.svg#icon-right"></use>
                                </svg>
                            </a>
                        </div>
                        <div class="ek--body--matches--ranking--body">
                            <div class="ek--body--matches--ranking--own">
                                <h4 class="ek--body--matches--ranking--country">Azerbaijan</h4>
                                <block>
                                    <strong class="ek--body--matches--ranking--number" style="display: flex">1</strong>
                                </block>
                                <div class="ek--body--matches--ranking--icons">
                                    <svg class="icon icon-ek--arrow--up fill--green">
                                        <use xlink:href="/static/img/icons.svg#icon-ek--arrow--up"></use>
                                    </svg>
                                </div>
                            </div>
                            <div class="ek--body--matches--ranking--own">
                                <h4 class="ek--body--matches--ranking--country">{{  __('translation.home_global') }}</h4>
                                <block>
                                    <strong class="ek--body--matches--ranking--number" style="display: flex">1</strong>
                                </block>
                                <div class="ek--body--matches--ranking--icons">
                                    <svg class="icon icon-ek--arrow--down fill--red">
                                        <use xlink:href="/static/img/icons.svg#icon-ek--arrow--down"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="ek--body--inner--team ek--body--inner--4--section">

                <!-- EK Body Inner Line Up -->
                <a href="#" class="ek--body--inner--lineup bg--navy--700">
                    <svg class="icon icon-ek--lineup">
                        <use xlink:href="{{ url("/static/img/icons.svg#icon-ek--lineup") }}"></use>
                    </svg>
                    <h3 class="ek--body--inner--lineup--title text--navy--200">{{  __('translation.menu_lineup') }}</h3>
                </a>
                <!-- End EK Body Inner Line Up -->

                <!-- EK Body Inner Training -->
                <a href="#" class="ek--body--inner--training bg--navy--700" >
                    <svg class="icon icon-ek--daily">
                        <use xlink:href="/static/img/icons.svg#icon-ek--daily"></use>
                    </svg>
                    <h3 class="ek--body--inner--training--title text--navy--200">{{  __('translation.menu_training') }}</h3>
                </a>
                <!-- End EK Body Inner Training -->

                <!-- EK Body Inner Stadium -->
                <a href="#" class="ek--body--inner--stadium bg--navy--700">
                    <img src="{{ url("/static/img/stadium-green.png") }}" alt="">
                    <h3 class="ek--body--inner--lineup--title text--navy--200">{{  __('translation.menu_stadium') }}</h3>
                </a>
                <!-- End EK Body Inner Stadium -->

                <!-- EK Body Inner Tournaments -->
                <a href="#" class="ek--body--inner--tournaments bg--navy--700" >
                    <svg class="icon icon-ek--challenge">
                        <use xlink:href="/static/img/icons.svg#icon-ek--challenge"></use>
                    </svg>
                    <h3 class="ek--body--inner--training--title text--navy--200">{{  __('translation.menu_tournaments') }}</h3>
                </a>
                <!-- End EK Body Inner Tournaments -->

            </div>
            <!-- End EK Body Inner Matches -->
        </section>



        <!-- End EK Body Inner -->
        <section class="ek--mobile--play">
            <div class="container d-flex justify-content-between alig">
                <div class="ek--body--head--money">
                    <!-- Ek Body Head Money Dollar -->
                    <div class="ek--body--head--money--dollar bg--navy--700">
                        <svg class="icon icon-ek--budget fill--green"><use xlink:href="/static/img/icons.svg#icon-ek--budget"></use></svg>
                        <p class="text--white ek--size--16">14,2 M</p>
                    </div>
                    <!-- End Ek Body Head Money Dollar -->
                    <!-- Ek Body Head Money Dollar -->
                    <div class="ek--body--head--money--coins bg--navy--700">
                        <svg class="icon icon-ek--coins fill--yellow"><use xlink:href="/static/img/icons.svg#icon-ek--coins"></use></svg>
                        <p class="text--white ek--size--16">70,290</p>
                    </div>
                    <!-- End Ek Body Head Money Dollar -->

                </div>
                <a class="ek--mobile--play--next bg--green"  href="@{/play-match}">Next match</a>
            </div>
        </section>
        <!-- Ek Register Form -->
        <section class="ek--register">
            <div class="ek--register--body">


                <div id="ek--create--sign" class="bg--navy--700 ek--create--sign ek--manager ek--parent" style="display: block;">
                    <!-- EK Landing Create Manage Account  Modal Head -->
                    <div class="ek--create--head bg--navy--700">
                        <img src="/static/img/Other/ek--player.svg" alt="">
                    </div>
                    <!-- End EK Landing Create Manage Account  Modal Head -->
                    <!-- EK Landing Manage Account Title -->
                    <h4 class="ek--create--title text--white">Welcome back!</h4>
                    <!-- End EK Landing Manage Account Title -->
                    <!-- EK Landing Manage Account Form -->
{{--                    <form action="">--}}
                        <!-- EK Landing Create Manage Account Social Account -->
                        <!--<button class="ek&#45;&#45;social bg&#45;&#45;facebook"><img src="/static/img/Other/ek&#45;&#45;facebook.svg" alt=""> <span class="text&#45;&#45;white">Play with Facebook</span></button>-->
{{--                        <a href="#" onClick="socialSignin('facebook');" class="ek--social bg--facebook"><img src="/static/img/Other/ek--facebook.svg" alt=""> <span class="text--white">Play with Facebook</span></a>--}}
{{--                        <small  if="${param.check!=null}"   style="${param.check==3 ? 'display:block;' : 'display:none;'}" class="ek--validation text--red"><svg class="icon icon-ek--warning ek--svg--size fill--red"><use xlink:href="/static/img/icons.svg#icon-ek--warning"></use></svg><span  text="${param.message}"></span></small>--}}

{{--                        <!--<button class="ek&#45;&#45;social bg&#45;&#45;google"><img src="/static/img/Other/ek&#45;&#45;google.svg" alt=""> <span class="text&#45;&#45;white">Play with Google</span></button>-->--}}
{{--                        <!--href="user"-->--}}
{{--                        <a href="#" class="ek--social bg--google" onClick="socialSignin('google');">--}}
{{--                            <img src="/static/img/Other/ek--google.svg" alt=""> <span class="text--white">Play with Google</span>--}}
{{--                        </a>--}}

                        <form id="social-login-form" action="" method="POST" style="display: none;">
                            {{ csrf_field() }}
                            <input id="social-login-access-token" name="social-login-access-token" type="text">
                            <input id="social-login-tokenId" name="social-login-tokenId" type="text">
                        </form>
                        <!-- EK Landing Create Manage Account Social Account -->
{{--                    </form>--}}
                    <!-- EK Landing Create Manage Account Caption -->
                    <div class="ek--manager--caption">
                        <div class="ek--manager--caption--hr bg--navy--600"></div>
                        <p class="ek--manager--caption--email text--navy--200">Login with Email</p>
                        <div class="ek--manager--caption--hr bg--navy--600"></div>
                    </div>
                    <!-- End EK Landing Create Manage Account Caption -->
                    <form onsubmit="return false;" >
                        <!-- EK Landing Create Manage Account Name Email Password -->
                        <div class="ek--footer--body--form--group">
                            <label for="ek--manager--email" class="ek--footer--body--form--label text--navy--300">Email</label>
                            <input type="email" id="loginEmail" name="email" class="ek--manager--email bg--navy--1000 text--white" required>
                        </div>
                        <!-- Password Eye -->
                        <div class="ek--footer--body--form--group ek--password">
                            <label for="ek--manager--password" class="ek--footer--body--form--label text--navy--300">Password</label>
                            <input type="password" id="loginPass" name="password" class="ek--manager--password  bg--navy--1000 text--white" required>
                            <small  id="loginError" class="ek--validation text--red">
                                <svg class="icon icon-ek--warning ek--svg--size fill--red"><use xlink:href="/static/img/icons.svg#icon-ek--warning"></use></svg>
                                <span id="message"></span>
                            </small>
                            <!-- Passowrd Show Hide  -->
                            <div class="ek--password--eye">
                                <svg class="icon icon-eye">
                                    <use xlink:href="/static/img/icons.svg#icon-eye"></use>
                                </svg>
                                <svg class="icon icon-eye-2">
                                    <use xlink:href="/static/img/icons.svg#icon-eye-2"></use>
                                </svg>
                            </div>
                        </div>


                        <!-- EK Forget Password -->
                        <div class="ek--create--forget">
                            <a href="#">Forgot your password?</a>
                        </div>
                        <!-- End EK Forget Password -->

                        <!-- End EK Landing Create Manage Account Name Email Password -->
                        <button  type="submit" class="ek--create--button--play ek--form--button button--navy--500 text--navy--300" id="signin"><img src="/static/img/Dashboard/loading2.png" style="display:none;wid 25px;height:25px;"><span>Play Game</span></button>
                    </form>
                    <!-- End EK Landing Manage Account Form -->
                    <div class="ek--create--signin">
                        <p class="text--navy--400">Create Club</p>
                        <a  href="{{ route('overview') }}" class="text--green ek--sign--button--up">Sign up</a>
                    </div>
                </div>

                <div id="ek--create--forget" class="bg--navy--700 ek--create--sign ek--manager ek--parent" style="display: none">
                    <!-- EK Landing Create Manage Account  Modal Head -->
                    <div class="ek--create--head bg--navy--700">
                        <img src="/static/img/Other/ek--lock.svg" alt="">
                    </div>
                    <!-- End EK Landing Create Manage Account  Modal Head -->
                    <a id="forgetPasswordEmailBack" class="ek--manager--back bg--navy--700" href="#create-club"><img src="/static/img/Other/ek--left--arrow.svg" alt=""></a>

                    <!-- EK Landing Manage Account Title -->
                    <h4 class="ek--create--title text--white">Password assistance</h4>
                    <!-- End EK Landing Manage Account Title -->
                    <!-- EK Landing Create Club Modal Preface -->
                    <p class="ek--create--preface text--navy--200">Create a club to play the Eleven Kings</p>
                    <!-- Ends EK Landing Create Club Modal Preface -->
                    <form action="" onsubmit="return false;">
                        <!-- EK Landing Create Manage Account Name Email Password -->
                        <div class="ek--footer--body--form--group ek--password">
                            <label for="ek--manager--email" class="ek--footer--body--form--label text--navy--300">Email</label>
                            <input type="email" class="ek--manager--password bg--navy--1000 text--white" id="forgetPwdEmail" required>
                            <small id="forgetEmailMessage" class="ek--validation text--red">
                                <svg class="icon icon-ek--warning ek--svg--size fill--red">
                                    <use xlink:href="/static/img/icons.svg#icon-ek--warning"></use>
                                </svg>
                                <span></span>
                            </small>
                            <!-- Passowrd Show Hide  -->
                            {{--<div class="ek--password--eye">
                                <svg class="icon icon-eye">
                                    <use xlink:href="/static/img/icons.svg#icon-eye"></use>
                                </svg>
                                <svg class="icon icon-eye-2">
                                    <use xlink:href="/static/img/icons.svg#icon-eye-2"></use>
                                </svg>
                            </div>--}}
                        </div>
                        <!-- End EK Landing Create Manage Account Name Email Password -->
                        <button id="forgetPwd" type="submit" class="ek--create--button--play button--navy--500 text--navy--300 ek--form--button"><img src="/static/img/Dashboard/loading2.png" style="display:none;wid 25px;height:25px;"> <span>Continue</span></button>
                    </form>
                    <!-- End EK Landing Manage Account Form -->
                    <div class="ek--create--signin">
                        <p class="text--navy--400">Do you remember password?</p>
                        <a  href="{{ route('login',['lang' =>app()->getLocale()]) }}" class="text--green ek--sign--button--up">Sign in</a>
                    </div>
                </div>

                <div id="ek--create--send" class="bg--navy--700 ek--create--sign ek--manager ek--parent" style="display: none;">
                    <!-- EK Landing Create Manage Account  Modal Head -->
                    <div class="ek--create--head bg--navy--700">
                        <img src="/static/img/Other/ek--sendmail.svg" alt="">
                    </div>
                    <!-- End EK Landing Create Manage Account  Modal Head -->
                    <a id="sendemailback" class="ek--manager--back bg--navy--700" href="#create-club"><img src="/static/img/Other/ek--left--arrow.svg" alt=""></a>

                    <!-- EK Landing Manage Account Title -->
                    <h4 class="ek--create--title text--white">Send Email</h4>
                    <!-- End EK Landing Manage Account Title -->
                    <!-- EK Landing Create Club Modal Preface -->
                    <p class="ek--create--preface text--navy--200">Please check your inbox and just to make sure, also your junk mail folder</p>
                    <!-- Ends EK Landing Create Club Modal Preface -->
                    <button type="submit" class="ek--create--button--play ek--form--button"><a class="button--green text--navy--700"  id="verifyCode">Verify code</a></button>
                </div>

                <div id="ek--create--change" class="bg--navy--700 ek--create--sign ek--manager ek--parent" style="display: none;">
                    <!-- EK Landing Create Manage Account  Modal Head -->
                    <div class="ek--create--head bg--navy--700">
                        <img src="/static/img/Other/ek--lock.svg" alt="">
                    </div>
                    <!-- End EK Landing Create Manage Account  Modal Head -->
                    <a id="createNewPassBack" class="ek--manager--back bg--navy--700" href="#create-club"><img src="/static/img/Other/ek--left--arrow.svg" alt=""></a>

                    <!-- EK Landing Manage Account Title -->
                    <h4 class="ek--create--title text--white">Create a new password</h4>
                    <!-- End EK Landing Manage Account Title -->
                    <!-- EK Landing Create Club Modal Preface -->
                    <p class="ek--create--preface text--navy--200" id="emaill1"></p>
                    <!-- Ends EK Landing Create Club Modal Preface -->
                    <form action="" onsubmit="return false;">
                        <!-- EK Landing Create Manage Account Name Email Password -->
                        <div class="ek--footer--body--form--group">
                            <label for="ek--manager--email" class="ek--footer--body--form--label text--navy--300">New password</label>
                            <input type="password" class="ek--manager--email bg--navy--1000 text--white" id="newPass" required>
                            <input type="text" hidden id="code">
                            <small id="changePassMessage" class="ek--validation text--red"><svg class="icon icon-ek--warning ek--svg--size fill--red"><use xlink:href="/static/img/icons.svg#icon-ek--warning"></use></svg><span></span></small>

                        </div>
                        <!-- End EK Landing Create Manage Account Name Email Password -->
                        <button id="changePass" type="submit" class="ek--create--button--play button--navy--500 text--navy--300 ek--form--button"><img src="/static/img/Dashboard/loading2.png" style="display:none;wid 25px;height:25px;"><span>Change Password</span></button>
                    </form>
                    <!-- End EK Landing Manage Account Form -->
                </div>

                <div id="ek--create--verifycode" class="bg--navy--700 ek--create--sign ek--manager ek--parent" style="display: none;">
                    <!-- EK Landing Create Manage Account  Modal Head -->
                    <div class="ek--create--head bg--navy--700">
                        <img src="/static/img/Other/ek--lock.svg" alt="">
                    </div>
                    <!-- End EK Landing Create Manage Account  Modal Head -->
                    <a id="forgetPassBack" class="ek--manager--back bg--navy--700" href="#create-club"><img src="/static/img/Other/ek--left--arrow.svg" alt=""></a>

                    <!-- EK Landing Manage Account Title -->
                    <h4 class="ek--create--title text--white">Verification</h4>
                    <!-- End EK Landing Manage Account Title -->
                    <!-- EK Landing Create Club Modal Preface -->
                    <p class="ek--create--preface text--navy--200" id="emaill"></p>
                    <!-- Ends EK Landing Create Club Modal Preface -->
                    <small id="forgetVerifyMessage" class="ek--validation text--red"><svg class="icon icon-ek--warning ek--svg--size fill--red"><use xlink:href="/static/img/icons.svg#icon-ek--warning"></use></svg><span></span></small>

                    <form action="" onsubmit="return false;">
                        <!-- EK Landing Create Manage Account Name Email Password -->
                        <div class="ek--create--verify">
                            <div class="ek--footer--body--form--group">
                                <input id="one" type="number" min="0" max="9" maxlength="1" style="-webkit-appearance: none;" class="quantity ek--manager--email bg--navy--1000 text--white">
                            </div>
                            <div class="ek--footer--body--form--group">
                                <input id="two" type="number" min="0" max="9" maxlength="1" style="-webkit-appearance: none;" class="quantity ek--manager--email bg--navy--1000 text--white">
                            </div>
                            <div class="ek--footer--body--form--group">
                                <input id="three" type="number" min="0" max="9" maxlength="1" style="-webkit-appearance: none;" class="quantity ek--manager--email bg--navy--1000 text--white">
                            </div>
                            <div class="ek--footer--body--form--group">
                                <input id="four" type="number" min="0" max="9" maxlength="1" style="-webkit-appearance: none;" class="quantity ek--manager--email bg--navy--1000 text--white">
                            </div>
                            <div class="ek--footer--body--form--group">
                                <input id="five" type="number" min="0" max="9" maxlength="1" style="-webkit-appearance: none;" class="quantity ek--manager--email bg--navy--1000 text--white">
                            </div>
                            <div class="ek--footer--body--form--group">
                                <input id="six" type="number" min="0" max="9" maxlength="1" style="-webkit-appearance: none;" class="quantity ek--manager--email bg--navy--1000 text--white">
                            </div>
                        </div>
                        <!-- End EK Landing Create Manage Account Name Email Password -->
                        <button type="submit" class="ek--create--button--play button--navy--500 text--navy--300 ek--form--button" id="confirmCode"><img src="/static/img/Dashboard/loading2.png" style="display:none; wid 25px;height:25px;"> <span>Confirm</span></a></button>
                        <a class="ek--create--button--resend" href="#" id="resendCode">Resend Code</a>

                    </form>
                    <!-- End EK Landing Manage Account Form -->
                </div>

                <div id="ek--create--success" class="bg--navy--700 ek--create--sign ek--manager" style="display: none;">
                    <!-- EK Landing Create Manage Account  Modal Head -->
                    <div class="ek--create--head bg--navy--700">
                        <img src="/static/img/Other/ek-check.svg" alt="">
                    </div>
                    <!-- End EK Landing Create Manage Account  Modal Head -->

                    <!-- EK Landing Manage Account Title -->
                    <h4 class="ek--create--title text--white">Successfully changed</h4>
                    <!-- End EK Landing Manage Account Title -->
                    <!-- EK Landing Create Club Modal Preface -->
                    <p class="ek--create--preface text--navy--200">You are directed to the game.</p>
                    <!-- Ends EK Landing Create Club Modal Preface -->
                    <button type="submit" class="ek--create--button--play ek--form--button "><a class="button--green text--navy--700 "  href="{{ route('home') }}" >Play Game</a></button>
                </div>

            </div>
        </section>
        <!-- End Ek Register Form -->
    </main>
    <!-- =========================
        EndSection -  Main
    ============================== -->
</div>
<!-- =========================
    End Section -  Wrapper
============================== -->

<script src="https://www.gstatic.com/firebasejs/7.17.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.17.0/firebase-analytics.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.17.0/firebase-auth.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="/static/js/libs.min.js"></script>
<script src="/static/js/common.js"></script>
<script src="/static/js/login.js"></script>

<script>
    // For Firebase JS SDK v7.20.0 and later, measurementId is optional
    var firebaseConfig = {
        apiKey: "AIzaSyCUiNkJtRU7aesLJbV7SBIzLNvk3Vr7Km8",
        authDomain: "elevenkings-1fb92.firebaseapp.com",
        databaseURL: "https://elevenkings-1fb92.firebaseio.com",
        projectId: "elevenkings-1fb92",
        storageBucket: "elevenkings-1fb92.appspot.com",
        messagingSenderId: "159055200810",
        appId: "1:159055200810:web:5442d6686cc7442bf83953",
        measurementId: "G-0DVEH4Q1Y9"
    };

    // var firebaseConfig = {
    //     apiKey: "AIzaSyAGJNXqXR0IJ_8WSNa_8KuIqn1yAHNCadU",
    //     authDomain: "eleven-kings-pro.firebaseapp.com",
    //     databaseURL: "https://eleven-kings-pro.firebaseio.com",
    //     projectId: "eleven-kings-pro",
    //     storageBucket: "eleven-kings-pro.appspot.com",
    //     messagingSenderId: "420735125105",
    //     appId: "1:420735125105:web:54acf691e83c0cd02d3dd0",
    //     measurementId: "G-7YYLJFTNFG"
    // };
    // Initialize Firebase


    // var firebaseConfig = {
    //     apiKey: "AIzaSyCkYLRHF3mvbSobeg-YQAHMv6TBunazKII",
    //     authDomain: "ektest-3b643.firebaseapp.com",
    //     projectId: "ektest-3b643",
    //     storageBucket: "ektest-3b643.appspot.com",
    //     messagingSenderId: "529914762775",
    //     appId: "1:529914762775:web:e4e6e1c050c184751b2e52",
    //     measurementId: "G-EH3340ZE5T"
    // };

    firebase.initializeApp(firebaseConfig);
    firebase.analytics();

    var facebookProvider = new  firebase.auth.FacebookAuthProvider();
    var googleProvider = new firebase.auth.GoogleAuthProvider();
    var facebookCallbackLink = '/login/facebook/callback';
    var googleCallbackLink = '/login/google/callback';


    async function socialSignin(provider) {
        var socialProvider = null;
        if (provider == "facebook") {
            socialProvider = facebookProvider;
            document.getElementById('social-login-form').action = facebookCallbackLink;
        } else if (provider == "google") {
            socialProvider = googleProvider;
            document.getElementById('social-login-form').action = googleCallbackLink;
        } else {
            return;
        }
        firebase.auth().signInWithPopup(socialProvider).then(function(result) {
            result.user.getIdToken().then(function(result) {
                // console.log(result)
                document.getElementById('social-login-tokenId').value = result;
                document.getElementById('social-login-form').submit();
            });
        }).catch(function(error) {
            if (error.code == 'auth/account-exists-with-different-credential'){
                socialSignin('google')
            }
            // do error handling
            console.log(error);
        });
    }
</script>

</body>
</html>

