@extends('client.layout.authenticated')
@section('content')

    <!-- EK Body Squad -->
    <section class="ek--squad">
        <!-- EK Body Squad Head -->
        <div class="ek--squad--head bg--navy--700">
            <!-- EK Body Squad Head Title -->
            <h3 class="text--white ek--size--20">{{  __('translation.menu_squad') }}</h3>
            <!-- End EK Body Squad Head Title -->
        </div>
        <!-- End EK Body Squad Head -->
        <!-- EK Body Squad Table -->
        <div class="ek--squad--table bg--navy--700">
            <!-- EK Body Squad Table Head -->
            <div class="ek--squad--table--head">
                <div class="ek--squad--table--head--pos text--navy--200 ek--size--16">Pos</div>
                <div class="ek--squad--table--head--player text--navy--200 ek--size--16">Player name</div>
                <div class="ek--squad--table--head--age text--navy--200 ek--size--16">Age</div>
                <div class="ek--squad--table--head--abl text--navy--200 ek--size--16">Abl</div>
                <div class="ek--squad--table--head--energy text--navy--200 ek--size--16">Energy</div>
                <div class="ek--squad--table--head--matches text--navy--200 ek--size--16">Matches</div>
                <div class="ek--squad--table--head--goals text--navy--200 ek--size--16">Goals</div>
                <div class="ek--squad--table--head--value text--navy--200 ek--size--16">Value</div>
            </div>
            <!-- End EK Body Squad Table Head -->
            <!-- EK Body Squad Table body -->
            <div class="ek--squad--table--body">
                <!-- EK Body Squad Table Body Inner -->
                @foreach(Session::get("club")["players"] as $player)
                    @if($player["positionId"] == 1)
                        <div class="ek--squad--table--body--inner bg--navy--600 ek--squad--table--body--player--opener" data-id="{{ $player["id"] }}">
                            <!-- EK Body Squad Table Body Pos -->
                            <div  class="ek--squad--table--body--pos bg--orange--dark text--white ek--size--16--500" >G</div>

                            <!-- End EK Body Squad Table Body Pos -->
                            <!-- EK Body Squad Table Body Player -->
                            <div class="ek--squad--table--body--player ek--size--16">
                                <!-- EK Body Squad Table Body Player Flag -->
                                <img src="{{ url("static/img/Flags/".$player["country"]["code"].".svg") }}">
                                <!-- End EK Body Squad Table Body Player Flag -->
                                <!-- EK Body Squad Table Body Player Name -->
                                <a class="text--white ek--squad--open" href="#">{{ $player["name"] }}</a>
                                <!-- End EK Body Squad Table Body Player Name -->
                                <!-- Ek Body Squad Player Modal -->

                            </div>
                            <!-- End EK Body Squad Table Body Player -->
                            <!-- EK Body Squad Table Body Age -->
                            <div class="ek--squad--table--body--age text--white ek--size--16">{{ $player["age"] }}</div>
                            <!-- End EK Body Squad Table Body Age -->
                            <!-- EK Body Squad Table Body Abl -->
                            <div class="ek--squad--table--body--abl text--white ek--size--16">{{ number_format($player["ability"],0) }}</div>
                            <!-- End EK Body Squad Table Body Abl -->
                            <!-- EK Body Squad Table Body Energy -->
                            <div class="ek--squad--table--body--energy">
                                <!-- EK Body Squad Table Body Energy Progress -->
                                <div class="ek--squad--table--body--energy--progress bg--navy--500">
                                    <div class="progress">

                                        @if($player["energy"] <=100 && $player["energy"] > 90)
                                            <div class="progress-bar bg--green--dark--two" role="progressbar" style="width:{{ $player["energy"] }}%" aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                        @endif

                                        @if($player["energy"] <=90 && $player["energy"] > 55)
                                            <div class="progress-bar bg--green--two" role="progressbar" style="width:{{ $player["energy"] }}%"  aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                        @endif

                                        @if($player["energy"] <=55 && $player["energy"] > 25)
                                            <div class="progress-bar bg--yellow--two" role="progressbar" style="width:{{ $player["energy"] }}%" aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                        @endif

                                        @if($player["energy"] <=25)
                                            <div class="progress-bar bg--orange" role="progressbar" style="width:{{ $player["energy"] }}%" aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                        @endif

                                    </div>
                                </div>
                                <!-- End EK Body Squad Table Body Energy Progress -->
                                <!-- EK Body Squad Table Body Energy Progress Amount -->
                                <span class="text--green--dark--two ek--size--16">{{ $player["energy"] }}</span><span class="text--green--dark--two ek--size--16">%</span>
                                <!-- End EK Body Squad Table Body Energy Progress Amount -->
                            </div>
                            <!-- End EK Body Squad Table Body Energy -->
                            <!-- EK Body Squad Table Body Matches -->
                            <div class="ek--squad--table--body--matches text--white ek--size--16">{{ $player["appearance"] }}</div>
                            <!-- End EK Body Squad Table Body Matches -->
                            <!-- EK Body Squad Table Body Goals -->
                            <div class="ek--squad--table--body--goals text--white ek--size--16">{{ $player["goals"] }}</div>
                            <!-- End EK Body Squad Table Body Goals -->
                            <!-- EK Body Squad Table Body Value -->
                            @if($player["value"] < 1000)
                                <div class="ek--squad--table--body--value text--white ek--size--16"><span class="ek--squad--table--body--value text--white ek--size--16">{{ $player["value"] }}</span></div>
                            @endif

                            @if($player["value"] >= 1000 && $player["value"] < 1000000)
                                <div class="ek--squad--table--body--value text--white ek--size--16"><span class="ek--squad--table--body--value text--white ek--size--16">{{ number_format($player["value"]/1000,1) }}</span><span class="ek--squad--table--body--value text--white ek--size--16">K</span></div>
                            @endif

                            @if($player["value"] >= 1000000 && $player["value"] < 1000000000)
                                <div class="ek--squad--table--body--value text--white ek--size--16"><span class="ek--squad--table--body--value text--white ek--size--16">{{ number_format($player["value"]/1000000,1) }}</span><span class="ek--squad--table--body--value text--white ek--size--16">M</span></div>
                            @endif

                            @if($player["value"] >= 1000000000)
                                <div class="ek--squad--table--body--value text--white ek--size--16"><span class="ek--squad--table--body--value text--white ek--size--16">{{ number_format($player["value"]/1000000000,1) }}</span><span class="ek--squad--table--body--value text--white ek--size--16">B</span></div>
                            @endif

                            <!-- End EK Body Squad Table Body Value -->
                        </div>
                    @endif
                @endforeach

                @foreach(Session::get("club")["players"] as $player)
                    @if($player["positionId"] == 2)
                        <div class="ek--squad--table--body--inner bg--navy--600 ek--squad--table--body--player--opener" data-id="{{ $player["id"] }}">
                            <!-- EK Body Squad Table Body Pos -->
                            <div  class="ek--squad--table--body--pos bg--yellow text--white ek--size--16--500" >D</div>

                            <!-- End EK Body Squad Table Body Pos -->
                            <!-- EK Body Squad Table Body Player -->
                            <div class="ek--squad--table--body--player ek--size--16">
                                <!-- EK Body Squad Table Body Player Flag -->
                                <img src="{{ url("static/img/Flags/".$player["country"]["code"].".svg") }}">
                                <!-- End EK Body Squad Table Body Player Flag -->
                                <!-- EK Body Squad Table Body Player Name -->
                                <a class="text--white ek--squad--open" href="#">{{ $player["name"] }}</a>
                                <!-- End EK Body Squad Table Body Player Name -->
                                <!-- Ek Body Squad Player Modal -->

                            </div>
                            <!-- End EK Body Squad Table Body Player -->
                            <!-- EK Body Squad Table Body Age -->
                            <div class="ek--squad--table--body--age text--white ek--size--16">{{ $player["age"] }}</div>
                            <!-- End EK Body Squad Table Body Age -->
                            <!-- EK Body Squad Table Body Abl -->
                            <div class="ek--squad--table--body--abl text--white ek--size--16">{{ number_format($player["ability"],0) }}</div>
                            <!-- End EK Body Squad Table Body Abl -->
                            <!-- EK Body Squad Table Body Energy -->
                            <div class="ek--squad--table--body--energy">
                                <!-- EK Body Squad Table Body Energy Progress -->
                                <div class="ek--squad--table--body--energy--progress bg--navy--500">
                                    <div class="progress">

                                        @if($player["energy"] <=100 && $player["energy"] > 90)
                                            <div class="progress-bar bg--green--dark--two" role="progressbar" style="width:{{ $player["energy"] }}%" aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                        @endif

                                        @if($player["energy"] <=90 && $player["energy"] > 55)
                                            <div class="progress-bar bg--green--two" role="progressbar" style="width:{{ $player["energy"] }}%"  aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                        @endif

                                        @if($player["energy"] <=55 && $player["energy"] > 25)
                                            <div class="progress-bar bg--yellow--two" role="progressbar" style="width:{{ $player["energy"] }}%" aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                        @endif

                                        @if($player["energy"] <=25)
                                            <div class="progress-bar bg--orange" role="progressbar" style="width:{{ $player["energy"] }}%" aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                        @endif

                                    </div>
                                </div>
                                <!-- End EK Body Squad Table Body Energy Progress -->
                                <!-- EK Body Squad Table Body Energy Progress Amount -->
                                <span class="text--green--dark--two ek--size--16">{{ $player["energy"] }}</span><span class="text--green--dark--two ek--size--16">%</span>
                                <!-- End EK Body Squad Table Body Energy Progress Amount -->
                            </div>
                            <!-- End EK Body Squad Table Body Energy -->
                            <!-- EK Body Squad Table Body Matches -->
                            <div class="ek--squad--table--body--matches text--white ek--size--16">{{ $player["appearance"] }}</div>
                            <!-- End EK Body Squad Table Body Matches -->
                            <!-- EK Body Squad Table Body Goals -->
                            <div class="ek--squad--table--body--goals text--white ek--size--16">{{ $player["goals"] }}</div>
                            <!-- End EK Body Squad Table Body Goals -->
                            <!-- EK Body Squad Table Body Value -->
                            @if($player["value"] < 1000)
                                <div class="ek--squad--table--body--value text--white ek--size--16"><span class="ek--squad--table--body--value text--white ek--size--16">{{ $player["value"] }}</span></div>
                            @endif

                            @if($player["value"] >= 1000 && $player["value"] < 1000000)
                                <div class="ek--squad--table--body--value text--white ek--size--16"><span class="ek--squad--table--body--value text--white ek--size--16">{{ number_format($player["value"]/1000,1) }}</span><span class="ek--squad--table--body--value text--white ek--size--16">K</span></div>
                            @endif

                            @if($player["value"] >= 1000000 && $player["value"] < 1000000000)
                                <div class="ek--squad--table--body--value text--white ek--size--16"><span class="ek--squad--table--body--value text--white ek--size--16">{{ number_format($player["value"]/1000000,1) }}</span><span class="ek--squad--table--body--value text--white ek--size--16">M</span></div>
                            @endif

                            @if($player["value"] >= 1000000000)
                                <div class="ek--squad--table--body--value text--white ek--size--16"><span class="ek--squad--table--body--value text--white ek--size--16">{{ number_format($player["value"]/1000000000,1) }}</span><span class="ek--squad--table--body--value text--white ek--size--16">B</span></div>
                        @endif

                        <!-- End EK Body Squad Table Body Value -->
                        </div>
                    @endif
                @endforeach

                @foreach(Session::get("club")["players"] as $player)
                    @if($player["positionId"] == 3)
                        <div class="ek--squad--table--body--inner bg--navy--600 ek--squad--table--body--player--opener" data-id="{{ $player["id"] }}">
                            <!-- EK Body Squad Table Body Pos -->
                            <div  class="ek--squad--table--body--pos bg--cyan--two text--white ek--size--16--500" >M</div>

                            <!-- End EK Body Squad Table Body Pos -->
                            <!-- EK Body Squad Table Body Player -->
                            <div class="ek--squad--table--body--player ek--size--16">
                                <!-- EK Body Squad Table Body Player Flag -->
                                <img src="{{ url("static/img/Flags/".$player["country"]["code"].".svg") }}">
                                <!-- End EK Body Squad Table Body Player Flag -->
                                <!-- EK Body Squad Table Body Player Name -->
                                <a class="text--white ek--squad--open" href="#">{{ $player["name"] }}</a>
                                <!-- End EK Body Squad Table Body Player Name -->
                                <!-- Ek Body Squad Player Modal -->

                            </div>
                            <!-- End EK Body Squad Table Body Player -->
                            <!-- EK Body Squad Table Body Age -->
                            <div class="ek--squad--table--body--age text--white ek--size--16">{{ $player["age"] }}</div>
                            <!-- End EK Body Squad Table Body Age -->
                            <!-- EK Body Squad Table Body Abl -->
                            <div class="ek--squad--table--body--abl text--white ek--size--16">{{ number_format($player["ability"],0) }}</div>
                            <!-- End EK Body Squad Table Body Abl -->
                            <!-- EK Body Squad Table Body Energy -->
                            <div class="ek--squad--table--body--energy">
                                <!-- EK Body Squad Table Body Energy Progress -->
                                <div class="ek--squad--table--body--energy--progress bg--navy--500">
                                    <div class="progress">

                                        @if($player["energy"] <=100 && $player["energy"] > 90)
                                            <div class="progress-bar bg--green--dark--two" role="progressbar" style="width:{{ $player["energy"] }}%" aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                        @endif

                                        @if($player["energy"] <=90 && $player["energy"] > 55)
                                            <div class="progress-bar bg--green--two" role="progressbar" style="width:{{ $player["energy"] }}%"  aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                        @endif

                                        @if($player["energy"] <=55 && $player["energy"] > 25)
                                            <div class="progress-bar bg--yellow--two" role="progressbar" style="width:{{ $player["energy"] }}%" aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                        @endif

                                        @if($player["energy"] <=25)
                                            <div class="progress-bar bg--orange" role="progressbar" style="width:{{ $player["energy"] }}%" aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                        @endif

                                    </div>
                                </div>
                                <!-- End EK Body Squad Table Body Energy Progress -->
                                <!-- EK Body Squad Table Body Energy Progress Amount -->
                                <span class="text--green--dark--two ek--size--16">{{ $player["energy"] }}</span><span class="text--green--dark--two ek--size--16">%</span>
                                <!-- End EK Body Squad Table Body Energy Progress Amount -->
                            </div>
                            <!-- End EK Body Squad Table Body Energy -->
                            <!-- EK Body Squad Table Body Matches -->
                            <div class="ek--squad--table--body--matches text--white ek--size--16">{{ $player["appearance"] }}</div>
                            <!-- End EK Body Squad Table Body Matches -->
                            <!-- EK Body Squad Table Body Goals -->
                            <div class="ek--squad--table--body--goals text--white ek--size--16">{{ $player["goals"] }}</div>
                            <!-- End EK Body Squad Table Body Goals -->
                            <!-- EK Body Squad Table Body Value -->
                            @if($player["value"] < 1000)
                                <div class="ek--squad--table--body--value text--white ek--size--16"><span class="ek--squad--table--body--value text--white ek--size--16">{{ $player["value"] }}</span></div>
                            @endif

                            @if($player["value"] >= 1000 && $player["value"] < 1000000)
                                <div class="ek--squad--table--body--value text--white ek--size--16"><span class="ek--squad--table--body--value text--white ek--size--16">{{ number_format($player["value"]/1000,1) }}</span><span class="ek--squad--table--body--value text--white ek--size--16">K</span></div>
                            @endif

                            @if($player["value"] >= 1000000 && $player["value"] < 1000000000)
                                <div class="ek--squad--table--body--value text--white ek--size--16"><span class="ek--squad--table--body--value text--white ek--size--16">{{ number_format($player["value"]/1000000,1) }}</span><span class="ek--squad--table--body--value text--white ek--size--16">M</span></div>
                            @endif

                            @if($player["value"] >= 1000000000)
                                <div class="ek--squad--table--body--value text--white ek--size--16"><span class="ek--squad--table--body--value text--white ek--size--16">{{ number_format($player["value"]/1000000000,1) }}</span><span class="ek--squad--table--body--value text--white ek--size--16">B</span></div>
                            @endif

                        <!-- End EK Body Squad Table Body Value -->
                        </div>
                    @endif
                @endforeach

                @foreach(Session::get("club")["players"] as $player)
                    @if($player["positionId"] == 4)
                        <div class="ek--squad--table--body--inner bg--navy--600 ek--squad--table--body--player--opener" data-id="{{ $player["id"] }}">
                            <!-- EK Body Squad Table Body Pos -->
                            <div  class="ek--squad--table--body--pos bg--purple text--white ek--size--16--500" >F</div>

                            <!-- End EK Body Squad Table Body Pos -->
                            <!-- EK Body Squad Table Body Player -->
                            <div class="ek--squad--table--body--player ek--size--16">
                                <!-- EK Body Squad Table Body Player Flag -->
                                <img src="{{ url("static/img/Flags/".$player["country"]["code"].".svg") }}">
                                <!-- End EK Body Squad Table Body Player Flag -->
                                <!-- EK Body Squad Table Body Player Name -->
                                <a class="text--white ek--squad--open" href="#">{{ $player["name"] }}</a>
                                <!-- End EK Body Squad Table Body Player Name -->
                                <!-- Ek Body Squad Player Modal -->

                            </div>
                            <!-- End EK Body Squad Table Body Player -->
                            <!-- EK Body Squad Table Body Age -->
                            <div class="ek--squad--table--body--age text--white ek--size--16">{{ $player["age"] }}</div>
                            <!-- End EK Body Squad Table Body Age -->
                            <!-- EK Body Squad Table Body Abl -->
                            <div class="ek--squad--table--body--abl text--white ek--size--16">{{ number_format($player["ability"],0) }}</div>
                            <!-- End EK Body Squad Table Body Abl -->
                            <!-- EK Body Squad Table Body Energy -->
                            <div class="ek--squad--table--body--energy">
                                <!-- EK Body Squad Table Body Energy Progress -->
                                <div class="ek--squad--table--body--energy--progress bg--navy--500">
                                    <div class="progress">

                                        @if($player["energy"] <=100 && $player["energy"] > 90)
                                            <div class="progress-bar bg--green--dark--two" role="progressbar" style="width:{{ $player["energy"] }}%" aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                        @endif

                                        @if($player["energy"] <=90 && $player["energy"] > 55)
                                            <div class="progress-bar bg--green--two" role="progressbar" style="width:{{ $player["energy"] }}%"  aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                        @endif

                                        @if($player["energy"] <=55 && $player["energy"] > 25)
                                            <div class="progress-bar bg--yellow--two" role="progressbar" style="width:{{ $player["energy"] }}%" aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                        @endif

                                        @if($player["energy"] <=25)
                                            <div class="progress-bar bg--orange" role="progressbar" style="width:{{ $player["energy"] }}%" aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                        @endif

                                    </div>
                                </div>
                                <!-- End EK Body Squad Table Body Energy Progress -->
                                <!-- EK Body Squad Table Body Energy Progress Amount -->
                                <span class="text--green--dark--two ek--size--16">{{ $player["energy"] }}</span><span class="text--green--dark--two ek--size--16">%</span>
                                <!-- End EK Body Squad Table Body Energy Progress Amount -->
                            </div>
                            <!-- End EK Body Squad Table Body Energy -->
                            <!-- EK Body Squad Table Body Matches -->
                            <div class="ek--squad--table--body--matches text--white ek--size--16">{{ $player["appearance"] }}</div>
                            <!-- End EK Body Squad Table Body Matches -->
                            <!-- EK Body Squad Table Body Goals -->
                            <div class="ek--squad--table--body--goals text--white ek--size--16">{{ $player["goals"] }}</div>
                            <!-- End EK Body Squad Table Body Goals -->
                            <!-- EK Body Squad Table Body Value -->
                            @if($player["value"] < 1000)
                                <div class="ek--squad--table--body--value text--white ek--size--16"><span class="ek--squad--table--body--value text--white ek--size--16">{{ $player["value"] }}</span></div>
                            @endif

                            @if($player["value"] >= 1000 && $player["value"] < 1000000)
                                <div class="ek--squad--table--body--value text--white ek--size--16"><span class="ek--squad--table--body--value text--white ek--size--16">{{ number_format($player["value"]/1000,1) }}</span><span class="ek--squad--table--body--value text--white ek--size--16">K</span></div>
                            @endif

                            @if($player["value"] >= 1000000 && $player["value"] < 1000000000)
                                <div class="ek--squad--table--body--value text--white ek--size--16"><span class="ek--squad--table--body--value text--white ek--size--16">{{ number_format($player["value"]/1000000,1) }}</span><span class="ek--squad--table--body--value text--white ek--size--16">M</span></div>
                            @endif

                            @if($player["value"] >= 1000000000)
                                <div class="ek--squad--table--body--value text--white ek--size--16"><span class="ek--squad--table--body--value text--white ek--size--16">{{ number_format($player["value"]/1000000000,1) }}</span><span class="ek--squad--table--body--value text--white ek--size--16">B</span></div>
                            @endif

                        <!-- End EK Body Squad Table Body Value -->
                        </div>
                    @endif
                @endforeach

                <div id="ek--squad--modals--refill--buy" class="ek--squad--background" style="display: none">
                    <div class="ek--squad--modals--refill bg--navy--700 modal">
                        <!-- Ek Body Squad Refill Buy Modal Head -->
                        <div class="ek--squad--modals--refill--head">
                            <div class="ek--squad--modals--refill--head--energy bg--navy--600">
                                <svg class="icon icon-ek--energy fill--navy--200"><use xlink:href="{{url("static/img/icons.svg#icon-ek--energy")}}"></use></svg>
                            </div>
                            <!-- Ek Body Squad Refill Buy Modal Want -->
                            <div class="ek--squad--modals--refill--head--want">
                                <h4 class="text--white ek--size--20">Refill Energy</h4>
                                <p>Do you want to refill players' energy?</p>
                                <!-- Ek Body Squad Refill Buy Modal Want Coins -->
                                <div class="ek--squad--modals--refill--head--want--coins bg--navy--800">
                                    <span class="text--navy--100 ek--size--18">It will cost you</span>
                                    <!-- Ek Body Squad Refill Buy Modal Want Coins Amount -->
                                    <div class="ek--squad--modals--refill--head--want--amount">
                                        <span class="text--white ek--size--16" id="cost"></span>
                                        <img src="{{url("static/img/Dashboard/ek--want--coins.svg")}}" />
                                    </div>
                                    <!-- End Ek Body Squad Refill Buy Modal Want Coins Amount -->
                                </div>
                                <!-- End Ek Body Squad Refill Buy Modal Want Coins -->
                            </div>
                            <!-- End Ek Body Squad Refill Buy Modal Want -->
                        </div>
                        <!-- End Ek Body Squad Refill Buy Modal Head -->

                        <div class="ek--squad--modals--refill--buttons">
                            <a class="ek--squad--modals--refill--buttons--cancel2 text--white ek--size--16 bg--navy--600" href="#ek--squad--modals--refill">Cancel</a>
                            <!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;energy2 text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;blue" href="#ek&#45;&#45;notification">Refill Energy</a>-->
                            <button  class="ek--squad--modals--refill--buttons--energy2 text--white ek--size--16 bg--blue"  id="refillOne" data-id="">Refill Energy</button>
                        </div>
                    </div>
                </div>
                <div id="ek--notification" class="ek--squad--background" style="display: none">
                    <div  class="ek--notification bg--navy--700 " style="width: 552px;height: fit-content;margin: auto;">
                        <div class="ek--notification--body">
                            <!-- Ek Body Squad Message Notification Modal Left -->
                            <div class="ek--notification--left bg--green--dark--two"></div>
                            <!-- End Ek Body Squad Message Notification Modal left -->
                            <!-- Ek Body Squad Message Notification Modal Right -->
                            <div class="ek--notification--right">
                                <div class="ek--notification--right--head">
                                    <div class="ek--notification--right--image bg--green--dark--two">
                                        <svg class="icon icon-ek--correct fill--white"><use xlink:href="static/img/icons.svg#icon-ek--correct"></use></svg>
                                    </div>
                                    <div class="ek--notification--right--info">
                                        <h4 class="text--white ek--size--20">Player energy update!</h4>
                                        <p class="text--navy--100 ek--size--18" id="playerName" ><span>'s energy has successfully filled!</span></p>
                                    </div>
                                </div>
                                <div class="ek--notification--right--cancel">
                                    <a class="text--white bg--navy--600 ek--size--16--500" href="#" id="squadClose">Close</a>
                                </div>
                            </div>
                            <!-- End Ek Body Squad Message Notification Modal Right -->
                        </div>
                    </div>
                </div>
                <div id="ek--squad--modals--player" class="ek--squad--background" style="display: none">
                    <div class="ek--close">
                        <a class="ek--close--icon bg--navy--700" href="#" rel="modal:close">
                            <svg class="icon icon-ek--close--icon fill--navy--200"><use xlink:href="{{url("static/img/icons.svg#icon-ek--close--icon")}}"></use></svg>
                        </a>
                    </div>
                    <div  class="ek--squad--modals--player bg--navy--700 modal">

                        <!-- Ek Body Squad Player Modal Head -->
                        <div class="ek--squad--modals--head">
                            <!-- Ek Body Squad Player Modal Head Image -->
                            <div class="ek--squad--modals--head--image bg--navy--900">
                                <img src="{{url("static/img/Other/ek--null--image.png")}}" alt="">
                            </div>
                            <!-- End Ek Body Squad Player Modal Head Image -->

                            <!-- Ek Body Squad Player Modal Head About -->
                            <div class="ek--squad--modals--head--about">
                                <!-- Ek Body Squad Player Modal Head About Name -->
                                <h3 class="ek--size--20--400 text--white" id="mplayerName"></h3>
                                <!-- End Ek Body Squad Player Modal Head About Name -->
                                <!-- Ek Body Squad Player Modal Head About Country -->
                                <div class="ek--squad--modals--head--about--country" id="mCountry">

                                </div>
                                <!-- End Ek Body Squad Player Modal Head About Country -->
                                <!-- Ek Body Squad Player Modal Head About Position -->
                                <div id="mPosition"></div>
                                <!-- End Ek Body Squad Player Modal Head About Position -->
                            </div>
                            <!-- End Ek Body Squad Player Modal Head About -->
                        </div>
                        <!-- End Ek Body Squad Player Modal Head -->
                        <!-- Ek Body Squad Player Modal Inner -->
                        <div class="ek--squad--modals--inner">
                            <!-- Ek Body Squad Player Modal Inner Feature -->
                            <div class="ek--squad--modals--inner--feature">
                                <div class="ek--squad--modals--inner--feature--age">
                                    <p class="ek--size--28--500 text--white" id="mPlayerAge"></p>
                                    <h4 class="ek--size--16 text--navy--200">Age</h4>
                                </div>
                                <div class="ek--squad--modals--inner--feature--abl">
                                    <p class="ek--size--28--500 text--white" id="mPlayerAbility" ></p>
                                    <h4 class="ek--size--16 text--navy--200">Abl</h4>
                                </div>
                                <div id="value" class="ek--squad--modals--inner--feature--value">




                                </div>
                            </div>
                            <!-- End Ek Body Squad Player Modal Inner Feature -->
                            <!-- Ek Body Squad Player Modal Inner Values -->
                            <div class="ek--squad--modals--inner--values">
                                <!-- Ek Body Squad Player Modal Inner Values Sections Energy -->
                                <div class="ek--squad--modals--inner--values--section bg--navy--600">
                                    <!-- Ek Body Squad Player Modal Inner Values Sections left -->
                                    <div class="ek--squad--modals--inner--values--section--left">
                                        <img src="{{ url("static/img/squad/energy.svg") }}">
                                        <!--<svg class="icon icon-ek&#45;&#45;energy fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;energy"></use></svg>-->
                                        <span class="ek--size--16 text--navy--200">Energy</span>
                                    </div>
                                    <!-- End Ek Body Squad Player Modal Inner Values Sections left -->
                                    <!-- Ek Body Squad Player Modal Inner Values Sections Progress -->
                                    <div style="display: flex;align-items: center">
                                    <div class="ek--squad--modals--inner--values--section--progress bg--navy--500">
                                        <div id="mProgress" class="progress">


                                        </div>

                                    </div>

                                    <span class="text--green--dark--two ek--size--16" id="mPlayerEnergy"></span>
                                    <span class="text--green--dark--two ek--size--16">%</span>
                                    </div>

                                    <!-- End Ek Body Squad Player Modal Inner Values Sections Progress -->
                                </div>
                                <!-- End Ek Body Squad Player Modal Inner Values Sections Energy -->

                                <div class="ek--squad--modals--inner--values--section bg--navy--600">
                                    <!-- Ek Body Squad Player Modal Inner Values Sections left -->
                                    <div class="ek--squad--modals--inner--values--section--left">
                                        <img src="{{ url("static/img/squad/salary.svg") }}">
                                        <!--<svg class="icon icon-ek&#45;&#45;ball fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;ball"></use></svg>-->
                                        <span class="ek--size--16 text--navy--200">Salary</span>
                                    </div>
                                    <!-- End Ek Body Squad Player Modal Inner Values Sections left -->
                                    <span class="text--white ek--size--16--500" id="mSalary"></span>
                                </div>

                                <!-- Ek Body Squad Player Modal Inner Values Sections Goals -->
                                <div class="ek--squad--modals--inner--values--section bg--navy--600">
                                    <!-- Ek Body Squad Player Modal Inner Values Sections left -->
                                    <div class="ek--squad--modals--inner--values--section--left">
                                        <img src="{{ url("static/img/squad/goal.svg") }}">
                                        <!--<svg class="icon icon-ek&#45;&#45;ball fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;ball"></use></svg>-->
                                        <span class="ek--size--16 text--navy--200">Goals</span>
                                    </div>
                                    <!-- End Ek Body Squad Player Modal Inner Values Sections left -->
                                    <span class="text--white ek--size--16--500" id="mGoals"></span>
                                </div>
                                <!-- End Ek Body Squad Player Modal Inner Values Sections Goals -->
                                <!-- Ek Body Squad Player Modal Inner Values Sections Played Match -->
                                <div class="ek--squad--modals--inner--values--section bg--navy--600">
                                    <!-- Ek Body Squad Player Modal Inner Values Sections left -->
                                    <div class="ek--squad--modals--inner--values--section--left">
                                        <img src="{{ url("static/img/squad/played.svg") }}">
                                        <!--<svg class="icon icon-ek&#45;&#45;stadium fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;stadium"></use></svg>-->
                                        <span class="ek--size--16 text--navy--200">Played Match</span>
                                    </div>
                                    <!-- End Ek Body Squad Player Modal Inner Values Sections left -->
                                    <span class="text--white ek--size--16--500" id="mAppearance"></span>
                                </div>
                                <!-- End Ek Body Squad Player Modal Inner Values Sections Played Match -->
                                <!-- Ek Body Squad Player Modal Inner Values Sections Red Card -->
                                <div class="ek--squad--modals--inner--values--section bg--navy--600">
                                    <!-- Ek Body Squad Player Modal Inner Values Sections left -->
                                    <div class="ek--squad--modals--inner--values--section--left">
                                        <img src="{{ url("static/img/squad/red_card.svg") }}">
                                        <!--<svg class="icon icon-ek&#45;&#45;card fill&#45;&#45;red"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;card"></use></svg>-->
                                        <span class="ek--size--16 text--navy--200">Red Card</span>
                                    </div>
                                    <!-- End Ek Body Squad Player Modal Inner Values Sections left -->
                                    <span class="text--white ek--size--16--500" id="mRedCard"></span>
                                </div>
                                <!-- End Ek Body Squad Player Modal Inner Values Sections Red Card  -->
                                <!-- Ek Body Squad Player Modal Inner Values Sections Red Card -->
                                <div class="ek--squad--modals--inner--values--section bg--navy--600">
                                    <!-- Ek Body Squad Player Modal Inner Values Sections left -->
                                    <div class="ek--squad--modals--inner--values--section--left">
                                        <img src="{{ url("static/img/squad/yellow_card.svg") }}">
                                        <!--<svg class="icon icon-ek&#45;&#45;card fill&#45;&#45;yellow"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;card"></use></svg>-->
                                        <span class="ek--size--16 text--navy--200">Yellow Card</span>
                                    </div>
                                    <!-- End Ek Body Squad Player Modal Inner Values Sections left -->
                                    <span class="text--white ek--size--16--500" id="mYellowCard"></span>
                                </div>
                                <!-- End Ek Body Squad Player Modal Inner Values Sections Red Card  -->
                            </div>
                            <!-- End Ek Body Squad Player Modal Inner Values -->
                            <!-- Ek Body Squad Player Modal Inner Refill Energy -->
                            <div id="mButtons"></div>
                            <!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;refill bg&#45;&#45;blue text&#45;&#45;white" href="#ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill">Refill Energy</a>-->

                            <!-- End Ek Body Squad Player Modal Inner Refill Energy -->
                        </div>
                        <!-- End Ek Body Squad Player Modal Inner -->
                    </div>
                </div>

                <div id="ek--soft--notenough" class="ek--modal ek--modal--border--left--orange bg--navy--700 modal" style="display:none">
                    <div class="ek--modal--body">
                        <div class="ek--modal--body--left">
                            <div class="ek--modal--body--left--image bg--orange--light">
                                <svg class="icon icon-ek--warning fill--white"><use xlink:href="{{ url("static/img/icons.svg#icon-ek--warning") }}"></use></svg>
                            </div>
                        </div>
                        <div class="ek--modal--body--right">
                            <h3 class="text--white ek--size--20">Ek Coin is not enough</h3>
                            <p class="text--navy--100 ek--size--18">There is not enough EK Coins to refill player's energy</p>
                            <div class="ek--modal--body--buttons">
                                <a class="ek--modal--body--buttons--cancel text--white ek--size--16" href="#ek--soft" rel="modal:open">Cancel</a>

                                <!--<a class="ek&#45;&#45;modal&#45;&#45;body&#45;&#45;buttons&#45;&#45;other text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500 bg&#45;&#45;blue" href="javascript:void(0)">Buy EK Coins</a>-->
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- End EK Body Squad Table Body -->



        </div>
        <!-- End EK Body Squad Table -->
    </section>

@endsection




@section('appendix')
    <div id="loading" class="ek--modal--loading">
        <div class="ek--modal--loading--body">
            <div class="ek--modal--loading--body--inner">
                <div class="ek--modal--loading--body--image bg--navy--700">
                    <img src="static/img/Dashboard/ek--loading@2x.png" />
                </div>
                <p class="text--navy--100 ek--size--16"></p>
            </div>
        </div>
    </div>

    <form name="refreshForm">
        <input type="hidden" name="visited" value="" />
    </form>

@endsection

@section("javascript")
    <script src="{{url("static/js/squad.js")}}"></script>
    <script src="{{url("static/js/finance.js")}}"></script>
@endsection
