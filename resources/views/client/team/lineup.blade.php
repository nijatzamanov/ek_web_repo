@extends('client.layout.authenticated')
@section('content')


    <!-- Ek Line Up -->
    <section class="ek--lineup">
        <!-- EK Line Up Body -->
        <div class="ek--lineup--body">
            <!-- EK Line Up Left -->
            <div class="ek--lineup--body--left bg--navy--700">
                <!-- EK Line Up Left Head -->
                <div class="ek--lineup--body--left--head">
                    <h3 class="text--white ek--size--20">{{  __('translation.menu_lineup') }}</h3>
                    <div class="ek--lineup--body--left--tools">
                        <div class="ek--lineup--body--left--tools--button bg--navy--500" data-toggle="tooltip" id="reset" title="Reset">
                            <svg class="icon icon-ek--reset fill--white"><use xlink:href="static/img/icons.svg#icon-ek--reset"></use></svg>
                        </div>
                        <div class="ek--lineup--body--left--tools--button bg--navy--500" data-toggle="tooltip" id="random" title="{{  __('translation.auto_select_header') }}">
                            <svg class="icon icon-ek--random"><use xlink:href="static/img/icons.svg#icon-ek--random"></use></svg>
                        </div>
                        <div class="ek--lineup--body--left--tools--button bg--blue" data-toggle="tooltip" id="refillTeam" title="{{  __('translation.action_refill_energy') }}">
                            <svg class="icon icon-ek--energy fill--white"><use xlink:href="static/img/icons.svg#icon-ek--energy"></use></svg>
                        </div>
                    </div>
                </div>
                <!-- End EK Line Up Left Head -->
                <!-- EK Line Up Stadium -->
                <div class="ek--lineup--stadium">
                    <!-- EK Line Up Stadium Image -->
                    <img class="ek--lineup--stadium--image" src="{{ url("static/img/Stadium_flat.png") }}" alt="Stadium_flat">
                    <!-- End EK Line Up Stadium Image -->
                    <!-- EK Line Up Stadium Position -->


                    <div id="formationType" hidden>{{ Session::get("club")["formation"]["format"] }}</div>
                    <div class="ek--lineup--stadium--position ek--lineup--stadium--position--442"  data-id="442">

                        <!-- EK Line Up Stadium Position Forward -->
                        <div class="ek--lineup--stadium--position--forwards">
                            <div class="ek--lineup--stadium--position--player forwardOne forwardStadium" playertype="fd">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Forward Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Forward</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Forward Empty -->




                            </div>
                            <div class="ek--lineup--stadium--position--player forwardTwo forwardStadium" playertype="fd">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Forward Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Forward</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Forward Empty -->

                            </div>
                        </div>
                        <!-- EK Line Up Stadium Position Forward -->


                        <!-- EK Line Up Stadium Position Midfielder -->
                        <div class="ek--lineup--stadium--position--midlielders">
                            <div class="ek--lineup--stadium--position--player midfielderOne midfielderStadium" playertype="mf">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Midfielder Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Midfielder</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player midfielderTwo midfielderStadium" playertype="mf">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Midfielder Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Midfielder</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player midfielderThree midfielderStadium" playertype="mf">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Midfielder Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Midfielder</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player midfielderFour midfielderStadium" playertype="mf">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Midfielder Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Midfielder</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                        </div>
                        <!-- EK Line Up Stadium Position Midfielder -->


                        <!-- EK Line Up Stadium Position Defender -->
                        <div class="ek--lineup--stadium--position--defenders">
                            <div class="ek--lineup--stadium--position--player defenderOne defenderStadium" playertype="df">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Defender Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty" playertype="df">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Defender</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player defenderTwo defenderStadium" playertype="df">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Defender Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Defender</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player defenderThree defenderStadium" playertype="df">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Defender Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Defender</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player defenderFour defenderStadium" playertype="df">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Defender Empty -->
                                <div class="ek--lineup--stadium--position--player--empty" >
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Defender</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                        </div>

                        <!-- EK Line Up Stadium Position Defender -->


                        <!-- EK Line Up Stadium Position Goalkeep -->
                        <div class="ek--lineup--stadium--position--goalkeep">
                            <div class="ek--lineup--stadium--position--player goalkeepOne goalkeepStadium" playertype="gk">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Goalkeep Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty" >
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Goalkeep.</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Goalkeep Empty -->

                            </div>
                        </div>

                        <!-- EK Line Up Stadium Position Goalkeep -->
                    </div>
                    <div class="ek--lineup--stadium--position ek--lineup--stadium--position--433" data-id="433"  >
                         <!-- EK Line Up Stadium Position Forward -->
                        <div class="ek--lineup--stadium--position--forwards">
                                <div class="ek--lineup--stadium--position--player forwardOne forwardStadium" playertype="fd">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Forward Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Forward</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Forward Empty -->



                                </div>
                                <div class="ek--lineup--stadium--position--player forwardTwo forwardStadium" playertype="fd">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Forward Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Forward</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Forward Empty -->

                                </div>
                                <div class="ek--lineup--stadium--position--player forwardThree forwardStadium" playertype="fd">
                                        <!-- Drag Position OverDiv -->
                                        <!-- <div class="ek--lineup--over"></div> -->
                                        <!-- End Drag Position OverDiv -->
                                        <!-- EK Line Up Stadium Position Forward Empty -->
                                        <div class="ek--lineup--stadium--position--player--empty">
                                            <div class="ek--lineup--stadium--position--choose">
                                                <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                            </div>
                                            <span>Forward</span>
                                        </div>
                                        <!-- End EK Line Up Stadium Position Forward Empty -->

                                    </div>
                        </div>
                        <!-- EK Line Up Stadium Position Forward -->


                        <!-- EK Line Up Stadium Position Midfielder -->
                        <div class="ek--lineup--stadium--position--midlielders">
                            <div class="ek--lineup--stadium--position--player midfielderOne midfielderStadium" playertype="mf">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Midfielder Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Midfielder</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player midfielderTwo midfielderStadium" playertype="mf">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Midfielder Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Midfielder</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player midfielderThree midfielderStadium" playertype="mf">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Midfielder Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Midfielder</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>

                        </div>
                        <!-- EK Line Up Stadium Position Midfielder -->


                        <!-- EK Line Up Stadium Position Defender -->
                        <div class="ek--lineup--stadium--position--defenders">
                            <div class="ek--lineup--stadium--position--player defenderOne defenderStadium" playertype="df">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Defender Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty" playertype="df">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Defender</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player defenderTwo defenderStadium" playertype="df">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Defender Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Defender</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player defenderThree defenderStadium" playertype="df">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Defender Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Defender</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player defenderFour defenderStadium" playertype="df">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Defender Empty -->
                                <div class="ek--lineup--stadium--position--player--empty" >
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Defender</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                        </div>

                        <!-- EK Line Up Stadium Position Defender -->


                        <!-- EK Line Up Stadium Position Goalkeep -->
                        <div class="ek--lineup--stadium--position--goalkeep">
                            <div class="ek--lineup--stadium--position--player goalkeepOne goalkeepStadium" playertype="gk">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Goalkeep Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty" >
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Goalkeep.</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Goalkeep Empty -->

                            </div>
                        </div>

                        <!-- EK Line Up Stadium Position Goalkeep -->
                    </div>
                    <div class="ek--lineup--stadium--position ek--lineup--stadium--position--352" data-id="352"  >
                          <!-- EK Line Up Stadium Position Forward -->
                        <div class="ek--lineup--stadium--position--forwards">
                                <div class="ek--lineup--stadium--position--player forwardOne forwardStadium" playertype="fd">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Forward Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Forward</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Forward Empty -->


                                </div>
                                <div class="ek--lineup--stadium--position--player forwardTwo forwardStadium" playertype="fd">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Forward Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Forward</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Forward Empty -->

                                </div>
                        </div>
                        <!-- EK Line Up Stadium Position Forward -->


                        <!-- EK Line Up Stadium Position Midfielder -->
                        <div class="ek--lineup--stadium--position--midlielders">
                            <div class="ek--lineup--stadium--position--player midfielderOne midfielderStadium" playertype="mf">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Midfielder Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Midfielder</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player midfielderTwo midfielderStadium" playertype="mf">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Midfielder Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Midfielder</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player midfielderThree midfielderStadium" playertype="mf">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Midfielder Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Midfielder</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player midfielderFour midfielderStadium" playertype="mf">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Midfielder Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Midfielder</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player midfielderFive midfielderStadium" playertype="mf">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Midfielder Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Midfielder</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Midfielder Empty -->

                                </div>
                        </div>
                        <!-- EK Line Up Stadium Position Midfielder -->


                        <!-- EK Line Up Stadium Position Defender -->
                        <div class="ek--lineup--stadium--position--defenders">
                            <div class="ek--lineup--stadium--position--player defenderOne defenderStadium" playertype="df">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Defender Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty" playertype="df">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Defender</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player defenderTwo defenderStadium" playertype="df">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Defender Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Defender</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player defenderThree defenderStadium" playertype="df">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Defender Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Defender</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                        </div>

                        <!-- EK Line Up Stadium Position Defender -->


                        <!-- EK Line Up Stadium Position Goalkeep -->
                        <div class="ek--lineup--stadium--position--goalkeep">
                            <div class="ek--lineup--stadium--position--player goalkeepOne goalkeepStadium" playertype="gk">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Goalkeep Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty" >
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Goalkeep.</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Goalkeep Empty -->

                            </div>
                        </div>

                        <!-- EK Line Up Stadium Position Goalkeep -->
                    </div>
                    <div class="ek--lineup--stadium--position ek--lineup--stadium--position--343" data-id="343" >
                           <!-- EK Line Up Stadium Position Forward -->
                        <div class="ek--lineup--stadium--position--forwards">
                                <div class="ek--lineup--stadium--position--player forwardOne forwardStadium" playertype="fd">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Forward Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Forward</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Forward Empty -->


                                </div>
                                <div class="ek--lineup--stadium--position--player forwardTwo forwardStadium" playertype="fd">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Forward Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Forward</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Forward Empty -->

                                </div>
                                <div class="ek--lineup--stadium--position--player forwardThree forwardStadium" playertype="fd">
                                        <!-- Drag Position OverDiv -->
                                        <!-- <div class="ek--lineup--over"></div> -->
                                        <!-- End Drag Position OverDiv -->
                                        <!-- EK Line Up Stadium Position Forward Empty -->
                                        <div class="ek--lineup--stadium--position--player--empty">
                                            <div class="ek--lineup--stadium--position--choose">
                                                <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                            </div>
                                            <span>Forward</span>
                                        </div>
                                        <!-- End EK Line Up Stadium Position Forward Empty -->

                                    </div>
                        </div>
                        <!-- EK Line Up Stadium Position Forward -->


                        <!-- EK Line Up Stadium Position Midfielder -->
                        <div class="ek--lineup--stadium--position--midlielders">
                            <div class="ek--lineup--stadium--position--player midfielderOne midfielderStadium" playertype="mf">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Midfielder Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Midfielder</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player midfielderTwo midfielderStadium" playertype="mf">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Midfielder Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Midfielder</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player midfielderThree midfielderStadium" playertype="mf">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Midfielder Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Midfielder</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player midfielderFour midfielderStadium" playertype="mf">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Midfielder Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Midfielder</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                        </div>
                        <!-- EK Line Up Stadium Position Midfielder -->


                        <!-- EK Line Up Stadium Position Defender -->
                        <div class="ek--lineup--stadium--position--defenders">
                            <div class="ek--lineup--stadium--position--player defenderOne defenderStadium" playertype="df">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Defender Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty" playertype="df">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Defender</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player defenderTwo defenderStadium" playertype="df">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Defender Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Defender</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player defenderThree defenderStadium" playertype="df">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Defender Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Defender</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                        </div>

                        <!-- EK Line Up Stadium Position Defender -->


                        <!-- EK Line Up Stadium Position Goalkeep -->
                        <div class="ek--lineup--stadium--position--goalkeep">
                            <div class="ek--lineup--stadium--position--player goalkeepOne goalkeepStadium" playertype="gk">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Goalkeep Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty" >
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Goalkeep.</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Goalkeep Empty -->

                            </div>
                        </div>

                        <!-- EK Line Up Stadium Position Goalkeep -->
                    </div>
                    <div class="ek--lineup--stadium--position ek--lineup--stadium--position--451" data-id="451" >
                           <!-- EK Line Up Stadium Position Forward -->
                           <div class="ek--lineup--stadium--position--forwards">
                                <div class="ek--lineup--stadium--position--player forwardOne forwardStadium" playertype="fd">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Forward Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Forward</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Forward Empty -->


                                </div>
                        </div>
                        <!-- EK Line Up Stadium Position Forward -->


                        <!-- EK Line Up Stadium Position Midfielder -->
                        <div class="ek--lineup--stadium--position--midlielders">
                            <div class="ek--lineup--stadium--position--player midfielderOne midfielderStadium" playertype="mf">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Midfielder Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Midfielder</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player midfielderTwo midfielderStadium" playertype="mf">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Midfielder Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Midfielder</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player midfielderThree midfielderStadium" playertype="mf">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Midfielder Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Midfielder</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player midfielderFour midfielderStadium" playertype="mf">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Midfielder Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Midfielder</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player midfielderFive midfielderStadium" playertype="mf">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Midfielder Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Midfielder</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Midfielder Empty -->

                                </div>
                        </div>
                        <!-- EK Line Up Stadium Position Midfielder -->


                        <!-- EK Line Up Stadium Position Defender -->
                        <div class="ek--lineup--stadium--position--defenders">
                            <div class="ek--lineup--stadium--position--player defenderOne defenderStadium" playertype="df">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Defender Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty" playertype="df">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Defender</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player defenderTwo defenderStadium" playertype="df">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Defender Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Defender</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player defenderThree defenderStadium" playertype="df">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Defender Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Defender</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player defenderFour defenderStadium" playertype="df">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Defender Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Defender</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Defender Empty -->

                                </div>
                        </div>

                        <!-- EK Line Up Stadium Position Defender -->


                        <!-- EK Line Up Stadium Position Goalkeep -->
                        <div class="ek--lineup--stadium--position--goalkeep">
                            <div class="ek--lineup--stadium--position--player goalkeepOne goalkeepStadium" playertype="gk">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Goalkeep Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty" >
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Goalkeep.</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Goalkeep Empty -->

                            </div>
                        </div>

                        <!-- EK Line Up Stadium Position Goalkeep -->
                    </div>
                    <div class="ek--lineup--stadium--position ek--lineup--stadium--position--523" data-id="523"  >
                           <!-- EK Line Up Stadium Position Forward -->
                           <div class="ek--lineup--stadium--position--forwards">
                                <div class="ek--lineup--stadium--position--player forwardOne forwardStadium" playertype="fd">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Forward Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Forward</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Forward Empty -->



                                </div>
                                <div class="ek--lineup--stadium--position--player forwardTwo forwardStadium" playertype="fd">
                                        <!-- Drag Position OverDiv -->
                                        <!-- <div class="ek--lineup--over"></div> -->
                                        <!-- End Drag Position OverDiv -->
                                        <!-- EK Line Up Stadium Position Forward Empty -->
                                        <div class="ek--lineup--stadium--position--player--empty">
                                            <div class="ek--lineup--stadium--position--choose">
                                                <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                            </div>
                                            <span>Forward</span>
                                        </div>
                                        <!-- End EK Line Up Stadium Position Forward Empty -->


                                    </div>
                                <div class="ek--lineup--stadium--position--player forwardThree forwardStadium" playertype="fd">
                                        <!-- Drag Position OverDiv -->
                                        <!-- <div class="ek--lineup--over"></div> -->
                                        <!-- End Drag Position OverDiv -->
                                        <!-- EK Line Up Stadium Position Forward Empty -->
                                        <div class="ek--lineup--stadium--position--player--empty">
                                            <div class="ek--lineup--stadium--position--choose">
                                                <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                            </div>
                                            <span>Forward</span>
                                        </div>
                                        <!-- End EK Line Up Stadium Position Forward Empty -->


                                    </div>
                        </div>
                        <!-- EK Line Up Stadium Position Forward -->


                        <!-- EK Line Up Stadium Position Midfielder -->
                        <div class="ek--lineup--stadium--position--midlielders">
                            <div class="ek--lineup--stadium--position--player midfielderOne midfielderStadium" playertype="mf">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Midfielder Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Midfielder</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player midfielderTwo midfielderStadium" playertype="mf">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Midfielder Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Midfielder</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                        </div>
                        <!-- EK Line Up Stadium Position Midfielder -->


                        <!-- EK Line Up Stadium Position Defender -->
                        <div class="ek--lineup--stadium--position--defenders">
                            <div class="ek--lineup--stadium--position--player defenderOne defenderStadium" playertype="df">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Defender Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty" playertype="df">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Defender</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player defenderTwo defenderStadium" playertype="df">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Defender Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Defender</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player defenderThree defenderStadium" playertype="df">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Defender Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Defender</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player defenderFour defenderStadium" playertype="df">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Defender Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Defender</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Defender Empty -->

                                </div>
                            <div class="ek--lineup--stadium--position--player defenderFive defenderStadium" playertype="df">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Defender Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Defender</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Defender Empty -->

                                </div>
                        </div>

                        <!-- EK Line Up Stadium Position Defender -->


                        <!-- EK Line Up Stadium Position Goalkeep -->
                        <div class="ek--lineup--stadium--position--goalkeep">
                            <div class="ek--lineup--stadium--position--player goalkeepOne goalkeepStadium" playertype="gk">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Goalkeep Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty" >
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Goalkeep.</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Goalkeep Empty -->

                            </div>
                        </div>

                        <!-- EK Line Up Stadium Position Goalkeep -->
                    </div>
                    <div class="ek--lineup--stadium--position ek--lineup--stadium--position--532" data-id="532"  >
                          <!-- EK Line Up Stadium Position Forward -->
                          <div class="ek--lineup--stadium--position--forwards">
                                <div class="ek--lineup--stadium--position--player forwardOne forwardStadium" playertype="fd">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Forward Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Forward</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Forward Empty -->


                                </div>
                                <div class="ek--lineup--stadium--position--player forwardTwo forwardStadium" playertype="fd">
                                        <!-- Drag Position OverDiv -->
                                        <!-- <div class="ek--lineup--over"></div> -->
                                        <!-- End Drag Position OverDiv -->
                                        <!-- EK Line Up Stadium Position Forward Empty -->
                                        <div class="ek--lineup--stadium--position--player--empty">
                                            <div class="ek--lineup--stadium--position--choose">
                                                <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                            </div>
                                            <span>Forward</span>
                                        </div>
                                        <!-- End EK Line Up Stadium Position Forward Empty -->


                                    </div>
                        </div>
                        <!-- EK Line Up Stadium Position Forward -->


                        <!-- EK Line Up Stadium Position Midfielder -->
                        <div class="ek--lineup--stadium--position--midlielders">
                            <div class="ek--lineup--stadium--position--player midfielderOne midfielderStadium" playertype="mf">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Midfielder Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Midfielder</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player midfielderTwo midfielderStadium" playertype="mf">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Midfielder Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Midfielder</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player midfielderThree midfielderStadium" playertype="mf">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Midfielder Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Midfielder</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Midfielder Empty -->

                                </div>
                        </div>
                        <!-- EK Line Up Stadium Position Midfielder -->


                        <!-- EK Line Up Stadium Position Defender -->
                        <div class="ek--lineup--stadium--position--defenders">
                            <div class="ek--lineup--stadium--position--player defenderOne defenderStadium" playertype="df">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Defender Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty" playertype="df">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Defender</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player defenderTwo defenderStadium" playertype="df">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Defender Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Defender</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player defenderThree defenderStadium" playertype="df">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Defender Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Defender</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player defenderFour defenderStadium" playertype="df">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Defender Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Defender</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Defender Empty -->

                                </div>
                            <div class="ek--lineup--stadium--position--player defenderFive defenderStadium" playertype="df">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Defender Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Defender</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Defender Empty -->

                                </div>
                        </div>

                        <!-- EK Line Up Stadium Position Defender -->


                        <!-- EK Line Up Stadium Position Goalkeep -->
                        <div class="ek--lineup--stadium--position--goalkeep">
                            <div class="ek--lineup--stadium--position--player goalkeepOne goalkeepStadium" playertype="gk">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Goalkeep Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty" >
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Goalkeep.</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Goalkeep Empty -->

                            </div>
                        </div>

                        <!-- EK Line Up Stadium Position Goalkeep -->
                    </div>
                    <div class="ek--lineup--stadium--position ek--lineup--stadium--position--541" data-id="541"  >
                          <!-- EK Line Up Stadium Position Forward -->
                          <div class="ek--lineup--stadium--position--forwards">
                                <div class="ek--lineup--stadium--position--player forwardOne forwardStadium" playertype="fd">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Forward Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Forward</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Forward Empty -->


                                </div>
                        </div>
                        <!-- EK Line Up Stadium Position Forward -->


                        <!-- EK Line Up Stadium Position Midfielder -->
                        <div class="ek--lineup--stadium--position--midlielders">
                            <div class="ek--lineup--stadium--position--player midfielderOne midfielderStadium" playertype="mf">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Midfielder Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Midfielder</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player midfielderTwo midfielderStadium" playertype="mf">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Midfielder Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Midfielder</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Midfielder Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player midfielderThree midfielderStadium" playertype="mf">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Midfielder Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Midfielder</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Midfielder Empty -->

                                </div>
                                <div class="ek--lineup--stadium--position--player midfielderFour midfielderStadium" playertype="mf">
                                        <!-- Drag Position OverDiv -->
                                        <!-- <div class="ek--lineup--over"></div> -->
                                        <!-- End Drag Position OverDiv -->
                                        <!-- EK Line Up Stadium Position Midfielder Empty -->
                                        <div class="ek--lineup--stadium--position--player--empty">
                                            <div class="ek--lineup--stadium--position--choose">
                                                <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                            </div>
                                            <span>Midfielder</span>
                                        </div>
                                        <!-- End EK Line Up Stadium Position Midfielder Empty -->

                                    </div>
                        </div>
                        <!-- EK Line Up Stadium Position Midfielder -->


                        <!-- EK Line Up Stadium Position Defender -->
                        <div class="ek--lineup--stadium--position--defenders">
                            <div class="ek--lineup--stadium--position--player defenderOne defenderStadium" playertype="df">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Defender Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty" playertype="df">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Defender</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player defenderTwo defenderStadium" playertype="df">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Defender Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Defender</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player defenderThree defenderStadium" playertype="df">
                                <!-- Drag Position OverDiv -->
                                <!-- <div class="ek--lineup--over"></div> -->
                                <!-- End Drag Position OverDiv -->
                                <!-- EK Line Up Stadium Position Defender Empty -->
                                <div class="ek--lineup--stadium--position--player--empty">
                                    <div class="ek--lineup--stadium--position--choose">
                                        <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                    </div>
                                    <span>Defender</span>
                                </div>
                                <!-- End EK Line Up Stadium Position Defender Empty -->

                            </div>
                            <div class="ek--lineup--stadium--position--player defenderFour defenderStadium" playertype="df">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Defender Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Defender</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Defender Empty -->

                                </div>
                            <div class="ek--lineup--stadium--position--player defenderFive defenderStadium" playertype="df">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Defender Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty">
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Defender</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Defender Empty -->

                                </div>
                        </div>

                        <!-- EK Line Up Stadium Position Defender -->


                        <!-- EK Line Up Stadium Position Goalkeep -->
                        <div class="ek--lineup--stadium--position--goalkeep">
                            <div class="ek--lineup--stadium--position--player goalkeepOne goalkeepStadium" playertype="gk">
                                    <!-- Drag Position OverDiv -->
                                    <!-- <div class="ek--lineup--over"></div> -->
                                    <!-- End Drag Position OverDiv -->
                                    <!-- EK Line Up Stadium Position Goalkeep Empty -->
                                    <div class="ek--lineup--stadium--position--player--empty" >
                                        <div class="ek--lineup--stadium--position--choose">
                                            <svg class="icon icon-ek--plus"><use xlink:href="static/img/icons.svg#icon-ek--plus"></use></svg>
                                        </div>
                                        <span>Goalkeep.</span>
                                    </div>
                                    <!-- End EK Line Up Stadium Position Goalkeep Empty -->

                            </div>
                        </div>

                        <!-- EK Line Up Stadium Position Goalkeep -->
                    </div>
                    <!-- End EK Line Up Stadium Position -->
                </div>
                <!-- End EK Line Up Stadium -->
            </div>
            <!-- End EK Line Up Left -->
            <!-- EK Line Up Right -->
            <div class="ek--lineup--body--right bg--navy--700">
                <!-- EK Line Up Right Tabs -->
                <div class="ek--lineup--body--tabs">
                    <!-- EK Line Up Right Tabs Head -->
                    <div class="ek--lineup--body--tabs--head">
                        <!-- EK Line Up Right Tabs Head Buttons -->
                        <div id="ek--lineup--body--tabs--substitutions" class="ek--lineup--body--tabs--button button--active">{{  __('translation.tab_substitutions') }}</div>
                        <div id="ek--lineup--body--tabs--formation" class="ek--lineup--body--tabs--button">{{  __('translation.tab_formation') }}</div>
                        <div id="ek--lineup--body--tabs--tactics" class="ek--lineup--body--tabs--button">{{  __('translation.tab_tactics') }}</div>
                        <!-- EK Line Up Right Tabs Head Buttons -->
                    </div>
                    <!-- End EK Line Up Right Tabs Head -->
                    <!-- EK Line Up Right Tabs Content -->
                    <div class="ek--lineup--body--tabs--content">
                        <!-- EK Line Up Right Tabs Content Substitutions -->
                        <div class="ek--lineup--body--tabs--substitutions ek--lineup--body--tabs--content--table">
                            <!-- EK Line Up Right Tabs Content Substitutions Head -->
                            <div class="ek--lineup--body--tabs--substitutions--head">
                                <div class="ek--lineup--body--tabs--substitutions--head--pos ek--size--16 text--navy--200">Pos</div>
                                <div class="ek--lineup--body--tabs--substitutions--head--player ek--size--16 text--navy--200">Player name</div>
                                <div class="ek--lineup--body--tabs--substitutions--head--abl ek--size--16 text--navy--200">Abl</div>
                                <div class="ek--lineup--body--tabs--substitutions--head--energy ek--size--16 text--navy--200">Energy</div>
                            </div>
                            <!-- End EK Line Up Right Tabs Content Substitutions Head -->
                            <!-- EK Line Up Right Tabs Content Substitutions Scroll -->
                            <div class="ek--lineup--body--tabs--substitutions--scroll" id="substitutions">

                            </div>
                            <!-- End EK Line Up Right Tabs Content Substitutions Scroll -->
                        </div>
                        <!-- End EK Line Up Right Tabs Content Substitutions -->
                        <!-- EK Line Up Right Tabs Content Formation -->
                        <div class="ek--lineup--body--tabs--formation ek--lineup--body--tabs--content--table">
                            <!-- EK Line Up Right Tabs Content Formation Inner -->
                            <div class="ek--lineup--body--tabs--formation--inner">
                                <!-- EK Line Up Right Tabs Content Formation Inner Format -->

                                <!--<div class="ek&#45;&#45;lineup&#45;&#45;body&#45;&#45;tabs&#45;&#45;flex">-->
                                    <!--<div id="ek&#45;&#45;lineup&#45;&#45;stadium&#45;&#45;position&#45;&#45;343" class="ek&#45;&#45;lineup&#45;&#45;body&#45;&#45;tabs&#45;&#45;formation&#45;&#45;tactics bg&#45;&#45;navy&#45;&#45;600 text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 formation" th:classappend="${session.club.formation.id==4 ? 'button&#45;&#45;active' : ''}" data-id="4" format-id="3-4-3">3-4-3</div>-->

                                <!--</div>-->

                                <div class="ek--lineup--body--tabs--flex">
                                <div id="ek--lineup--stadium--position--343" @if(Session::get("club")["formation"]["id"] == 4) class="ek--lineup--body--tabs--formation--tactics bg--navy--600 text--white ek--size--16 formation button--active" @else class="ek--lineup--body--tabs--formation--tactics bg--navy--600 text--white ek--size--16 formation" @endif data-id="4" format-id="3-4-3">{{  __('translation.formation_4') }}</div>
                                </div>
                                <div class="ek--lineup--body--tabs--flex">
                                    <div id="ek--lineup--stadium--position--352" @if(Session::get("club")["formation"]["id"] == 7) class="ek--lineup--body--tabs--formation--tactics bg--navy--600 text--white ek--size--16 formation button--active" @else class="ek--lineup--body--tabs--formation--tactics bg--navy--600 text--white ek--size--16 formation" @endif data-id="7" format-id="3-5-2">{{  __('translation.formation_7') }}</div>
                                </div>
                                <div class="ek--lineup--body--tabs--flex">
                                <div id="ek--lineup--stadium--position--433" @if(Session::get("club")["formation"]["id"] == 1) class="ek--lineup--body--tabs--formation--tactics bg--navy--600 text--white ek--size--16 formation button--active" @else class="ek--lineup--body--tabs--formation--tactics bg--navy--600 text--white ek--size--16 formation" @endif data-id="1" format-id="4-3-3">{{  __('translation.formation_1') }}</div>
                                </div>
                                <div class="ek--lineup--body--tabs--flex">
                                    <div id="ek--lineup--stadium--position--442" @if(Session::get("club")["formation"]["id"] == 2) class="ek--lineup--body--tabs--formation--tactics bg--navy--600 text--white ek--size--16 formation button--active" @else class="ek--lineup--body--tabs--formation--tactics bg--navy--600 text--white ek--size--16 formation" @endif data-id="2" format-id="4-4-2">{{  __('translation.formation_2') }}</div>
                                </div>
                                <div class="ek--lineup--body--tabs--flex">
                                    <div id="ek--lineup--stadium--position--451" @if(Session::get("club")["formation"]["id"] == 5) class="ek--lineup--body--tabs--formation--tactics bg--navy--600 text--white ek--size--16 formation button--active" @else class="ek--lineup--body--tabs--formation--tactics bg--navy--600 text--white ek--size--16 formation" @endif data-id="5" format-id="4-5-1">{{  __('translation.formation_5') }}</div>
                                </div>
                                <div class="ek--lineup--body--tabs--flex">
                                    <div id="ek--lineup--stadium--position--523" @if(Session::get("club")["formation"]["id"] == 8) class="ek--lineup--body--tabs--formation--tactics bg--navy--600 text--white ek--size--16 formation button--active" @else class="ek--lineup--body--tabs--formation--tactics bg--navy--600 text--white ek--size--16 formation" @endif data-id="8" format-id="5-2-3">{{  __('translation.formation_8') }}</div>

                                </div>
                                <div class="ek--lineup--body--tabs--flex">
                                    <div id="ek--lineup--stadium--position--532" @if(Session::get("club")["formation"]["id"] == 6) class="ek--lineup--body--tabs--formation--tactics bg--navy--600 text--white ek--size--16 formation button--active" @else class="ek--lineup--body--tabs--formation--tactics bg--navy--600 text--white ek--size--16 formation" @endif data-id="6" format-id="5-3-2">{{  __('translation.formation_6') }}</div>

                                </div>
                                <div class="ek--lineup--body--tabs--flex">
                                <div id="ek--lineup--stadium--position--541" @if(Session::get("club")["formation"]["id"] == 3) class="ek--lineup--body--tabs--formation--tactics bg--navy--600 text--white ek--size--16 formation button--active" @else class="ek--lineup--body--tabs--formation--tactics bg--navy--600 text--white ek--size--16 formation" @endif data-id="3" format-id="5-4-1">{{  __('translation.formation_3') }}</div>
                                </div>
                                    <!-- End EK Line Up Right Tabs Content Formation Inner Format -->
                            </div>
                            <!-- End EK Line Up Right Tabs Content Formation Inner -->
                        </div>
                        <!-- End EK Line Up Right Tabs Content Formation -->
                        <div class="ek--lineup--body--tabs--tactics ek--lineup--body--tabs--content--table">
                            <div class="ek--lineup--body--tabs--tactics--inner bg--navy--600">
                                <h3 class="text--white ek--size--16--500">{{  __('translation.tactic_game_style') }}</h3>
                                <form>
                                    <input type="range" id="gamestyle" min="1" max="3" step="1" value="0" data-orientation="horizontal" >
                                    <div class="ek--lineup--body--tabs--tactics--inner--type">
                                        <span class="soft text--left">Soft</span>
                                        <span class="normal text--center range--active">Normal</span>
                                        <span class="aggresive text--right">Aggresive</span>
                                    </div>
                                </form>
                            </div>
                            <div class="ek--lineup--body--tabs--tactics--inner bg--navy--600">
                                <h3 class="text--white ek--size--16--500">{{  __('translation.tactic_passing_style') }}</h3>
                                <form>
                                    <input type="range" id="passingstyle" min="1" max="3" step="1" value="0" data-orientation="horizontal" >
                                    <div class="ek--lineup--body--tabs--tactics--inner--type">
                                        <span class="soft text--left range--active">Soft</span>
                                        <span class="normal text--center">Normal</span>
                                        <span class="aggresive text--right">Aggresive</span>
                                    </div>
                                </form>
                            </div>
                            <div class="ek--lineup--body--tabs--tactics--inner bg--navy--600">
                                <h3 class="text--white ek--size--16--500">{{  __('translation.tactic_pressing_style') }}</h3>
                                <form>
                                    <input type="range" id="pressingstyle" min="1" max="3" step="1" value="0" data-orientation="horizontal" >
                                    <div class="ek--lineup--body--tabs--tactics--inner--type">
                                        <span class="soft text--left">Soft</span>
                                        <span class="normal text--center">Normal</span>
                                        <span class="aggresive text--right range--active">Aggresive</span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- End EK Line Up Right Tabs Content -->
                </div>
                <!-- End EK Line Up Right Tabs -->
            </div>
            <!-- End EK Line Up Right -->
        </div>
        <!-- End EK Line Up Body -->
    </section>
    <!-- End Ek Line Up -->

@endsection

@section('appendix')
    <div id="ek--squad--modals--refill--buy" class="ek--squad--modals--refill bg--navy--700 modal">
        <!-- Ek Body Squad Refill Buy Modal Head -->
        <div class="ek--squad--modals--refill--head">
            <div class="ek--squad--modals--refill--head--energy bg--navy--600">
                <svg class="icon icon-ek--energy fill--navy--200"><use xlink:href="static/img/icons.svg#icon-ek--energy"></use></svg>
            </div>
            <!-- Ek Body Squad Refill Buy Modal Want -->
            <div class="ek--squad--modals--refill--head--want">
                <h4 class="text--white ek--size--20">{{  __('translation.action_refill_energy') }}</h4>
                <p>{{  __('translation.refill_energy_dialog_all_player') }}</p>
                <!-- Ek Body Squad Refill Buy Modal Want Coins -->
                <div class="ek--squad--modals--refill--head--want--coins bg--navy--800">
                    <span class="text--navy--100 ek--size--18">{{  __('translation.buy_coin_message') }}</span>
                    <!-- Ek Body Squad Refill Buy Modal Want Coins Amount -->
                    <div class="ek--squad--modals--refill--head--want--amount">
                        <span class="text--white ek--size--16" id="cost"></span>
                        <img src="{{url("static/img/Dashboard/ek--want--coins.svg")}}" />
                    </div>
                    <!-- End Ek Body Squad Refill Buy Modal Want Coins Amount -->
                </div>
                <!-- End Ek Body Squad Refill Buy Modal Want Coins -->
            </div>
            <!-- End Ek Body Squad Refill Buy Modal Want -->
        </div>
        <!-- End Ek Body Squad Refill Buy Modal Head -->

        <div class="ek--squad--modals--refill--buttons">
            <a class="ek--squad--modals--refill--buttons--cancel2 text--white ek--size--16 bg--navy--600" href="#" rel="modal:close">{{  __('translation.play_dialogs_action_cancel') }}</a>
            <!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;energy2 text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;blue" href="#ek&#45;&#45;notification">Refill Energy</a>-->
            <button  class="ek--squad--modals--refill--buttons--energy2 text--white ek--size--16 bg--blue"  id="refill" >{{  __('translation.action_refill_energy') }}</button>

        </div>
    </div>


    <div id="ek--notification" class="ek--notification bg--navy--700 modal">
        <div class="ek--notification--body">
            <!-- Ek Body Squad Message Notification Modal Left -->
            <div class="ek--notification--left bg--green--dark--two"></div>
            <!-- End Ek Body Squad Message Notification Modal left -->
            <!-- Ek Body Squad Message Notification Modal Right -->
            <div class="ek--notification--right">
                <div class="ek--notification--right--head">
                    <div class="ek--notification--right--image bg--green--dark--two">
                        <svg class="icon icon-ek--correct fill--white"><use xlink:href="img/icons.svg#icon-ek--correct"></use></svg>
                    </div>
                    <div class="ek--notification--right--info">
                        <h4 class="text--white ek--size--20">Team energy update!</h4>
                        <p class="text--navy--100 ek--size--18" >Team's energy has successfully filled!</p>
                    </div>
                </div>
                <div class="ek--notification--right--cancel">
                    <a class="text--white bg--navy--600 ek--size--16--500" href="#" rel="modal:close">{{  __('translation.play_dialogs_action_cancel') }}</a>
                </div>
            </div>
            <!-- End Ek Body Squad Message Notification Modal Right -->
        </div>
    </div>


    <div id="ek--soft--notenough" class="ek--modal ek--modal--border--left--orange bg--navy--700 modal">
        <div class="ek--modal--body">
            <div class="ek--modal--body--left">
                <div class="ek--modal--body--left--image bg--orange--light">
                    <svg class="icon icon-ek--warning fill--white"><use xlink:href="{{ url("static/img/icons.svg#icon-ek--warning") }}"></use></svg>
                </div>
            </div>
            <div class="ek--modal--body--right">
                <h3 class="text--white ek--size--20">Ek Coin is not enough</h3>
                <p class="text--navy--100 ek--size--18">There is not enough to refill energy</p>
                <div class="ek--modal--body--buttons">
                    <a class="ek--modal--body--buttons--cancel text--white ek--size--16" href="#ek--soft" rel="modal:close">{{  __('translation.play_dialogs_action_cancel') }}</a>
                    <!--<a class="ek&#45;&#45;modal&#45;&#45;body&#45;&#45;buttons&#45;&#45;other text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500 bg&#45;&#45;blue" href="javascript:void(0)">Buy EK Coins</a>-->
                </div>
            </div>
        </div>
    </div>



    <div id="loading" class="ek--modal--loading" style="z-index: 110;">
        <div class="ek--modal--loading--body">
            <div class="ek--modal--loading--body--inner">
                <div class="ek--modal--loading--body--image bg--navy--700">
                    <img src="{{url("static/img/Dashboard/ek--loading@2x.png")}}" />
                </div>
                <p class="text--navy--100 ek--size--16">Formation is changed..</p>
            </div>
        </div>
    </div>
@endsection

@section("javascript")

    <script src="{{url("static/js/lineup.js")}}"></script>

    <script>

        var preloader = document.getElementById('loading');
        function myFunction() {

            preloader.style.display ='none';
            $('#loading').fadeOut('slow');

        }


    </script>
@endsection

