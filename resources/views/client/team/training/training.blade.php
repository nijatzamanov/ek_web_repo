@extends('client.layout.authenticated')
@section('content')

    <!-- Ek Camp -->
    <section class="ek--camp">
        <div class="ek--camp--body">
            <div class="ek--camp--body--choose bg--navy--700">
                <div class="ek--camp--body--choose--head">
                    <h3 class="text--white ek--size--20">Choose training camp</h3>
                </div>
                <div class="ek--camp--body--choose--training">


                    @foreach( Session::get("camps") as $camp )
                    <div class="ek--camp--body--choose--training--inner bg--navy--600">
                        <div class="ek--camp--body--choose--training--head">
                            <div class="ek--camp--body--choose--training--location bg--navy--500"><svg class="icon icon-ek--location fill--white"><use xlink:href="static/img/icons.svg#icon-ek--location"></use></svg></div>
                            <span class="text--white ek--size--18">{{ $camp["location"] }}</span>
                        </div>
                        <div class="ek--camp--body--choose--training--transition">
                            <div class="ek--camp--body--choose--training--transition--hotel">
                                <h3 class="ek--camp--body--choose--training--transition--title" >Hotel</h3>
                                <div class="ek--camp--body--choose--training--transition--image bg--white">
                                    <img src="{{ url("static/img/Logo/ek--hilton@2x.png") }}">
                                </div>
                            </div>
                            <div class="ek--camp--body--choose--training--transition--airlines">
                                    <h3 class="ek--camp--body--choose--training--transition--title" >Airlines</h3>
                                    <div class="ek--camp--body--choose--training--transition--image bg--white">
                                        <img src="{{ url("static/img/Logo/ek--airline@2x.png") }}">
                                    </div>
                            </div>
                            <div class="ek--camp--body--choose--training--transition--transport">
                                    <h3 class="ek--camp--body--choose--training--transition--title" >Transport</h3>
                                    <div class="ek--camp--body--choose--training--transition--image bg--white">
                                        <img src="{{ url("static/img/Logo/ek--auto@2x.png") }}">
                                    </div>
                            </div>
                        </div>
                        <div class="ek--camp--body--choose--training--feature bg--navy--700">
                            <div class="ek--camp--body--choose--training--feature--component">
                                <h3>Duration</h3>
                                <p>{{ $camp["duration"] }} days</p>
                            </div>
                            <div class="ek--camp--body--choose--training--feature--component">
                                <h3>Abl. Change</h3>
                                <p><span>{{ $camp["startAbility"] }}</span>-<span>{{ $camp["endAbility"] }}</span></p>
                            </div>
                            <div class="ek--camp--body--choose--training--feature--component">
                                <h3>Cost</h3>
                                <!--<p th:text="${camp.cost}">$11M</p>-->
                                @if($camp["cost"] < 1000)
                                    <p><span>{{ $camp["cost"] }}</span></p>
                                @endif

                                @if($camp["cost"] >= 1000 && $camp["cost"] < 1000000)
                                    <p><span>{{ number_format($camp["cost"]/1000,1) }}</span><span >K</span></p>
                                @endif

                                @if($camp["cost"] >= 1000000 && $camp["cost"] < 1000000000)
                                    <p><span>{{ number_format($camp["cost"]/1000000,1) }}</span><span >M</span></p>
                                @endif

                                @if($camp["cost"] >= 1000000000)
                                    <p><span>{{ number_format($camp["cost"]/1000000000,1) }}</span><span >B</span></p>
                                @endif

                            </div>
                        </div>
                        <div class="ek--camp--body--choose--training--thiscamp">
                            <a class="bg--blue ek--size--16--500 text--white" id="chooseCamp" href="#" data-id="{{ $camp["id"] }}">Choose this camp</a>
                        </div>
                        <!-- Ek Camp Training -->
                        <div id="ek--training{{$camp["id"]}}"  class="ek--modal bg--navy--700 modal">
                            <div class="ek--modal--body">
                                <div class="ek--modal--body--left">
                                    <div class="ek--modal--body--left--image bg--navy--600">
                                        <svg class="icon icon-ek--location fill--navy--200"><use xlink:href="static/img/icons.svg#icon-ek--location"></use></svg>
                                    </div>
                                </div>
                                <div class="ek--modal--body--right">
                                    <h3 class="text--white ek--size--20">Training camp</h3>
                                    <p class="text--navy--100 ek--size--18">Are you sure to take your team to the camp at <span class="text--navy--100 ek--size--18">{{ $camp["location"] }}</span>?</p>
                                    <div class="ek--modal--body--coins">
                                        <div class="ek--camp--body--choose--training--feature--component bg--navy--800">
                                            <h3>Duration</h3>
                                            <p>{{ $camp["duration"] }} days</p>
                                        </div>
                                        <div class="ek--camp--body--choose--training--feature--component bg--navy--800">
                                            <h3>Abl. Change</h3>
                                            <p><span>{{ $camp["startAbility"] }}</span>-<span>{{ $camp["endAbility"] }}</span></p>
                                        </div>
                                        <div class="ek--camp--body--choose--training--feature--component bg--navy--800">
                                            <h3>Cost</h3>
                                            <!--<p th:text="${camp.cost}">$11M</p>-->
                                            @if($camp["cost"] < 1000)
                                                <p><span>{{ $camp["cost"] }}</span></p>
                                            @endif

                                            @if($camp["cost"] >= 1000 && $camp["cost"] < 1000000)
                                                <p><span>{{ number_format($camp["cost"]/1000,1) }}</span><span >K</span></p>
                                            @endif

                                            @if($camp["cost"] >= 1000000 && $camp["cost"] < 1000000000)
                                                <p><span>{{ number_format($camp["cost"]/1000000,1) }}</span><span >M</span></p>
                                            @endif

                                            @if($camp["cost"] >= 1000000000)
                                                <p><span>{{ number_format($camp["cost"]/1000000000,1) }}</span><span >B</span></p>
                                            @endif

                                        </div>

                                    </div>
                                    <div class="ek--modal--body--buttons">
                                        <a class="ek--modal--body--buttons--cancel text--white ek--size--16" href="#" rel="modal:close">Cancel</a>
                                        <!--<a class="ek&#45;&#45;modal&#45;&#45;body&#45;&#45;buttons&#45;&#45;other ek&#45;&#45;start&#45;&#45;training&#45;&#45;camp text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500 bg&#45;&#45;blue" href="#" rel="modal:close">Go camp </a>-->
                                    <button class="ek--modal--body--buttons--other ek--start--training--camp text--white ek--size--16--500 bg--blue" id="trainingCamp" data-id="{{$camp["id"]}}">Go camp</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Ek Camp Training -->
                        <!-- Ek Camp Training Loading -->
                        <div class="ek--training--loading ek--modal--loading">
                            <div class="ek--modal--loading--body">
                                <div class="ek--modal--body m-auto bg--navy--700">
                                    <div class="ek--modal--body--left">
                                        <div class="ek--modal--body--left--image bg--navy--600">
                                            <svg class="icon icon-ek--location fill--navy--200"><use xlink:href="static/img/icons.svg#icon-ek--location"></use></svg>
                                        </div>
                                    </div>
                                    <div class="ek--modal--body--right">
                                        <h3 class="text--white ek--size--20 text--start">Training camp</h3>
                                        <p class="text--navy--100 ek--size--18 text--start">Camp is loading..</p>
                                        <h4 class="text--white text--start ek--size--18">Baku is the capital of Azerbaijan</h4>
                                        <div class="ek--modal--body--map bg--navy--600"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Ek Camp Training Loading -->

                        <div id="ek--soft--notenough" class="ek--modal ek--modal--border--left--orange bg--navy--700 modal">
                            <div class="ek--modal--body">
                                <div class="ek--modal--body--left">
                                    <div class="ek--modal--body--left--image bg--orange--light">
                                        <svg class="icon icon-ek--warning fill--white"><use xlink:href="static/img/icons.svg#icon-ek--warning"></use></svg>
                                    </div>
                                </div>
                                <div class="ek--modal--body--right">
                                    <h3 class="text--white ek--size--20">Money is not enough</h3>
                                    <p class="text--navy--100 ek--size--18">There is not enough money to go Training camp</p>
                                    <div class="ek--modal--body--buttons">
                                        <a class="ek--modal--body--buttons--cancel text--white ek--size--16" href="#ek--soft" rel="modal:open">Cancel</a>

                                        <!--<a class="ek&#45;&#45;modal&#45;&#45;body&#45;&#45;buttons&#45;&#45;other text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500 bg&#45;&#45;blue" href="javascript:void(0)">Buy EK Coins</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div id="ek--camp--exist" class="ek--modal ek--modal--border--left--orange bg--navy--700 modal">
                            <div class="ek--modal--body">
                                <div class="ek--modal--body--left">
                                    <div class="ek--modal--body--left--image bg--orange--light">
                                        <svg class="icon icon-ek--warning fill--white"><use xlink:href="static/img/icons.svg#icon-ek--warning"></use></svg>
                                    </div>
                                </div>
                                <div class="ek--modal--body--right">
                                    <h3 class="text--white ek--size--20">You already have Training Camp</h3>
                                    <p class="text--navy--100 ek--size--18">You already have used your Training Camp limit to decided time period</p>
                                    <div class="ek--modal--body--buttons">
                                        <a class="ek--modal--body--buttons--cancel text--white ek--size--16" href="#ek--soft" rel="modal:open">Cancel</a>

                                        <!--<a class="ek&#45;&#45;modal&#45;&#45;body&#45;&#45;buttons&#45;&#45;other text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500 bg&#45;&#45;blue" href="javascript:void(0)">Buy EK Coins</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>





                        <div id="training" class="ek--modal ek--modal--border--left--orange bg--navy--700 modal">
                            <div class="ek--modal--body">
                                <div class="ek--modal--body--left">
                                    <div class="ek--modal--body--left--image bg--orange--light">
                                        <svg class="icon icon-ek--warning fill--white"><use xlink:href="static/img/icons.svg#icon-ek--warning"></use></svg>
                                    </div>
                                </div>
                                <div class="ek--modal--body--right">
                                    <!--<h3 class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;20">Money is not enough</h3>-->
                                    <p class="text--navy--100 ek--size--18">You already have used your Training Camp limit to decided time period</p>
                                    <div class="ek--modal--body--buttons">
                                        <a class="ek--modal--body--buttons--cancel text--white ek--size--16" href="#ek--soft" rel="modal:open">Cancel</a>

                                        <!--<a class="ek&#45;&#45;modal&#45;&#45;body&#45;&#45;buttons&#45;&#45;other text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500 bg&#45;&#45;blue" href="javascript:void(0)">Buy EK Coins</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>









                    </div>
                    @endforeach




                </div>
            </div>

        </div>

    </section>

@endsection

@section('javascript')
    <script src="{{url("static/js/training.js")}}"></script>
@endsection

