@extends('client.layout.authenticated')
@section('content')

            <!-- Ek Daily -->
            <section class="ek--daily">
                <!-- Training Complete -->
                <div class="ek--complete bg--navy--700" id="ek--complete">
                    <!-- Ek Daily Body Head -->
                    <div class="ek--daily--body--head">
                        <!-- Ek Daily Body Head Title -->
                        <h3 class="text--white ek--size--20">Training completed</h3>
                        <!-- End Ek Daily Body Head Title -->
                    </div>
                    <!-- Ek Daily Body Head -->
                    <!-- Ek Daily Body Select -->
                    <div class="ek--daily--body--select">
                        <!-- Ek Daily Body Player -->
                        <div class="ek--daily--body--players" style="width: 100%">
                            <!-- Ek Daily Body Player Head -->
                            <div class="ek--daily--body--players--head">
                                <!-- Ek Daily Body Player Head Player -->
                                <div class="ek--daily--body--players--head--player text--navy--200 ek--size--16">Player name</div>
                                <!-- End Ek Daily Body Player Head Player -->
                                <!-- Ek Daily Body Player Head Age -->
                                <div class="ek--daily--body--players--head--age text--navy--200 ek--size--16">Age</div>
                                <!-- End Ek Daily Body Player Head Age -->
                                <!-- Ek Daily Body Player Head Abl -->
                                <div class="ek--daily--body--players--head--abl text--navy--200 ek--size--16">Abl</div>
                                <!-- End Ek Daily Body Player Head Abl -->
                                <!-- Ek Daily Body Player Head Energy -->
                                <div class="ek--daily--body--players--head--energy text--navy--200 ek--size--16">Energy</div>
                                <!-- Ek Daily Body Player Head Energy -->
                            </div>
                            <!-- End Ek Daily Body Player Head -->
                            <!-- Ek Daily Body Player List -->
                            <div class="ek--daily--body--players--list">

                                @php
                                    $players = Session::get("club")["players"];
                                    function sortByAbilityChanged($a, $b) {
                                        return $a['abilityChanged'] < $b['abilityChanged'];
                                    }

                                    usort($players, 'sortByAbilityChanged');
                                @endphp

                                <!-- Ek Daily Body Player List Inner -->
                                @foreach( $players as $player)
                                    <div class="ek--daily--body--players--inner bg--navy--600">
                                        <!-- Ek Daily Body Player List Inner Player -->
                                        <div class="ek--daily--body--players--inner--player text--white ek--size-16">{{ $player["name"] }}</div>
                                        <!-- End Ek Daily Body Player List Inner Player -->
                                        <!-- Ek Daily Body Player List Inner Age -->
                                        <div class="ek--daily--body--players--inner--age text--white ek--size-16">{{ $player["age"] }}</div>
                                        <!-- End Ek Daily Body Player List Inner Age -->
                                        <!-- Ek Daily Body Player List Inner Abl -->
                                        <div class="ek--daily--body--players--inner--abl text--white ek--size-16">
                                            <span class="text--white ek--size--16">{{ number_format($player["ability"],0) }}</span>
                                            <span class="ek--margin">
                                                <span class="text--green ek--size--16" >+</span>
                                                <span class="text--green ek--size--16 ml-0">{{ number_format($player["abilityChanged"],2) }}</span>
                                           </span>
                                        </div>
                                        <!-- End Ek Daily Body Player List Inner Abl -->
                                        <!-- Ek Daily Body Player List Inner Energy -->
                                        <div class="ek--daily--body--players--inner--energy">
                                            <!-- Ek Daily Body Player List Inner Energy Progress -->
                                            <div class="ek--daily--body--players--inner--energy--progress bg--navy--500">
                                                <div class="progress">
                                                    @if($player["energy"] <=100 && $player["energy"] > 90)
                                                        <div class="progress-bar bg--green--dark--two" role="progressbar" style="width:{{ $player["energy"] }}%" aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                                    @endif

                                                    @if($player["energy"] <=90 && $player["energy"] > 55)
                                                        <div class="progress-bar bg--green--two" role="progressbar" style="width:{{ $player["energy"] }}%"  aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                                    @endif

                                                    @if($player["energy"] <=55 && $player["energy"] > 25)
                                                        <div class="progress-bar bg--yellow--two" role="progressbar" style="width:{{ $player["energy"] }}%" aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                                    @endif

                                                    @if($player["energy"] <=25)
                                                        <div class="progress-bar bg--orange" role="progressbar" style="width:{{ $player["energy"] }}%" aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                                    @endif
                                                </div>
                                            </div>
                                            <!-- End Ek Daily Body Player List Inner Energy Progress -->

                                            <span class="text--green--dark--two ek--size--16" >{{$player["energy"]}}</span><span class="text--green--dark--two ek--size--16">%</span>
                                            <span class="ek--margin">
                                                <span class="text--red ek--size--16">-<span class="text--red ek--size--16 ml-0">{{$player["energyChanged"]}}</span></span>
                                             </span>
                                        </div>
                                        <!-- End Ek Daily Body Player List Inner Energy -->
                                    </div>
                                @endforeach
                                <!-- End Ek Daily Body Player List Inner -->


                            </div>
                            <!-- End Ek Daily Body Player List -->
                        </div>
                        <!-- End Ek Daily Body Player -->
                    </div>
                    <!-- End Ek Daily Body Select -->
                </div>
                <!-- End Ek Training Complete -->
            </section>
            <!-- End Ek Daily -->

@endsection



@section('appendix')
<!-- Ek Camp Training Succes -->
<div id="ek--training--success" class="ek--modal--loading ek--training--success modal">
    <div class="ek--modal--loading--body">
        <div class="ek--modal--body ek--modal--border--left--green--dark--two bg--navy--700">
            <div class="ek--modal--body--left">
                <div class="ek--modal--body--left--image bg--green--dark--two">
                    <svg class="icon icon-ek--correct fill--white"><use xlink:href="static/img/icons.svg#icon-ek--correct"></use></svg>
                </div>
            </div>
            <div class="ek--modal--body--right">
                <h3 class="text--white ek--size--20 text--start">Training camp completed successfully</h3>
                <p class="text--navy--100 ek--size--18 text--start">Your team's average ability increased:</p>
                <div class="ek--training--success--ability bg--navy--800">
                    <p class="text--green ek--size--28--500">4.3</p>
                </div>
                <div class="ek--modal--body--buttons">
                    <a class="ek--modal--body--buttons--cancel ek--training--success--cancel text--white ek--size--16" href="#">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Ek Camp Training success -->



<div id="loading" class="ek--modal--loading modal">
    <div class="ek--modal--loading--body">
        <div class="ek--modal--loading--body--inner">
            <div class="ek--modal--loading--body--image bg--navy--700">
                <img src="{{ url("static/img/Dashboard/ek--loading@2x.png") }}" />
            </div>
            <p class="text--navy--100 ek--size--16">Training is started...</p>
        </div>
    </div>
</div>
@endsection

