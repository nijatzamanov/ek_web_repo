@extends('client.layout.authenticated')
@section('content')


    <!-- Ek Daily -->
    <section class="ek--daily">
        <!-- Ek Daily Body -->
        <div class="ek--daily--body bg--navy--700">
            <!-- Ek Daily Body Head -->
            <div class="ek--daily--body--head">
                <!-- Ek Daily Body Head Title -->
                <h3 class="text--white ek--size--20">Select players for training</h3>
                <!-- End Ek Daily Body Head Title -->
            </div>
            <!-- Ek Daily Body Head -->
            <!-- Ek Daily Body Select -->
            <div class="ek--daily--body--select">
                <!-- Ek Daily Body Player -->
                <div class="ek--daily--body--players">
                    <!-- Ek Daily Body Player Head -->
                    <div class="ek--daily--body--players--head">
                        <!-- Ek Daily Body Player Head All -->
                        <div class="ek--daily--body--players--head--all">
                            <!-- Ek Daily Body Player Head All Check -->
                            <div class="ek--daily--body--players--head--check">
                                <input type="checkbox" id="all-check">
                                <label class="text--navy--200 ek--size--16">All</label>
                            </div>
                            <!-- End Ek Daily Body Player Head All Check -->
                        </div>
                        <!-- End Ek Daily Body Player Head All -->
                        <!-- Ek Daily Body Player Head Player -->
                        <div class="ek--daily--body--players--head--player text--navy--200 ek--size--16">{{  __('translation.daily_training_player') }}</div>
                        <!-- End Ek Daily Body Player Head Player -->
                        <!-- Ek Daily Body Player Head Age -->
                        <div class="ek--daily--body--players--head--age text--navy--200 ek--size--16">{{  __('translation.daily_training_age') }}</div>
                        <!-- End Ek Daily Body Player Head Age -->
                        <!-- Ek Daily Body Player Head Abl -->
                        <div class="ek--daily--body--players--head--abl text--navy--200 ek--size--16">{{  __('translation.daily_training_ability') }}</div>
                        <!-- End Ek Daily Body Player Head Abl -->
                        <!-- Ek Daily Body Player Head Energy -->
                        <div class="ek--daily--body--players--head--energy text--navy--200 ek--size--16">{{  __('translation.daily_training_energy') }}</div>
                        <!-- Ek Daily Body Player Head Energy -->
                    </div>
                    <!-- End Ek Daily Body Player Head -->




                    <!-- Ek Daily Body Player List -->
                    <div class="ek--daily--body--players--list">
                        <!-- Ek Daily Body Player List Inner -->





                        <!-- Ek Daily Body Player List Inner -->
                        @foreach( Session::get("club")["players"] as $player )
                        <div class="ek--daily--body--players--inner bg--navy--600" id="players--check">
                            <!-- Ek Daily Body Player List Inner Select -->
                            <div class="ek--daily--body--players--inner--select">
                                <div class="ek--daily--body--players--inner--check" >
                                    @if($player["energy"] > 25)
                                        <input type="checkbox" id="player-check" name="player" data-id="{{ $player["id"] }}" />
                                    @endif

                                    @if($player["energy"] <= 25)
                                    <input type="checkbox" id="player-check" class="disable" disabled/>
                                    @endif

                                    <label for="player-check" class="text--navy--200 ek--size--16"></label>
                                </div>
                            </div>


                            <!-- End Ek Daily Body Player List Inner Select -->
                            <!-- Ek Daily Body Player List Inner Player -->
                            <div class="ek--daily--body--players--inner--player text--white ek--size-16">{{$player["name"]}}</div>
                            <!-- End Ek Daily Body Player List Inner Player -->
                            <!-- Ek Daily Body Player List Inner Age -->
                            <div class="ek--daily--body--players--inner--age text--white ek--size-16">{{$player["age"]}}</div>
                            <!-- End Ek Daily Body Player List Inner Age -->
                            <!-- Ek Daily Body Player List Inner Abl -->
                            <div class="ek--daily--body--players--inner--abl text--white ek--size-16">
                                <span class="text--white ek--size--16">{{ number_format($player["ability"],0) }}</span>
                            </div>
                            <!-- End Ek Daily Body Player List Inner Abl -->
                            <!-- Ek Daily Body Player List Inner Energy -->
                            <div class="ek--daily--body--players--inner--energy">
                                <!-- Ek Daily Body Player List Inner Energy Progress -->
                                <div class="ek--daily--body--players--inner--energy--progress bg--navy--500">
                                    <div class="progress">
                                        @if($player["energy"] <= 100 && $player["energy"] > 90)
                                            <div class="progress-bar bg--green--dark--two" role="progressbar" style="width:{{ $player["energy"] }}%" aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                        @endif

                                        @if($player["energy"] <=90 && $player["energy"] > 55)
                                            <div class="progress-bar bg--green--two" role="progressbar" style="width:{{ $player["energy"] }}%"  aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                        @endif

                                        @if($player["energy"] <=55 && $player["energy"] > 25)
                                            <div class="progress-bar bg--yellow--two" role="progressbar" style="width:{{ $player["energy"] }}%" aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                        @endif

                                        @if($player["energy"] <=25)
                                            <div class="progress-bar bg--orange" role="progressbar" style="width:{{ $player["energy"] }}%" aria-valuemin="0" aria-valuenow="{{$player["energy"]}}" aria-valuemax="100"></div>
                                        @endif
                                    </div>
                                </div>
                                <!-- End Ek Daily Body Player List Inner Energy Progress -->
                                <span class="text--green--two ek--size--16">{{$player["energy"]}}</span><span class="text--green--two ek--size--16">%</span>
                            </div>
                            <!-- End Ek Daily Body Player List Inner Energy -->
                        </div>
                        @endforeach
                        <!-- End Ek Daily Body Player List Inner -->
                    </div>
                    <!-- End Ek Daily Body Player List -->
                </div>
                <!-- End Ek Daily Body Player -->
                <!-- Ek Daily Body Players Count -->
                <div class="ek--daily--body--players--count">
                    <!-- Ek Daily Body Players Count Amount -->
                    <p class="ek--daily--body--players--count--amount text--navy--200 ek--size--16"><span>0</span> players selected</p>
                    <!-- End Ek Daily Body Players Count Amount -->
                    <!-- Ek Daily Body Players Count Training -->
                    <a class="ek--daily--body--players--count--training ek--size--16--500" href="#">Select training type</a>
                    <!-- End Ek Daily Body Players Count Training -->
                </div>
                <!-- End Ek Daily Body Players Count -->
            </div>
            <!-- End Ek Daily Body Select -->
        </div>
        <!-- End Ek Daily Body -->

        <!-- Ek Training -->
        <div class="ek--training">
            <!-- Ek Training Type -->
            <div class="ek--training--type bg--navy--700">
                <!-- Ek Training Type Head -->
                <div class="ek--training--type--head">
                    <!-- Ek Training Type Body  Back-->
                    <div class="ek--back ek--training--type--head--back bg--navy--500">
                        <svg class="icon icon-arrow_left fill--navy--200"><use xlink:href="static/img/icons.svg#icon-arrow_left"></use></svg>
                    </div>
                    <!-- End Ek Training Type Body  Back-->
                    <h3 class="text--white ek--size--20">Choose training type</h3>
                </div>
                <!-- End Ek Training Type Head -->
                <!-- Ek Training Type Body -->
                <div class="ek--training--type--body">
                    <!-- Ek Training Type Body Inner -->
                    <div class="ek--training--type--body--inner bg--navy--600">
                        <div class="ek--training--type--body--inner--image bg--navy--700">
                            <img src="{{url("static/img/Dashboard/ek--training--low@2x.png")}}" />
                        </div>
                        <h3 class="ek--training--type--body--inner--title text--white ek--size--18">Soft Training</h3>
                        <div class="ek--training--type--body--inner--feature">
                            <div class="ek--training--type--body--inner--feature--left">
                                <h4 class="text--navy--200 ek--size--16">Energy Loss</h4>
                                <p class="text--white ek--size--16--500">Low</p>
                            </div>
                            <div class="ek--training--type--body--inner--feature--right">
                                <h4 class="text--navy--200 ek--size--16">Improvement</h4>
                                <p class="text--white ek--size--16--500"><span class="text--green ek--size--16--500">50% - 70%</span> of potensial</p>
                            </div>
                        </div>
                        <!--<a class="ek&#45;&#45;training&#45;&#45;type&#45;&#45;body&#45;&#45;inner&#45;&#45;button bg&#45;&#45;blue text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" href="#ek&#45;&#45;soft" rel="modal:open">Start training</a>-->
                        <button class="ek--training--type--body--inner--button bg--blue text--white ek--size--16--500"  id="trainingCostButton" data-id="1">Start training</button>
                        <!-- Ek Soft Modal -->
                        <div id="ek--soft" class="ek--modal bg--navy--700 modal">
                            <div class="ek--modal--body">
                                <div class="ek--modal--body--left">
                                    <div class="ek--modal--body--left--image bg--navy--600">
                                        <img src="{{url("static/img/Dashboard/ek--training--low--two@2x.png")}}" />
                                    </div>
                                </div>
                                <div class="ek--modal--body--right " >
                                    <h3 class="text--white ek--size--20">Soft Training</h3>
                                    <p class="text--navy--100 ek--size--18">Do you want to start Soft training?</p>
                                    <div class="ek--modal--body--coins bg--navy--800">
                                        <span class="ek--modal--body--coins--left text--navy--100 ek--size--18">It will cost you</span>
                                        <span class="ek--modal--body--coins--right text--white ek--size--16" id="training1">6 <img src="{{url("static/img/Dashboard/ek--training--low--two@2x.png")}}" /></span>
                                    </div>
                                    <div class="ek--modal--body--buttons">
                                        <a class="ek--modal--body--buttons--cancel text--white ek--size--16" href="#" rel="modal:close">Cancel</a>

                                        <!--<a class="ek&#45;&#45;modal&#45;&#45;body&#45;&#45;buttons&#45;&#45;other ek&#45;&#45;start&#45;&#45;training text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500 bg&#45;&#45;blue" href="#" rel="modal:close">Start training</a>-->
                                        <button class="ek--modal--body--buttons--other ek--start--training text--white ek--size--16--500 bg--blue"  id="trainingButton" data-id="1">Start Training</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Ek Soft Modal



             <!-- Ek Normal Modal -->
                        <div id="ek--normal" class="ek--modal bg--navy--700 modal">
                            <div class="ek--modal--body">
                                <div class="ek--modal--body--left">
                                    <div class="ek--modal--body--left--image bg--navy--600">
                                        <img src="{{url("static/img/Dashboard/ek--training--low--two@2x.png")}}" />
                                    </div>
                                </div>
                                <div class="ek--modal--body--right " >
                                    <h3 class="text--white ek--size--20">Normal Training</h3>
                                    <p class="text--navy--100 ek--size--18">Do you want to start Normal training?</p>
                                    <div class="ek--modal--body--coins bg--navy--800">
                                        <span class="ek--modal--body--coins--left text--navy--100 ek--size--18">It will cost you</span>
                                        <span class="ek--modal--body--coins--right text--white ek--size--16" id="training1">6 <img src="{{url("static/img/Dashboard/ek--want--coins.svg")}}" /></span>
                                    </div>
                                    <div class="ek--modal--body--buttons">
                                        <a class="ek--modal--body--buttons--cancel text--white ek--size--16" href="#" rel="modal:close">Cancel</a>

                                        <!--<a class="ek&#45;&#45;modal&#45;&#45;body&#45;&#45;buttons&#45;&#45;other ek&#45;&#45;start&#45;&#45;training text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500 bg&#45;&#45;blue" href="#" rel="modal:close">Start training</a>-->
                                        <button class="ek--modal--body--buttons--other ek--start--training text--white ek--size--16--500 bg--blue"  id="trainingButton" data-id="2">Start Training</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Ek Normal Modal




             <!-- Ek High Modal -->
                        <div id="ek--high" class="ek--modal bg--navy--700 modal">
                            <div class="ek--modal--body">
                                <div class="ek--modal--body--left">
                                    <div class="ek--modal--body--left--image bg--navy--600">
                                        <img src="{{ url("static/img/Dashboard/ek--training--low--two@2x.png") }}" />
                                    </div>
                                </div>
                                <div class="ek--modal--body--right " >
                                    <h3 class="text--white ek--size--20">High Training</h3>
                                    <p class="text--navy--100 ek--size--18">Do you want to start High training?</p>
                                    <div class="ek--modal--body--coins bg--navy--800">
                                        <span class="ek--modal--body--coins--left text--navy--100 ek--size--18">It will cost you</span>
                                        <span class="ek--modal--body--coins--right text--white ek--size--16" id="training1">6 <img src="{{ url("static/img/Dashboard/ek--want--coins.svg") }}" /></span>
                                    </div>
                                    <div class="ek--modal--body--buttons">
                                        <a class="ek--modal--body--buttons--cancel text--white ek--size--16" href="#" rel="modal:close">Cancel</a>

                                        <!--<a class="ek&#45;&#45;modal&#45;&#45;body&#45;&#45;buttons&#45;&#45;other ek&#45;&#45;start&#45;&#45;training text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500 bg&#45;&#45;blue" href="#" rel="modal:close">Start training</a>-->
                                        <button class="ek--modal--body--buttons--other ek--start--training text--white ek--size--16--500 bg--blue"  id="trainingButton" data-id="3">Start Training</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Ek High Modal

                        <!-- Ek Training Start -->
                        <div id="ek--soft--start" class="ek--modal--loading">
                            <div class="ek--modal--loading--body">
                                <div class="ek--modal--loading--body--inner">
                                    <div class="ek--modal--loading--body--image bg--navy--700">
                                        <img src="{{ url("static/img/Dashboard/ek--loading@2x.png") }}" />
                                    </div>
                                    <p class="text--navy--100 ek--size--16">Training is started..</p>
                                </div>
                            </div>
                        </div>
                        <!-- </div> -->
                        <!-- End Ek Training Start -->


                        <!-- Ek Soft Modal -->
                        <div id="ek--soft--notenough" class="ek--modal ek--modal--border--left--orange bg--navy--700 modal">
                            <div class="ek--modal--body">
                                <div class="ek--modal--body--left">
                                    <div class="ek--modal--body--left--image bg--orange--light">
                                        <svg class="icon icon-ek--warning fill--white"><use xlink:href="{{ url("static/img/icons.svg#icon-ek--warning") }}"></use></svg>
                                    </div>
                                </div>
                                <div class="ek--modal--body--right">
                                    <h3 class="text--white ek--size--20">Ek Coin is not enough</h3>
                                    <p class="text--navy--100 ek--size--18">There is not enough EK Coins to start Daily Training</p>
                                    <div class="ek--modal--body--buttons">
                                        <a class="ek--modal--body--buttons--cancel text--white ek--size--16" href="#ek--soft" rel="modal:open">Cancel</a>

                                        <!--<a class="ek&#45;&#45;modal&#45;&#45;body&#45;&#45;buttons&#45;&#45;other text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500 bg&#45;&#45;blue" href="javascript:void(0)">Buy EK Coins</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Ek Soft Modal -->
                    </div>
                    <div class="ek--training--type--body--inner bg--navy--600">
                        <div class="ek--training--type--body--inner--image bg--navy--700">
                            <img src="{{ url("static/img/Dashboard/ek--training--average@2x.png") }}" />
                        </div>
                        <h3 class="ek--training--type--body--inner--title text--white ek--size--18">Normal Training</h3>
                        <div class="ek--training--type--body--inner--feature">
                            <div class="ek--training--type--body--inner--feature--left">
                                <h4 class="text--navy--200 ek--size--16">Energy Loss</h4>
                                <p class="text--white ek--size--16--500">Average</p>
                            </div>
                            <div class="ek--training--type--body--inner--feature--right">
                                <h4 class="text--navy--200 ek--size--16">Improvement</h4>
                                <p class="text--white ek--size--16--500"><span class="text--green ek--size--16--500">100%</span> of potensial</p>
                            </div>
                        </div>
                        <!--<a class="ek&#45;&#45;training&#45;&#45;type&#45;&#45;body&#45;&#45;inner&#45;&#45;button bg&#45;&#45;blue text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" href="javascript:void(0)">Start training</a>-->
                        <button class="ek--training--type--body--inner--button bg--blue text--white ek--size--16--500"  id="trainingCostButton" data-id="2">Start training</button>


                    </div>
                    <div class="ek--training--type--body--inner bg--navy--600">
                        <div class="ek--training--type--body--inner--image bg--navy--700">
                            <img src="{{ url("static/img/Dashboard/ek--traning--high@2x.png") }}" />
                        </div>
                        <h3 class="ek--training--type--body--inner--title text--white ek--size--18">High Training</h3>
                        <div class="ek--training--type--body--inner--feature">
                            <div class="ek--training--type--body--inner--feature--left">
                                <h4 class="text--navy--200 ek--size--16">Energy Loss</h4>
                                <p class="text--white ek--size--16--500">High</p>
                            </div>
                            <div class="ek--training--type--body--inner--feature--right">
                                <h4 class="text--navy--200 ek--size--16">Improvement</h4>
                                <p class="text--white ek--size--16--500"><span class="text--green ek--size--16--500">120% - 150%</span> of potensial</p>
                            </div>
                        </div>
                        <!--<a class="ek&#45;&#45;training&#45;&#45;type&#45;&#45;body&#45;&#45;inner&#45;&#45;button bg&#45;&#45;blue text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" href="javascript:void(0)">Start training</a>-->
                        <button class="ek--training--type--body--inner--button bg--blue text--white ek--size--16--500"  id="trainingCostButton" data-id="3">Start training</button>

                    </div>
                    <!-- End Ek Training Type Body Inner -->
                </div>
                <!-- End Ek Training Type Body -->
            </div>
            <!-- End Ek Training Type -->
        </div>
        <!-- End Ek Training -->
        <!-- Ek Training Complete -->

        <!-- End Ek Training Complete -->
    </section>

@endsection

@section("javascript")
    <script src="{{ url("static/js/daily.js") }}"></script>
@endsection

