@extends('client.layout.authenticated')
@section('content')

    <!-- Ek Transfer -->
    <section class="ek--transfer">
        <!-- Ek Transfer Body -->
        <div class="ek--transfer--body bg--navy--700">
            <!-- Ek Transfer Body Head -->
            <div class="ek--transfer--body--head">
                <h3 class="text--white ek--size--20">Transfer</h3>
            </div>
            <!-- Ek Transfer Body Head -->
            <!-- Ek Transfer Body Tabs -->
            <div class="ek--transfer--body--tabs">
            <!-- Ek Transfer Body Tabs Head -->
            <div class="ek--transfer--body--tabs--head">
                <button id="ek--transfer--body--tabs--buy" class="ek--transfer--body--tabs--button button--active">Buy player</button>
                <button id="ek--transfer--body--tabs--sell" class="ek--transfer--body--tabs--button">Sell player</button>
                <button id="ek--transfer--body--tabs--history" class="ek--transfer--body--tabs--button">History</button>
            </div>
            <!-- End Ek Transfer Body Tabs Head -->
            <!-- Ek Transfer Body Tabs Content -->
            <div class="ek--transfer--body--tabs--content bg--navy--700">
                <!-- Ek Transfer Body Tabs Content Buy -->
                <div class="ek--transfer--body--tabs--buy content--active text--white">
                    <!-- Ek Transfer Body Tabs Content Buy Head -->
                    <div class="ek--transfer--body--tabs--buy--head">
                        <div class="ek--transfer--body--tabs--buy--head--pos ek--size--16 text--navy--200">Pos</div>
                        <div class="ek--transfer--body--tabs--buy--head--player ek--size--16 text--navy--200">Player name</div>
                        <div class="ek--transfer--body--tabs--buy--head--age ek--size--16 text--navy--200">Age</div>
                        <div class="ek--transfer--body--tabs--buy--head--abl ek--size--16 text--navy--200">Abl</div>
{{--                        <div class="ek--transfer--body--tabs--buy--head--matches ek--size--16 text--navy--200">Matches</div>--}}
                        <div class="ek--transfer--body--tabs--buy--head--goals ek--size--16 text--navy--200">Goals</div>
                        <div class="ek--transfer--body--tabs--buy--head--price ek--size--16 text--navy--200">Price</div>
                    </div>
                    <!-- End Ek Transfer Body Tabs Content Buy Head -->
                    <!-- Ek Transfer Body Tabs Content Buy Body -->
                    <div class="ek--transfer--body--tabs--buy--body" id="buyList">
                        <!-- Ek Transfer Body Tabs Content Inner -->
                        <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner bg&#45;&#45;navy&#45;&#45;600" th:each="buyPlayer : ${res.data.buyList}" th:if="${#lists.size(session.buyList)>0 }">-->
                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;pos">-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;pos&#45;&#45;type bg&#45;&#45;orange&#45;&#45;dark ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${buyPlayer.positionId == 1}">G</div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;pos&#45;&#45;type bg&#45;&#45;yellow ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white"  th:if="${buyPlayer.positionId == 2}">D</div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;pos&#45;&#45;type bg&#45;&#45;cyan&#45;&#45;two ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${buyPlayer.positionId == 3}">M</div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;pos&#45;&#45;type bg&#45;&#45;purple ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${buyPlayer.positionId == 4}">F</div>-->
                            <!--</div>-->
                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;player ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">-->
                                <!--&lt;!&ndash;<img src="img/Flags/ek&#45;&#45;aze.svg" alt="ek&#45;&#45;aze" />&ndash;&gt;-->
                                <!--<img th:src="@{${'img/Flags/'+ buyPlayer.country.code +'.svg'}}" alt="">-->
                                <!--<a id="buyPlayerModalLink" th:data-id="${buyPlayer.playerCode}"  class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white"  th:text="${buyPlayer.playerName}">FC Anaconda</a>-->
                                <!--&lt;!&ndash; Ek Transfer Player Modal &ndash;&gt;-->
                                <!--<div  th:id="'ek&#45;&#45;transfer&#45;&#45;modal' + ${buyPlayer.playerCode}" class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;player bg&#45;&#45;navy&#45;&#45;700 modal">-->
                                    <!--<div class="ek&#45;&#45;close">-->
                                        <!--<a class="ek&#45;&#45;close&#45;&#45;icon bg&#45;&#45;navy&#45;&#45;700" href="#" rel="modal:close">-->
                                            <!--<svg class="icon icon-ek&#45;&#45;close&#45;&#45;icon fill&#45;&#45;navy&#45;&#45;200"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;close&#45;&#45;icon"></use></svg>-->
                                        <!--</a>-->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Head &ndash;&gt;-->
                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head">-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Head Image &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;image bg&#45;&#45;navy&#45;&#45;900">-->
                                            <!--<img src="img/Other/ek&#45;&#45;null&#45;&#45;image.png" alt="">-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Head Image &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Head About &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about">-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Head About Name &ndash;&gt;-->
                                            <!--<h3 class="ek&#45;&#45;size&#45;&#45;20&#45;&#45;400 text&#45;&#45;white" th:text="${buyPlayer.playerName}">Gwernach Boren</h3>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About Name &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Head About Country &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;country">-->
                                                <!--&lt;!&ndash;<img src="img/Flags/ek&#45;&#45;us.svg" alt="">&ndash;&gt;-->
                                                <!--<img th:src="@{${'img/Flags/'+ buyPlayer.country.code +'.svg'}}" alt="">-->
                                                <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;20&#45;&#45;400" th:text="${buyPlayer.country.code}">USA</span>-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About Country &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Head About Position &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;position bg&#45;&#45;yellow text&#45;&#45;navy&#45;&#45;900 ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${buyPlayer.position}">Defender</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About Position &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About &ndash;&gt;-->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Head &ndash;&gt;-->
                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Inner &ndash;&gt;-->
                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner">-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Feature &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature">-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature&#45;&#45;age">-->
                                                <!--<p class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${buyPlayer.age}">27</p>-->
                                                <!--<h4 class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Age</h4>-->
                                            <!--</div>-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature&#45;&#45;abl">-->
                                                <!--<p class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${#numbers.formatDecimal(buyPlayer.ability,0,0)} ">80</p>-->
                                                <!--<h4 class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Abl</h4>-->
                                            <!--</div>-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature&#45;&#45;value">-->
                                                <!--<p class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white"  >-->
                                                    <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">$</span>-->
                                                    <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" id="userMoney" th:if="${buyPlayer.value <1000}" th:text="${buyPlayer.value}"><span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" id="amount"></span></span>-->
                                                    <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${buyPlayer.value >=1000 and buyPlayer.value < 1000000}"  th:with="result=${buyPlayer.value/1000}"><span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" id="userMoney" th:text="${result}"></span> <span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" id="amount">K</span></span>-->
                                                    <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${buyPlayer.value >=1000000 and buyPlayer.value < 1000000000}"  th:with="result=${buyPlayer.value/1000000}" > <span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" id="userMoney" th:text="${result}"></span><span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" id="amount">M</span></span>-->
                                                    <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${buyPlayer.value >=1000000000}"  th:with="result=${buyPlayer.value/1000000000}"><span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" id="userMoney"  th:text="${result}"></span>  <span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" id="amount">B</span></span>-->


                                                <!--</p>-->
                                                <!--<h4 class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Value</h4>-->
                                            <!--</div>-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Feature &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values">-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Energy &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                    <!--<svg class="icon icon-ek&#45;&#45;player&#45;&#45;country fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;player&#45;&#45;country"></use></svg>-->
                                                    <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Country</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Right &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;right">-->
                                                    <!--<img th:src="@{${'img/Flags/'+ buyPlayer.country.code +'.svg'}}" alt="">-->
                                                    <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${buyPlayer.country.code}">USA</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Right &ndash;&gt;-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Energy &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Goals &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                    <!--<svg class="icon icon-ek&#45;&#45;ball fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;ball"></use></svg>-->
                                                    <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Goals</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${buyPlayer.goals}">27</span>-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Goals &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Played Match &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                    <!--<svg class="icon icon-ek&#45;&#45;stadium fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;stadium"></use></svg>-->
                                                    <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Played Match</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${buyPlayer.appearance}">116</span>-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Played Match &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Red Card &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                    <!--<svg class="icon icon-ek&#45;&#45;card fill&#45;&#45;red"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;card"></use></svg>-->
                                                    <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Red Card</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${buyPlayer.redCards}">4</span>-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Red Card  &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Red Card &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                    <!--<svg class="icon icon-ek&#45;&#45;card fill&#45;&#45;yellow"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;card"></use></svg>-->
                                                    <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200" >Yellow Card</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${buyPlayer.yellowCards}">21</span>-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Red Card  &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Refill Energy &ndash;&gt;-->
                                        <!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;refill bg&#45;&#45;blue text&#45;&#45;white" th:data-id="${buyPlayer.playerCode}"  id="openSecBuyModal">Buy player</a>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Refill Energy &ndash;&gt;-->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner &ndash;&gt;-->
                                <!--</div>-->
                                <!--&lt;!&ndash; End Ek Transfer Player Modal &ndash;&gt;-->
                                <!--&lt;!&ndash; Ek Transfer Player Buy Modal &ndash;&gt;-->
                                <!--<div th:id="'ek&#45;&#45;transfer&#45;&#45;buy' + ${buyPlayer.playerCode}" class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill bg&#45;&#45;navy&#45;&#45;700 modal">-->
                                    <!--&lt;!&ndash; Ek Body Squad Refill Modal Head &ndash;&gt;-->
                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head">-->
                                        <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Energy &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head&#45;&#45;energy bg&#45;&#45;navy&#45;&#45;600">-->
                                            <!--<img src="img/Other/ek&#45;&#45;null&#45;&#45;image.png" alt="null">-->
                                        <!--</div>-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head&#45;&#45;want">-->
                                            <!--<h4 class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;20">Buy player</h4>-->
                                            <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Energy Player Name &ndash;&gt;-->
                                            <!--<p>Do you want to buy <span th:text="${buyPlayer.playerName}"></span>?</p>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Energy Player Name &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Energy &ndash;&gt;-->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head &ndash;&gt;-->
                                    <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons &ndash;&gt;-->
                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons">-->
                                        <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons Cancel &ndash;&gt;-->
                                        <!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;cancel text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;navy&#45;&#45;600" href="#ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;player" rel="modal:open">Cancel</a>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons Cancel &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons Energy &ndash;&gt;-->
                                        <!--<input type="hidden" />-->
                                        <!--&lt;!&ndash;<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;energy text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;blue" href="#ek&#45;&#45;transfer&#45;&#45;succes" rel="modal:open">Buy player</a>&ndash;&gt;-->
                                        <!--<button class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;energy text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;blue" id="buyPlayer" th:href="@{/transfer/buyPlayer}" th:data-id="${buyPlayer.playerCode}">Buy player</button>-->

                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons Energy &ndash;&gt;-->
                                    <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons &ndash;&gt;-->
                                    <!--<a href="#close-modal" rel="modal:close" class="close-modal ">Close</a>-->
                                <!--</div>-->
                                <!--&lt;!&ndash; End Ek Transfer Player Buy Modal &ndash;&gt;-->
                                <!--&lt;!&ndash; Ek Transfer Player Buy Modal &ndash;&gt;-->
                                <!--<div id="ek&#45;&#45;transfer&#45;&#45;succes" class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill ek&#45;&#45;modal&#45;&#45;border&#45;&#45;left&#45;&#45;green&#45;&#45;dark&#45;&#45;two bg&#45;&#45;navy&#45;&#45;700 modal">-->
                                    <!--&lt;!&ndash; Ek Body Squad Refill Modal Head &ndash;&gt;-->
                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head">-->
                                        <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Energy &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head&#45;&#45;energy bg&#45;&#45;green&#45;&#45;dark&#45;&#45;two">-->
                                            <!--<svg class="icon icon-ek&#45;&#45;correct fill&#45;&#45;white"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;correct"></use></svg>-->
                                        <!--</div>-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head&#45;&#45;want">-->
                                            <!--<h4 class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;20">You bought the player</h4>-->
                                            <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Energy Player Name &ndash;&gt;-->
                                            <!--<p><span th:text="${buyPlayer.playerName}"></span> successfully added to the team</p>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Energy Player Name &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Energy &ndash;&gt;-->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head &ndash;&gt;-->
                                    <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons &ndash;&gt;-->
                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons">-->
                                        <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons Cancel &ndash;&gt;-->
                                        <!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;cancel text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;navy&#45;&#45;600" href="#ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;player" rel="modal:open">Cancel</a>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons Cancel &ndash;&gt;-->
                                    <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons &ndash;&gt;-->
                                    <!--<a href="#close-modal" rel="modal:close" class="close-modal ">Close</a>-->
                                <!--</div>-->
                                <!--&lt;!&ndash; End Ek Transfer Player Buy Modal &ndash;&gt;-->
                            <!--</div>-->
                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;age ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${buyPlayer.age}">18</div>-->
                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;abl ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${#numbers.formatDecimal(buyPlayer.ability,0,0)} " >80</div>-->
                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;matches ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${buyPlayer.appearance}">118</div>-->
                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;goals ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${buyPlayer.goals}">1</div>-->
                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">-->
                                <!--<span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">$</span>-->
                                <!--<span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${buyPlayer.value <1000}" th:text="${buyPlayer.value}"></span>-->
                                <!--<span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${buyPlayer.value >=1000 and buyPlayer.value < 1000000}"  th:with="result=${buyPlayer.value/1000}" ><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${result}"></span><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">K</span></span>-->
                                <!--<span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${buyPlayer.value >=1000000 and buyPlayer.value < 1000000000}"  th:with="result=${buyPlayer.value/1000000}" ><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${result}"></span><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">M</span></span>-->
                                <!--<span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${buyPlayer.value >=1000000000}"  th:with="result=${buyPlayer.value/1000000000}" ><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${result}"></span> <span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">B</span></span>-->


                            <!--</div>-->
                        <!--</div>-->

                        <!-- End Ek Transfer Body Tabs Content Inner -->
                    </div>
                    <!-- End Ek Transfer Body Tabs Content Buy Body -->
                </div>
                <!-- End Ek Transfer Body Tabs Content Buy -->
                <!-- Ek Transfer Body Tabs Content Sell -->
                <div class="ek--transfer--body--tabs--sell content--active text--white">
                    <!-- Ek Transfer Body Tabs Content Sell Head -->
                    <div class="ek--transfer--body--tabs--sell--head">
                            <div class="ek--transfer--body--tabs--buy--head--pos ek--size--16 text--navy--200">Pos</div>
                            <div class="ek--transfer--body--tabs--buy--head--player ek--size--16 text--navy--200">Player name</div>
                            <div class="ek--transfer--body--tabs--buy--head--age ek--size--16 text--navy--200">Age</div>
                            <div class="ek--transfer--body--tabs--buy--head--abl ek--size--16 text--navy--200">Abl</div>
{{--                            <div class="ek--transfer--body--tabs--buy--head--matches ek--size--16 text--navy--200">Matches</div>--}}
                            <div class="ek--transfer--body--tabs--buy--head--goals ek--size--16 text--navy--200">Goals</div>
                            <div class="ek--transfer--body--tabs--buy--head--price ek--size--16 text--navy--200">Price</div>
                    </div>
                    <!-- End Ek Transfer Body Tabs Content Sell Head -->
                    <!-- Ek Transfer Body Tabs Content Sell Body -->
                    <div class="ek--transfer--body--tabs--sell--body" id="sellList">


                                                      <div id="ek--transfer--sell--success" class="ek--squad--modals--refill ek--modal--border--left--green--dark--two bg--navy--700 modal">
                                                           <div class="ek--squad--modals--refill--head">
                                                                    <!-- Ek Body Squad Refill Modal Head  Energy -->
                                                                    <div class="ek--squad--modals--refill--head--energy bg--green--dark--two">
                                                                            <svg class="icon icon-ek--correct fill--white"><use xlink:href="static/img/icons.svg#icon-ek--correct"></use></svg>
                                                                        </div>
                                                                    <div class="ek--squad--modals--refill--head--want">
                                                                            <h4 class="text--white ek--size--20">You sold the player</h4>
                                                                            <!-- Ek Body Squad Refill Modal Head  Energy Player Name -->
                                                                            <p>Player successfully sold</p>
                                                                            <!-- End Ek Body Squad Refill Modal Head  Energy Player Name -->
                                                                        </div>
                                                                    <!-- End Ek Body Squad Refill Modal Head  Energy -->
                                                                </div>
                                                          <!-- End Ek Body Squad Refill Modal Head -->
                                                            <!-- Ek Body Squad Refill Modal Head  Buttons -->
                                                            <div class="ek--squad--modals--refill--buttons">
                                                                 <!-- Ek Body Squad Refill Modal Head  Buttons Cancel -->
                                                                    <a class="ek--squad--modals--refill--buttons--cancel text--white ek--size--16 bg--navy--600" href="#ek--transfer--sell--succes" rel="modal:close">Cancel</a>
                                                       <!-- End Ek Body Squad Refill Modal Head  Buttons Cancel -->
                                                              </div>
                                                               <!-- End Ek Body Squad Refill Modal Head  Buttons -->
                                                       </div>
                                                        <!-- End Ek Transfer Player Buy Modal -->



                        <!-- Ek Transfer Body Tabs Content Inner -->

                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner bg&#45;&#45;navy&#45;&#45;600" th:each="player : ${session.players}" th:if="${player.positionId==1}">-->
                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;pos">-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;pos&#45;&#45;type bg&#45;&#45;orange&#45;&#45;dark ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" >G</div>-->

                            <!--</div>-->
                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;player ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">-->
                                <!--&lt;!&ndash;<img src="img/Flags/ek&#45;&#45;aze.svg" alt="ek&#45;&#45;aze" />&ndash;&gt;-->
                                <!--<img th:src="@{${'img/Flags/'+ player.country.code +'.svg'}}" alt="">-->
                                <!--<a class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" id="sellPlayerModalLink" th:data-id="${player.id}"  th:text="${player.name}" >FC Anaconda</a>-->
                                <!--&lt;!&ndash; Ek Transfer Player Modal &ndash;&gt;-->
                                <!--<div th:id="'ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;modal' + ${player.id}" class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;player bg&#45;&#45;navy&#45;&#45;700 modal">-->
                                    <!--<div class="ek&#45;&#45;close">-->
                                        <!--<a class="ek&#45;&#45;close&#45;&#45;icon bg&#45;&#45;navy&#45;&#45;700" href="#" rel="modal:close">-->
                                            <!--<svg class="icon icon-ek&#45;&#45;close&#45;&#45;icon fill&#45;&#45;navy&#45;&#45;200"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;close&#45;&#45;icon"></use></svg>-->
                                        <!--</a>-->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Head &ndash;&gt;-->
                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head">-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Head Image &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;image bg&#45;&#45;navy&#45;&#45;900">-->
                                            <!--<img src="img/Other/ek&#45;&#45;null&#45;&#45;image.png" alt="">-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Head Image &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Head About &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about">-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Head About Name &ndash;&gt;-->
                                            <!--<h3 class="ek&#45;&#45;size&#45;&#45;20&#45;&#45;400 text&#45;&#45;white"th:text="${player.name}"></h3>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About Name &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Head About Country &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;country">-->
                                                <!--<img th:src="@{${'img/Flags/'+ player.country.code +'.svg'}}" alt="">-->
                                                <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;20&#45;&#45;400" th:text="${player.country.code}">USA</span>-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About Country &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Head About Position &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;position bg&#45;&#45;orange&#45;&#45;dark text&#45;&#45;navy&#45;&#45;900 ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:if="${player.positionId==1}">Goalkeeper</div>-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;position bg&#45;&#45;yellow text&#45;&#45;navy&#45;&#45;900 ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:if="${player.positionId==2}">Defender</div>-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;position bg&#45;&#45;cyan&#45;&#45;two text&#45;&#45;navy&#45;&#45;900 ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:if="${player.positionId==3}">Midfielder</div>-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;position bg&#45;&#45;purple text&#45;&#45;navy&#45;&#45;900 ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:if="${player.positionId==4}">Forward</div>-->




                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About Position &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About &ndash;&gt;-->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Head &ndash;&gt;-->
                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Inner &ndash;&gt;-->
                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner">-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Feature &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature">-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature&#45;&#45;age">-->
                                                <!--<p class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${player.age}">27</p>-->
                                                <!--<h4 class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Age</h4>-->
                                            <!--</div>-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature&#45;&#45;abl">-->
                                                <!--<p class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${#numbers.formatDecimal(player.ability,0,0)} ">80</p>-->
                                                <!--<h4 class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Abl</h4>-->
                                            <!--</div>-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature&#45;&#45;value">-->
                                                <!--<p class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">-->
                                                    <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">$</span>-->
                                                    <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${player.value <1000}" th:text="${player.value}"></span>-->
                                                    <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${player.value >=1000 and player.value < 1000000}"  th:with="result=${player.value/1000}" ><span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${result}"></span> <span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">K</span></span>-->
                                                    <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${player.value >=1000000 and player.value < 1000000000}"  th:with="result=${player.value/1000000}" ><span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${result}"></span> <span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">M</span></span>-->
                                                    <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${player.value >=1000000000}"  th:with="result=${player.value/1000000000}" > <span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${result}"></span><span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">B</span></span>-->

                                                <!--</p>-->
                                                <!--<h4 class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Value</h4>-->
                                            <!--</div>-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Feature &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values">-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Energy &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                    <!--<svg class="icon icon-ek&#45;&#45;player&#45;&#45;country fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;player&#45;&#45;country"></use></svg>-->
                                                    <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Country</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Right &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;right">-->
                                                    <!--<img th:src="@{${'img/Flags/'+ player.country.code +'.svg'}}" alt="">-->
                                                    <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${player.country.code} ">USA</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Right &ndash;&gt;-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Energy &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Goals &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                    <!--<svg class="icon icon-ek&#45;&#45;ball fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;ball"></use></svg>-->
                                                    <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Goals</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${player.goals} " >27</span>-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Goals &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Played Match &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                    <!--<svg class="icon icon-ek&#45;&#45;stadium fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;stadium"></use></svg>-->
                                                    <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Played Match</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${player.appearance} ">116</span>-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Played Match &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Red Card &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                    <!--<svg class="icon icon-ek&#45;&#45;card fill&#45;&#45;red"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;card"></use></svg>-->
                                                    <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Red Card</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500"th:text="${player.redCard} " >4</span>-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Red Card  &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Red Card &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                    <!--<svg class="icon icon-ek&#45;&#45;card fill&#45;&#45;yellow"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;card"></use></svg>-->
                                                    <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200" >Yellow Card</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${player.yellowCard} ">21</span>-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Red Card  &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Refill Energy &ndash;&gt;-->
                                        <!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;refill bg&#45;&#45;blue text&#45;&#45;white" th:data-id="${player.id}" id="openSecSellModal" >Sell player</a>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Refill Energy &ndash;&gt;-->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner &ndash;&gt;-->
                                <!--</div>-->
                                <!--&lt;!&ndash; End Ek Transfer Player Modal &ndash;&gt;-->
                                <!--&lt;!&ndash; Ek Transfer Player Buy Modal &ndash;&gt;-->
                                <!--<div  th:id="'ek&#45;&#45;transfer&#45;&#45;sell' + ${player.id}" class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill bg&#45;&#45;navy&#45;&#45;700 modal">-->
                                    <!--&lt;!&ndash; Ek Body Squad Refill Modal Head &ndash;&gt;-->
                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head">-->
                                        <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Energy &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head&#45;&#45;energy bg&#45;&#45;navy&#45;&#45;600">-->
                                            <!--<img src="img/Other/ek&#45;&#45;null&#45;&#45;image.png" alt="null">-->
                                        <!--</div>-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head&#45;&#45;want">-->
                                            <!--<h4 class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;20">Sell player</h4>-->
                                            <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Energy Player Name &ndash;&gt;-->
                                            <!--<p>Do you want to sell <span th:text="${player.name} "></span>?</p>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Energy Player Name &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Energy &ndash;&gt;-->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head &ndash;&gt;-->
                                    <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons &ndash;&gt;-->
                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons">-->
                                        <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons Cancel &ndash;&gt;-->
                                        <!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;cancel text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;navy&#45;&#45;600" href="#ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;modal" rel="modal:open">Cancel</a>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons Cancel &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons Energy &ndash;&gt;-->
                                        <!--&lt;!&ndash;<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;energy text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;blue" href="#ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;succes" rel="modal:open">Sell player</a>&ndash;&gt;-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons Energy &ndash;&gt;-->
                                        <!--<button class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;energy text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;blue"id="sellPlayer"  th:data-id="${player.id}">Sell Player</button>-->
                                    <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons &ndash;&gt;-->
                                    <!--<a href="#close-modal" rel="modal:close" class="close-modal">Close</a>-->
                                <!--</div>-->
                                <!--&lt;!&ndash; End Ek Transfer Player Buy Modal &ndash;&gt;-->
                                <!--&lt;!&ndash; Ek Transfer Player Buy Modal &ndash;&gt;-->
                                <!--<div id="ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;succes" class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill ek&#45;&#45;modal&#45;&#45;border&#45;&#45;left&#45;&#45;green&#45;&#45;dark&#45;&#45;two bg&#45;&#45;navy&#45;&#45;700 modal">-->
                                    <!--&lt;!&ndash; Ek Body Squad Refill Modal Head &ndash;&gt;-->
                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head">-->
                                        <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Energy &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head&#45;&#45;energy bg&#45;&#45;green&#45;&#45;dark&#45;&#45;two">-->
                                            <!--<svg class="icon icon-ek&#45;&#45;correct fill&#45;&#45;white"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;correct"></use></svg>-->
                                        <!--</div>-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head&#45;&#45;want">-->
                                            <!--<h4 class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;20">You sold the player</h4>-->
                                            <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Energy Player Name &ndash;&gt;-->
                                            <!--<p><span th:text="${player.name} "></span> successfully sold</p>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Energy Player Name &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Energy &ndash;&gt;-->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head &ndash;&gt;-->
                                    <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons &ndash;&gt;-->
                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons">-->
                                        <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons Cancel &ndash;&gt;-->
                                        <!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;cancel text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;navy&#45;&#45;600" href="#ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;succes" rel="modal:close">Cancel</a>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons Cancel &ndash;&gt;-->
                                    <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons &ndash;&gt;-->
                                <!--</div>-->
                                <!--&lt;!&ndash; End Ek Transfer Player Buy Modal &ndash;&gt;-->
                            <!--</div>-->
                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;age ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${player.age} ">18</div>-->
                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;abl ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${#numbers.formatDecimal(player.ability,0,0)} ">80</div>-->
                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;matches ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${player.appearance} ">118</div>-->
                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;goals ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${player.goals} ">1</div>-->
                            <!--&lt;!&ndash;<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${player.value} ">$45M</div>&ndash;&gt;-->

                                <!--<span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">$</span>-->
                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${player.value < 1000}" th:text="${player.value}">$45M</div>-->
                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${player.value >= 1000 and player.value< 1000000}" th:with="result=${player.value/1000}"><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${result}">$45</span><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">K</span></div>-->
                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${player.value >= 1000000 and player.value< 1000000000}" th:with="result=${player.value/1000000}"><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${result}">$45</span><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">M</span></div>-->
                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${player.value >= 1000000000 }" th:with="result=${player.value/1000000000}"><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${result}">$45</span><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">B</span></div>-->


                        <!--</div>-->

                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner bg&#45;&#45;navy&#45;&#45;600" th:each="player : ${session.players}" th:if="${player.positionId==2}">-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;pos">-->
                                    <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;pos&#45;&#45;type bg&#45;&#45;yellow ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" >D</div>-->

                                <!--</div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;player ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">-->
                                    <!--&lt;!&ndash;<img src="img/Flags/ek&#45;&#45;aze.svg" alt="ek&#45;&#45;aze" />&ndash;&gt;-->
                                    <!--<img th:src="@{${'img/Flags/'+ player.country.code +'.svg'}}" alt="">-->
                                    <!--<a class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" id="sellPlayerModalLink" th:data-id="${player.id}"  th:text="${player.name}" >FC Anaconda</a>-->
                                    <!--&lt;!&ndash; Ek Transfer Player Modal &ndash;&gt;-->
                                    <!--<div th:id="'ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;modal' + ${player.id}" class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;player bg&#45;&#45;navy&#45;&#45;700 modal">-->
                                        <!--<div class="ek&#45;&#45;close">-->
                                            <!--<a class="ek&#45;&#45;close&#45;&#45;icon bg&#45;&#45;navy&#45;&#45;700" href="#" rel="modal:close">-->
                                                <!--<svg class="icon icon-ek&#45;&#45;close&#45;&#45;icon fill&#45;&#45;navy&#45;&#45;200"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;close&#45;&#45;icon"></use></svg>-->
                                            <!--</a>-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Head &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head">-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Head Image &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;image bg&#45;&#45;navy&#45;&#45;900">-->
                                                <!--<img src="img/Other/ek&#45;&#45;null&#45;&#45;image.png" alt="">-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Head Image &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Head About &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about">-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Head About Name &ndash;&gt;-->
                                                <!--<h3 class="ek&#45;&#45;size&#45;&#45;20&#45;&#45;400 text&#45;&#45;white"th:text="${player.name}"></h3>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About Name &ndash;&gt;-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Head About Country &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;country">-->
                                                    <!--<img th:src="@{${'img/Flags/'+ player.country.code +'.svg'}}" alt="">-->
                                                    <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;20&#45;&#45;400" th:text="${player.country.code}">USA</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About Country &ndash;&gt;-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Head About Position &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;position bg&#45;&#45;orange&#45;&#45;dark text&#45;&#45;navy&#45;&#45;900 ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:if="${player.positionId==1}">Goalkeeper</div>-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;position bg&#45;&#45;yellow text&#45;&#45;navy&#45;&#45;900 ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:if="${player.positionId==2}">Defender</div>-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;position bg&#45;&#45;cyan&#45;&#45;two text&#45;&#45;navy&#45;&#45;900 ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:if="${player.positionId==3}">Midfielder</div>-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;position bg&#45;&#45;purple text&#45;&#45;navy&#45;&#45;900 ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:if="${player.positionId==4}">Forward</div>-->




                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About Position &ndash;&gt;-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Head &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Inner &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner">-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Feature &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature">-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature&#45;&#45;age">-->
                                                    <!--<p class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${player.age}">27</p>-->
                                                    <!--<h4 class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Age</h4>-->
                                                <!--</div>-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature&#45;&#45;abl">-->
                                                    <!--<p class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${#numbers.formatDecimal(player.ability,0,0)} ">80</p>-->
                                                    <!--<h4 class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Abl</h4>-->
                                                <!--</div>-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature&#45;&#45;value">-->
                                                    <!--<p class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">-->

                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${player.value <1000}" th:text="${player.value}"></span>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${player.value >=1000 and player.value < 1000000}"  th:with="result=${player.value/1000}" ><span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${result}"></span> <span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">K</span></span>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${player.value >=1000000 and player.value < 1000000000}"  th:with="result=${player.value/1000000}" ><span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${result}"></span> <span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">M</span></span>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${player.value >=1000000000}"  th:with="result=${player.value/1000000000}" > <span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${result}"></span><span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">B</span></span>-->

                                                    <!--</p>-->
                                                    <!--<h4 class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Value</h4>-->
                                                <!--</div>-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Feature &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values">-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Energy &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                        <!--<svg class="icon icon-ek&#45;&#45;player&#45;&#45;country fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;player&#45;&#45;country"></use></svg>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Country</span>-->
                                                    <!--</div>-->
                                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Right &ndash;&gt;-->
                                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;right">-->
                                                        <!--<img th:src="@{${'img/Flags/'+ player.country.code +'.svg'}}" alt="">-->
                                                        <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${player.country.code} ">USA</span>-->
                                                    <!--</div>-->
                                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Right &ndash;&gt;-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Energy &ndash;&gt;-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Goals &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                        <!--<svg class="icon icon-ek&#45;&#45;ball fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;ball"></use></svg>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Goals</span>-->
                                                    <!--</div>-->
                                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${player.goals} " >27</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Goals &ndash;&gt;-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Played Match &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                        <!--<svg class="icon icon-ek&#45;&#45;stadium fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;stadium"></use></svg>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Played Match</span>-->
                                                    <!--</div>-->
                                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${player.appearance} ">116</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Played Match &ndash;&gt;-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Red Card &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                        <!--<svg class="icon icon-ek&#45;&#45;card fill&#45;&#45;red"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;card"></use></svg>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Red Card</span>-->
                                                    <!--</div>-->
                                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500"th:text="${player.redCard} " >4</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Red Card  &ndash;&gt;-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Red Card &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                        <!--<svg class="icon icon-ek&#45;&#45;card fill&#45;&#45;yellow"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;card"></use></svg>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200" >Yellow Card</span>-->
                                                    <!--</div>-->
                                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${player.yellowCard} ">21</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Red Card  &ndash;&gt;-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Refill Energy &ndash;&gt;-->
                                            <!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;refill bg&#45;&#45;blue text&#45;&#45;white" th:data-id="${player.id}" id="openSecSellModal" >Sell player</a>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Refill Energy &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner &ndash;&gt;-->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; End Ek Transfer Player Modal &ndash;&gt;-->
                                    <!--&lt;!&ndash; Ek Transfer Player Buy Modal &ndash;&gt;-->
                                    <!--<div  th:id="'ek&#45;&#45;transfer&#45;&#45;sell' + ${player.id}" class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill bg&#45;&#45;navy&#45;&#45;700 modal">-->
                                        <!--&lt;!&ndash; Ek Body Squad Refill Modal Head &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head">-->
                                            <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Energy &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head&#45;&#45;energy bg&#45;&#45;navy&#45;&#45;600">-->
                                                <!--<img src="img/Other/ek&#45;&#45;null&#45;&#45;image.png" alt="null">-->
                                            <!--</div>-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head&#45;&#45;want">-->
                                                <!--<h4 class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;20">Sell player</h4>-->
                                                <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Energy Player Name &ndash;&gt;-->
                                                <!--<p>Do you want to sell <span th:text="${player.name} "></span>?</p>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Energy Player Name &ndash;&gt;-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Energy &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons">-->
                                            <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons Cancel &ndash;&gt;-->
                                            <!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;cancel text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;navy&#45;&#45;600" href="#ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;modal" rel="modal:open">Cancel</a>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons Cancel &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons Energy &ndash;&gt;-->
                                            <!--&lt;!&ndash;<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;energy text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;blue" href="#ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;succes" rel="modal:open">Sell player</a>&ndash;&gt;-->
                                            <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons Energy &ndash;&gt;-->
                                            <!--<button class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;energy text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;blue"id="sellPlayer"  th:data-id="${player.id}">Sell Player</button>-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons &ndash;&gt;-->
                                        <!--<a href="#close-modal" rel="modal:close" class="close-modal">Close</a>-->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; End Ek Transfer Player Buy Modal &ndash;&gt;-->
                                    <!--&lt;!&ndash; Ek Transfer Player Buy Modal &ndash;&gt;-->
                                    <!--<div id="ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;succes" class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill ek&#45;&#45;modal&#45;&#45;border&#45;&#45;left&#45;&#45;green&#45;&#45;dark&#45;&#45;two bg&#45;&#45;navy&#45;&#45;700 modal">-->
                                        <!--&lt;!&ndash; Ek Body Squad Refill Modal Head &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head">-->
                                            <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Energy &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head&#45;&#45;energy bg&#45;&#45;green&#45;&#45;dark&#45;&#45;two">-->
                                                <!--<svg class="icon icon-ek&#45;&#45;correct fill&#45;&#45;white"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;correct"></use></svg>-->
                                            <!--</div>-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head&#45;&#45;want">-->
                                                <!--<h4 class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;20">You sold the player</h4>-->
                                                <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Energy Player Name &ndash;&gt;-->
                                                <!--<p><span th:text="${player.name} "></span> successfully sold</p>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Energy Player Name &ndash;&gt;-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Energy &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons">-->
                                            <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons Cancel &ndash;&gt;-->
                                            <!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;cancel text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;navy&#45;&#45;600" href="#ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;succes" rel="modal:close">Cancel</a>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons Cancel &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons &ndash;&gt;-->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; End Ek Transfer Player Buy Modal &ndash;&gt;-->
                                <!--</div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;age ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${player.age} ">18</div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;abl ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${#numbers.formatDecimal(player.ability,0,0)} ">80</div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;matches ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${player.appearance} ">118</div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;goals ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${player.goals} ">1</div>-->
                                <!--&lt;!&ndash;<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${player.value} ">$45M</div>&ndash;&gt;-->

                                <!--<span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">$</span>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${player.value < 1000}" th:text="${player.value}">$45M</div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${player.value >= 1000 and player.value< 1000000}" th:with="result=${player.value/1000}"><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${result}">$45</span><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">K</span></div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${player.value >= 1000000 and player.value< 1000000000}" th:with="result=${player.value/1000000}"><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${result}">$45</span><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">M</span></div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${player.value >= 1000000000 }" th:with="result=${player.value/1000000000}"><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${result}">$45</span><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">B</span></div>-->


                            <!--</div>-->

                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner bg&#45;&#45;navy&#45;&#45;600" th:each="player : ${session.players}" th:if="${player.positionId==3}">-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;pos">-->

                                    <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;pos&#45;&#45;type bg&#45;&#45;cyan&#45;&#45;two ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" >M</div>-->

                                <!--</div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;player ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">-->
                                    <!--&lt;!&ndash;<img src="img/Flags/ek&#45;&#45;aze.svg" alt="ek&#45;&#45;aze" />&ndash;&gt;-->
                                    <!--<img th:src="@{${'img/Flags/'+ player.country.code +'.svg'}}" alt="">-->
                                    <!--<a class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" id="sellPlayerModalLink" th:data-id="${player.id}"  th:text="${player.name}" >FC Anaconda</a>-->
                                    <!--&lt;!&ndash; Ek Transfer Player Modal &ndash;&gt;-->
                                    <!--<div th:id="'ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;modal' + ${player.id}" class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;player bg&#45;&#45;navy&#45;&#45;700 modal">-->
                                        <!--<div class="ek&#45;&#45;close">-->
                                            <!--<a class="ek&#45;&#45;close&#45;&#45;icon bg&#45;&#45;navy&#45;&#45;700" href="#" rel="modal:close">-->
                                                <!--<svg class="icon icon-ek&#45;&#45;close&#45;&#45;icon fill&#45;&#45;navy&#45;&#45;200"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;close&#45;&#45;icon"></use></svg>-->
                                            <!--</a>-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Head &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head">-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Head Image &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;image bg&#45;&#45;navy&#45;&#45;900">-->
                                                <!--<img src="img/Other/ek&#45;&#45;null&#45;&#45;image.png" alt="">-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Head Image &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Head About &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about">-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Head About Name &ndash;&gt;-->
                                                <!--<h3 class="ek&#45;&#45;size&#45;&#45;20&#45;&#45;400 text&#45;&#45;white"th:text="${player.name}"></h3>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About Name &ndash;&gt;-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Head About Country &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;country">-->
                                                    <!--<img th:src="@{${'img/Flags/'+ player.country.code +'.svg'}}" alt="">-->
                                                    <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;20&#45;&#45;400" th:text="${player.country.code}">USA</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About Country &ndash;&gt;-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Head About Position &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;position bg&#45;&#45;orange&#45;&#45;dark text&#45;&#45;navy&#45;&#45;900 ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:if="${player.positionId==1}">Goalkeeper</div>-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;position bg&#45;&#45;yellow text&#45;&#45;navy&#45;&#45;900 ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:if="${player.positionId==2}">Defender</div>-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;position bg&#45;&#45;cyan&#45;&#45;two text&#45;&#45;navy&#45;&#45;900 ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:if="${player.positionId==3}">Midfielder</div>-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;position bg&#45;&#45;purple text&#45;&#45;navy&#45;&#45;900 ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:if="${player.positionId==4}">Forward</div>-->




                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About Position &ndash;&gt;-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Head &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Inner &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner">-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Feature &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature">-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature&#45;&#45;age">-->
                                                    <!--<p class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${player.age}">27</p>-->
                                                    <!--<h4 class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Age</h4>-->
                                                <!--</div>-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature&#45;&#45;abl">-->
                                                    <!--<p class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${#numbers.formatDecimal(player.ability,0,0)} ">80</p>-->
                                                    <!--<h4 class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Abl</h4>-->
                                                <!--</div>-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature&#45;&#45;value">-->
                                                    <!--<p class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">$</span>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${player.value <1000}" th:text="${player.value}"></span>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${player.value >=1000 and player.value < 1000000}"  th:with="result=${player.value/1000}" ><span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${result}"></span> <span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">K</span></span>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${player.value >=1000000 and player.value < 1000000000}"  th:with="result=${player.value/1000000}" ><span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${result}"></span> <span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">M</span></span>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${player.value >=1000000000}"  th:with="result=${player.value/1000000000}" > <span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${result}"></span><span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">B</span></span>-->

                                                    <!--</p>-->
                                                    <!--<h4 class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Value</h4>-->
                                                <!--</div>-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Feature &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values">-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Energy &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                        <!--<svg class="icon icon-ek&#45;&#45;player&#45;&#45;country fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;player&#45;&#45;country"></use></svg>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Country</span>-->
                                                    <!--</div>-->
                                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Right &ndash;&gt;-->
                                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;right">-->
                                                        <!--<img th:src="@{${'img/Flags/'+ player.country.code +'.svg'}}" alt="">-->
                                                        <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${player.country.code} ">USA</span>-->
                                                    <!--</div>-->
                                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Right &ndash;&gt;-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Energy &ndash;&gt;-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Goals &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                        <!--<svg class="icon icon-ek&#45;&#45;ball fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;ball"></use></svg>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Goals</span>-->
                                                    <!--</div>-->
                                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${player.goals} " >27</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Goals &ndash;&gt;-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Played Match &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                        <!--<svg class="icon icon-ek&#45;&#45;stadium fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;stadium"></use></svg>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Played Match</span>-->
                                                    <!--</div>-->
                                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${player.appearance} ">116</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Played Match &ndash;&gt;-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Red Card &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                        <!--<svg class="icon icon-ek&#45;&#45;card fill&#45;&#45;red"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;card"></use></svg>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Red Card</span>-->
                                                    <!--</div>-->
                                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500"th:text="${player.redCard} " >4</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Red Card  &ndash;&gt;-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Red Card &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                        <!--<svg class="icon icon-ek&#45;&#45;card fill&#45;&#45;yellow"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;card"></use></svg>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200" >Yellow Card</span>-->
                                                    <!--</div>-->
                                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${player.yellowCard} ">21</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Red Card  &ndash;&gt;-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Refill Energy &ndash;&gt;-->
                                            <!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;refill bg&#45;&#45;blue text&#45;&#45;white" th:data-id="${player.id}" id="openSecSellModal" >Sell player</a>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Refill Energy &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner &ndash;&gt;-->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; End Ek Transfer Player Modal &ndash;&gt;-->
                                    <!--&lt;!&ndash; Ek Transfer Player Buy Modal &ndash;&gt;-->
                                    <!--<div  th:id="'ek&#45;&#45;transfer&#45;&#45;sell' + ${player.id}" class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill bg&#45;&#45;navy&#45;&#45;700 modal">-->
                                        <!--&lt;!&ndash; Ek Body Squad Refill Modal Head &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head">-->
                                            <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Energy &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head&#45;&#45;energy bg&#45;&#45;navy&#45;&#45;600">-->
                                                <!--<img src="img/Other/ek&#45;&#45;null&#45;&#45;image.png" alt="null">-->
                                            <!--</div>-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head&#45;&#45;want">-->
                                                <!--<h4 class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;20">Sell player</h4>-->
                                                <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Energy Player Name &ndash;&gt;-->
                                                <!--<p>Do you want to sell <span th:text="${player.name} "></span>?</p>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Energy Player Name &ndash;&gt;-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Energy &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons">-->
                                            <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons Cancel &ndash;&gt;-->
                                            <!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;cancel text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;navy&#45;&#45;600" href="#ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;modal" rel="modal:open">Cancel</a>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons Cancel &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons Energy &ndash;&gt;-->
                                            <!--&lt;!&ndash;<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;energy text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;blue" href="#ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;succes" rel="modal:open">Sell player</a>&ndash;&gt;-->
                                            <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons Energy &ndash;&gt;-->
                                            <!--<button class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;energy text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;blue"id="sellPlayer"  th:data-id="${player.id}">Sell Player</button>-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons &ndash;&gt;-->
                                        <!--<a href="#close-modal" rel="modal:close" class="close-modal">Close</a>-->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; End Ek Transfer Player Buy Modal &ndash;&gt;-->
                                    <!--&lt;!&ndash; Ek Transfer Player Buy Modal &ndash;&gt;-->
                                    <!--<div id="ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;succes" class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill ek&#45;&#45;modal&#45;&#45;border&#45;&#45;left&#45;&#45;green&#45;&#45;dark&#45;&#45;two bg&#45;&#45;navy&#45;&#45;700 modal">-->
                                        <!--&lt;!&ndash; Ek Body Squad Refill Modal Head &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head">-->
                                            <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Energy &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head&#45;&#45;energy bg&#45;&#45;green&#45;&#45;dark&#45;&#45;two">-->
                                                <!--<svg class="icon icon-ek&#45;&#45;correct fill&#45;&#45;white"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;correct"></use></svg>-->
                                            <!--</div>-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head&#45;&#45;want">-->
                                                <!--<h4 class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;20">You sold the player</h4>-->
                                                <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Energy Player Name &ndash;&gt;-->
                                                <!--<p><span th:text="${player.name} "></span> successfully sold</p>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Energy Player Name &ndash;&gt;-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Energy &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons">-->
                                            <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons Cancel &ndash;&gt;-->
                                            <!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;cancel text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;navy&#45;&#45;600" href="#ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;succes" rel="modal:close">Cancel</a>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons Cancel &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons &ndash;&gt;-->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; End Ek Transfer Player Buy Modal &ndash;&gt;-->
                                <!--</div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;age ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${player.age} ">18</div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;abl ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${#numbers.formatDecimal(player.ability,0,0)} ">80</div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;matches ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${player.appearance} ">118</div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;goals ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${player.goals} ">1</div>-->
                                <!--&lt;!&ndash;<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${player.value} ">$45M</div>&ndash;&gt;-->

                                <!--<span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">$</span>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${player.value < 1000}" th:text="${player.value}">$45M</div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${player.value >= 1000 and player.value< 1000000}" th:with="result=${player.value/1000}"><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${result}">$45</span><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">K</span></div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${player.value >= 1000000 and player.value< 1000000000}" th:with="result=${player.value/1000000}"><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${result}">$45</span><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">M</span></div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${player.value >= 1000000000 }" th:with="result=${player.value/1000000000}"><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${result}">$45</span><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">B</span></div>-->


                            <!--</div>-->

                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner bg&#45;&#45;navy&#45;&#45;600" th:each="player : ${session.players}" th:if="${player.positionId==4}">-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;pos">-->
                                              <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;pos&#45;&#45;type bg&#45;&#45;purple ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">F</div>-->

                                <!--</div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;player ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">-->
                                    <!--&lt;!&ndash;<img src="img/Flags/ek&#45;&#45;aze.svg" alt="ek&#45;&#45;aze" />&ndash;&gt;-->
                                    <!--<img th:src="@{${'img/Flags/'+ player.country.code +'.svg'}}" alt="">-->
                                    <!--<a class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" id="sellPlayerModalLink" th:data-id="${player.id}"  th:text="${player.name}" >FC Anaconda</a>-->
                                    <!--&lt;!&ndash; Ek Transfer Player Modal &ndash;&gt;-->
                                    <!--<div th:id="'ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;modal' + ${player.id}" class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;player bg&#45;&#45;navy&#45;&#45;700 modal">-->
                                        <!--<div class="ek&#45;&#45;close">-->
                                            <!--<a class="ek&#45;&#45;close&#45;&#45;icon bg&#45;&#45;navy&#45;&#45;700" href="#" rel="modal:close">-->
                                                <!--<svg class="icon icon-ek&#45;&#45;close&#45;&#45;icon fill&#45;&#45;navy&#45;&#45;200"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;close&#45;&#45;icon"></use></svg>-->
                                            <!--</a>-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Head &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head">-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Head Image &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;image bg&#45;&#45;navy&#45;&#45;900">-->
                                                <!--<img src="img/Other/ek&#45;&#45;null&#45;&#45;image.png" alt="">-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Head Image &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Head About &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about">-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Head About Name &ndash;&gt;-->
                                                <!--<h3 class="ek&#45;&#45;size&#45;&#45;20&#45;&#45;400 text&#45;&#45;white"th:text="${player.name}"></h3>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About Name &ndash;&gt;-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Head About Country &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;country">-->
                                                    <!--<img th:src="@{${'img/Flags/'+ player.country.code +'.svg'}}" alt="">-->
                                                    <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;20&#45;&#45;400" th:text="${player.country.code}">USA</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About Country &ndash;&gt;-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Head About Position &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;position bg&#45;&#45;orange&#45;&#45;dark text&#45;&#45;navy&#45;&#45;900 ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:if="${player.positionId==1}">Goalkeeper</div>-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;position bg&#45;&#45;yellow text&#45;&#45;navy&#45;&#45;900 ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:if="${player.positionId==2}">Defender</div>-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;position bg&#45;&#45;cyan&#45;&#45;two text&#45;&#45;navy&#45;&#45;900 ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:if="${player.positionId==3}">Midfielder</div>-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;position bg&#45;&#45;purple text&#45;&#45;navy&#45;&#45;900 ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:if="${player.positionId==4}">Forward</div>-->




                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About Position &ndash;&gt;-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Head &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Inner &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner">-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Feature &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature">-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature&#45;&#45;age">-->
                                                    <!--<p class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${player.age}">27</p>-->
                                                    <!--<h4 class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Age</h4>-->
                                                <!--</div>-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature&#45;&#45;abl">-->
                                                    <!--<p class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${#numbers.formatDecimal(player.ability,0,0)} ">80</p>-->
                                                    <!--<h4 class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Abl</h4>-->
                                                <!--</div>-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature&#45;&#45;value">-->
                                                    <!--<p class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">$</span>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${player.value <1000}" th:text="${player.value}"></span>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${player.value >=1000 and player.value < 1000000}"  th:with="result=${player.value/1000}" ><span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${result}"></span> <span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">K</span></span>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${player.value >=1000000 and player.value < 1000000000}"  th:with="result=${player.value/1000000}" ><span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${result}"></span> <span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">M</span></span>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${player.value >=1000000000}"  th:with="result=${player.value/1000000000}" > <span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${result}"></span><span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">B</span></span>-->

                                                    <!--</p>-->
                                                    <!--<h4 class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Value</h4>-->
                                                <!--</div>-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Feature &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values">-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Energy &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                        <!--<svg class="icon icon-ek&#45;&#45;player&#45;&#45;country fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;player&#45;&#45;country"></use></svg>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Country</span>-->
                                                    <!--</div>-->
                                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Right &ndash;&gt;-->
                                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;right">-->
                                                        <!--<img th:src="@{${'img/Flags/'+ player.country.code +'.svg'}}" alt="">-->
                                                        <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${player.country.code} ">USA</span>-->
                                                    <!--</div>-->
                                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Right &ndash;&gt;-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Energy &ndash;&gt;-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Goals &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                        <!--<svg class="icon icon-ek&#45;&#45;ball fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;ball"></use></svg>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Goals</span>-->
                                                    <!--</div>-->
                                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${player.goals} " >27</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Goals &ndash;&gt;-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Played Match &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                        <!--<svg class="icon icon-ek&#45;&#45;stadium fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;stadium"></use></svg>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Played Match</span>-->
                                                    <!--</div>-->
                                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${player.appearance} ">116</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Played Match &ndash;&gt;-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Red Card &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                        <!--<svg class="icon icon-ek&#45;&#45;card fill&#45;&#45;red"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;card"></use></svg>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Red Card</span>-->
                                                    <!--</div>-->
                                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500"th:text="${player.redCard} " >4</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Red Card  &ndash;&gt;-->
                                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Red Card &ndash;&gt;-->
                                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                        <!--<svg class="icon icon-ek&#45;&#45;card fill&#45;&#45;yellow"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;card"></use></svg>-->
                                                        <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200" >Yellow Card</span>-->
                                                    <!--</div>-->
                                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                                    <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${player.yellowCard} ">21</span>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Red Card  &ndash;&gt;-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Refill Energy &ndash;&gt;-->
                                            <!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;refill bg&#45;&#45;blue text&#45;&#45;white" th:data-id="${player.id}" id="openSecSellModal" >Sell player</a>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Refill Energy &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner &ndash;&gt;-->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; End Ek Transfer Player Modal &ndash;&gt;-->
                                    <!--&lt;!&ndash; Ek Transfer Player Buy Modal &ndash;&gt;-->
                                    <!--<div  th:id="'ek&#45;&#45;transfer&#45;&#45;sell' + ${player.id}" class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill bg&#45;&#45;navy&#45;&#45;700 modal">-->
                                        <!--&lt;!&ndash; Ek Body Squad Refill Modal Head &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head">-->
                                            <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Energy &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head&#45;&#45;energy bg&#45;&#45;navy&#45;&#45;600">-->
                                                <!--<img src="img/Other/ek&#45;&#45;null&#45;&#45;image.png" alt="null">-->
                                            <!--</div>-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head&#45;&#45;want">-->
                                                <!--<h4 class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;20">Sell player</h4>-->
                                                <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Energy Player Name &ndash;&gt;-->
                                                <!--<p>Do you want to sell <span th:text="${player.name} "></span>?</p>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Energy Player Name &ndash;&gt;-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Energy &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons">-->
                                            <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons Cancel &ndash;&gt;-->
                                            <!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;cancel text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;navy&#45;&#45;600" href="#ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;modal" rel="modal:open">Cancel</a>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons Cancel &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons Energy &ndash;&gt;-->
                                            <!--&lt;!&ndash;<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;energy text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;blue" href="#ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;succes" rel="modal:open">Sell player</a>&ndash;&gt;-->
                                            <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons Energy &ndash;&gt;-->
                                            <!--<button class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;energy text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;blue"id="sellPlayer"  th:data-id="${player.id}">Sell Player</button>-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons &ndash;&gt;-->
                                        <!--<a href="#close-modal" rel="modal:close" class="close-modal">Close</a>-->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; End Ek Transfer Player Buy Modal &ndash;&gt;-->
                                    <!--&lt;!&ndash; Ek Transfer Player Buy Modal &ndash;&gt;-->
                                    <!--<div id="ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;succes" class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill ek&#45;&#45;modal&#45;&#45;border&#45;&#45;left&#45;&#45;green&#45;&#45;dark&#45;&#45;two bg&#45;&#45;navy&#45;&#45;700 modal">-->
                                        <!--&lt;!&ndash; Ek Body Squad Refill Modal Head &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head">-->
                                            <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Energy &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head&#45;&#45;energy bg&#45;&#45;green&#45;&#45;dark&#45;&#45;two">-->
                                                <!--<svg class="icon icon-ek&#45;&#45;correct fill&#45;&#45;white"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;correct"></use></svg>-->
                                            <!--</div>-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;head&#45;&#45;want">-->
                                                <!--<h4 class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;20">You sold the player</h4>-->
                                                <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Energy Player Name &ndash;&gt;-->
                                                <!--<p><span th:text="${player.name} "></span> successfully sold</p>-->
                                                <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Energy Player Name &ndash;&gt;-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Energy &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons">-->
                                            <!--&lt;!&ndash; Ek Body Squad Refill Modal Head  Buttons Cancel &ndash;&gt;-->
                                            <!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;cancel text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;navy&#45;&#45;600" href="#ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;succes" rel="modal:close">Cancel</a>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons Cancel &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Refill Modal Head  Buttons &ndash;&gt;-->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; End Ek Transfer Player Buy Modal &ndash;&gt;-->
                                <!--</div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;age ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${player.age} ">18</div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;abl ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${#numbers.formatDecimal(player.ability,0,0)} ">80</div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;matches ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${player.appearance} ">118</div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;goals ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${player.goals} ">1</div>-->
                                <!--&lt;!&ndash;<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${player.value} ">$45M</div>&ndash;&gt;-->

                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">-->

                                <!--<span >$</span>-->

                                <!--<span  th:if="${player.value < 1000}" th:text="${player.value}">$45M</span>-->
                                <!--<span  th:if="${player.value >= 1000 and player.value< 1000000}" th:with="result=${player.value/1000}"><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${result}">$45</span><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">K</span></span>-->
                                <!--<span  th:if="${player.value >= 1000000 and player.value< 1000000000}" th:with="result=${player.value/1000000}"><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${result}">$45</span><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">M</span></span>-->
                                <!--<span  th:if="${player.value >= 1000000000 }" th:with="result=${player.value/1000000000}"><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${result}">$45</span><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">B</span></span>-->
                                <!--</div>-->

                            <!--</div>-->

                        <!-- End Ek Transfer Body Tabs Content Inner -->
                    </div>
                    <!-- End Ek Transfer Body Tabs Content Sell Body -->
                </div>
                <!-- End Ek Transfer Body Tabs Content Sell -->
                <!-- Ek Transfer Body Tabs Content History -->
                <div class="ek--transfer--body--tabs--history content--active text--white">
                <!-- Ek Transfer Body Tabs Content Sell Head -->
                <div class="ek--transfer--body--tabs--history--head">
                    <div class="ek--transfer--body--tabs--history--head--pos ek--size--16 text--navy--200">Opr.</div>
                    <div class="ek--transfer--body--tabs--history--head--player ek--size--16 text--navy--200">Player name</div>
                    <div class="ek--transfer--body--tabs--history--head--age ek--size--16 text--navy--200">Age</div>
                    <div class="ek--transfer--body--tabs--history--head--abl ek--size--16 text--navy--200">Abl</div>
                    <div class="ek--transfer--body--tabs--history--head--price ek--size--16 text--navy--200">Price</div>
                </div>
                <!-- End Ek Transfer Body Tabs Content Sell Head -->
                <!-- Ek Transfer Body Tabs Content Sell Body -->
                <div class="ek--transfer--body--tabs--history--body" id="historyList">
                    <!-- Ek Transfer Body Tabs Content Inner -->
                    <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner bg&#45;&#45;navy&#45;&#45;600" th:each="history,count : ${session.historyList}">-->
                        <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;pos">-->
                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;pos&#45;&#45;type ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">-->
                                <!--<svg th:if="${history.transferStatus==2}" class="icon icon-arrow_drop_up fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-arrow_drop_up"></use></svg>-->
                                <!--<svg th:if="${history.transferStatus!=2}" class="icon icon-arrow_drop_up fill&#45;&#45;red"><use xlink:href="img/icons.svg#icon-arrow_drop_down"></use></svg>-->

                            <!--</div>-->
                        <!--</div>-->
                        <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;player ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">-->
                            <!--&lt;!&ndash;<img src="img/Flags/ek&#45;&#45;aze.svg" alt="ek&#45;&#45;aze" />&ndash;&gt;-->
                            <!--<img th:src="@{${'img/Flags/'+ history.country.code +'.svg'}}" alt="">-->
                            <!--<a class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" id="historyLink" th:data-id="${count.index+1}" th:text="${history.playerName} ">FC Anaconda</a>-->
                            <!--&lt;!&ndash; Ek Transfer Player Modal &ndash;&gt;-->
                            <!--<div th:id="'ek&#45;&#45;transfer&#45;&#45;history' +${count.index+1}"  class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;player bg&#45;&#45;transparent modal" style="box-shadow: unset">-->
                                <!--<div class="ek&#45;&#45;close">-->
                                    <!--<a class="ek&#45;&#45;close&#45;&#45;icon bg&#45;&#45;navy&#45;&#45;700" href="#" rel="modal:close">-->
                                        <!--<svg class="icon icon-ek&#45;&#45;close&#45;&#45;icon fill&#45;&#45;navy&#45;&#45;200"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;close&#45;&#45;icon"></use></svg>-->
                                    <!--</a>-->
                                <!--</div>-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;main bg&#45;&#45;navy&#45;&#45;700">-->
                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Head &ndash;&gt;-->
                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head">-->
                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Head Image &ndash;&gt;-->
                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;image bg&#45;&#45;navy&#45;&#45;900">-->
                                        <!--<img src="img/Other/ek&#45;&#45;null&#45;&#45;image.png" alt="">-->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Head Image &ndash;&gt;-->
                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Head About &ndash;&gt;-->
                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about">-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Head About Name &ndash;&gt;-->
                                        <!--<h3 class="ek&#45;&#45;size&#45;&#45;20&#45;&#45;400 text&#45;&#45;white" th:text="${history.playerName} ">Gwernach Boren</h3>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About Name &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Head About Country &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;country">-->
                                            <!--<img th:src="@{${'img/Flags/'+ history.country.code +'.svg'}}" alt="">-->
                                            <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;20&#45;&#45;400" th:text="${history.country.code} ">USA</span>-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About Country &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Head About Position &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;head&#45;&#45;about&#45;&#45;position bg&#45;&#45;yellow text&#45;&#45;navy&#45;&#45;900 ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${history.position} ">Defender</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About Position &ndash;&gt;-->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Head About &ndash;&gt;-->
                                <!--</div>-->
                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Head &ndash;&gt;-->
                                <!--&lt;!&ndash; Ek Body Squad Player Modal Inner &ndash;&gt;-->
                                <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner">-->
                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Feature &ndash;&gt;-->
                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature">-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature&#45;&#45;age">-->
                                            <!--<p class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${history.age} ">27</p>-->
                                            <!--<h4 class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Age</h4>-->
                                        <!--</div>-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature&#45;&#45;abl">-->
                                            <!--<p class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${#numbers.formatDecimal(history.ability,0,0)} ">80</p>-->
                                            <!--<h4 class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Abl</h4>-->
                                        <!--</div>-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;feature&#45;&#45;value">-->
                                            <!--<p class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">-->
                                                <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">$</span>-->
                                                <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${history.value <1000}" th:text="${history.value}"></span>-->
                                                <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${history.value >=1000 and history.value < 1000000}"  th:with="result=${history.value/1000}" ><span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${result}"></span><span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">K</span></span>-->
                                                <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${history.value >=1000000 and history.value < 1000000000}"  th:with="result=${history.value/1000000}" ><span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${result}"></span><span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">M</span></span>-->
                                                <!--<span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:if="${history.value >=1000000000}"  th:with="result=${history.value/1000000000}" ><span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white" th:text="${result}"></span><span class="ek&#45;&#45;size&#45;&#45;28&#45;&#45;500 text&#45;&#45;white">B</span></span>-->

                                            <!--</p>-->
                                            <!--<h4 class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Value</h4>-->
                                        <!--</div>-->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Feature &ndash;&gt;-->
                                    <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values &ndash;&gt;-->
                                    <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values">-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Energy &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                <!--<svg class="icon icon-ek&#45;&#45;player&#45;&#45;country fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;player&#45;&#45;country"></use></svg>-->
                                                <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Country</span>-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Right &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;right">-->
                                                <!--<img th:src="@{${'img/Flags/'+ history.country.code +'.svg'}}" alt="">-->
                                                <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${history.country.code} ">USA</span>-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Right &ndash;&gt;-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Energy &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Goals &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                <!--<svg class="icon icon-ek&#45;&#45;ball fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;ball"></use></svg>-->
                                                <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Goals</span>-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                            <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${history.goals} ">27</span>-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Goals &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Played Match &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                <!--<svg class="icon icon-ek&#45;&#45;stadium fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;stadium"></use></svg>-->
                                                <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Played Match</span>-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                            <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${history.appearance} ">116</span>-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Played Match &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Red Card &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                <!--<svg class="icon icon-ek&#45;&#45;card fill&#45;&#45;red"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;card"></use></svg>-->
                                                <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Red Card</span>-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                            <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${history.redCards} ">4</span>-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Red Card  &ndash;&gt;-->
                                        <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections Red Card &ndash;&gt;-->
                                        <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section bg&#45;&#45;navy&#45;&#45;600">-->
                                            <!--&lt;!&ndash; Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                            <!--<div class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;inner&#45;&#45;values&#45;&#45;section&#45;&#45;left">-->
                                                <!--<svg class="icon icon-ek&#45;&#45;card fill&#45;&#45;yellow"><use xlink:href="img/icons.svg#icon-ek&#45;&#45;card"></use></svg>-->
                                                <!--<span class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Yellow Card</span>-->
                                            <!--</div>-->
                                            <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections left &ndash;&gt;-->
                                            <!--<span class="text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500" th:text="${history.yellowCards} ">21</span>-->
                                        <!--</div>-->
                                        <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values Sections Red Card  &ndash;&gt;-->
                                        <!---->
                                    <!--</div>-->
                                    <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner Values &ndash;&gt;-->
                                <!--</div>-->
                                <!--&lt;!&ndash; End Ek Body Squad Player Modal Inner &ndash;&gt;-->
                                <!--</div>-->
                                <!--&lt;!&ndash; Ek Transfer History Modal &ndash;&gt;-->
                                <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;history bg&#45;&#45;navy&#45;&#45;700">-->
                                    <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;history&#45;&#45;left">-->
                                        <!--<h4 class="ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;navy&#45;&#45;200">Transfer to:</h4>-->
                                        <!--<p class="ek&#45;&#45;size&#45;&#45;18 text&#45;&#45;white" th:text="${history.toClub} "><span class="ek&#45;&#45;size&#45;&#45;16"> <img  th:src="@{${'img/Flags/'+ history.country.code +'.svg'}}"   th:text="${history.country.code} "/> USA</span></p>-->
                                    <!--</div>-->
                                    <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;history&#45;&#45;right">-->
                                         <!--<svg th:if="history.transferStatus==2" class="icon icon-arrow_drop_up fill&#45;&#45;green"><use xlink:href="img/icons.svg#icon-arrow_drop_up"></use></svg>-->
                                        <!--<svg  th:if="history.transferStatus!=2" class="icon icon-arrow_drop_down fill&#45;&#45;red"><use xlink:href="img/icons.svg#icon-arrow_drop_down"></use></svg>-->
                                        <!--<p class="text&#45;&#45;navy&#45;&#45;200 ek&#45;&#45;size&#45;&#45;16" th:text="${#strings.substring(history.transferDate,0,10)}">2 Jun 2019</p>-->

                                    <!--</div>-->
                                <!--</div>-->
                                <!--&lt;!&ndash; End Ek Transfer History Modal &ndash;&gt;-->
                            <!--</div>-->
                            <!--&lt;!&ndash; End Ek Transfer Player Modal &ndash;&gt;-->
                        <!--</div>-->
                        <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;age ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${history.age} ">18</div>-->
                        <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;abl ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${#numbers.formatDecimal(history.ability,0,0)} ">80</div>-->
                        <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" >-->
                            <!--<span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">$</span>-->
                            <!--<span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${history.value <1000}" th:text="${history.value}"></span>-->
                            <!--<span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${history.value >=1000 and history.value < 1000000}"  th:with="result=${history.value/1000}" ><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${result}"></span><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">K</span></span>-->
                            <!--<span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${history.value >=1000000 and history.value < 1000000000}"  th:with="result=${history.value/1000000}" ><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${result}"></span><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">M</span></span>-->
                            <!--<span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:if="${history.value >=1000000000}"  th:with="result=${history.value/1000000000}" ><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${result}"></span><span class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">B</span></span>-->


                        <!--</div>-->
                    <!--</div>-->

                    <!-- End Ek Transfer Body Tabs Content Inner -->
                </div>
                <!-- End Ek Transfer Body Tabs Content Sell Body -->
                </div>
                <!-- End Ek Transfer Body Tabs Content History -->
            </div>
            <!-- End Ek Transfer Body Tabs Content -->
            </div>
            <!-- End Ek Transfer Body Tabs -->
        </div>
        <!-- End Ek Transfer Body -->
    </section>

@endsection

@section("appendix")
    <div id="ek--soft--notenough" class="ek--modal ek--modal--border--left--orange bg--navy--700 modal">
        <div class="ek--modal--body">
            <div class="ek--modal--body--left">
                <div class="ek--modal--body--left--image bg--orange--light">
                    <svg class="icon icon-ek--warning fill--white"><use xlink:href="static/img/icons.svg#icon-ek--warning"></use></svg>
                </div>
            </div>
            <div class="ek--modal--body--right">
                <h3 class="text--white ek--size--20">Money is not enough</h3>
                <p class="text--navy--100 ek--size--18">There is not enough money to buy player</p>
                <div class="ek--modal--body--buttons">
                    <a class="ek--modal--body--buttons--cancel text--white ek--size--16" href="#ek--soft" rel="modal:open">Cancel</a>
                </div>
            </div>
        </div>
    </div>

    <div id="ek--soft--state" class="ek--modal ek--modal--border--left--orange bg--navy--700 modal">
        <div class="ek--modal--body">
            <div class="ek--modal--body--left">
                <div class="ek--modal--body--left--image bg--orange--light">
                    <svg class="icon icon-ek--warning fill--white"><use xlink:href="static/img/icons.svg#icon-ek--warning"></use></svg>
                </div>
            </div>
            <div class="ek--modal--body--right">
                <h3 class="text--white ek--size--20">You have reached to the transfer limit</h3>
                <p class="text--navy--100 ek--size--18" id="text"></p>
                <div class="ek--modal--body--buttons">
                    <a class="ek--modal--body--buttons--cancel text--white ek--size--16" href="#ek--soft" rel="modal:open">Cancel</a>

                    <!--<a class="ek&#45;&#45;modal&#45;&#45;body&#45;&#45;buttons&#45;&#45;other text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16&#45;&#45;500 bg&#45;&#45;blue" href="javascript:void(0)">Buy EK Coins</a>-->
                </div>
            </div>
        </div>
    </div>

    <div id="loading" class="ek--modal--loading">
        <div class="ek--modal--loading--body">
            <div class="ek--modal--loading--body--inner">
                <div class="ek--modal--loading--body--image bg--navy--700">
                    <img src="static/img/Dashboard/ek--loading@2x.png" />
                </div>
                <p class="text--navy--100 ek--size--16"></p>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{url("static/js/transfer.js")}}"></script>
@endsection
