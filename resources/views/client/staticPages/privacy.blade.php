<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
<HEAD>
    <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <META http-equiv="X-UA-Compatible" content="IE=8">
    <TITLE>bcl_70782728.htm</TITLE>
    <META name="generator" content="BCL easyConverter SDK 5.0.140">
    <STYLE type="text/css">

        body {margin-top: 0px;margin-left: 0px;}

        #page_1 {position:relative; overflow: hidden;margin: 108px 0px 112px 96px;padding: 0px;border: none;width: 720px;}





        #page_2 {position:relative; overflow: hidden;margin: 90px 0px 95px 96px;padding: 0px;border: none;width: 720px;}





        #page_3 {position:relative; overflow: hidden;margin: 109px 0px 95px 96px;padding: 0px;border: none;width: 720px;}





        #page_4 {position:relative; overflow: hidden;margin: 90px 0px 128px 96px;padding: 0px;border: none;width: 720px;}





        #page_5 {position:relative; overflow: hidden;margin: 89px 0px 93px 96px;padding: 0px;border: none;width: 720px;}





        #page_6 {position:relative; overflow: hidden;margin: 90px 0px 137px 96px;padding: 0px;border: none;width: 720px;}





        #page_7 {position:relative; overflow: hidden;margin: 89px 0px 99px 96px;padding: 0px;border: none;width: 720px;}





        #page_8 {position:relative; overflow: hidden;margin: 90px 0px 97px 96px;padding: 0px;border: none;width: 720px;}





        #page_9 {position:relative; overflow: hidden;margin: 90px 0px 95px 96px;padding: 0px;border: none;width: 720px;}
        #page_9 #id9_1 {border:none;margin: 0px 0px 0px 0px;padding: 0px;border:none;width: 720px;overflow: hidden;}
        #page_9 #id9_2 {border:none;margin: 6px 0px 0px 0px;padding: 0px;border:none;width: 624px;overflow: hidden;}





        #page_10 {position:relative; overflow: hidden;margin: 90px 0px 128px 96px;padding: 0px;border: none;width: 720px;}





        #page_11 {position:relative; overflow: hidden;margin: 88px 0px 230px 96px;padding: 0px;border: none;width: 720px;}





        .ft0{font: bold 27px 'Arial';line-height: 32px;}
        .ft1{font: bold 21px 'Arial';line-height: 24px;}
        .ft2{font: 16px 'Times New Roman';text-decoration: underline;color: #1155cc;line-height: 19px;}
        .ft3{font: bold 15px 'Arial';color: #999999;line-height: 18px;}
        .ft4{font: italic bold 16px 'Arial';line-height: 18px;}
        .ft5{font: 15px 'Gautami';line-height: 30px;}
        .ft6{font: 15px 'Arial';line-height: 18px;}
        .ft7{font: 15px 'Arial';line-height: 21px;}
        .ft8{font: 15px 'Arial';line-height: 20px;}
        .ft9{font: 15px 'Arial';line-height: 17px;}
        .ft10{font: 15px 'Gautami';line-height: 29px;}
        .ft11{font: 15px 'Arial';margin-left: 17px;line-height: 20px;}
        .ft12{font: 15px 'Arial';margin-left: 17px;line-height: 17px;}
        .ft13{font: 15px 'Arial';margin-left: 17px;line-height: 21px;}
        .ft14{font: 15px 'Arial';margin-left: 9px;line-height: 20px;}
        .ft15{font: 15px 'Arial';margin-left: 9px;line-height: 17px;}
        .ft16{font: 15px 'Arial';margin-left: 9px;line-height: 21px;}
        .ft17{font: bold 16px 'Arial';line-height: 19px;}
        .ft18{font: bold 21px 'Arial';line-height: 30px;}

        .p0{text-align: left;padding-left: 164px;margin-top: 0px;margin-bottom: 0px;}
        .p1{text-align: left;padding-left: 232px;margin-top: 38px;margin-bottom: 0px;}
        .p2{text-align: left;padding-left: 233px;margin-top: 9px;margin-bottom: 0px;}
        .p3{text-align: left;padding-left: 225px;margin-top: 23px;margin-bottom: 0px;}
        .p4{text-align: left;padding-left: 73px;margin-top: 45px;margin-bottom: 0px;}
        .p5{text-align: left;margin-top: 23px;margin-bottom: 0px;}
        .p6{text-align: justify;padding-right: 96px;margin-top: 6px;margin-bottom: 0px;}
        .p7{text-align: justify;padding-right: 96px;margin-top: 2px;margin-bottom: 0px;}
        .p8{text-align: justify;padding-right: 96px;margin-top: 18px;margin-bottom: 0px;}
        .p9{text-align: justify;padding-right: 96px;margin-top: 20px;margin-bottom: 0px;}
        .p10{text-align: left;margin-top: 20px;margin-bottom: 0px;}
        .p11{text-align: left;margin-top: 0px;margin-bottom: 0px;}
        .p12{text-align: left;margin-top: 62px;margin-bottom: 0px;}
        .p13{text-align: justify;padding-right: 97px;margin-top: 20px;margin-bottom: 0px;}
        .p14{text-align: left;margin-top: 15px;margin-bottom: 0px;}
        .p15{text-align: justify;padding-right: 97px;margin-top: 23px;margin-bottom: 0px;}
        .p16{text-align: left;margin-top: 16px;margin-bottom: 0px;}
        .p17{text-align: left;margin-top: 17px;margin-bottom: 0px;}
        .p18{text-align: justify;padding-left: 38px;padding-right: 97px;margin-top: 23px;margin-bottom: 0px;text-indent: -29px;}
        .p19{text-align: justify;padding-left: 38px;padding-right: 97px;margin-top: 0px;margin-bottom: 0px;text-indent: -29px;}
        .p20{text-align: left;padding-left: 9px;margin-top: 0px;margin-bottom: 0px;}
        .p21{text-align: left;padding-left: 9px;margin-top: 3px;margin-bottom: 0px;}
        .p22{text-align: justify;padding-left: 38px;padding-right: 96px;margin-top: 3px;margin-bottom: 0px;text-indent: -29px;}
        .p23{text-align: left;padding-left: 38px;padding-right: 97px;margin-top: 3px;margin-bottom: 0px;text-indent: -29px;}
        .p24{text-align: left;padding-left: 38px;padding-right: 97px;margin-top: 0px;margin-bottom: 0px;text-indent: -29px;}
        .p25{text-align: left;padding-left: 9px;margin-top: 1px;margin-bottom: 0px;}
        .p26{text-align: left;padding-left: 38px;padding-right: 96px;margin-top: 3px;margin-bottom: 0px;text-indent: -29px;}
        .p27{text-align: left;padding-left: 38px;padding-right: 96px;margin-top: 0px;margin-bottom: 0px;text-indent: -29px;}
        .p28{text-align: left;padding-left: 38px;padding-right: 97px;margin-top: 1px;margin-bottom: 0px;text-indent: -29px;}
        .p29{text-align: justify;padding-left: 38px;padding-right: 96px;margin-top: 0px;margin-bottom: 0px;text-indent: -29px;}
        .p30{text-align: left;margin-top: 82px;margin-bottom: 0px;}
        .p31{text-align: left;padding-left: 8px;margin-top: 0px;margin-bottom: 0px;}
        .p32{text-align: left;margin-top: 5px;margin-bottom: 0px;}
        .p33{text-align: justify;padding-right: 96px;margin-top: 4px;margin-bottom: 0px;}
        .p34{text-align: left;margin-top: 19px;margin-bottom: 0px;}
        .p35{text-align: justify;padding-right: 96px;margin-top: 0px;margin-bottom: 0px;}
        .p36{text-align: left;margin-top: 35px;margin-bottom: 0px;}
        .p37{text-align: left;margin-top: 14px;margin-bottom: 0px;}
        .p38{text-align: justify;padding-right: 97px;margin-top: 38px;margin-bottom: 0px;}
        .p39{text-align: justify;padding-right: 96px;margin-top: 3px;margin-bottom: 0px;}
        .p40{text-align: left;margin-top: 39px;margin-bottom: 0px;}
        .p41{text-align: justify;padding-right: 96px;margin-top: 15px;margin-bottom: 0px;}
        .p42{text-align: justify;padding-right: 97px;margin-top: 0px;margin-bottom: 0px;}
        .p43{text-align: left;padding-left: 4px;margin-top: 13px;margin-bottom: 0px;}




    </STYLE>
</HEAD>

<BODY>
<DIV id="page_1">


    <P class="p0 ft0">TERMS & CONDITIONS</P>
    <P class="p1 ft1">ELEVEN KINGS</P>
    <P class="p2 ft2"><A href="https://elevenkings.com/">https://elevenkings.com/</A></P>
    <P class="p3 ft3">Last updated: 01.06.2020</P>
    <P class="p4 ft4">PLEASE READ THESE TERMS AND CONDITIONS CAREFULLY</P>
    <P class="p5 ft1">AGREEMENT TO TERMS</P>
    <P class="p6 ft6">These Terms and Conditions constitute a legally binding agreement made between you, whether personally or on behalf of an entity (“you”) and “Eleven Kings” <SPAN class="ft5">​</SPAN>(“we,” “us” or “our”), concerning your access to and use of the “Eleven Kings” mobile applications and “elevenkings.com” website as well as any other media form, media channel, mobile website or mobile application related, linked, or otherwise connected thereto (collectively, the “Site”). You agree that by accessing the Site, you have read, understood, and agree to be bound by all of these Terms and Conditions Use. IF YOU DO NOT AGREE WITH ALL OF THESE TERMS and</P>
    <P class="p7 ft7">CONDITIONS, THEN YOU ARE EXPRESSLY PROHIBITED FROM USING THE SITE AND YOU MUST DISCONTINUE USE IMMEDIATELY.</P>
    <P class="p8 ft8">Supplemental terms and conditions or documents that may be posted on the Site from time to time are hereby expressly incorporated herein by reference. We reserve the right, in our sole discretion, to make changes or modifications to these Terms and Conditions at any time and for any reason. We will alert you about any changes by updating the “Last updated” date of these Terms and Conditions and you waive any right to receive specific notice of each such change. It is your responsibility to periodically review these Terms and Conditions to stay informed of updates. You will be subject to, and will be deemed to have been made aware of and to have accepted, the changes in any revised Terms and Conditions by your continued use of the Site after the date such revised Terms are posted.</P>
    <P class="p9 ft8">The information provided on the Site is not intended for distribution to or use by any person or entity in any jurisdiction or country where such distribution or use would be contrary to law or regulation or which would subject us to any registration requirement within such jurisdiction or country. Accordingly, those persons who choose to access the Site from other locations do so on their own initiative and are solely responsible for compliance with local laws, if and to the extent local laws are applicable.</P>
    <P class="p10 ft9">The Site is intended for users who are at least 13 years old. Persons under the age of 13 are</P>
</DIV>
<DIV id="page_2">


    <P class="p11 ft9">not permitted to register for the Site.</P>
    <P class="p12 ft1">INTELLECTUAL PROPERTY RIGHTS</P>
    <P class="p6 ft8">Unless otherwise indicated, the Site is our proprietary property and all source code, databases, functionality, software, website designs, audio, video, text, photographs, and graphics on the Site (collectively, the “Content”) and the trademarks, service marks, and logos contained therein (the “Marks”) are owned or controlled by us or licensed to us, and are protected by copyright and trademark laws and various other intellectual property rights and unfair competition laws of the United States, foreign jurisdictions, and international conventions. The Content and the Marks are provided on the Site “AS IS” for your information and personal use only. Except as expressly provided in these Terms of Use, no part of the Site and no Content or Marks may be copied, reproduced, aggregated, republished, uploaded, posted, publicly displayed, encoded, translated, transmitted, distributed, sold, licensed, or otherwise exploited for any commercial purpose whatsoever, without our express prior written permission.</P>
    <P class="p13 ft7">Provided that you are eligible to use the Site, you are granted a limited license to access and use the Site and to download or print a copy of any portion of the Content to which you have properly gained access solely for your personal, <NOBR>non-commercial</NOBR> use. We reserve all rights not expressly granted to you in and to the Site, Content and the Marks.</P>
    <P class="p14 ft1">USER REPRESENTATIONS</P>
    <P class="p6 ft9">By using the Site, you represent and warrant that: [(1) all registration information you submit will be true, accurate, current, and complete; <SPAN class="ft10">​</SPAN>(2) you will maintain the accuracy of such information and promptly update such registration information as necessary;] <SPAN class="ft10">​</SPAN>(3) you have the legal capacity and you agree to comply with these Terms of Use; [(4) you are not under the age of 13;] (5) not a minor in the jurisdiction in which you reside[, or if a minor, you have received parental permission to use the Site]; (6) you will not access the Site through automated or <NOBR>non-human</NOBR> means, whether through a bot, script or otherwise; (7) you will not use the Site for any illegal or unauthorized purpose; and (8) your use of the Site will not violate any applicable law or regulation.</P>
    <P class="p15 ft7">If you provide any information that is untrue, inaccurate, not current, or incomplete, we have the right to suspend or terminate your account and refuse any and all current or future use of the Site (or any portion thereof).</P>
    <P class="p16 ft1">USER REGISTRATION</P>
    <P class="p6 ft7">You will be required to register with the Site. You agree to keep your password confidential and will be responsible for all use of your account and password. We reserve the right to remove, reclaim, or change a username you select if we determine, in our sole discretion, that such username is inappropriate, obscene, or otherwise objectionable.</P>
</DIV>
<DIV id="page_3">


    <P class="p11 ft1">PROHIBITED ACTIVITIES</P>
    <P class="p6 ft7">You may not access or use the Site for any purpose other than that for which we make the Site available. The Site may not be used in connection with any commercial endeavors except those that are specifically endorsed or approved by us.</P>
    <P class="p17 ft9">As a user of the Site, you agree not to:</P>
    <P class="p18 ft8"><SPAN class="ft9">1.</SPAN><SPAN class="ft11">systematically retrieve data or other content from the Site to create or compile, directly or indirectly, a collection, compilation, database, or directory without written permission from us.</SPAN></P>
    <P class="p19 ft8"><SPAN class="ft9">2.</SPAN><SPAN class="ft11">make any unauthorized use of the Site, including collecting usernames and/or email addresses of users by electronic or other means for the purpose of sending unsolicited email, or creating user accounts by automated means or under false pretenses.</SPAN></P>
    <P class="p20 ft9"><SPAN class="ft9">3.</SPAN><SPAN class="ft12">use a buying agent or purchasing agent to make purchases on the Site.</SPAN></P>
    <P class="p21 ft9"><SPAN class="ft9">4.</SPAN><SPAN class="ft12">use the Site to advertise or offer to sell goods and services.</SPAN></P>
    <P class="p22 ft8"><SPAN class="ft9">5.</SPAN><SPAN class="ft11">circumvent, disable, or otherwise interfere with </SPAN><NOBR>security-related</NOBR> features of the Site, including features that prevent or restrict the use or copying of any Content or enforce limitations on the use of the Site and/or the Content contained therein.</P>
    <P class="p20 ft9"><SPAN class="ft9">6.</SPAN><SPAN class="ft12">engage in unauthorized framing of or linking to the Site.</SPAN></P>
    <P class="p23 ft7"><SPAN class="ft9">7.</SPAN><SPAN class="ft13">trick, defraud, or mislead us and other users, especially in any attempt to learn sensitive account information such as user passwords;</SPAN></P>
    <P class="p24 ft8"><SPAN class="ft9">8.</SPAN><SPAN class="ft11">make improper use of our support services or submit false reports of abuse or misconduct.</SPAN></P>
    <P class="p19 ft8"><SPAN class="ft9">9.</SPAN><SPAN class="ft11">engage in any automated use of the system, such as using scripts to send comments or messages, or using any data mining, robots, or similar data gathering and extraction tools.</SPAN></P>
    <P class="p24 ft8"><SPAN class="ft9">10.</SPAN><SPAN class="ft14">interfere with, disrupt, or create an undue burden on the Site or the networks or services connected to the Site.</SPAN></P>
    <P class="p20 ft9"><SPAN class="ft9">11.</SPAN><SPAN class="ft15">attempt to impersonate another user or person or use the username of another user.</SPAN></P>
    <P class="p25 ft9"><SPAN class="ft9">12.</SPAN><SPAN class="ft15">sell or otherwise transfer your profile.</SPAN></P>
    <P class="p26 ft7"><SPAN class="ft9">13.</SPAN><SPAN class="ft16">use any information obtained from the Site in order to harass, abuse, or harm another person.</SPAN></P>
    <P class="p24 ft8"><SPAN class="ft9">14.</SPAN><SPAN class="ft14">use the Site as part of any effort to compete with us or otherwise use the Site and/or the Content for any </SPAN><NOBR>revenue-generating</NOBR> endeavor or commercial enterprise.</P>
    <P class="p27 ft8"><SPAN class="ft9">15.</SPAN><SPAN class="ft14">decipher, decompile, disassemble, or reverse engineer any of the software comprising or in any way making up a part of the Site.</SPAN></P>
    <P class="p24 ft8"><SPAN class="ft9">16.</SPAN><SPAN class="ft14">attempt to bypass any measures of the Site designed to prevent or restrict access to the Site, or any portion of the Site.</SPAN></P>
    <P class="p27 ft8"><SPAN class="ft9">17.</SPAN><SPAN class="ft14">harass, annoy, intimidate, or threaten any of our employees or agents engaged in providing any portion of the Site to you.</SPAN></P>
    <P class="p20 ft9"><SPAN class="ft9">18.</SPAN><SPAN class="ft15">delete the copyright or other proprietary rights notice from any Content.</SPAN></P>
    <P class="p28 ft7"><SPAN class="ft9">19.</SPAN><SPAN class="ft16">copy or adapt the Site’s software, including but not limited to Flash, PHP, HTML, JavaScript, or other code.</SPAN></P>
</DIV>
<DIV id="page_4">


    <P class="p29 ft8"><SPAN class="ft9">20.</SPAN><SPAN class="ft14">upload or transmit (or attempt to upload or to transmit) viruses, Trojan horses, or other material, including excessive use of capital letters and spamming (continuous posting of repetitive text), that interferes with any party’s uninterrupted use and enjoyment of the Site or modifies, impairs, disrupts, alters, or interferes with the use, features, functions, operation, or maintenance of the Site.</SPAN></P>
    <P class="p19 ft8"><SPAN class="ft9">21.</SPAN><SPAN class="ft14">upload or transmit (or attempt to upload or to transmit) any material that acts as a passive or active information collection or transmission mechanism, including without limitation, clear graphics interchange formats (“gifs”), 1×1 pixels, web bugs, cookies, or other similar devices (sometimes referred to as “spyware” or “passive collection mechanisms” or “pcms”).</SPAN></P>
    <P class="p29 ft8"><SPAN class="ft9">22.</SPAN><SPAN class="ft14">except as may be the result of standard search engine or Internet browser usage, use, launch, develop, or distribute any automated system, including without limitation, any spider, robot, cheat utility, scraper, or offline reader that accesses the Site, or using or launching any unauthorized script or other software.</SPAN></P>
    <P class="p20 ft9"><SPAN class="ft9">23.</SPAN><SPAN class="ft15">disparage, tarnish, or otherwise harm, in our opinion, us and/or the Site.</SPAN></P>
    <P class="p21 ft9"><SPAN class="ft9">24.</SPAN><SPAN class="ft15">use the Site in a manner inconsistent with any applicable laws or regulations.</SPAN></P>
    <P class="p30 ft1">GUIDELINES FOR REVIEWS</P>
    <P class="p6 ft8">We may provide you areas on the Site to leave reviews or ratings. When posting a review, you must comply with the following criteria: (1) you should have firsthand experience with the person/entity being reviewed; (2) your reviews should not contain offensive profanity, or abusive, racist, offensive, or hate language; (3) your reviews should not contain discriminatory references based on religion, race, gender, national origin, age, marital status, sexual orientation, or disability; (4) your reviews should not contain references to illegal activity; (5) you should not be affiliated with competitors if posting negative reviews; (6) you should not make any conclusions as to the legality of conduct; (7) you may not post any false or misleading statements; and (8) you may not organize a campaign encouraging others to post reviews, whether positive or negative.</P>
    <P class="p9 ft8">We may accept, reject, or remove reviews in our sole discretion. We have absolutely no obligation to screen reviews or to delete reviews, even if anyone considers reviews objectionable or inaccurate. Reviews are not endorsed by us, and do not necessarily represent our opinions or the views of any of our affiliates or partners. We do not assume liability for any review or for any claims, liabilities, or losses resulting from any review. By posting a review, you hereby grant to us a perpetual, <NOBR>non-exclusive,</NOBR> worldwide, <NOBR>royalty-free,</NOBR> <NOBR>fully-paid,</NOBR> assignable, and sublicensable right and license to reproduce, modify, translate, transmit by any means, display, perform, and/or distribute all content relating to reviews.</P>
    <P class="p31 ft10">​</P>
</DIV>
<DIV id="page_5">


    <P class="p11 ft1">MOBILE APPLICATION LICENSE</P>
    <P class="p32 ft17">Use License</P>
    <P class="p33 ft8">If you access the Site via a mobile application, then we grant you a revocable, <NOBR>non-exclusive,</NOBR> <NOBR>non-transferable,</NOBR> limited right to install and use the mobile application on wireless electronic devices owned or controlled by you, and to access and use the mobile application on such devices strictly in accordance with the terms and conditions of this mobile application license contained in these Terms of Use. You shall not: (1) decompile, reverse engineer, disassemble, attempt to derive the source code of, or decrypt the application; (2) make any modification, adaptation, improvement, enhancement, translation, or derivative work from the application; (3) violate any applicable laws, rules, or regulations in connection with your access or use of the application; (4) remove, alter, or obscure any proprietary notice (including any notice of copyright or trademark) posted by us or the licensors of the application; (5) use the application for any revenue generating endeavor, commercial enterprise, or other purpose for which it is not designed or intended; (6) make the application available over a network or other environment permitting access or use by multiple devices or users at the same time; (7) use the application for creating a product, service, or software that is, directly or indirectly, competitive with or in any way a substitute for the application; (8) use the application to send automated queries to any website or to send any unsolicited commercial <NOBR>e-mail;</NOBR> or (9) use any proprietary information or any of our interfaces or our other intellectual property in the design, development, manufacture, licensing, or distribution of any applications, accessories, or devices for use with the application.</P>
    <P class="p34 ft17">Apple and Android Devices</P>
    <P class="p33 ft8">The following terms apply when you use a mobile application obtained from either the Apple Store or Google Play (each an “App Distributor”) to access the Site: (1) the license granted to you for our mobile application is limited to a <NOBR>non-transferable</NOBR> license to use the application on a device that utilizes the Apple iOS or Android operating systems, as applicable, and in accordance with the usage rules set forth in the applicable App Distributor’s terms of service; (2) we are responsible for providing any maintenance and support services with respect to the mobile application as specified in the terms and conditions of this mobile application license contained in these Terms of Use or as otherwise required under applicable law, and you acknowledge that each App Distributor has no obligation whatsoever to furnish any maintenance and support services with respect to the mobile application; (3) in the event of any failure of the mobile application to conform to any applicable warranty, you may notify the applicable App Distributor, and the App Distributor, in accordance with its terms and policies, may refund the purchase price, if any, paid for the mobile application, and to the maximum extent permitted by applicable law, the App Distributor will have no other warranty obligation whatsoever with respect to the mobile application; (4) you represent and warrant that (i) you are not located in a country that is subject to a U.S. government embargo, or that has been designated by the U.S. government as a “terrorist supporting” country and (ii) you are not listed on any U.S. government list of prohibited or restricted parties; (5) you must comply with applicable <NOBR>third-party</NOBR> terms of agreement when using the mobile application, e.g., if you have a VoIP application, then you must not be in violation of their wireless data service agreement when using the mobile application; and (6) you acknowledge and agree that the App Distributors</P>
</DIV>
<DIV id="page_6">


    <P class="p35 ft7">are <NOBR>third-party</NOBR> beneficiaries of the terms and conditions in this mobile application license contained in these Terms of Use, and that each App Distributor will have the right (and will be deemed to have accepted the right) to enforce the terms and conditions in this mobile application license contained in these Terms of Use against you as a <NOBR>third-party</NOBR> beneficiary thereof.</P>
    <P class="p36 ft1">SOCIAL MEDIA</P>
    <P class="p6 ft8">As part of the functionality of the Site, you may link your account with online accounts you have with <NOBR>third-party</NOBR> service providers (each such account, a <NOBR>“Third-Party</NOBR> Account”) by either: (1) providing your <NOBR>Third-Party</NOBR> Account login information through the Site; or (2) allowing us to access your <NOBR>Third-Party</NOBR> Account, as is permitted under the applicable terms and conditions that govern your use of each <NOBR>Third-Party</NOBR> Account. You represent and warrant that you are entitled to disclose your <NOBR>Third-Party</NOBR> Account login information to us and/or grant us access to your <NOBR>Third-Party</NOBR> Account, without breach by you of any of the terms and conditions that govern your use of the applicable <NOBR>Third-Party</NOBR> Account, and without obligating us to pay any fees or making us subject to any usage limitations imposed by the <NOBR>third-party</NOBR> service provider of the <NOBR>Third-Party</NOBR> Account. By granting us access to any <NOBR>Third-Party</NOBR> Accounts, you understand that (1) we may access, make available, and store (if applicable) any content that you have provided to and stored in your <NOBR>Third-Party</NOBR> Account (the “Social Network Content”) so that it is available on and through the Site via your account, including without limitation any friend lists and (2) we may submit to and receive from your <NOBR>Third-Party</NOBR> Account additional information to the extent you are notified when you link your account with the <NOBR>Third-Party</NOBR> Account. Depending on the <NOBR>Third-Party</NOBR> Accounts you choose and subject to the privacy settings that you have set in such <NOBR>Third-Party</NOBR> Accounts, personally identifiable information that you post to your <NOBR>Third-Party</NOBR> Accounts may be available on and through your account on the Site. Please note that if a <NOBR>Third-Party</NOBR> Account or associated service becomes unavailable or our access to such <NOBR>Third-Party</NOBR> Account is terminated by the <NOBR>third-party</NOBR> service provider, then Social Network Content may no longer be available on and through the Site. You will have the ability to disable the connection between your account on the Site and your <NOBR>Third-Party</NOBR> Accounts at any time. PLEASE NOTE THAT YOUR RELATIONSHIP WITH THE <NOBR>THIRD-PARTY</NOBR> SERVICE PROVIDERS ASSOCIATED WITH YOUR <NOBR>THIRD-PARTY</NOBR> ACCOUNTS IS GOVERNED SOLELY BY YOUR AGREEMENT(S) WITH SUCH <NOBR>THIRD-PARTY</NOBR> SERVICE PROVIDERS. We make no effort to review any Social Network Content for any purpose, including but not limited to, for accuracy, legality, or <NOBR>non-infringement,</NOBR> and we are not responsible for any Social Network Content. You acknowledge and agree that we may access your email address book associated with a <NOBR>Third-Party</NOBR> Account and your contacts list stored on your mobile device or tablet computer solely for purposes of identifying and informing you of those contacts who have also registered to use the Site. You can deactivate the connection between the Site and your <NOBR>Third-Party</NOBR> Account by contacting us using the contact information below or through your account settings (if applicable). We will attempt to delete any information stored on our servers that was obtained through such <NOBR>Third-Party</NOBR> Account, except the username and profile picture that become associated with your account.</P>
</DIV>
<DIV id="page_7">


    <P class="p11 ft1">SUBMISSIONS</P>
    <P class="p6 ft8">You acknowledge and agree that any questions, comments, suggestions, ideas, feedback, or other information regarding the Site ("Submissions") provided by you to us are <NOBR>non-confidential</NOBR> and shall become our sole property. We shall own exclusive rights, including all intellectual property rights, and shall be entitled to the unrestricted use and dissemination of these Submissions for any lawful purpose, commercial or otherwise, without acknowledgment or compensation to you. You hereby waive all moral rights to any such Submissions, and you hereby warrant that any such Submissions are original with you or that you have the right to submit such Submissions. You agree there shall be no recourse against us for any alleged or actual infringement or misappropriation of any proprietary right in your Submissions.</P>
    <P class="p34 ft1"><NOBR>THIRD-PARTY</NOBR> WEBSITES AND CONTENT</P>
    <P class="p6 ft8">The Site may contain (or you may be sent via the Site) links to other websites <NOBR>("Third-Party</NOBR> Websites") as well as articles, photographs, text, graphics, pictures, designs, music, sound, video, information, applications, software, and other content or items belonging to or originating from third parties <NOBR>("Third-Party</NOBR> Content"). Such <NOBR>Third-Party</NOBR> Websites and <NOBR>Third-Party</NOBR> Content are not investigated, monitored, or checked for accuracy, appropriateness, or completeness by us, and we are not responsible for any <NOBR>Third-Party</NOBR> Websites accessed through the Site or any <NOBR>Third-Party</NOBR> Content posted on, available through, or installed from the Site, including the content, accuracy, offensiveness, opinions, reliability, privacy practices, or other policies of or contained in the <NOBR>Third-Party</NOBR> Websites or the <NOBR>Third-Party</NOBR> Content. Inclusion of, linking to, or permitting the use or installation of any <NOBR>Third-Party</NOBR> Websites or any <NOBR>Third-Party</NOBR> Content does not imply approval or endorsement thereof by us. If you decide to leave the Site and access the <NOBR>Third-Party</NOBR> Websites or to use or install any <NOBR>Third-Party</NOBR> Content, you do so at your own risk, and you should be aware these Terms of Use no longer govern. You should review the applicable terms and policies, including privacy and data gathering practices, of any website to which you navigate from the Site or relating to any applications you use or install from the Site. Any purchases you make through <NOBR>Third-Party</NOBR> Websites will be through other websites and from other companies, and we take no responsibility whatsoever in relation to such purchases which are exclusively between you and the applicable third party. You agree and acknowledge that we do not endorse the products or services offered on <NOBR>Third-Party</NOBR> Websites and you shall hold us harmless from any harm caused by your purchase of such products or services. Additionally, you shall hold us harmless from any losses sustained by you or harm caused to you relating to or resulting in any way from any <NOBR>Third-Party</NOBR> Content or any contact with <NOBR>Third-Party</NOBR> Websites.</P>
    <P class="p34 ft1">ADVERTISERS</P>
    <P class="p6 ft8">We allow advertisers to display their advertisements and other information in certain areas of the Site, such as sidebar advertisements or banner advertisements. If you are an advertiser, you shall take full responsibility for any advertisements you place on the Site and any services provided on the Site or products sold through those advertisements. Further, as an advertiser, you warrant and represent that you possess all rights and authority to place advertisements on the Site, including, but not limited to, intellectual property rights, publicity rights, and contractual</P>
</DIV>
<DIV id="page_8">


    <P class="p35 ft7">rights. [As an advertiser, you agree that such advertisements are subject to our Digital Millennium Copyright Act (“DMCA”) Notice and Policy provisions as described below, and you understand and agree there will be no refund or other compensation for DMCA <NOBR>takedown-related</NOBR> issues.] We simply provide the space to place such advertisements, and we have no other relationship with advertisers.</P>
    <P class="p37 ft1">SITE MANAGEMENT</P>
    <P class="p6 ft8">We reserve the right, but not the obligation, to: (1) monitor the Site for violations of these Terms of Use; (2) take appropriate legal action against anyone who, in our sole discretion, violates the law or these Terms of Use, including without limitation, reporting such user to law enforcement authorities; (3) in our sole discretion and without limitation, refuse, restrict access to, limit the availability of, or disable (to the extent technologically feasible) any of your Contributions or any portion thereof; (4) in our sole discretion and without limitation, notice, or liability, to remove from the Site or otherwise disable all files and content that are excessive in size or are in any way burdensome to our systems; and (5) otherwise manage the Site in a manner designed to protect our rights and property and to facilitate the proper functioning of the Site.</P>
    <P class="p38 ft18">DIGITAL MILLENNIUM COPYRIGHT ACT (DMCA) NOTICE AND POLICY</P>
    <P class="p11 ft17">Notifications</P>
    <P class="p39 ft8">We respect the intellectual property rights of others. If you believe that any material available on or through the Site infringes upon any copyright you own or control, please immediately notify our Designated Copyright Agent using the contact information provided below (a “Notification”). A copy of your Notification will be sent to the person who posted or stored the material addressed in the Notification. Please be advised that pursuant to federal law you may be held liable for damages if you make material misrepresentations in a Notification. Thus, if you are not sure that material located on or linked to by the Site infringes your copyright, you should consider first contacting an attorney.</P>
    <P class="p9 ft8">All Notifications should meet the requirements of DMCA 17 U.S.C. § 512(c)(3) and include the following information: (1) A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed; (2) identification of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works on the Site are covered by the Notification, a representative list of such works on the Site; (3) identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to permit us to locate the material; (4) information reasonably sufficient to permit us to contact the complaining party, such as an address, telephone number, and, if available, an email address at which the complaining party may be contacted; (5) a statement that the complaining party has a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law; and (6) a statement that the information in the notification</P>
</DIV>
<DIV id="page_9">


    <DIV id="id9_1">
        <P class="p35 ft7">is accurate, and under penalty of perjury, that the complaining party is authorized to act on behalf of the owner of an exclusive right that is allegedly infringed upon.</P>
        <P class="p40 ft1">TERM AND TERMINATION</P>
        <P class="p6 ft8">These Terms of Use shall remain in full force and effect while you use the Site. WITHOUT LIMITING ANY OTHER PROVISION OF THESE TERMS OF USE, WE RESERVE THE RIGHT TO, IN OUR SOLE DISCRETION AND WITHOUT NOTICE OR LIABILITY, DENY ACCESS TO AND USE OF THE SITE (INCLUDING BLOCKING CERTAIN IP ADDRESSES), TO ANY PERSON FOR ANY REASON OR FOR NO REASON, INCLUDING WITHOUT LIMITATION FOR BREACH OF ANY REPRESENTATION, WARRANTY, OR COVENANT CONTAINED IN THESE TERMS OF USE OR OF ANY APPLICABLE LAW OR REGULATION. WE MAY TERMINATE YOUR USE OR PARTICIPATION IN THE SITE OR DELETE [YOUR ACCOUNT AND] ANY CONTENT OR INFORMATION THAT YOU POSTED AT ANY TIME, WITHOUT WARNING, IN OUR SOLE DISCRETION.</P>
        <P class="p9 ft7">If we terminate or suspend your account for any reason, you are prohibited from registering and creating a new account under your name, a fake or borrowed name, or the name of any third party, even if you may be acting on behalf of the third party. In addition to terminating or suspending your account, we reserve the right to take appropriate legal action, including without limitation pursuing civil, criminal, and injunctive redress.</P>
        <P class="p37 ft1">MODIFICATIONS AND INTERRUPTIONS</P>
        <P class="p6 ft7">We reserve the right to change, modify, or remove the contents of the Site at any time or for any reason at our sole discretion without notice. However, we have no obligation to update any information on our Site. We also reserve the right to modify or discontinue all or part of the Site without notice at any time. We will not be liable to you or any third party for any modification, price change, suspension, or discontinuance of the Site.</P>
        <P class="p41 ft8">We cannot guarantee the Site will be available at all times. We may experience hardware, software, or other problems or need to perform maintenance related to the Site, resulting in interruptions, delays, or errors. We reserve the right to change, revise, update, suspend, discontinue, or otherwise modify the Site at any time or for any reason without notice to you. You agree that we have no liability whatsoever for any loss, damage, or inconvenience caused by your inability to access or use the Site during any downtime or discontinuance of the Site. Nothing in these Terms of Use will be construed to obligate us to maintain and support the Site or to supply any corrections, updates, or releases in connection therewith.</P>
        <P class="p40 ft1">CORRECTIONS</P>
    </DIV>
    <DIV id="id9_2">
        <P class="p11 ft7">There may be information on the Site that contains typographical errors, inaccuracies, or omissions that may relate to the Site, including descriptions, pricing, availability, and various</P>
    </DIV>
</DIV>
<DIV id="page_10">


    <P class="p42 ft7">other information. We reserve the right to correct any errors, inaccuracies, or omissions and to change or update the information on the Site at any time, without prior notice.</P>
    <P class="p17 ft1">DISCLAIMER</P>
    <P class="p6 ft8">THE SITE IS PROVIDED ON AN <NOBR>AS-IS</NOBR> AND <NOBR>AS-AVAILABLE</NOBR> BASIS. YOU AGREE THAT YOUR USE OF THE SITE SERVICES WILL BE AT YOUR SOLE RISK. TO THE FULLEST EXTENT PERMITTED BY LAW, WE DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED, IN CONNECTION WITH THE SITE AND YOUR USE THEREOF, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND <NOBR>NON-INFRINGEMENT.</NOBR> WE MAKE NO WARRANTIES OR REPRESENTATIONS ABOUT THE ACCURACY OR COMPLETENESS OF THE SITE’S CONTENT OR THE CONTENT OF ANY WEBSITES LINKED TO THIS SITE AND WE WILL ASSUME NO LIABILITY OR RESPONSIBILITY FOR ANY (1) ERRORS, MISTAKES, OR INACCURACIES OF CONTENT AND MATERIALS, (2) PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER, RESULTING FROM YOUR ACCESS TO AND USE OF THE SITE, (3) ANY UNAUTHORIZED ACCESS TO OR USE OF OUR SECURE SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION AND/OR FINANCIAL INFORMATION STORED THEREIN, (4) ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM THE SITE, (5) ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE WHICH MAY BE TRANSMITTED TO OR THROUGH THE SITE BY ANY THIRD PARTY, AND/OR (6) ANY ERRORS OR OMISSIONS IN ANY CONTENT AND MATERIALS OR FOR ANY LOSS OR DAMAGE OF ANY KIND INCURRED AS A RESULT OF THE USE OF ANY CONTENT POSTED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE VIA THE SITE. WE DO NOT WARRANT, ENDORSE, GUARANTEE, OR ASSUME RESPONSIBILITY FOR ANY PRODUCT OR SERVICE ADVERTISED OR OFFERED BY A THIRD PARTY THROUGH THE SITE, ANY HYPERLINKED WEBSITE, OR ANY WEBSITE OR MOBILE APPLICATION FEATURED IN ANY BANNER OR OTHER ADVERTISING, AND WE WILL NOT BE A PARTY TO OR IN ANY WAY BE RESPONSIBLE FOR MONITORING ANY TRANSACTION BETWEEN YOU AND ANY <NOBR>THIRD-PARTY</NOBR> PROVIDERS OF PRODUCTS OR SERVICES. AS WITH THE PURCHASE OF A PRODUCT OR SERVICE THROUGH ANY MEDIUM OR IN ANY ENVIRONMENT, YOU SHOULD USE YOUR BEST JUDGMENT AND EXERCISE CAUTION WHERE APPROPRIATE.</P>
    <P class="p43 ft10">​</P>
    <P class="p11 ft1">USER DATA</P>
    <P class="p39 ft8">We will maintain certain data that you transmit to the Site for the purpose of managing the Site, as well as data relating to your use of the Site. Although we perform regular routine backups of data, you are solely responsible for all data that you transmit or that relates to any activity you have undertaken using the Site. You agree that we shall have no liability to you for any loss or corruption of any such data, and you hereby waive any right of action against us arising from any such loss or corruption of such data.</P>
</DIV>
<DIV id="page_11">


    <P class="p42 ft18">ELECTRONIC COMMUNICATIONS, TRANSACTIONS, AND SIGNATURES</P>
    <P class="p35 ft8">Visiting the Site, sending us emails, and completing online forms constitute electronic communications. You consent to receive electronic communications, and you agree that all agreements, notices, disclosures, and other communications we provide to you electronically, via email and on the Site, satisfy any legal requirement that such communication be in writing.</P>
    <P class="p35 ft8">YOU HEREBY AGREE TO THE USE OF ELECTRONIC SIGNATURES, CONTRACTS, ORDERS, AND OTHER RECORDS, AND TO ELECTRONIC DELIVERY OF NOTICES, POLICIES, AND RECORDS OF TRANSACTIONS INITIATED OR COMPLETED BY US OR VIA THE SITE. You hereby waive any rights or requirements under any statutes, regulations, rules, ordinances, or other laws in any jurisdiction which require an original signature or delivery or retention of <NOBR>non-electronic</NOBR> records, or to payments or the granting of credits by any means other than electronic means.</P>
    <P class="p34 ft1">CALIFORNIA USERS AND RESIDENTS</P>
    <P class="p6 ft7">If any complaint with us is not satisfactorily resolved, you can contact the Complaint Assistance Unit of the Division of Consumer Services of the California Department of Consumer Affairs in writing at 1625 North Market Blvd., Suite N 112, Sacramento, California 95834 or by telephone at (800) <NOBR>952-5210</NOBR> or (916) <NOBR>445-1254.</NOBR></P>
    <P class="p14 ft1">MISCELLANEOUS</P>
    <P class="p6 ft8">These Terms of Use and any policies or operating rules posted by us on the Site constitute the entire agreement and understanding between you and us. Our failure to exercise or enforce any right or provision of these Terms of Use shall not operate as a waiver of such right or provision. These Terms of Use operate to the fullest extent permissible by law. We may assign any or all of our rights and obligations to others at any time. We shall not be responsible or liable for any loss, damage, delay, or failure to act caused by any cause beyond our reasonable control. If any provision or part of a provision of these Terms of Use is determined to be unlawful, void, or unenforceable, that provision or part of the provision is deemed severable from these Terms of Use and does not affect the validity and enforceability of any remaining provisions. There is no joint venture, partnership, employment or agency relationship created between you and us as a result of these Terms of Use or use of the Site. You agree that these Terms of Use will not be construed against us by virtue of having drafted them. You hereby waive any and all defenses you may have based on the electronic form of these Terms of Use and the lack of signing by the parties hereto to execute these Terms of Use.</P>
</DIV>
</BODY>
</HTML>
