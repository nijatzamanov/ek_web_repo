@extends('client.layout.authenticated')
@section('content')
    <!-- EK Marketing -->
    <div class="ek--marketing">
        <!-- EK Marketing Body -->
        <div class="ek--marketing--body bg--navy--700" >
            <!-- EK Marketing Body Head -->
            <div class="ek--marketing--body--head text--white ek--size--20">Marketing</div>
            <!-- End EK Marketing Body Head -->

            <!-- EK Marketing Inner -->
            <div class="ek--marketing--inner">
                <!-- EK Marketing Agency -->
                @foreach(Session::get("marketings") as $marketing)
                <div class="ek--marketing--agency bg--navy--600" th:each="marketing : ${session.marketing}">
                    <!-- EK Marketing Agency Name -->
                    <div class="ek--marketing--agency--name ek--size--18 text--white" th:text="${marketing.name}">{{ $marketing["name"] }}</div>
                    <!-- End EK Marketing Agency Name -->
                    <!-- EK Marketing Agency Feature -->
                    <div class="ek--marketing--agency--feature bg--navy--700">
                        <!-- EK Marketing Agency Feature Left -->
                        <div class="ek--marketing--agency--feature--left text--navy--100 ek--size--16">Design</div>
                        <!-- End EK Marketing Agency Feature Left -->
                        <!-- EK Marketing Agency Feature Right -->
                        <div class="ek--marketing--agency--feature--right">
                            <!-- EK Marketing Agency Feature Degree -->
                            <ul class="ek--marketing--agency--degree">
                                <!-- EK Marketing Agency Feature Degree Items -->
                                @for($i = 0; $i < $marketing["designLevel"]; $i++)
                                    <li class="ek--marketing--agency--degree--item item--active"></li>
                                @endfor

                                @for($i = 0; $i < 4-$marketing["designLevel"]; $i++)
                                    <li class="ek--marketing--agency--degree--item"></li>
                                @endfor
                              <!-- End EK Marketing Agency Feature Degree Items -->
                            </ul>
                            <!-- End EK Marketing Agency Feature Degree -->
                        </div>
                        <!-- End EK Marketing Agency Feature Right -->
                    </div>
                    <!-- End EK Marketing Agency Feature -->
                    <!-- EK Marketing Agency Feature -->
                    <div class="ek--marketing--agency--feature bg--navy--700">
                        <!-- EK Marketing Agency Feature Left -->
                        <div class="ek--marketing--agency--feature--left text--navy--100 ek--size--16">Creativity</div>
                        <!-- End EK Marketing Agency Feature Left -->
                        <!-- EK Marketing Agency Feature Right -->
                        <div class="ek--marketing--agency--feature--right">
                            <!-- EK Marketing Agency Feature Degree -->
                            <ul class="ek--marketing--agency--degree">
                                <!-- EK Marketing Agency Feature Degree Items -->
                                @for($i = 0; $i < $marketing["creativityLevel"]; $i++)
                                    <li class="ek--marketing--agency--degree--item item--active"></li>
                                @endfor

                                @for($i = 0; $i < 4-$marketing["creativityLevel"]; $i++)
                                    <li class="ek--marketing--agency--degree--item"></li>
                                @endfor
                                <!-- End EK Marketing Agency Feature Degree Items -->
                            </ul>
                            <!-- End EK Marketing Agency Feature Degree -->
                        </div>
                        <!-- End EK Marketing Agency Feature Right -->
                    </div>
                    <!-- End EK Marketing Agency Feature -->
                    <!-- EK Marketing Agency Feature -->
                    <div class="ek--marketing--agency--feature bg--navy--700">
                        <!-- EK Marketing Agency Feature Left -->
                        <div class="ek--marketing--agency--feature--left text--navy--100 ek--size--16">Management</div>
                        <!-- End EK Marketing Agency Feature Left -->
                        <!-- EK Marketing Agency Feature Right -->
                        <div class="ek--marketing--agency--feature--right">
                            <!-- EK Marketing Agency Feature Degree -->
                            <ul class="ek--marketing--agency--degree">
                                <!-- EK Marketing Agency Feature Degree Items -->

                                @for($i = 0; $i < $marketing["managementLevel"]; $i++)
                                    <li class="ek--marketing--agency--degree--item item--active"></li>
                                @endfor

                                @for($i = 0; $i < 4-$marketing["managementLevel"]; $i++)
                                    <li class="ek--marketing--agency--degree--item"></li>
                                @endfor

                                <!-- End EK Marketing Agency Feature Degree Items -->
                            </ul>
                            <!-- End EK Marketing Agency Feature Degree -->
                        </div>
                        <!-- End EK Marketing Agency Feature Right -->
                    </div>
                    <!-- End EK Marketing Agency Feature -->
                    <!-- EK Marketing Agency Price -->
                    <p class="ek--marketing--agency--price ek--size-16 text--navy--100">Weekly expense:
                        @if($marketing["weeklyCost"] < 1000)
                            <span class="ek--marketing--agency--price ek--size-16 text--navy--100">{{ $marketing["weeklyCost"] }}</span>
                        @endif

                        @if($marketing["weeklyCost"] >= 1000 && $marketing["weeklyCost"] < 1000000)
                            <span class="ek--marketing--agency--price ek--size-16 text--navy--100">{{ number_format($marketing["weeklyCost"]/1000,1) }}</span><span class="ek--marketing--agency--price ek--size-16 text--navy--100">K</span>
                        @endif

                        @if($marketing["weeklyCost"] >= 1000000 && $marketing["weeklyCost"] < 1000000000)
                            <span class="ek--marketing--agency--price ek--size-16 text--navy--100">{{ number_format($marketing["weeklyCost"]/1000000,1) }}</span><span class="ek--marketing--agency--price ek--size-16 text--navy--100">M</span>
                        @endif

                        @if($marketing["weeklyCost"] >= 1000000000)
                            <span class="ek--marketing--agency--price ek--size-16 text--navy--100">{{ number_format($marketing["weeklyCost"]/1000000000,1) }}</span><span class="ek--marketing--agency--price ek--size-16 text--navy--100">B</span>
                        @endif
                    </p>
                    <!-- End EK Marketing Agency Price -->
                    <!-- EK Marketing Agency Button -->
                    <div class="ek--marketing--agency--button">
                        <!-- EK Marketing Agency Choose -->
                        <!--<a class="ek&#45;&#45;marketing&#45;&#45;agency&#45;&#45;choose ek&#45;&#45;size&#45;&#45;16&#45;&#45;500 text&#45;&#45;white bg&#45;&#45;blue" href="#">Choose</a>-->

                        @if( $marketing["selected"] == true )
                            <button class="ek--marketing--agency--choose ek--size--16--500 text--white bg--blue choose" data-id="{{ $marketing["marketingId"] }}" id="cancel">Cancel</button>
                        @endif
                        @if( $marketing["selected"] == false && Session::get("marketingFalseSize") == 3 )
                            <button class="ek--marketing--agency--choose ek--size--16--500 text--white bg--navy--500 btn-is-disabled" data-id="{{ $marketing["marketingId"] }}" id="choose">Choose</button>
                        @endif

                        @if( $marketing["selected"] == false && Session::get("marketingFalseSize") != 3 )
                            <button class="ek--marketing--agency--choose ek--size--16--500 text--white bg--blue" data-id="{{ $marketing["marketingId"] }}" id="choose">Choose</button>
                        @endif

                        <!-- End EK Marketing Agency Choose -->
                    </div>
                    <!-- End EK Marketing Agency Button -->
                </div>
                @endforeach
                <!-- End EK Marketing Agency -->


            </div>
            <!-- End EK Marketing Inner -->
        </div>
        <!-- End EK Marketing Body -->
    </div>
@endsection
@section("javascript")
    <script src="{{url("static/js/finance.js")}}"></script>

    <script src="{{url("static/js/marketing.js")}}"></script>
@endsection
