@extends('client.layout.authenticated')
@section('content')
    <!-- EK Body Coins -->
    <section class="ek--coins">
        <!-- EK Body Coins Body -->
        <div class="ek--coins--body">
            <!-- EK Body Coins Body Head -->
            <div class="ek--coins--body--head bg--navy--700">
                <h3 class="text--white ek--size--20">EK Coins</h3>
            </div>
            <!-- End EK Body Coins Body Head -->

            <!-- EK Body Coins Body Tabs -->
            <div class="ek--coins--tab bg--navy--700">
                <!-- EK Body Coins Body Tabs Buttons -->
                <div class="ek--coins--tab--head">
                    <button id="ek--coins--tab--income" class="ek--coins--tab--button button--active">This season income</button>
                    <button id="ek--coins--tab--expenses" class="ek--coins--tab--button">This season expenses</button>
                </div>
                <!-- End EK Body Coins Body Tabs Buttons -->
                <!-- Ek Coins Body Tab Content -->
                <div class="ek--coins--tab--content">
                    <!-- Ek Coins Body Tab InCome -->
                    <div class="ek--coins--tab--income ek--coins--tab--table">
                            <div class="ek--coins--tab--list">


                            <!-- Ek Coins Body Tab Inner -->
                                @foreach( Session::get("coins")["coinsIncome"] as $key=>$value )
                                    <div class="ek--coins--tab--inner bg--navy--600">
                                        <!-- Ek Coins Body Tab Inner Player -->
                                        <div class="ek--coins--tab--inner--player text--white ek--size--16"><span class="ek--coins--tab--inner--player text--white ek--size--16">{{ ucwords(implode(' ', preg_split('/(?=[A-Z])/', $key))) }}</span></div>
                                        <!-- End Ek Coins Body Tab Inner Player -->
                                        <!-- Ek Coins Body Tab Inner Amount -->
                                        <div class="ek--coins--tab--inner--amount text--white ek--size--16">
                                                <div>
                                                    @if($value < 1000)
                                                        <span class="ek--coins--tab--inner--amount text--white ek--size--16"><svg class="icon icon-ek--coins fill--yellow" style="height:20px;width:30px;padding-top:5px"><use xlink:href="{{ url("static/img/icons.svg#icon-ek--coins") }}"></use></svg><span>{{$value }}</span></span>
                                                    @endif

                                                    @if($value >= 1000 && $value < 1000000)
                                                        <span class="ek--coins--tab--inner--amount text--white ek--size--16"><svg class="icon icon-ek--coins fill--yellow" style="height:20px;width:30px;padding-top:5px"><use xlink:href="{{ url("static/img/icons.svg#icon-ek--coins") }}"></use></svg><span>{{ number_format($value/1000,1) }}</span></span><span class="ek--coins--tab--inner--amount text--white ek--size--16">K</span>
                                                    @endif

                                                    @if($value >= 1000000 && $value < 1000000000)
                                                        <span class="ek--coins--tab--inner--amount text--white ek--size--16"><svg class="icon icon-ek--coins fill--yellow" style="height:20px;width:30px;padding-top:5px"><use xlink:href="{{ url("static/img/icons.svg#icon-ek--coins") }}"></use></svg><span>{{ number_format($value/1000000,1) }}</span></span><span class="ek--coins--tab--inner--amount text--white ek--size--16">M</span>
                                                    @endif

                                                    @if($value >= 1000000000)
                                                        <span class="ek--coins--tab--inner--amount text--white ek--size--16"><svg class="icon icon-ek--coins fill--yellow" style="height:20px;width:30px;padding-top:5px"><use xlink:href="{{ url("static/img/icons.svg#icon-ek--coins") }}"></use></svg><span>{{ number_format($value/1000000000,1) }}</span></span><span class="ek--coins--tab--inner--amount text--white ek--size--16">B</span>
                                                    @endif
                                                </div>
                                        </div>
                                        <!-- End Ek Coins Body Tab Inner Amount -->
                                    </div>
                                @endforeach
                            <!-- End Ek Coins Body Tab Inner -->




                        </div>
                        <!-- End Ek Coins Body Tab List -->
                    </div>
                    <!-- End Ek Coins Body Tab InCome -->
                    <!-- Ek Coins Body Tab Expenses -->
                    <div class="ek--coins--tab--expenses ek--coins--tab--table">
                                <div class="ek--coins--tab--list">


                                <!-- Ek Coins Body Tab Inner -->
                                @foreach( Session::get("coins")["coinsExpence"] as $key=>$value )
                                        <div class="ek--coins--tab--inner bg--navy--600">
                                            <!-- Ek Coins Body Tab Inner Player -->
                                            <div class="ek--coins--tab--inner--player text--white ek--size--16"><span class="ek--coins--tab--inner--player text--white ek--size--16">{{ ucwords(implode(' ', preg_split('/(?=[A-Z])/', $key))) }}</span></div>
                                            <!-- End Ek Coins Body Tab Inner Player -->
                                            <!-- Ek Coins Body Tab Inner Amount -->
                                            <div class="ek--coins--tab--inner--amount text--white ek--size--16">
                                                @if($value < 0)
                                                    <div>
{{--                                                        <span class="ek--coins--tab--inner--amount text--white ek--size--16">$</span>--}}
                                                    @if($value < 1000)
                                                            <span class="ek--coins--tab--inner--amount text--white ek--size--16"><svg class="icon icon-ek--coins fill--yellow" style="height:20px;width:30px;padding-top:5px"><use xlink:href="{{ url("static/img/icons.svg#icon-ek--coins") }}"></use></svg><span>{{ -$value }}</span></span>
                                                        @endif

                                                        @if($value >= 1000 && $value < 1000000)
                                                            <span class="ek--coins--tab--inner--amount text--white ek--size--16"><svg class="icon icon-ek--coins fill--yellow" style="height:20px;width:30px;padding-top:5px"><use xlink:href="{{ url("static/img/icons.svg#icon-ek--coins") }}"></use></svg><span>{{ number_format(-$value/1000,1) }}</span></span><span class="ek--coins--tab--inner--amount text--white ek--size--16">K</span>
                                                        @endif

                                                        @if($value >= 1000000 && $value < 1000000000)
                                                            <span class="ek--coins--tab--inner--amount text--white ek--size--16"><svg class="icon icon-ek--coins fill--yellow" style="height:20px;width:30px;padding-top:5px"><use xlink:href="{{ url("static/img/icons.svg#icon-ek--coins") }}"></use></svg><span>{{ number_format(-$value/1000000,1) }}</span></span><span class="ek--coins--tab--inner--amount text--white ek--size--16">M</span>
                                                        @endif

                                                        @if($value >= 1000000000)
                                                            <span class="ek--coins--tab--inner--amount text--white ek--size--16"><svg class="icon icon-ek--coins fill--yellow" style="height:20px;width:30px;padding-top:5px"><use xlink:href="{{ url("static/img/icons.svg#icon-ek--coins") }}"></use></svg><span>{{ number_format(-$value/1000000000,1) }}</span></span><span class="ek--coins--tab--inner--amount text--white ek--size--16">B</span>
                                                        @endif
                                                    </div>
                                                @else
                                                    <div>
                                                        @if($value < 1000)
                                                            <span class="ek--coins--tab--inner--amount text--white ek--size--16"><svg class="icon icon-ek--coins fill--yellow" style="height:20px;width:30px;padding-top:5px"><use xlink:href="{{ url("static/img/icons.svg#icon-ek--coins") }}"></use></svg><span>{{ $value }}</span></span>
                                                        @endif

                                                        @if($value >= 1000 && $value < 1000000)
                                                            <span class="ek--coins--tab--inner--amount text--white ek--size--16"><svg class="icon icon-ek--coins fill--yellow" style="height:20px;width:30px;padding-top:5px"><use xlink:href="{{ url("static/img/icons.svg#icon-ek--coins") }}"></use></svg><span>{{ number_format($value/1000,1) }}</span></span><span class="ek--coins--tab--inner--amount text--white ek--size--16">K</span>
                                                        @endif

                                                        @if($value >= 1000000 && $value < 1000000000)
                                                            <span class="ek--coins--tab--inner--amount text--white ek--size--16"><svg class="icon icon-ek--coins fill--yellow" style="height:20px;width:30px;padding-top:5px"><use xlink:href="{{ url("static/img/icons.svg#icon-ek--coins") }}"></use></svg><span>{{ number_format($value/1000000,1) }}</span></span><span class="ek--coins--tab--inner--amount text--white ek--size--16">M</span>
                                                        @endif

                                                        @if($value >= 1000000000)
                                                            <span class="ek--coins--tab--inner--amount text--white ek--size--16"><svg class="icon icon-ek--coins fill--yellow" style="height:20px;width:30px;padding-top:5px"><use xlink:href="{{ url("static/img/icons.svg#icon-ek--coins") }}"></use></svg><span>{{ number_format($value/1000000000,1) }}</span></span><span class="ek--coins--tab--inner--amount text--white ek--size--16">B</span>
                                                        @endif
                                                    </div>
                                                @endif
                                            </div>
                                            <!-- End Ek Coins Body Tab Inner Amount -->
                                        </div>
                                @endforeach
                                <!-- End Ek Coins Body Tab Inner -->


                            </div>
                            <!-- End Ek Coins Body Tab List -->
                    </div>
                    <!-- End Ek Coins Body Tab Expenses -->
                </div>
                <!-- End Ek Coins Body Tab Content -->
            </div>
            <!-- End EK Body Coins Body Tabs -->
        </div>
        <!-- End EK Body Coins Body -->
    </section>
@endsection
