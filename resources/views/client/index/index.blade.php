@extends('client.layout.layout')
@section('content')
    <!-- =========================
    Section - Header
============================== -->
    <header class="ek--landing ek--landing_mobile bg--navy--1000">
        <section class="container">
            <!-- EK Landing Head -->
            <div class="ek--landing--head">
                <!-- EK Landing Head Left -->
                <div class="ek--landing--left">
                    <!-- EK Landing Head Logo -->
                    <div class="ek--landing--logo">
                        <a href="javascript:void(0)">
                            <img class="ek--landing--head--image" src="{{ url('static/img/Landing/ek-logo-white.png') }}"
                                 alt="Ek--logo" title="Eleven Kings Logo">
                        </a>
                    </div>
                    <!-- End EK Landing Head Logo -->
                </div>
                <!-- End EK Landing Head Left -->

                <!-- EK Landing Head Right -->
                <div class="ek--landing--right">
                    <!-- EK Landing Head Language -->
                    <div class="ek--landing--language button--navy--700">

                        <div class="ek--landing--language--nav">
                            <!-- EK landing Head Language Image -->
                            @if(!Session::get("language"))
                                <img class="ek--landing--language--image" src="{{ url('static/img/Other/english.svg') }}"
                                     alt="english">
                                <p class="ek--landing--language--country text--navy--200">English</p>
                            @elseif(Session::get("language") == 'en')
                                <img class="ek--landing--language--image" src="{{ url('static/img/Other/english.svg') }}"
                                     alt="english">
                                <p class="ek--landing--language--country text--navy--200">English</p>
                            @elseif(Session::get("language") == 'az')
                                <img class="ek--landing--language--image" src="{{ url('static/img/Other/ek--azerbaijan.svg') }}"
                                     alt="english">
                                <p class="ek--landing--language--country text--navy--200">Azərbaycanca</p>
                            @elseif(Session::get("language") == 'tr')
                                <img class="ek--landing--language--image" src="{{ url('static/img/Other/ek--turkey.svg') }}"
                                     alt="english">
                                <p class="ek--landing--language--country text--navy--200">Türkçe</p>
                            @elseif(Session::get("language") == 'ru')
                                <img class="ek--landing--language--image" src="{{ url('static/img/Other/ek--russian.svg') }}"
                                     alt="english">
                                <p class="ek--landing--language--country text--navy--200">Pусский</p>
                            @elseif(Session::get("language") == 'pt')
                                <img class="ek--landing--language--image" src="{{ url('static/img/Other/ek--portegues.svg') }}"
                                     alt="english">
                                <p class="ek--landing--language--country text--navy--200">Portuguese</p>

                            @elseif(Session::get("language") == 'es')
                                <img class="ek--landing--language--image" src="{{ url('static/img/Other/ek--espanyol.svg') }}"
                                     alt="english">
                                <p class="ek--landing--language--country text--navy--200">Español</p>

                            @endif

                            <!-- End EK landing Head Language Arrow Up and Arrow Down -->
                        </div>
                        <!-- EK Landing Head Language Content -->
                        <div class="ek--landing--language--content bg--navy--700">

                            <!-- EK Landing Head Language Content Country Language -->
                            <a class="ek--landing--language--link text--navy--200" href="{{route("change_language", 'en')}}"><img
                                    src="{{ url('static/img/Other/english.svg') }}"
                                    alt="english"><span>English</span></a>

                            <a class="ek--landing--language--link text--navy--200" href="{{route("change_language", 'az')}}"><img
                                    src="{{ url('static/img/Other/ek--azerbaijan.svg') }}" alt="Azerbaijan"><span>Azərbaycanca</span></a>

                            <a class="ek--landing--language--link text--navy--200" href="{{route("change_language", 'tr')}}"><img
                                    src="{{ url('static/img/Other/ek--turkey.svg') }}" alt="turkey"><span>Türkçe</span></a>

                            <a class="ek--landing--language--link text--navy--200" href="{{route("change_language", 'ru')}}"><img
                                    src="{{ url('static/img/Other/ek--russian.svg') }}"
                                    alt="russian"><span>Pусский</span></a>

                            <a class="ek--landing--language--link text--navy--200" href="{{route("change_language", 'pt')}}"><img
                                    src="{{ url('static/img/Other/ek--portegues.svg') }}" alt="portuguese"><span>Portuguese</span></a>

                            <a class="ek--landing--language--link text--navy--200" href="{{route("change_language", 'es')}}"><img
                                    src="{{ url('static/img/Other/ek--espanyol.svg') }}"
                                    alt="español"><span>Español</span></a>

                            <!-- EK Landing Head Language Content Country Language -->
                        </div>
                        <!-- End EK Landing Head Language Content -->
                    </div>
                    <!-- End EK Landing Head Language -->

                    <!-- EK Landing Head Sign In -->
                    <div class="ek--landing--sign button--green">
                        <a class="text--navy--900" href="{{ route('login', ['lang' => app()->getLocale()]) }}">{{ __('translation.landing_signin') }}</a>
                    </div>
                    <!-- End EK Landing Head Sign In -->
                </div>
                <!-- End EK Landing Head Right -->

            </div>
            <!--End EK Landing Head -->
            <!-- EK Landing Body -->
            <div class="ek--landing--body">
                <div class="ek--landing--body--inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 ps-0">
                                <div class="row">
                                    <div class="col-md-6">
                                        <!-- EK Landing Body Title -->
                                        <h1 class="ek--landing--body--title text--white">Eleven Kings</h1>
                                        <span class="ek--landing--body--span">{{ __('translation.landing_cover_span') }}</span>
                                        <!-- End EK Landing Body Title -->
                                        <!-- EK Landing Body Paragraph -->
                                        <p class="ek--landing--body--paragraph text--navy--200">{{ __('translation.landing_cover_paragraph') }}</p>
                                        <!-- End EK Landing Body Paragraph -->
                                        <!-- EK Landing Body Play Now -->
                                        <a class="ek--landing--body--play button--green text--navy--900"  href="{{ route('overview') }}"><img
                                                class="ek--landing--body--play--image" src="/static/img/Other/forma.svg" alt="forma"> {{ __('translation.landing_cover_play_button') }} <img class="ek--landing--body--play--arrow" src="/static/img/Other/arrow--play.svg"
                                                     alt="arrow--play"></a>
                                        <!-- End EK Landing Body Play Now -->
                                        <!-- EK Landing Body Stories -->
                                        <div class="ek--landing--body--stories">
                                            <!-- EK Landing Body Google Play Store -->
                                            <a href="https://play.google.com/store/apps/details?id=com.elevenkings.football">
                                                <img class="ek--landing--body--stories--google" src="/static/img/Other/google--strore@2x.png"
                                                     alt="google--strore" target="_blank">
                                            </a>
                                            <!-- End EK Landing Body Google Play Store -->
                                            <!-- EK Landing Body App Store -->
                                        {{--<a href="https://apps.apple.com/us/app/eleven-kings/id1485862939">
                                            <img class="ek--landing--body--stories--apple" src="/static/img/Other/app--store@2x.png"
                                                alt="app--store" target="_blank">
                                        </a>--}}
                                        <!-- End EK Landing Body App Store -->
                                        </div>
                                        <!-- End EK Landing Body Stories -->
                                    </div>
                                    {{--<div class="col-md-6">
                                        <img src="{{ url('static/img/Landing/cover.png') }}" alt="">
                                    </div>--}}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- End EK Landing Body -->
        </section>
        <!-- EK Landing Body Scroll More -->
        <section class="ek--landing--body--scroll">
            <!-- EK Landing Body Scroll More Button -->
            <div class="ek--landing--body--scroll--button button--navy--700">
                <img src="/static/img/Other/arrow--scroll.svg" alt="">
            </div>
            <!-- End EK Landing Body Scroll More Button -->
            <!-- EK Landing Body Scroll Title -->
            <p class="ek--landing--body--scroll--title text--navy--500">{{ __('translation.landing_cover_scroll_down') }}</p>
            <!-- End EK Landing Body Scroll Title -->
        </section>
        <!-- End EK Landing Body Scroll More -->

    </header>
    <!-- =========================
        End Section - Header
    ============================== -->

    <!-- =========================
        Section - Main
    ============================== -->
    <main class="ek--main">
        <!-- EK Landing Main Feature -->
        <section id="ek--feature" class="ek--main--feature bg--white">
            <div class="container">
                <!-- EK Landing Main Inner -->
                <div class="ek--main--feature--inner">
                    <!-- EK Landing Main Inner Feature Divs -->
                    <div class="ek--main--feature--win">
                        <!-- EK Landing Main Inner Feature Divs -->
                        <div class="ek--main--feature--win--image bg--yellow"><img
                                src="/static/img/Other/ek--t--shirt.svg" alt="ek--t--shirt"></div>
                        <!-- End EK Landing Main Inner Feature Divs -->
                        <!-- EK Landing Main Inner Feature Title -->
                        <h2 class="ek--main--feature--win--title text--navy--900">{{ __('translation.landing_main_feature_bg_yellow_title') }}</h2>
                        <!-- End EK Landing Main Inner Feature Title -->
                        <!-- EK Landing Main Inner Feature Paragraph -->
                        <p class="ek--main--feature--win--paragraph text--navy--500">{{ __('translation.landing_main_feature_bg_yellow_paragraph') }}</p>
                        <!-- End EK Landing Main Inner Feature Paragraph -->
                    </div>
                    <div class="ek--main--feature--win">
                        <!-- EK Landing Main Inner Feature Divs -->
                        <div class="ek--main--feature--win--image bg--blue"><img src="/static/img/Other/ek--shooes.svg"
                                                                                 alt="ek--shooes"></div>
                        <!-- End EK Landing Main Inner Feature Divs -->
                        <!-- EK Landing Main Inner Feature Title -->
                        <h2 class="ek--main--feature--win--title text--navy--900">{{ __('translation.landing_main_feature_bg_blue_title') }}</h2>
                        <!-- End EK Landing Main Inner Feature Title -->
                        <!-- EK Landing Main Inner Feature Paragraph -->
                        <p class="ek--main--feature--win--paragraph text--navy--500">{{ __('translation.landing_main_feature_bg_blue_paragraph') }}</p>
                        <!-- End EK Landing Main Inner Feature Paragraph -->
                    </div>
                    <div class="ek--main--feature--win">
                        <!-- EK Landing Main Inner Feature Divs -->
                        <div class="ek--main--feature--win--image bg--green--dark--two"><img
                                src="/static/img/Other/ek--medal.svg" alt="ek--medal"></div>
                        <!-- End EK Landing Main Inner Feature Divs -->
                        <!-- EK Landing Main Inner Feature Title -->
                        <h2 class="ek--main--feature--win--title text--navy--900">{{ __('translation.landing_main_feature_bg_green_title') }}</h2>
                        <!-- End EK Landing Main Inner Feature Title -->
                        <!-- EK Landing Main Inner Feature Paragraph -->
                        <p class="ek--main--feature--win--paragraph text--navy--500">{{ __('translation.landing_main_feature_bg_green_paragraph') }}</p>
                        <!-- End EK Landing Main Inner Feature Paragraph -->
                    </div>
                    <!-- End EK Landing Main Inner Feature Divs -->
                </div>
                <!-- End EK Landing Main Inner -->
            </div>
        </section>
        <!-- EK Landing Main Feature -->

        <!-- EK Landing Main Game Features -->
        <section class="ek--main--game bg--white--two">
            <div class="container">
                <!-- EK Landing Main Game Features Inner -->
                <div class="ek--main--game--inner">
                    <!-- EK Landing Main Game Inner Feature -->
                    <div class="ek--main--game--feature">
                        <div class="ek--badge bg--yellow text--navy--900">{{ __('translation.landing_main_game_feature_badge') }}</div>
                        <h3 class="ek--main--game--feature--title text--navy--900">{{ __('translation.landing_main_game_feature_title') }}</h3>
                        <p class="ek--main--game--feature--paragraph text--navy--500">{{ __('translation.landing_main_game_feature_paragraph') }}</p>
                    </div>
                    <!-- End EK Landing Main Game Inner Feature -->
                    <!-- EK Landing Main Game Tabs -->
                    <div class="ek--main--game--feature--row">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                                <!-- EK Landing Main Game Tabs Navbar -->
                                <div class="ek--main--game--feature--navbar">
                                    <div class="list-group" id="list-tab" role="tablist">
                                        <a class="list-group-item list-group-item-action text--navy--500 show"
                                           aria-controls="list-home" aria-selected="false">
                                            <img src="/static/img/Navbar/ek--best--club.svg" alt="ek--best--club"> {{ __('translation.landing_main_game_feature_list_item_1') }}</a>
                                        <a class="list-group-item list-group-item-action text--navy--500"
                                           aria-controls="list-profile" aria-selected="false">
                                            <img src="/static/img/Navbar/ek--daily--training.svg" alt="ek--daily--training">{{ __('translation.landing_main_game_feature_list_item_2') }}</a>
                                        <a class="list-group-item list-group-item-action text--navy--500"
                                           aria-controls="list-messages" aria-selected="false">
                                            <img src="/static/img/Navbar/ek--transfer.svg" alt="ek--transfer"> {{ __('translation.landing_main_game_feature_list_item_3') }}</a>
                                        <a class="list-group-item list-group-item-action text--navy--500"
                                           aria-controls="list-settings" aria-selected="true">
                                            <img src="/static/img/Navbar/ek--statistic.svg" alt="ek--statistic">{{ __('translation.landing_main_game_feature_list_item_4') }}</a>
                                        <a class="list-group-item list-group-item-action text--navy--500"
                                           aria-controls="list-settings" aria-selected="true">
                                            <img src="/static/img/Navbar/ek---league.svg" alt="ek---league"> {{ __('translation.landing_main_game_feature_list_item_5') }}</a>
                                    </div>
                                    <!-- EK Landing Main Game Play Online -->
                                    <div class="ek--main--game--feature--play">
                                        <p class="ek--main--game--feature--start text--navy--500">{{ __('translation.landing_main_game_feature_start') }}</p>
                                        <a class="ek--main--game--feature--online button--green text--navy--900"
                                            href="{{ route('overview',['lang' =>app()->getLocale()]) }}">{{ __('translation.landing_main_game_feature_start_button') }}</a>
                                    </div>
                                    <!-- End EK Landing Main Game Play Online -->
                                </div>
                                <!-- End EK Landing Main Game Tabs Navbar -->
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                                <!-- EK Landing Main Game Tabs Content -->
                                <div class="ek--main--game--feature--tabs">
                                    <div class="ek--main--game--feature--tools bg--white">
                                        <div class="ek--main--game--feature--tools--divs bg--red"></div>
                                        <div class="ek--main--game--feature--tools--divs bg--yellow"></div>
                                        <div class="ek--main--game--feature--tools--divs bg--green--dark--two"></div>
                                    </div>
                                    <div class="tab-content" id="nav-tabContent">
                                        <div class="tab-pane fade  active show" id="list-tactics" role="tabpanel"
                                             aria-labelledby="list-daily-list">
                                            <img src="/static/img/ek--tabs--image.png" alt="ek--tab--image">
                                        </div>
                                        <div class="tab-pane fade" id="list-daily" role="tabpanel"
                                             aria-labelledby="list-daily-list">
                                            <img src="/static/img/ek--tabs--image.png" alt="ek--tab--image">
                                        </div>
                                        <div class="tab-pane fade" id="list-transfer" role="tabpanel"
                                             aria-labelledby="list-transfer-list">
                                            <img src="/static/img/ek--tabs--image.png" alt="ek--tab--image">
                                        </div>
                                        <div class="tab-pane fade" id="list-statistic" role="tabpanel"
                                             aria-labelledby="list-statistic-list">
                                            <img src="/static/img/ek--tabs--image.png" alt="ek--tab--image">
                                        </div>
                                        <div class="tab-pane fade" id="list-league" role="tabpanel"
                                             aria-labelledby="list-league-list">
                                            <img src="/static/img/ek--tabs--image.png" alt="ek--tab--image">
                                        </div>
                                    </div>
                                </div>
                                <!-- End EK Landing Main Game Tabs Content -->
                            </div>
                        </div>
                    </div>
                    <!-- End EK landing Main Game Tabs -->
                </div>
                <!-- End EK Landing Main Game Features Inner -->
            </div>
        </section>
        <!-- End EK Landing Main Game Features -->

        <!-- EK Landing Main Mobile App  -->
        <section class="ek--main--mobile bg--gray">
            <div class="container">
                <!-- EK Landing Main Mobile Container -->
                <div class="ek--main--mobile--container">
                    <!-- EK Landing Main Mobile Container Inner -->
                    <div class="ek--main--mobile--container--inner">
                        <!-- EK Landing Main Mobile Device -->
                        <div class="ek--main--mobile--device">
                            <!-- EK landing Main Mobile Device Inner Content Image -->
                            <img class="ek--main--mobile--device--image" src="/static/img/Landing/ek--mobile-1.png"
                                 alt="ek--mobile--screen">
                            <!-- End EK landing Main Mobile Device Inner Content Image -->
                            <!-- EK Landing Main Mobile Device Head -->
                            <div class="ek--main--mobile--device--head bg--white">
                                <!-- EK Landing Main Mobile Device Head MicroPhone -->
                                <div class="ek--main--mobile--device--microphone bg--gray--two"></div>
                                <!-- End EK Landing Main Mobile Device Head MicroPhone -->
                                <!-- EK Landing Main Mobile Device Head Camera -->
                                <div class="ek--main--mobile--device--camera bg--gray--two"></div>
                                <!-- End EK Landing Main Mobile Device Head Camera -->
                            </div>
                            <!-- End EK Landing Main Mobile Device Head -->
                            <!-- EK Landing Main Mobile Device T-shirt Div  -->
                            <div class="ek--main--mobile--device--shirt bg--white">
                                <!-- EK Landing Main Mobile Device Uniform  -->
                                <div class="ek--main--mobile--device--uniform bg--yellow">
                                    <img src="/static/img/Other/ek--t--shirt.svg" alt="ek--t--shirt">
                                </div>
                                <!-- End EK Landing Main Mobile Device T-shirt Uniform  -->
                                <div class="ek--main--mobile--device--title bg--gray--light"></div>
                                <div class="ek--main--mobile--device--paragraph bg--gray--light"></div>
                            </div>
                            <!-- End EK Landing Main Mobile Device T-shirt Div  -->
                            <!-- EK Landing Main Mobile Device Shoes Div  -->
                            <div class="ek--main--mobile--device--shoes bg--white">
                                <!-- EK Landing Main Mobile Device Uniform  -->
                                <div class="ek--main--mobile--device--uniform bg--blue">
                                    <img src="/static/img/Other/ek--shooes.svg" alt="ek--shooes">
                                </div>
                                <!-- End EK Landing Main Mobile Device T-shirt Uniform  -->
                                <div class="ek--main--mobile--device--title bg--gray--light"></div>
                                <div class="ek--main--mobile--device--paragraph bg--gray--light"></div>
                            </div>
                            <!-- End EK Landing Main Mobile Device Shoes Div  -->
                            <!-- EK Landing Main Mobile Device Power -->
                            <div class="ek--main--mobile--device--power bg--white"></div>
                            <!-- End EK Landing Main Mobile Device Power -->
                            <!-- EK Landing Main Mobile Device Volume Up -->
                            <div class="ek--main--mobile--device--up bg--white"></div>
                            <!-- End EK Landing Main Mobile Device Volume Up -->
                            <!-- EK Landing Main Mobile Device Volume Up -->
                            <div class="ek--main--mobile--device--down bg--white"></div>
                            <!-- End EK Landing Main Mobile Device Volume Up -->
                        </div>
                        <!-- End EK Landing Main Mobile Device -->
                    </div>
                    <!-- End EK lANDING Main Mobile Container -->
                    <!-- EK Landing Main Mobile Available -->
                    <div class="ek--main--mobile--available">
                        <!-- EK Landing Main Mobile Available Badge -->
                        <div class="ek--badge bg--blue text--white">{{ __('translation.landing_main_mobile_badge') }}</div>
                        <!-- End EK Landing Main Mobile Available Badge -->
                        <!-- EK Landing Main Mobile Available Title -->
                        <h3 class="ek--main--mobile--available--title text--navy--900">{{ __('translation.landing_main_mobile_title') }}</h3>
                        <!-- End EK Landing Main Mobile Available Title -->
                        <!-- EK Landing Main Mobile Available Paragraph -->
                        <p class="ek--main--mobile--available--paragraph text--navy--500">{{ __('translation.landing_main_mobile_paragraph') }}</p>
                        <!-- End EK Landing Main Mobile Available Paragraph -->
                        <!-- EK Landing Main Mobile Available Stories -->
                        <div class="ek--main--mobile--available--stories">
                            <!-- EK Landing Main Mobile Available Google Play -->
                            <a href="https://play.google.com/store/apps/details?id=com.elevenkings.football"><img
                                    class="ek--main--mobile--available--google" src="/static/img/Other/google--strore@2x.png"
                                    alt="google--strore" target="_blank"></a>
                            <!-- End EK Landing Main Mobile Available Google Play -->
                            <!-- EK Landing Main Mobile Available App Store -->
{{--                            <a href="https://apps.apple.com/us/app/eleven-kings/id1485862939"><img--}}
{{--                                    class="ek--main--mobile--available--apple" src="/static/img/Other/app--store@2x.png"--}}
{{--                                    alt="app--store" target="_blank"></a>--}}
                            <!-- End EK Landing Main Mobile Available App Store -->
                        </div>
                        <!-- End EK Landing Main Mobile Available Stories -->
                    </div>
                    <!-- End EK Landing Main Mobile Available -->
                </div>
                <!-- End EK Landing Main Mobile Container -->
            </div>
        </section>
        <!-- End EK Landing Main Mobile App  -->

        <!-- EK Landing Main World Ranking  -->
        {{--<section class="ek--main--ranking bg--navy--900">
            <div class="container">
                <!-- EK Landing Main Ranking Container -->
                <div class="ek--main--ranking--container">
                    <!-- EK Landing Main Ranking Head -->
                    <div class="ek--main--ranking--head">
                        <div class="ek--badge bg--green--dark--two text--white">RANKING</div>
                        <!-- EK Landing Main Ranking Head Title -->
                        <h3 class="ek--main--ranking--head--title text--white">EK World Ranking</h3>
                        <!-- End EK Landing Main Ranking Head Title -->
                        <!-- EK Landing Main Ranking Head Paragraph -->
                        <p class="ek--main--ranking--head--paragraph text--navy--200">See who are the best in EK
                            Game</p>
                        <!-- End EK Landing Main Ranking Head Paragraph -->
                    </div>
                    <!-- End EK Landing Main Ranking Head -->
                    <!-- EK Landing Main Ranking Body Win -->
                    <div class="ek--main--ranking--body">
                        <!-- EK Landing Main Ranking Body Win Second -->
                        <div class="ek--main--ranking--win">
                            <!-- EK Landing Main Ranking Body Win Logo -->
                            <div class="ek--main--ranking--win--logo">
                                <img src="/static/img/ek--win--second.png" alt="ek--win--second">
                            </div>
                            <!-- End EK Landing Main Ranking Body Win Logo -->
                            <!-- EK Landing Main Ranking Body Win Club -->
                            <h3 class="ek--main--ranking--win--club text--navy--200">Arsenal Club</h3>
                            <!-- End EK Landing Main Ranking Body Win Club -->
                            <!-- EK Landing Main Ranking Body Win User -->
                            <div class="ek--main--ranking--win--user">
                                <!-- EK Landing Main Ranking Body Win User Image -->
                                <div class="ek--main--ranking--win--user--image ek--width--36">
                                    <img src="/static/img/ek--user.png" alt="ek--user">
                                </div>
                                <!-- End EK Landing Main Ranking Body Win User Image -->
                                <!-- EK Landing Main Ranking Body Win User Name -->
                                <h3 class="ek--main--ranking--win--user--name text--navy--200">Samad Rzayev</h3>
                                <!-- End EK Landing Main Ranking Body Win User Name -->
                            </div>
                            <!-- End EK Landing Main Ranking Body Win User -->
                        </div>
                        <!-- End EK Landing Main Ranking Body Win Second -->
                        <!-- EK Landing Main Ranking Body Win First -->
                        <div class="ek--main--ranking--win">
                            <!-- EK Landing Main Ranking Body Win Logo -->
                            <div class="ek--main--ranking--win--logo">
                                <img src="/static/img/ek--win--first.png" alt="ek--win--first">
                            </div>
                            <!-- End EK Landing Main Ranking Body Win Logo -->
                            <!-- EK Landing Main Ranking Body Win Club -->
                            <h3 class="ek--main--ranking--win--club text--white">Chelsea Club</h3>
                            <!-- End EK Landing Main Ranking Body Win Club -->
                            <!-- EK Landing Main Ranking Body Win User -->
                            <div class="ek--main--ranking--win--user">
                                <!-- EK Landing Main Ranking Body Win User Image -->
                                <div class="ek--main--ranking--win--user--image ek--width--44">
                                    <img src="/static/img/ek--user--2.png" alt="ek--user">
                                </div>
                                <!-- End EK Landing Main Ranking Body Win User Image -->
                                <!-- EK Landing Main Ranking Body Win User Name -->
                                <h3 class="ek--main--ranking--win--user--name text--white">ItsHashimovk</h3>
                                <!-- End EK Landing Main Ranking Body Win User Name -->
                            </div>
                            <!-- End EK Landing Main Ranking Body Win User -->
                        </div>
                        <!-- End EK Landing Main Ranking Body Win First -->
                        <!-- EK Landing Main Ranking Body Win First -->
                        <div class="ek--main--ranking--win">
                            <!-- EK Landing Main Ranking Body Win Logo -->
                            <div class="ek--main--ranking--win--logo">
                                <img src="/static/img/ek--win--third.png" alt="ek--win--third">
                            </div>
                            <!-- End EK Landing Main Ranking Body Win Logo -->
                            <!-- EK Landing Main Ranking Body Win Club -->
                            <h3 class="ek--main--ranking--win--club text--white">Chelsea Club</h3>
                            <!-- End EK Landing Main Ranking Body Win Club -->
                            <!-- EK Landing Main Ranking Body Win User -->
                            <div class="ek--main--ranking--win--user">
                                <!-- EK Landing Main Ranking Body Win User Image -->
                                <div class="ek--main--ranking--win--user--image ek--width--36">
                                    <img src="/static/img/ek--user--3.png" alt="ek--user">
                                </div>
                                <!-- End EK Landing Main Ranking Body Win User Image -->
                                <!-- EK Landing Main Ranking Body Win User Name -->
                                <h3 class="ek--main--ranking--win--user--name text--navy--200">Orkhan Nazarov</h3>
                                <!-- End EK Landing Main Ranking Body Win User Name -->
                            </div>
                            <!-- End EK Landing Main Ranking Body Win User -->
                        </div>
                        <!-- End EK Landing Main Ranking Body Win First -->
                    </div>
                    <!-- End EK Landing Main Ranking Body Win -->
                    <!-- EK landing Main Ranking Play Button -->
                    <a class="ek--main--ranking--play button--green text--navy--900"  href="{{ route('overview') }}">Play
                        online</a>
                    <!-- End EK landing Main Ranking Play Button -->
                </div>
                <!-- End EK Landing Main Ranking Container -->
            </div>
        </section>--}}
        <!-- End EK Landing Main World Ranking  -->


        <section class="ek--fb--community">
            <div class="ek--fb--community--body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 ps-0">
                            <div class="row">
                                <div class="col-md-6"></div>
                                <div class="col-md-6">
                                    <h1 class="ek--fb--community--body--title text--white">{{ __('translation.landing_fb_community_main_title') }}</h1>
                                    <span class="ek--fb--community--body--span">{{ __('translation.landing_fb_community_title') }}</span>
                                    <p class="ek--fb--community--body--paragraph text--white">{{ __('translation.landing_fb_community_paragraph') }}</p>

                                    <a class="ek--fb--community--body--join button--green text--navy--900" href="#x"> {{ __('translation.landing_fb_community_join_button') }}
                                        <img class="ek--fb--community--body--join--arrow" src="/static/img/Other/arrow--play.svg"
                                                 alt="arrow--join">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="ek--magazine bg--white">
            <div class="ek--magazine--body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 ps-0">
                            <div class="row">
                                <div class="col-md-7">
                                    <img class="main-page" src="/static/img/Landing/landing_magazine.png"
                                         alt="">
                                </div>
                                <div class="col-md-5">
                                    <h1 class="ek--magazine--body--title text--navy--900">{{ __('translation.landing_magazine_title') }}</h1>
                                    <p class="ek--magazine--body--p">{{ __('translation.landing_magazine_paragraph') }}</p>

                                    <a class="ek--magazine--body--view button--green text--navy--900" href="https://issuu.com/elevenkings" target="_blank"> {{ __('translation.landing_magazine_viewall_button') }}
                                        <img class="ek--magazine--body--view--arrow" src="/static/img/Other/arrow--play.svg"
                                             alt="arrow--view">
                                    </a>

                                    <div class="ek--magazine--body--stories">
                                        <a href="https://issuu.com/elevenkings" target="_blank">
                                            <img class="ek--magazine--body--stories--all" src="/static/img/Landing/magazines.png"
                                                 alt="magazines">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- EK landing Main FAQ  -->
        <section class="ek--main--faq bg--navy--1000">
            <div class="container">
                <!-- EK landing Main FAQ Container  -->
                <div class="ek--main--faq--container">
                    <!-- EK landing Main FAQ Head  -->
                    <div class="ek--main--faq--head">
                        <!-- EK landing Main FAQ Head Badge  -->
                        <div class="ek--badge bg--navy--700 text--white">{{ __('translation.landing_faq_badge') }}</div>
                        <!-- End EK landing Main FAQ Head Badge  -->

                        <!-- EK landing Main FAQ Head Title  -->
                        <h3 class="ek--main--faq--head--title text--white">{{ __('translation.landing_faq_title') }}</h3>
                        <!-- End EK landing Main FAQ Head Title  -->
                    </div>
                    <!-- End EK landing Main FAQ Head  -->
                    <!-- EK landing Main FAQ Inner  -->
                    <div class="ek--main--faq--inner">
                        <div class="ek--main--faq--inner--accordion">
                            {{--<button class="collapsible text--navy--200">How does Training work?</button>--}}
                            <button class="collapsible text--navy--200">{{ __('translation.landing_faq_question_1') }}</button>
                            <div class="collapsibleContent">
                                <p>{{ __('translation.landing_faq_answer_1') }}</p>
                            </div>
                        </div>
                        <div class="ek--main--faq--inner--accordion">
                            {{--<button class="collapsible text--navy--200">What are training points used for?</button>--}}
                            <button class="collapsible text--navy--200">{{ __('translation.landing_faq_question_2') }}</button>
                            <div class="collapsibleContent">
                                <p>
                                    {{ __('translation.landing_faq_answer_2') }}
{{--                                    There are many ways to increase your coin balance: <br>--}}
{{--                                    <ol>--}}
{{--                                        <li>Every time you win a game or win a tournament, you get coins</li>--}}
{{--                                        <li>There's a gift box on Homepage where you can get 25 Ek coins every 24 hours</li>--}}
{{--                                        <li>We share promo codes regulary on our social media pages or send notification to the Eleven Kings app. You can use those promo codes to increase your coin balance</li>--}}
{{--                                        <li>You can watch Video ads to get Free coins on Shop Menu</li>--}}
{{--                                        <li>You can purchase Ek coins.</li>--}}
{{--                                    </ol>--}}
                                </p>
                            </div>
                        </div>
                        <div class="ek--main--faq--inner--accordion">
                            {{--<button class="collapsible text--navy--200">I have completed an offer but have not received my</button>--}}
                            <button class="collapsible text--navy--200">{{ __('translation.landing_faq_question_3') }}</button>
                            <div class="collapsibleContent">
                                <p>{{ __('translation.landing_faq_answer_3') }}</p>
                            </div>
                        </div>
                        <div class="ek--main--faq--inner--accordion">
                            {{--<button class="collapsible text--navy--200">What are football associations and how they work </button>--}}
                            <button class="collapsible text--navy--200">{{ __('translation.landing_faq_question_4') }}</button>
                            <div class="collapsibleContent">
                                <p>
                                    {{ __('translation.landing_faq_answer_4') }}
{{--                                    The managers' performance ranked on Global and Local form on the Ranking page. <br>--}}
{{--                                    The "Global Ranking" shows TOP 50 users in the world who play Eleven Kings. In terms of the "Local Ranking", you can find TOP 50 users in your country.--}}
{{--                                    <br>--}}
{{--                                    Both types of ranking evaluate performance according to manager points. You earn the manager points by winning the matches, scoring a goal, transferring players, training players and being champion in championships.--}}
{{--                                    <br>--}}
{{--                                    Also, both ranking types divided into sections like "All Time Ranking" and "Current Ranking". All Time Ranking shows the general results from the creation of the game, but Current Ranking shows the last 1 month's results.--}}
                                </p>
                            </div>
                        </div>
                        <div class="ek--main--faq--inner--accordion">
                            <button class="collapsible text--navy--200">{{ __('translation.landing_faq_question_5') }}</button>
                            <div class="collapsibleContent">
                                <p>
                                    {{ __('translation.landing_faq_answer_5') }}
{{--                                    Eleven Kings offers you to sell your players as well as to buy new, rising stars. There're 3 tabs on "Transfer" Page--}}
{{--                                    <ol>--}}
{{--                                        <li>BUY - on this page you can find the players available for transfer. Be careful when choosing player. Check their age and ability. Try to buy young players. Because they have big potential to increase their ability. Also young players lose lower energy while playing the games and increase more ability points while training.</li>--}}
{{--                                        <li>SELL - on this page you can sell your players. There're limits on every position. Minimum 2 goalkeepers, 5 defenders, 5 midfielders and 3 forwards have to be in your squad.</li>--}}
{{--                                        <li>HISTORY - on this page you can find the transfer history you made.</li>--}}
{{--                                    </ol>--}}
                                </p>
                            </div>
                        </div>
                        <div class="ek--main--faq--inner--accordion">
                            <button class="collapsible text--navy--200">{{ __('translation.landing_faq_question_6') }}</button>
                            <div class="collapsibleContent">
                                <p>
                                    {{ __('translation.landing_faq_answer_6') }}
{{--                                    Training Camp gives you opportunity to improve your players, increase their ability. You can take your squad to the Training Camp only twice in a season. So be careful, create a strategy when to take the players to the Training Camp.--}}
                                </p>
                            </div>
                        </div>
                        {{--<div class="ek--main--faq--inner--accordion">
                            <button class="collapsible text--navy--200">Why do I have extra old players in my team
                            </button>
                            <div class="collapsibleContent">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                    exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>
                        <div class="ek--main--faq--inner--accordion">
                            <button class="collapsible text--navy--200">Only stars matter in Associations!</button>
                            <div class="collapsibleContent">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                    exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>
                        <div class="ek--main--faq--inner--accordion">
                            <button class="collapsible text--navy--200">How can I buy tokens</button>
                            <div class="collapsibleContent">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                    exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>
                        <div class="ek--main--faq--inner--accordion">
                            <button class="collapsible text--navy--200">Contact Us</button>
                            <div class="collapsibleContent">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                    exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>
                        <div class="ek--main--faq--inner--accordion">
                            <button class="collapsible text--navy--200">I do not see any video ads to earn boosters
                            </button>
                            <div class="collapsibleContent">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                    exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>
                        <div class="ek--main--faq--inner--accordion">
                            <button class="collapsible text--navy--200">When is the next update?</button>
                            <div class="collapsibleContent">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                    exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>--}}
                    </div>
                    <!-- End EK landing Main FAQ Inner  -->
                </div>
                <!-- End EK landing Main FAQ Container  -->
            </div>
        </section>
        <!-- End EK landing Main FAQ  -->
    </main>
    <!-- =========================
        End Section - Main
    ============================== -->

    <!-- =========================
        Section - Modals
    ============================== -->
    <div class="ek--modal">

    </div>
    <!-- =========================
        End Section - Modals
    ============================== -->

    <!-- =========================
        Section - Footer
    ============================== -->

    <footer class="ek--footer bg--navy--900">

        <section class="container">
            <!-- EK Landing Footer Head -->
            <div class="ek--footer--head">
                <!-- EK Landing Footer Head Left -->
                <div class="ek--footer--head--left">
                    <!-- EK Landing Footer Head Left Title -->
                    <h4 class="ek--footer--head--title text--white">{{ __('translation.landing_cover_play_button') }}</h4>
                    <!-- End EK Landing Footer Head Left Title -->
                    <!-- EK Landing Footer Head Left Paragraph -->
                    <p class="ek--footer--head--paragraph text--navy--200">{{ __('translation.landing_footer_register_now') }}</p>
                    <!-- End EK Landing Footer Head Left Paragraph -->
                </div>
                <!-- End EK Landing Footer Head Left -->
                <!-- EK Landing Footer Head Right -->
                <div class="ek--footer--head--right">
                    <!-- EK Landing Footer Head Right Sign In -->
                    <a class="ek--footer--head--sign ek--sign button--navy--700 text--white"  href="{{ route('login') }}">{{ __('translation.landing_signin') }}</a>
                    <!-- EK Landing Footer Head Right Sign In -->
                    <!-- EK Landing Footer Head Right Play Online -->
                    <a class="ek--footer--head--play ek--sign button--green text--navy--900"  href="{{ route('overview') }}">{{ __('translation.landing_main_game_feature_start_button') }}</a>
                    <!-- EK Landing Footer Head Right Play Online -->
                </div>
                <!-- End EK Landing Footer Head Right -->
            </div>
            <!-- End EK Landing Footer Head -->

            <!-- EK Landing Footer Body -->
            <div class="ek--footer--body">
                <!-- EK Landing Footer Body Left -->
                <div class="ek--footer--body--left">
                    <!-- EK Landing Footer Body Left Logo -->
                    <a class="ek--footer--body--logo" href="javascript:void(0)"><img src="/static/img/Logo/ek--logo--two.svg"
                                                                                     alt="Ek--logo"></a>
                    <!-- End EK Landing Footer Body Left Logo -->
                    <!-- EK Landing Footer Body Paragraph -->
                    <p class="ek--footer--body--paragraph text--navy--200">{{ __('translation.landing_footer_about') }}</p>
                    <!-- End EK landing Footer Body Paragraph -->
                    <!-- EK Landing Footer Body Copyright -->
                    <p class="ek--footer--body--copyright text--navy--400">© {{ date('Y') }} {{ __('translation.landing_footer_copyright') }} </p>
                    <!-- EK Landing Footer Body Copyright -->
                </div>
                <!-- End EK Landing Footer Body Left -->
                <!-- EK Landing Footer Body Center -->
                <div class="ek--footer--body--center">
                    <!-- EK Landing Footer Body Center Resources -->
                    <div class="ek--footer--body--nav">
                        <!-- EK Landing Footer Body Center Nav Title -->
                        <h4 class="ek--footer--body--nav--title text--white">{{ __('translation.landing_footer_resources_title') }}</h4>
                        <!-- End EK Landing Footer Body Center Nav Title -->
                        <!-- EK Landing Footer Body Center Nav -->
                        <ul class="ek--footer--body--nav--navbar ps-0">
                            <li class="ek--footer--body--nav--item"><a
                                    class="ek--footer--body--nav--link text--navy--200" href="javascript:void(0)">{{ __('translation.landing_footer_resources_item_link_1') }}</a></li>
                            <li class="ek--footer--body--nav--item"><a
                                    class="ek--footer--body--nav--link text--navy--200" href="javascript:void(0)">{{ __('translation.landing_footer_resources_item_link_2') }}</a></li>
                            <li class="ek--footer--body--nav--item"><a
                                    class="ek--footer--body--nav--link text--navy--200"
                                    href="javascript:void(0)">{{ __('translation.landing_footer_resources_item_link_3') }}</a></li>
                        </ul>
                        <!-- End EK Landing Footer Body Center Nav -->
                    </div>
                    <!-- End EK Landing Footer Body Center Resources -->

                    <!-- EK Landing Footer Body Center Resources -->
                    <div class="ek--footer--body--nav">
                        <!-- EK Landing Footer Body Center Nav Title -->
                        <h4 class="ek--footer--body--nav--title text--white">{{ __('translation.landing_footer_social_title') }}</h4>
                        <!-- End EK Landing Footer Body Center Nav Title -->
                        <!-- EK Landing Footer Body Center Nav -->
                        <ul class="ek--footer--body--nav--navbar ps-0">
                            <li class="ek--footer--body--nav--item"><a
                                    class="ek--footer--body--nav--link text--navy--200"
                                    href="https://www.facebook.com/elevenkingsgame/">Facebook</a></li>
                            <li class="ek--footer--body--nav--item"><a
                                    class="ek--footer--body--nav--link text--navy--200"
                                    href="https://twitter.com/elevenkingsgame">Twitter</a></li>
                            <li class="ek--footer--body--nav--item"><a
                                    class="ek--footer--body--nav--link text--navy--200"
                                    href="https://www.instagram.com/elevenkingsgame/">Instagram</a></li>
                        </ul>
                        <!-- End EK Landing Footer Body Center Nav -->
                    </div>
                    <!-- End EK Landing Footer Body Center Resources -->
                </div>
                <!-- End EK Landing Footer Body Center -->
                <!-- EK Landing Footer Body Right -->
                <div class="ek--footer--body--right">
                    <!-- EK Landing Footer Body Right Title -->
                    <h4 class="ek--footer--body--nav--title text--white">{{ __('translation.landing_footer_contact_title') }}</h4>
                    <!-- End EK Landing Footer Body Right Nav Title -->
                    <!-- EK Landing Footer Body Right Form -->
                    <form class="ek--footer--body--form" action="">
                        <div class="ek--footer--body--form--group">
                            <label class="ek--footer--body--form--label text--navy--300">{{ __('translation.landing_footer_contact_email_label') }}</label>
                            <input type="text"
                                   class="ek--footer--body--form--input ek--input bg--navy--1000 text--white">
                            <small class="ek--validation text--red">
                                <svg class="icon icon-ek--warning ek--svg--size fill--red">
                                    <use xlink:href="/static/img/icons.svg#icon-ek--warning"></use>
                                </svg>
                                <span>{{ __('translation.landing_footer_contact_error_message') }}</span></small>
                        </div>
                        <div class="ek--footer--body--form--group">
                            <label class="ek--footer--body--form--label text--navy--300">{{ __('translation.landing_footer_contact_text_label') }}</label>
                            <textarea name="" id="" cols="30" rows="10"
                                      class="ek--footer--body--form--textarea bg--navy--1000 text--white"></textarea>
                            <small class="ek--validation text--red">
                                <svg class="icon icon-ek--warning ek--svg--size fill--red">
                                    <use xlink:href="/static/img/icons.svg#icon-ek--warning"></use>
                                </svg>
                                <span>{{ __('translation.landing_footer_contact_error_message') }}</span></small>
                        </div>
                        <button type="submit"
                                class="ek--footer--body--form--button ek--sign text--navy--300 button--navy--700">{{ __('translation.landing_footer_contact_send_button') }}
                        </button>
                    </form>
                    <!-- End EK Landing Footer Body Right Form -->
                    <!-- EK Landing Footer Body Success  -->
                    <div class="ek--footer--body--form--success">
                        <!-- EK Landing Footer Body Success message -->
                        <div class="ek--message text--green">
                            <svg class="icon icon-ek--correct ek--svg--size fill--green">
                                <use xlink:href="/static/img/icons.svg#icon-ek--correct"></use>
                            </svg>
                            <span>{{ __('translation.landing_footer_contact_success_message') }}</span></div>
                        <!-- End EK Landing Footer Body Success message -->
                        <p class="ek--footer--body--form--message text--navy--200">{{ __('translation.landing_footer_contact_form_message') }}</p>
                        <a class="ek--footer--body--form--resed text--navy--200" href="javascript:void(0)">{{ __('translation.landing_footer_contact_form_send_new_message') }}</a>
                    </div>
                    <!-- End EK Landing Footer Body Success  -->
                </div>
                <!-- End EK Landing Footer Body Right -->
            </div>
            <!-- End EK Landing Footer Body -->
        </section>

    </footer>

    <!-- =========================
        End Section - Footer
    ============================== -->
@endsection
