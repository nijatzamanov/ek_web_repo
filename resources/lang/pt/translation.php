<?php
return [
    'landing_title'=>'Eleven Kings Jogo de gerente de futebol',
    'landing_signin'=>'Entrar',
    'landing_cover_title'=>'Eleven Kings',
    'landing_cover_span'=>'jogo de gerenciamento de futebol online',
    'landing_cover_paragraph'=>'Eleven Kings é um jogo de gerenciamento de futebol gratuito. Assuma o controle do clube, defina táticas, transfira jogadores, participe de torneios e torne-se a lenda! Prove suas habilidades como gerente de futebol, deixe o mundo conhecê-lo!',
    'landing_cover_play_button'=>'Jogue agora',
    'landing_cover_scroll_down'=>'Role para mais',
    'landing_main_feature_bg_yellow_title'=>'Crie um esquadrão vencedor',
    'landing_main_feature_bg_yellow_paragraph'=>'Treine jogadores e transfira novos craques.',
    'landing_main_feature_bg_blue_title'=>'Ganhe troféus',
    'landing_main_feature_bg_blue_paragraph'=>'Ligas, Copas e Desafios estão esperando por você.',
    'landing_main_feature_bg_green_title'=>'Você pode dar um lugar para todos os seus fãs?',
    'landing_main_feature_bg_green_paragraph'=>'Acumule pontos, aumente seu nível de gerente e deixe o mundo conhecê-lo!',
    'landing_main_game_feature_badge'=>'RECURSOS',
    'landing_main_game_feature_title'=>'Veja as características do jogo',
    'landing_main_game_feature_paragraph'=>'Você terá vários recursos para definir suas táticas especiais e estilo de jogo, gerenciar seu time de uma maneira diferente.',
    'landing_main_game_feature_list_item_1'=>'Melhores táticas de clube',
    'landing_main_game_feature_list_item_2'=>'Classificação Global e Local',
    'landing_main_game_feature_list_item_3'=>'Jogadores Especiais Toda Semana',
    'landing_main_game_feature_list_item_4'=>'Mercado de Transferência Dinâmica',
    'landing_main_game_feature_list_item_5'=>'Vários Torneios',
    'landing_main_game_feature_start'=>'Agora você está pronto, comece o jogo!',
    'landing_main_game_feature_start_button'=>'Jogue online',
    'landing_main_mobile_badge'=>'MÓVEL',
    'landing_main_mobile_title'=>'Eleven Kings está disponível no celular.',
    'landing_main_mobile_paragraph'=>'Todas as informações que você acabou de ver acima se aplicam ao aplicativo móvel. Baixe agora e entre no jogo!',
    'landing_fb_community_main_title'=>'Eleven Kings',
    'landing_fb_community_title'=>'Comunidade do Facebook',
    'landing_fb_community_paragraph'=>'Junte-se ao grupo do Facebook para discutir o jogo, encontrar ofertas extras e conselhos de usuários experientes.',
    'landing_fb_community_join_button'=>'Entrar',
    'landing_magazine_title'=>'EK MAG',
    'landing_magazine_paragraph'=>'Revista Mensal Online',
    'landing_magazine_viewall_button'=>'Ver tudo',
    'landing_faq_badge'=>'F.A.Q',
    'landing_faq_title'=>'Perguntas Frequentes',
    'landing_faq_question_1'=>'O jogo é gratuito?',
    'landing_faq_answer_1'=>'Eleven Kings Game é gratuito e você pode jogar todos os recursos gratuitamente.',
    'landing_faq_question_2'=>'Como posso obter moedas no jogo?',
    'landing_faq_answer_2'=>'Há muitas maneiras de aumentar seu saldo de moedas: 1) Cada vez que você ganha um jogo ou um torneio, você ganha moedas. 2) Há uma caixa de presente na página inicial onde você pode obter 25 moedas EK a cada 24 horas. 3) Compartilhamos códigos promocionais regularmente em nossas páginas de mídia social ou enviamos notificações para o aplicativo Eleven Kings. Você pode usar esses códigos promocionais para aumentar seu saldo de moedas. 4) Você pode assistir a anúncios em vídeo para obter moedas grátis no menu da loja. 5) Você pode comprar moedas EK.',
    'landing_faq_question_3'=>'Como posso melhorar meu elenco?',
    'landing_faq_answer_3'=>'Você pode transferir jogadores melhores na seção de Transferências, também pode comprar Jogadores Especiais (com habilidades superiores a 80 e idades entre 16 e 20 anos). Use Daily Training ou Training Camp para melhorar a habilidade de seus jogadores. O treino diário não tem limite, você pode usar quantas vezes quiser, mas o training camp tem um limite: 2 vezes em cada temporada.',
    'landing_faq_question_4'=>'Como funciona o Ranking Global e Local?',
    'landing_faq_answer_4'=>'O desempenho dos gerentes foi classificado no formulário Global e Local na página de Ranking. Ambos os tipos de ranking avaliam o desempenho de acordo com os pontos do gestor. Você pode ganhar pontos de gerente vencendo as partidas, marcando um gol, transferindo jogadores, treinando jogadores e sendo campeão em campeonatos. O Ranking Local é dividido em seções como "All Time Ranking" e "Últimos 30 dias". A classificação de todos os tempos mostra os pontos acumulados do gerente desde o início do jogo, mas a classificação dos últimos 30 dias mostra os resultados do último mês (últimos 30 dias). O ranking global é dividido em seções como "Ranking diário", "Ranking de todos os tempos" e "Últimos 30 dias". O Ranking Diário mostra os pontos do gerente acumulados nas últimas 24 horas. Ele reinicia às 00:00 GMT todos os dias. A classificação de todos os tempos e as classificações dos últimos 30 dias são as mesmas das classificações locais.',
    'landing_faq_question_5'=>'Como funciona o recurso de transferência?',
    'landing_faq_answer_5'=>'Eleven Kings oferece a você a venda de seus jogadores, bem como a compra de novas estrelas em ascensão. Existem 3 abas na página "Transferência": 1. COMPRAR - nesta página você pode encontrar os jogadores disponíveis para transferência. Tenha cuidado ao escolher um jogador. Verifique sua idade e capacidade. Tente comprar jogadores jovens. Porque eles têm um grande potencial para aumentar sua capacidade. Além disso, os jogadores jovens perdem menos energia durante os jogos e aumentam mais pontos de habilidade durante o treinamento. 2. VENDER - nesta página você pode vender seus jogadores. Há limites em todas as posições. Mínimo de 2 goleiros, 5 zagueiros, 5 meio-campistas e 3 atacantes devem estar no seu time. 3. HISTÓRICO - nesta página você encontra o histórico de transferências que fez.',
    'landing_faq_question_6'=>'Como posso obter os melhores resultados do Campo de Treinamento?',
    'landing_faq_answer_6'=>'O campo de treinamento dá a você a oportunidade de melhorar seus jogadores, aumentar sua capacidade. Você pode levar seu time para o Training Camp apenas duas vezes por temporada. Então tome cuidado, crie uma estratégia na hora de levar os jogadores para o Campo de Treinamento.',

    'landing_footer_register_now'=>'Registre-se agora e aproveite o jogo!',
    'landing_footer_about'=>'Assuma o controle do clube, defina táticas, transfira jogadores, participe de torneios e torne-se a lenda! Prove suas habilidades como gerente de futebol, deixe o mundo conhecê-lo!',
    'landing_footer_copyright'=>'Eleven Kings Inc.',
    'landing_footer_resources_title'=>'RECURSOS',
    'landing_footer_resources_item_link_1'=>'Política de Privacidade',
    'landing_footer_resources_item_link_2'=>'Termos e Condições',
    'landing_footer_resources_item_link_3'=>'Blogue',
    'landing_footer_social_title'=>'MÍDIA SOCIAL',
    'landing_footer_contact_title'=>'CONTATE-NOS',
    'landing_footer_contact_email_label'=>'Seu endereço de email',
    'landing_footer_contact_text_label'=>'Como podemos te ajudar?',
    'landing_footer_contact_send_button'=>'Enviar mensagem',
    'landing_footer_contact_error_message'=>'Um erro de mensagem inadequada.',
    'landing_footer_contact_success_message'=>'O e-mail foi enviado com sucesso!',
    'landing_footer_contact_form_message'=>'Retornaremos para você o mais breve possível.',
    'landing_footer_contact_form_send_new_message'=>'Envie uma nova mensagem',

    "app_name"=>"Eleven Kings Pro",
    "channel_name"=>"channel_name",
    "channel_description"=>"channel_description",
    "default_notification_channel_id"=>"666",
    "menu_challenge"=>"EK Challenge",
    "menu_etihad_challenge"=>"Etihad Challenge",
    "home_global"=>"Global",
    "email"=>"Contact: info@elevenkings.com",
    "presentation_header_one"=>"Crie uma seleção vencedora!",
    "presentation_header_two"=>"Ganhe troféus.",
    "presentation_header_three"=>"Fique no topo do mundo EK.",
    "presentation_body_one"=>"Você receberá 23 jogadores. Treine sua equipe e transfira os jogadores promissores. Os jogadores iniciantes podem se desenvolver rapidamente e obter melhores resultados.",
    "presentation_body_two"=>"As Ligas, Copas e Desafios estão esperando por você! Você pode jogar apenas uma Liga e um Torneio de Copas por Temporada. Mas o modo Desafio e Amigável é ilimitado e você pode jogar quantas vezes quiser.",
    "presentation_body_three"=>"Você ganha Pontos de Treinador toda vez que você ganha ou empata partidas, marca gols, ganha Troféus e transfere jogadores. Estes pontos ajudam a aumentar o seu nível de treinador. Mostre os resultados surpreendentes e deixe o mundo conhecê-lo!",
    "presentation_skip"=>"Pular",
    "presentation_next"=>"Próximo",
    "register_country_bottom_text_start"=>"Você já tem uma conta?",
    "register_country_bottom_text_end"=>"Faça login",
    "register_country_header"=>"Vamos começar!",
    "register_country_country_hint"=>"Escolha o seu país",
    "register_country_team_name_hint"=>"Nome do time",
    "register_country_button"=>"Continuar",
    "login_type_login_header"=>"Faça login na sua conta",
    "login_type_register_header"=>"Crie sua conta",
    "login_type_google"=>"Continuar com o Google",
    "login_type_facebook"=>"Continuar com o Facebook",
    "login_type_or"=>"ou",
    "login_type_button"=>"E-mail",
    "login_type_bottom_text_start"=>"Você não possui uma conta?",
    "login_type_bottom_text_end"=>"Registre-se",
    "login_type_bottom_text_privacy_start"=>"Eu concordo com todas as declarações em",
    "login_type_privacy_start"=>"Ao se registrar, você concorda em",
    "login_type_bottom_text_privacy_end"=>"Termos e Condições",

    "login_type_bottom_text_error"=>"Antes do registro, por favor, concorde com os Termos e Condições",
    "register_email_header"=>"Registre-se com o E-mail",
    "register_email_name_hint"=>"Nome completo",
    "register_email_email"=>"E-mail",
    "register_email_password"=>"Senha",
    "register_email_password_error"=>"A senha deve ter pelo menos 8 caracteres.",
    "login_email_password_error"=>"Senha incorreta. A senha que você digitou não pertence nenhuma conta",
    "login_email_header"=>"Bem-vindo de volta",
    "login_email_button"=>"Faça login",
    "login_email_forget"=>"Esqueceu a senha?",
    "forget_email_button"=>"Enviar",
    "forget_email_header"=>"Esqueceu a senha",
    "forget_password_header"=>"Redefina sua senha",
    "forget_password_text"=>"Você solicitou a redefinição da senha para:",
    "forget_password_button"=>"Confirmar",
    "player_item_salary"=>"Salário",
    "player_item_goal"=>"Gol",
    "player_item_energy"=>"Energia",
    "player_item_game"=>"Jogo",
    "player_item_yellow_card"=>"Cartão amarelo",
    "player_item_red_card"=>"cartão vermelho",
    "player_item_country"=>"País",
    "forget_password_password"=>"Nova senha",
    "forget_password_replay_password"=>"Repita a nova senha",

    "menu_home"=>"Pagina İnicial",
    "menu_team"=>"Time",
    "menu_squad"=>"Seleção",
    "menu_lineup"=>"Formação e Táticas",
    "menu_training"=>"Treinamento Diário",
    "menu_camp"=>"Campo de Treinamento",
    "menu_transfer"=>"Transferir",
    "menu_stadium"=>"Estádio",
    "menu_statistics"=>"Estatísticas",
    "menu_tournaments"=>"Torneios",
    "menu_league"=>"Liga de Reis",
    "menu_cup"=>"Copa Internacional",
    "menu_friendly"=>"Amigável",
    "menu_office"=>"Escritório",
    "menu_coins"=>"Moedas EK",
    "menu_budget"=>"Orçamento",
    "menu_marketing"=>"Marketing",
    "menu_account"=>"Conta",
    "menu_news"=>"Notícia",
    "menu_setting"=>"Perfil",
    "menu_info"=>"Informações",
    "menu_logout"=>"Sair",
    "menu_policy"=>"Política de Privacidade",
    "menu_about"=>"Sobre",
    "menu_promotions"=>"Promoções",
    "dialog_update"=>"Atualizar!",
    "play_online"=>"Jogar online",
    "home_header"=>"Início",
    "home_standing"=>"Em pé",
    "home_next_match"=>"Próxima partida",
    "home_play_match"=>"Jogar Partida",
    "home_loading"=>"Carregando ...",
    "home_current_ranking"=>"Classificação Atual",
    "home_manager_point"=>"Ponto de Treinador",
    "home_league"=>"Liga",
    "dash"=>"-",
    "hello_blank_fragment"=>"Olá, fragmento em branco",
    "next_match_header"=>"Próxima partida",
    "next_match_play"=>"Jogar",
    "next_match_other_play"=>"Você tem um tempo de espera. Se você quer jogar, escolha outra partida.",
    "live_header"=>"AO VIVO",
    "live_skip"=>"Pular",

    "balance" => "Saldo",
    "ek_coin" => "EK coin",
    "points" => "Pontos",
    "match_ready" => "Jogo pronto",
    "start_match" => "Comece a partida",
    "place" => "Lugar",

    "tab_substitutions"=>"Substituições",
    "tab_formation"=>"Formação",
    "tab_tactics"=>"Táticas",
    "formation_1"=>"4–3–3",
    "formation_2"=>"4–4–2",
    "formation_3"=>"5–4–1",
    "formation_4"=>"3–4–3",
    "formation_5"=>"4–5–1",
    "formation_6"=>"5–3–2",
    "formation_7"=>"3–5–2",
    "formation_8"=>"5–2–3",
    "tactic_game_style"=>"Estilo de jogo",
    "tactic_passing_style"=>"Estilo de passe",
    "tactic_pressing_style"=>"Estilo de ressão",
    "tactic_game_style_soft"=>"suave",
    "tactic_game_style_normal"=>"normal",
    "tactic_game_style_aggressive"=>"agressivo",
    "tactic_game_style_short"=>"curto",
    "tactic_game_style_mixed"=>"misto",
    "tactic_game_style_long"=>"longo",
    "live_match_stats"=>"Estatísticas da partida",
    "live_ft"=>"TT",
    "msg_squad"=>"Seleção",
    "msg_player_type_title"=>"J",
    "msg_players"=>"Jogadores",
    "msg_age"=>"Idade",
    "msg_abl"=>"Hab",
    "player_abl"=>"Habilidade",
    "error_facebook"=>"Erro de registro no Facebook!",
    "error_google"=>"Erro de registro no Google!",
    "error_already_registered"=>"O e-mail já está registrado no sistema",
    "error_code"=>"O código de verificação está errado!",
    "error_login_problem"=>"E-mail ou senha incorretos",
    "error_account"=>"Você não tem uma conta.",
    "error_system_problem"=>"O sistema está com um problema…",
    "error_payment_problem"=>"Ocorreu um problema com o pagamento...",
    "error_email_not_found"=>"E-mail não foi encontrado em nosso sistema",
    "error_play_problem"=>"Ops... Ocorreu um erro",
    "error_training_not_do"=>"Você já usou o limite do seu Campo de Treinamento para um período de tempo determinado!",
    "error_no_internet"=>"Sem conexão à internet!",
    "error_data_not_found"=>"Dados não encontrados",
    "action_next_match"=>"Próxima partida",
    "title_player"=>"Jogador",
    "action_refill_energy"=>"Reabastecer energia",
    "refill_energy_success"=>"Atualização da energia do jogador!",
    "training_dialog_header"=>"Treinamento",
    "training_dialog_success"=>"Treinamento Diário concluído.",
    "training_dialog_coin"=>"Não há moedas EK suficientes para iniciar o Treinamento Diário",
    "training_dialog_coin_player"=>"Não há moedas EK suficientes para comprar um jogador.",
    "training_dialog_budget_player"=>"Não há Orçamento suficiente para comprar um jogador",
    "buy_budget_dialog_coin"=>"Não há moedas EK suficientes para comprar Orçamento",
    "training_dialog_money"=>"Não há Orçamento suficiente para iniciar o Campo de Treinamento",
    "friendly_game_not"=>"Você não tem um jogo amigável",
    "friendly_game_button"=>"Arranjar",
    "challenge_game_not"=>"Você não tem um jogo de Desafio",
    "challenge_game_button"=>"Página do Desafio",
    "etihad_challenge_game_warning"=>"Para jogar este torneio, você precisa atualizar alcançar a 1ª Liga.",
    "etihad_challenge_game_not"=>"Você não tem um jogo do Desafio Etihad",
    "etihad_challenge_game_button"=>"Página do Desafio Etihad",
    "msg_defender"=>"DEFENSA",
    "msg_midfielder"=>"MEIO DE CAMPO",
    "msg_goalkeeper"=>"GOLEIRO",
    "msg_forward"=>"ATACANTE",
    "msg_value"=>"Val",
    "player_value"=>"Valor",
    "match_stats_stat"=>"Estatísticas",
    "match_stats_info"=>"Informações",
    "match_stats_reward"=>"Recompensa",
    "match_stats_header"=>"Estatísticas da partida",
    "match_stats_reward_bonus"=>"BÔNUS DO JOGO",
    "match_stats_reward_coin"=>"MOEDAS EK GANHADAS",
    "match_stats_possession"=>"POSSE",
    "match_stats_total_shots"=>"TOTAL DE TIROS",
    "match_stats_shoots_target"=>"TIROS NO ALVO",
    "match_stats_yellow_cards"=>"CARTÃO AMARELO",
    "match_stats_red_cards"=>"CARTÃO VERMELHO",
    "statistic_most_value"=>"Valioso",
    "statistic_top_scores"=>"Melhores Marcadores",
    "statistic_most_play"=>"Mais Jogado.",
    "statistic_best_ability"=>"Habilidade.",
    "statistic_header"=>"Estatística",
    "statistic_most_value_new"=>"Jogadores mais valiosos",
    "statistic_top_scores_new"=>"Melhores Marcadores",
    "statistic_most_play_new"=>"Jogadores mais jogados",
    "statistic_best_ability_new"=>"Melhores habilidades",
    "budget_header"=>"Orçamento",
    "budget_income"=>"Renda desta temporada",
    "budget_expenses"=>"Despesas desta temporada",
    "coins_header"=>"Moedas EK",
    "coins_income"=>"Renda desta temporada",
    "coins_expenses"=>"Despesas desta temporada",
    "buy_coin_header"=>"Deseja adicionar?",
    "buy_coin_message"=>"Isso custa",
    "buy_coin_success"=>"Você aumentou seu saldo de moedas com sucesso!",
    "buy_budget_success"=>"Você aumentou seu saldo de dinheiro com sucesso!",
    "marketing_header"=>"Marketing",
    "marketing_design"=>"Design",
    "marketing_creativity"=>"Criatividade",
    "marketing_management"=>"Gestão",
    "marketing_weekly"=>"Despesa semanal:",
    "marketing_choose"=>"Escolher",
    "marketing_cancel"=>"Cancelar",
    "daily_training_header"=>"Treinamento Diário",
    "daily_training_player"=>"Todos os jogadores",
    "daily_training_age"=>"Idade",
    "daily_training_ability"=>"Hab",
    "daily_training_energy"=>"Energia",
    "starting_lineup"=>"Escalação da partida",
    "action_clear_lineup"=>"LIMPAR A ESCALAÇÃO",
    "action_auto_select"=>"SELEÇÃO AUTOMÁTICA",
    "lineup_player_injured_header"=>"Jogador lesionado",
    "lineup_player_injured_info"=>"O jogador não poderá jogar nos próximos [match_count] devido a lesão. Você quer curar o jogador?",
    "button_continue"=>"Continuar",
    "button_train_again"=>"Treinar Novamente",
    "trainig_style_header"=>"Estilos de Treinamento",
    "training_style_energy_loss"=>"Perda de Energia",
    "training_style_improvement"=>"Melhoria",
    "training_result_header"=>"Resultados do Treinamento",
    "transfer_title_buy"=>"Comprar",
    "transfer_title_sell"=>"Vender",
    "transfer_title_history"=>"Histórico",
    "transfer_title"=>"Transferir",
    "transfer_price"=>"Preço",
    "transfer_build"=>"A janela de transferência não está aberta no momento. Você deseja abrir a janela de transferência?",
    "transfer_open"=>"Abrir",
    "transfer_request_message"=>"Você solicita o envio ao Clube. Você verá o resultado da transferência após uma partida",
    "transfer_success_text"=>"O [player_name] foi transferido com sucesso para o seu clube!",
    "transfer_error_text"=>"O clube rejeitou sua oferta.",
    "transfer_new_offer_text"=>"O clube aumentou seu lance pelo [player_name] em $[player_cost]. Você deseja aceitar?",
    "transfer_special_player"=>"Jogador especial",
    "camp_choose"=>"Escolha este Campo",
    "camp_hotel"=>"Hotel",
    "camp_airlines"=>"Companhias Aéreas",
    "camp_transport"=>"Transporte",
    "camp_duration"=>"Duração",
    "camp_ability"=>"Alteração de Habilidade",
    "camp_cost"=>"Custo",
    "camp_result_header"=>"Resultados do Campo de Treinamento",
    "transfer_action_buy"=>"Comprar",
    "transfer_action_pending"=>"Pendente...",
    "news_list_text"=>"O que aconteceu na Premier League (Primeira Liga) no sábado?",
    "news_list_date"=>"Patrocinadas",
    "news_title_game"=>"Jogo",
    "news_title_community"=>"Comunidade",
    "news_title"=>"Notícia",
    "league_standing"=>"Em pé",
    "league_result"=>"Resultado",
    "league_fixture"=>"Fixação",
    "league_top_scores"=>"Melhores Marcadores",
    "league_header"=>"@string/menu_league",
    "challenge_header"=>"@string/menu_challenge",
    "challenge_1_8"=>"1/8",
    "challenge_1_4"=>"1/4",
    "challenge_semi_final"=>"Semi final",
    "challenge_final"=>"Final",
    "challenge_first"=>"1° Parte",
    "challenge_second"=>"2° Parte",
    "cup_header"=>"@string/menu_cup",
    "friendly_header"=>"Amigável",
    "friendly_fixture"=>"Fixação",
    "friendly_result"=>"Resultado",
    "friendly_arrange"=>"Arranjar",
    "community_title"=>"Comunidade",
    "community_text"=>"O Gareth Bale, do Real Madrid, pode optar por uma mudança lucrativa para a China, os Emirados Árabes Unidos e uma das principais seleções europeias, a Major League Soccer (MLS) - mas, com acordo menor que o atual, que vale mais de 500.000 libras por semana, um galês de 29 anos de idade que pode assinar um contrato até 2022.",
    "community_date"=>"2 horas atrás",
    "community_text_title"=>"O que aconteceu na Premier League (Primeira Liga) no sábado?",
    "player_transfer_to"=>"Transferir para:",
    "player_transfer_from"=>"Transferir de:",
    "player_transfer_congratulation"=>"Parabéns! Novo jogador transferido!",
    "league_standing_place"=>"JN",
    "league_standing_team_name"=>"Clube N.",
    "league_standing_game"=>"GP",
    "league_standing_win"=>"W",
    "league_standing_draft"=>"D",
    "league_standing_lose"=>"L",
    "league_standing_goal"=>"G F/A",
    "league_standing_point"=>"J",
    "game_divider"=>"-",
    "friendly_arrange_league_1"=>"Liga 1",
    "friendly_arrange_league_2"=>"Liga 2",
    "friendly_arrange_league_3"=>"Liga 3",
    "friendly_arrange_league_4"=>"Liga 4",
    "friendly_arrange_league_5"=>"Liga 5",
    "friendly_arrange_power"=>"Poder",
    "friendly_arrange_choose_hint"=>"Escolha o nível do oponente",
    "friendly_arrange_opponent"=>"Seu oponente",
    "friendly_arrange_action_decline"=>"Declinar",
    "friendly_arrange_action_accept"=>"Aceitar",
    "friendly_arrange_league_power_1"=>"80-100",
    "friendly_arrange_league_power_2"=>"65-85",
    "friendly_arrange_league_power_3"=>"50-75",
    "friendly_arrange_league_power_4"=>"35-65",
    "friendly_arrange_league_power_5"=>"20-50",
    "home_next_other_match"=>"Outra partida",
    "settings_sound"=>"Som",
    "settings_vibration"=>"Vibração",
    "settings_language"=>"Idioma",
    "settings_russian"=>"Russo",
    "settings_french"=>"Francês",
    "settings_german"=>"Alemão",
    "settings_italian"=>"Italiano",
    "settings_indonesian"=>"Indonésio",
    "settings_header"=>"Configurações",
    "profile_header"=>"Perfil",
    "profile_level"=>"NÍVEL",
    "profile_info"=>"Informações",
    "profile_statistics"=>"Estatísticas",
    "profile_trophies"=>"Troféus",
    "edit_profile_cancel"=>"Cancelar",
    "edit_profile_done"=>"Feito",
    "edit_profile_title"=>"Editar Perfil",
    "edit_profile_change_image"=>"Alterar foto do perfil",
    "edit_profile_manager_name"=>"Nome do Treinador",
    "edit_profile_email_title"=>"E-mail",
    "edit_profile_team_name_title"=>"Nome do time",
    "edit_profile_current_password"=>"Senha atual",
    "edit_profile_new_password"=>"Nova senha",
    "edit_profile_new_password_error"=>"A senha deve ter pelo menos 8 caracteres.",
    "edit_profile_manager_name_length_error"=>"O comprimento do nome do Treinador deve ser pelo menos 3 caracteres",
    "error_permision_denied"=>"Por favor, verifique todas as permissões",
    "msg_image_loader_warning"=>"Você não escolheu uma imagem",
    "play_dialog_title"=>"Jogar",
    "play_dialog_energy"=>"A energia de alguns jogadores é muito baixa. Reabasteça a energia e tente novamente",
    "play_dialog_message"=>"Há jogadores suspensos na escalação da partida. Troque o jogador e tente novamente.",
    "play_dialogs_action_cancel"=>"Cancelar",
    "play_dialogs_second_message"=>"Isso vai te custar",
    "play_dialogs_training_first"=>"Você quer começar",
    "play_dialogs_training_second"=>"?",
    "refill_energy_action_ok"=>"OK",
    "button_text_buy_coin"=>"Compre moedas EK",
    "button_text_buy_budget"=>"Comprar Orçamento",
    "button_text_go_home"=>"Ir para Início",
    "goal_dialog_header"=>"Gol",
    "training_camp_dialog"=>"Você realmente deseja levar sua equipe ao campo em",
    "training_camp_loading_title"=>"Campo de Treinamento",
    "training_camp_loading_message"=>"O Campo está carregando…",
    "training_camp_second_message"=>"Baku é a capital do Azerbaijão.",
    "training_camp_success_message"=>"Parabéns!!!",
    "training_camp_success_second_message"=>"Seu campo de treinamento foi bem-sucedido!",
    "training_camp_success_third_message"=>"A capacidade média da sua equipe aumentou:",
    "training_camp_header"=>"Campo de Treinamento",
    "auto_select_header"=>"Seleção Automática",
    "auto_select_dialog_all_player"=>"Você deseja selecionar automaticamente a escalação da partida?",
    "refill_energy_dialog_all_player"=>"Você deseja reabastecer energia da equipe titular?",
    "refill_energy_dialog_player_message"=>"Você deseja reabastecer energia de",
    "sell_dialog_player_message"=>"Você quer vender",
    "buy_dialog_player_message"=>"Você quer comprar",
    "buy_dialog_from"=>"de",
    "sell_dialog_to"=>"para",
    "buy_dialog_message"=>"Você comprou",
    "sell_dialog_message"=>"Seu vendeu",
    "success_dialog_message"=>"Sucesso!",
    "player_buy_squad_error"=>"Você não pode comprar este jogador, porque você alcançou o número máximo na sua seleção.",
    "player_sell_error_team"=>"Você atingiu o tamanho mínimo da seleção de (14)!",
    "player_sell_error_goalkeeper"=>"Você atingiu o tamanho mínimo de goleiro (2) na seleção!",
    "player_sell_error_defender"=>"Você atingiu o tamanho mínimo de Zagueiro (5) na seleção!",
    "player_sell_error_mieldfielder"=>"Você atingiu o tamanho mínimo de meio campo (5) na seleção!",

    "player_sell_error_forward"=>"Você atingiu o tamanho mínimo de atacante (3) na seleção!",
    "verification_header"=>"Verificação",
    "verification_dialog_text"=>"Você verificou a conta com sucesso.",
    "verification_email_text"=>"Digite o código de 6 dígitos enviado para",
    "verification_code"=>"CÓDIGO",
    "verification_resend"=>"Reenviar o código",
    "verification_confirm"=>"Confirmar",
    "live_request_finish_cup"=>"Copa",
    "live_request_finish_challenge"=>"Desafio",
    "live_request_finish_fail_header"=>"Infelizmente",
    "live_request_finish_fail_second"=>"Você está fora",
    "live_request_finish_fail_bottom"=>"da competição",
    "live_request_finish_win_header"=>"Parabéns!",
    "live_request_finish_win_second"=>"VOCÊ GANHOU",
    "live_request_finish_win_bottom"=>"o Desafio!",
    "live_request_finish_win_bottom_cup"=>"a Copa!",
    "live_request_finish_league"=>"Liga",
    "live_request_finish_success_header"=>"Parabéns!",
    "live_request_finish_success_second"=>"Você foi promovido para próxima Liga!",
    "live_request_finish_stop_header"=>"Infelizmente",
    "live_request_finish_stop_second"=>"Você não foi promovido para próxima liga.",
    "live_request_finish_error_header"=>"Infelizmente",
    "live_request_finish_error_second"=>"Você foi rebaixado para uma Liga inferior.",
    "live_request_finish_success_champion"=>"Você é o campeão!",
    "live_player"=>"jogador",
    "terms_and_conditions_header"=>"Termos e Condições",
    "text_privacy_and_policy"=>"Política de Privacidade",
    "text_logout_dialog"=>"Você realmente deseja sair?",
    "text_logout_positive"=>"Sim",
    "text_next_match_loading"=>"Este jogo é apresentado a você por",
    "splash_update_button"=>"Atualizar",
    "splash_update_text"=>"Nova versão disponível. Por favor, atualize!",
    "profile_statistic_info_registered"=>"Registrado",
    "profile_statistic_info_globalRank"=>"Classificação global",
    "profile_statistic_info_localRank"=>"Classificação local",
    "profile_statistic_info_managerPoints"=>"Pontos de Treinador",
    "profile_statistic_played"=>"Jogado",
    "profile_statistic_won"=>"Vitória",
    "profile_statistic_draw"=>"Empate",
    "profile_statistic_lost"=>"Derrota",
    "profile_statistic_gfa"=>"G F/A",
    "profile_statistic_gfa_full"=>"Gols a Favor/Contra",
    "profile_statistic_trophies_leaguesWon"=>"Ligas vencidas",
    "profile_statistic_trophies_challengesWon"=>"Desafios vencidos",
    "profile_statistic_trophies_cupsWon"=>"Copas vencidas",
    "home_start_new_season" => "Comece uma nova temporada",
    "home_next_season_text"=>"Você quer começar a próxima temporada?",
    "home_next_challenge_text"=>"Você deseja iniciar o próximo Desafio?",
    "home_start"=>"Iniciar",
    "play_online_header"=>"Jogue online",
    "play_online_opponent_find"=>"Encontre um oponente",
    "play_online_opponent_find_info"=>"Jogue online agora com alguém com o mesmo poder que você!",
    "error_not_game"=>"Você não tem um novo jogo!",
    "version"=>"Versão:",
    "training_style_low_energy"=>"Baixo",
    "training_style_low_potential"=>"50 % - 70% de potencial",
    "training_style_low_name"=>"Treinamento suave",
    "training_style_awerage_energy"=>"Média",
    "training_style_awerage_potential"=>"100 % o de potencial",
    "training_style_awerage_name"=>"Treinamento normal",
    "training_style_high_energy"=>"Alto",
    "training_style_high_potemtial"=>"120 % - 150% de potencial",
    "training_style_high_name"=>"Treinamento duro",
    "raund_1"=>"1/8 final da 1° parte",
    "raund_2"=>"1/8 final da 2° parte",
    "raund_3"=>"1/4 final da 1° parte",
    "raund_4"=>"1/4 final da 2° parte",
    "raund_5"=>"1° parte da semifinal",
    "raund_6"=>"2° parte da semifinal",
    "raund_7"=>"final",
    "raund"=>"Rodada",
    "live_ht"=>"Resultado no Intervalo",
    "home_ranking"=>"Classificação",
    "top_rank_header"=>"Atualmente você está no",
    "top_rank_global"=>"Classificação global",
    "top_rank_local"=>"Classificação local",
    "top_rank_keep_going"=>"Continue!",
    "top_rank_all_time_ranking"=>"Classificação de todos os tempos",
    "top_rank_current_ranking"=>"Classificação Atual",
    "play_dialog_money"=>"Não há orçamento suficiente para iniciar o jogo",
    "transfer_not_open"=>"A janela de transferência não está aberta no momento.",
    "email_client"=>"Escolha um cliente de e-mail:",
    "next_match_start_text"=>"Os jogadores entram no estádio e começam a se aquecer…",
    "refill_energy_coin_player"=>"Você não tem EK suficiente para reabastecer a energia do jogador.",
    "refill_energy_coin_team"=>"Você não tem moedas EK suficientes para reabastecer a energia.",
    "refill_energy_coin_squad"=>"A escalação da partida deve ter 11 jogadores.",
    "refill_energy_coin_red"=>"Há um jogador suspenso ou lesionado na seleção. Por favor, faça uma alteração e tente novamente.",
    "refill_energy_coin_energy"=>"A energia dos jogadores é muito baixa. Reabasteça a energia, por favor.",
    "ambassador_success_header"=>"contrato de embaixador da marca assinado por 1 mês com",
    "ads_ambassador"=>"ETIHAD",
    "ambassador_header"=>"quer assinar contrato de embaixador da marca com",
    "ambassador_contract_length"=>"Duração do Contrato",
    "ambassador_duration"=>"1 mês",
    "ambassador_payment"=>"Pagamento",
    "ambassador_success_earned"=>"O jogador ganhou",
    "ambassador_success_club_earned"=>"Seu clube ganhou",
    "promotions_header"=>"Você tem um código de cupom?",
    "promotions_coupon_code"=>"Código do cupom",
    "tournament_rewards"=>"Recompensas do torneio:",
    "promotions_coupon_code_error"=>"O código do cupom está errado!",
    "promotions_coupon_code_dublicate"=>"Este cupom já está em uso!",
    "tournament_place"=>"Seu lugar",
    "challenge_wait"=>"Você ainda não chegou ao [level].",
    "stadium_header"=>"Estádio",
    "stadium_element_header"=>"Arena Etihad",
    "stadium_statistics"=>"Estatísticas",
    "stadium_list"=>"Estádios",
    "stadium_capacity"=>"Capacidade",
    "stadium_cost"=>"Custo",
    "stadium_upgrade"=>"Atualização",
    "stadium_level_update"=>"Você deseja atualizar",
    "stadium_level"=>"Nível",
    "stadium_dialog_header"=>"Atualizar o nível do estádio",
    "upgrade_stadium_level"=>"Atualizar o nível do estádio",
    "stadium_upgrade_success_mesage"=>"Sua atualização",
    "home_diary_success"=>"Seu presente diário!",
    "short_pool_question"=>"Responda à pergunta e obtenha",
    "short_pool_success_header"=>"Obrigado pela resposta!",
    "short_pool_reward"=>"Você ganhou:",
    "daily_training_non_select"=>"Você não escolheu nenhum jogador.",
    "empty_list_league_fixture"=>"A Liga ainda não começou.",
    "empty_list_league_result"=>"Todas as partidas foram jogadas.",
    "empty_list_league_top_scores"=>"@string/empty_list_league_fixture",
    "empty_list_friendly_fixture"=>"Nenhum jogo foi jogado.",
    "empty_list_friendly_result"=>"Nenhum jogo está organizado ainda.",
    "stadium_required"=>"Requisito",
    "stadium_dialog_money"=>"Não há orçamento suficiente para atualizar o estádio",
    "short_pool_question_one"=>"Quem marcará mais gols na temporada da Champions League de 2019/20?",
    "short_pool_question_two"=>"Qual time comemorará o próximo campeonato da Premier League? ",
    "short_pool_answer_one_one"=>"Kylian Mbappe",
    "short_pool_answer_one_two"=>"Karim Benzema",
    "short_pool_answer_two_one"=>"Manchester City",
    "short_pool_answer_two_two"=>"Liverpool",
    "daily_bonus_ok"=>"Uhuuu!",
    "daily_bonus_claim_now"=>"REIVINDICAR AGORA",
    "daily_bonus_text"=>"Volte a cada 3 horas para receber suas recompensas",
    "daily_bonus_header"=>"Seu Bônus",
    "daily_bonus_claiming"=>"Carregando…",
    "daily_bonus_claim"=>"REIVINDICAÇÃO",
    "coin_time_expired"=>"Expirado!",
    "coin_time_left"=>"Tempo restante",
    "coin_time_day"=>"dias",
    "buy_ek_coin_keep_rock"=>"Compre moedas EK e continue balançando!",
    "button_text_see_offers"=>"Ver ofertas",
    "win_dialog_text"=>"Vencedor do",
    "ranking_pop_up_place"=>"lugar",
    "bottom_menu_home"=>"Início",
    "bottom_menu_shop"=>"Loja",
    "bottom_menu_play_match"=>"Jogar partida",
    "bottom_menu_share"=>"Compartilhar",
    "bottom_menu_notification"=>"Notificação",
    "bottom_menu_menu"=>"Menu",
    "shop_player_coming_soon"=>"Em breve…",
    "notification"=>"Notificações",
    "notification_unread_header"=>"Não lida",
    "notification_archive_header"=>"Arquivo",
    "notification_empty_text"=>"Nenhuma notificação atual",
    "notification_common_info"=>"Suas transferências recentes, artigos de blog e outras notificações aparecerão aqui",
    "todo"=>"PARA FAZER",
    "home_new"=>"Novo",
    "tournament_winning_award"=>"Prêmios vencedores",
    "challenge_action_create"=>"Criar desafio",
    "challenge_warning"=>"Deseja criar esse desafio? Vai custar-lhe $",
    "tournament_opponent"=>"Oponentes",
    "tournament_entry_fee"=>"Taxa de entrada",
    "tournament_minute"=>"min",
    "tournament_hour"=>"horas",
    "tournament_day"=>"dia",
    "settings_bg_sound"=>"Música de fundo",
];
