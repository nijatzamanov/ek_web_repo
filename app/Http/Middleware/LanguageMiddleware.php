<?php

namespace App\Http\Middleware;

use Closure;

class LanguageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $segment = $request->segment(2);
        dd($segment);

        if ($request->method() === 'GET') {

            $segment = $request->segment(2);

            if (!array_key_exists($segment, config('app.locales'))) {
                $segments = $request->segments();
//                dd($segments);

                $fallback = session('locale') ?: config('app.fallback_locale');
//                $segments = array_prepend($segments, $fallback);

                return redirect()->back();
//                return redirect()->to('/en');
            } else {
                session(['locale' => $segment]);
                session(['language' => $segment]);
                app()->setLocale($segment);
//                dd(app()->getLocale());
            }
        }

        return $next($request);
    }
}
