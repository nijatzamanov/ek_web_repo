<?php

namespace App\Http\Controllers;

//use App\Model\Language;
use Illuminate\Http\Request;

class LanguageController extends Controller
{
    public function changeLanguage (Request $request, $code) {

        if ($request->method() === 'GET') {

            $segment = $request->segment(2);
            $prevLang = app()->getLocale();

            if (!array_key_exists($segment, config('app.locales'))) {
                $segments = $request->segments();
//                dd($request->segment(1));

                $fallback = session('locale') ?: config('app.fallback_locale');
                $segments = array_prepend($segments, $fallback);
                $url   = url()->previous();
                $url_explode = explode("/",$url);
                $url_explode[3] = $code;
                $redir = implode('/',$url_explode);
                return redirect()->back();

//                return redirect()->to($redir);
            }else{
                session(['locale' => $code]);
                session(['language' => $code]);
                app()->setLocale($code);
                return redirect()->back();
//                dd(app()->getLocale());

            }

        }

//        if ($exists) {
//            session(['language' => $code]);
//        }

        return back();
    }
}
