<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TrController extends Controller
{
    public function index()
    {

        $trDir = resource_path('langXml');

        foreach(glob($trDir.'/*.xml') as $file) {
            $path = $file;
            $fileNameArr = preg_split("#/#",$path);
            $fileName = $fileNameArr[count($fileNameArr)-1];
            $fileName = str_replace('.xml','',$fileName);
            if (!is_dir(resource_path('lang/'.$fileName.''))){
                mkdir(resource_path('lang/'.$fileName.''));
            }

            $filePath = resource_path('lang/'.$fileName.'/tr.php');


            $xml = simplexml_load_file($path);

            $handle = fopen($filePath, 'w+');
            $data = "<?php
                    return [
                ";

            foreach ($xml as $x) {
                foreach ($x->attributes() as $a => $b) {
                    $data .= '"' . $b . '" => "';
                }
                $x = $x[0];
                if (preg_match('/"/', $x))
                    $x = addslashes($x);
                $data .= '' . $x . '",' . PHP_EOL;
            }
            $data .= "];";

            fwrite($handle, $data);
        }

    }
}
