<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MarketingController extends Controller
{

    private $apiURL = 'marketing';

    public function getAllMarketingByUserId()
    {
        $uri = "$this->apiURL/all/".getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function removeUserSelectedMarketing(Request $request)
    {
        $uri = "$this->apiURL/remove/".getUserToken()."/".$request->marketingId;
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function setMarketing(Request $request)
    {
        $uri = "$this->apiURL/set/".getUserToken()."/".$request->marketingId;
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }
}
