<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PlayerController extends Controller
{
    public function edit()
    {
        return view("client.user.edit");
    }

    public function myProfile()
    {
        return view("client.user.myProfile");
    }

    public function settings()
    {
        return view("client.user.setting");
    }

    //new methods
    public function getPlayerInfo(Request $request)
    {
        foreach (session()->get("club")["players"] as $player){
            if($player["id"] == $request->playerId){
                return [ "data" => $player ];
            }
        }
    }
}
