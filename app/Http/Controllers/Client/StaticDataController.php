<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StaticDataController extends Controller
{
    private $apiURL = 'static';

    public function getCountries()
    {
        $uri = "$this->apiURL/country/all";
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function getFormations()
    {

    }

    public function getAllGameStyle()
    {

    }

    public function getAllTactics()
    {

    }

}
