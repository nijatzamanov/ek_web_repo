<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FriendlyController extends Controller
{

    private $apiURL = 'friendly';

    public function getFriendlyAllMatch()
    {
        $uri = "$this->apiURL/all/matches/".getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function declineMatch(Request $request)
    {
        $uri = "$this->apiURL/delete/match/".getUserToken()."/".$request->gameCode;
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function getNextMatch()
    {
        $uri = "$this->apiURL/next/match/".getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }else{
            return ['data' => null];
        }
    }

    public function playNextMatch()
    {
        $uri = "$this->apiURL/play/next/".getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function buildRandomClubForFriendlyGame(Request $request)
    {
        $uri = "$this->apiURL/random/club/".getUserToken()."/".$request->level;
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }
}
