<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{



    public function login()
    {
        return view('client.auth.login');
    }

    public function overview()
    {
        $countries = app('App\Http\Controllers\Client\StaticDataController')->getCountries();
        return view('client.home.overview',compact("countries"));
    }



}
