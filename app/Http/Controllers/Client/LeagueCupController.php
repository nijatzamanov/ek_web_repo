<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LeagueCupController extends Controller
{
    private $apiURL = 'league';

    public function getLeagueGames()
    {
        $uri = "$this->apiURL/games/".getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function getLeagueStanding()
    {
        $uri = "$this->apiURL/standing/".getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function getTopScorers()
    {
        $uri = "$this->apiURL/top/scorers/".getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }
}
