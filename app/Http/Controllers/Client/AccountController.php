<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App;
use PhpParser\Node\Expr\Cast\Object_;

class AccountController extends Controller
{
    private $apiURL = 'account';

    public function lang($locale, $id)
    {
        $langNames = ['English', 'Espanyol', 'Türkçe', 'Azərbaycan dili', 'Portuguese','عربى','Pусский'];
        App::setLocale($locale);
        session()->put('locale', $locale);
        session()->put('lang_name', $langNames[$id]);

        $uri = "$this->apiURL/update/".getUserToken();
        $res = apiPostRequest($uri,["sysLangId" => $id]);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return back();
        }

    }

}
