<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App;

class LoginController extends Controller
{

    private $apiURL = 'account';

    public function login(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|email',
            'password' => 'required|max:155'
        ]);
        $langCodes = ['en','es','tr','az','pt','ar','ru'];
        $langNames = ['English', 'Espanyol', 'Türkçe', 'Azərbaycan dili', 'Portuguese','عربى','Pусский'];
        $uri = "$this->apiURL/login/byEmail";
        $res = apiPostRequest($uri,$data);
        $res = json_decode($res,true);
        if ($res['code'] == 102){
            return ['state' => $res['message']];
        }
        if ($res['code'] == 100 && $res['message'] == 'success'){
            //Create sessions
            session()->put('authenticated',true);
            session()->put('user', $res['data']);

            //Change system language
//            $data["sysLangId"] = $res['lang_id'];
            $langId = $res['data']['langId'];
            App::setLocale($langCodes[$langId]);
            session()->put('locale', $langCodes[$langId]);
            session()->put('lang_name', $langNames[$langId]);

            return ['data' => $res];
        }
    }

    public function forget(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|email',
        ]);
        $data["sysLangId"] = 0;
        $uri = "$this->apiURL/forget/pwd/".$data['email'];
        $res = apiPostRequestUrlFields($uri);
        $res = json_decode($res,true);
        if ($res['code'] == 103)
            return ['state' => $res['message']];

        return ['data' => $res];
    }

    public function resendOTP(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|email',
        ]);
        $data["sysLangId"] = 0;
        $uri = "$this->apiURL/resend/otp/".$data['email'];
        $res = apiPostRequestUrlFields($uri);
        $res = json_decode($res,true);
        if ($res['code'] == 103)
            return ['state' => $res['message']];

        return ['data' => $res];
    }

    public function logout()
    {
        session()->flush();
        return redirect()->route('index');
    }

    public function verifyForget(Request $request)
    {

        $data = $request->validate([
            'email' => 'required|email',
            'code' => 'required|max:155',
        ]);
        $uri = "$this->apiURL/verify/pwd/code/".$data['email']."/".$data['code'];
        $res = apiPostRequestUrlFields($uri);
        $res = json_decode($res,true);
        if ($res['code'] == 100){
            return ['data' => $res];
        }
    }

    public function resetPwd(Request $request)
    {

        $data = $request->validate([
            'email' => 'required|email',
            'code' => 'required|max:155',
            'password' => 'required|max:155',
        ]);
        $uri = "$this->apiURL/reset/pwd/".$data['email']."/".$data['code']. "/".$data['password'];
        $res = apiPostRequestUrlFields($uri);
        $res = json_decode($res,true);
        if ($res['code'] == 100){
            return ['data' => $res];
        }
    }

}
