<?php

namespace App\Http\Controllers\Client\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Http\Request;

use App\Providers\RouteServiceProvider;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Kreait\Firebase\Auth as FirebaseAuth;
use Kreait\Firebase\Exception\FirebaseException;
use Illuminate\Validation\ValidationException;

class SocialLoginController extends Controller
{

//    use AuthenticatesUsers;

    protected $auth;
    protected $redirectTo = RouteServiceProvider::HOME;
    private $apiURL = 'account';
    private $claims = [];


    public function __construct(FirebaseAuth $auth)
    {
        $this->middleware('guest')->except('logout');
        $this->auth = $auth;
//        $this->auth = app('firebase.auth');

    }

    protected function login(Request $request)
    {
        try {
            $signInResult = $this->auth->signInWithEmailAndPassword($request['email'], $request['password']);
            $user = new User($signInResult->data());
            $result = Auth::login($user);
            return redirect($this->redirectTo);
        } catch (FirebaseException $e) {
            throw ValidationException::withMessages([$this->username() => [trans('auth.failed')],]);
        }
    }

    public function username()
    {
        return 'email';
    }

    public function handleCallback(Request $request, $provider)
    {
        $socialTokenId = $request->input('social-login-tokenId', '');
        try {

            $verifiedIdToken = $this->auth->verifyIdToken($socialTokenId);

            $data = [
                'fullName' => $verifiedIdToken->claims()->get('name'),
                'email' => $verifiedIdToken->claims()->get('email'),
                'transactionCode' => $verifiedIdToken->claims()->get('user_id'),
                'providerSecret' => $verifiedIdToken->claims()->get('user_id'),
                'sysLangId' => 0,
                'clubName'=> '',
                'countryId' => 1,
                'password' => $verifiedIdToken->claims()->get('user_id')
            ];

            $data['provider'] = strpos($verifiedIdToken->claims()->get('iss'),'google') ? 'google': 'facebook';

            $uri = "$this->apiURL/reg/social";

            $res = apiPostRequest($uri,$data);

            $res = json_decode($res,true);


            if ($res['code'] == 100 && $res['message'] == 'success'){

                session()->put('authenticated',true);
                session()->put('user', $res['data']);

                return redirect()->route('home');

            }elseif ($res['code'] == 105){

                $data = [
                    'email' => $verifiedIdToken->claims()->get('email'),
                    'password' => $verifiedIdToken->claims()->get('user_id')
                ];

                $data['provider'] = strpos($verifiedIdToken->claims()->get('iss'),'google') ? 'google': 'facebook';


                $uri = "$this->apiURL/login/social";

                $res = apiPostRequest($uri,$data);

                $res = json_decode($res,true);

                session()->put('authenticated',true);
                session()->put('user', $res['data']);

                return redirect()->route('home');
            }


        } catch (\InvalidArgumentException $e) {
            return redirect()->route('login');
        } catch (InvalidToken $e) {
            return redirect()->route('login');
        }
    }
}
