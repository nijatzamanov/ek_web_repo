<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProfileController extends Controller
{

    private $apiURL = 'account';


    public function index()
    {
        return view('client.index.index');
    }

    public function home()
    {
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("club",app('App\Http\Controllers\Client\ClubController')->getClubDetails()["data"]);
        session()->put("league_standing",app('App\Http\Controllers\Client\LeagueCupController')->getLeagueStanding()["data"]);
        session()->put("next_match",[app('App\Http\Controllers\Client\GameController')->getCLubNextMatch()["data"],'league']);
        //Rankings
        session()->put("local_ranks",app('App\Http\Controllers\Client\RankingController')->getLocalRankings()["data"]);
        session()->put("global_ranks",app('App\Http\Controllers\Client\RankingController')->getGlobalRankings()["data"]);

        session()->put("level",app('App\Http\Controllers\Client\StatisticsController')->getProfileStatistic()["data"]["managerLevel"]);


//        session()->put("league_games",app('App\Http\Controllers\Client\LeagueCupController')->getTopScorers()["data"]);
//        dd(session()->get("next_match"));
        return view('client.home.home');

    }

    public function playMatch()
    {
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("next_match",[app('App\Http\Controllers\Client\GameController')->getCLubNextMatch()["data"],'league']);
        return view('client.match.play-match');
    }

    public function squad()
    {
        session()->put("club",app('App\Http\Controllers\Client\ClubController')->getClubDetails()["data"]);
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("next_match",[app('App\Http\Controllers\Client\GameController')->getCLubNextMatch()["data"],'league']);

        return view("client.team.squad");
    }

    public function lineup()
    {
        session()->put("club",app('App\Http\Controllers\Client\ClubController')->getClubDetails()["data"]);
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("next_match",[app('App\Http\Controllers\Client\GameController')->getCLubNextMatch()["data"],'league']);

        return view("client.team.lineup");
    }

    public function daily()
    {
        session()->put("club",app('App\Http\Controllers\Client\ClubController')->getClubDetails()["data"]);
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("next_match",[app('App\Http\Controllers\Client\GameController')->getCLubNextMatch()["data"],'league']);

        return view("client.team.training.daily");
    }

    public function dailyCamp()
    {
        session()->put("club",app('App\Http\Controllers\Client\ClubController')->getClubDetails()["data"]);
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("next_match",[app('App\Http\Controllers\Client\GameController')->getCLubNextMatch()["data"],'league']);

        return view("client.team.training.daily-camp");
    }

    public function training()
    {
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("camps",app('App\Http\Controllers\Client\TrainingController')->trainingCampList()["data"]);
        session()->put("next_match",[app('App\Http\Controllers\Client\GameController')->getCLubNextMatch()["data"],'league']);

        return view("client.team.training.training");
    }

    public function trainingCamp()
    {
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("club",app('App\Http\Controllers\Client\ClubController')->getClubDetails()["data"]);
        session()->put("camps",app('App\Http\Controllers\Client\TrainingController')->trainingCampList()["data"]);
        session()->put("next_match",[app('App\Http\Controllers\Client\GameController')->getCLubNextMatch()["data"],'league']);

        return view("client.team.training.training-camp");
    }

    public function transfer()
    {
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("club",app('App\Http\Controllers\Client\ClubController')->getClubDetails()["data"]);
        session()->put("next_match",[app('App\Http\Controllers\Client\GameController')->getCLubNextMatch()["data"],'league']);

        return view("client.team.transfer");
    }

    public function stadium()
    {
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("stadiums",app('App\Http\Controllers\Client\StadiumController')->getAllStadium()["data"]);
        session()->put("stadium",app('App\Http\Controllers\Client\StadiumController')->getCurrentStadium()["data"]);
        session()->put("stats",app('App\Http\Controllers\Client\StadiumController')->statisticUserStadium()["data"]);

        return view("client.match.stadium");
    }


    public function statistics()
    {
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("stats",app('App\Http\Controllers\Client\StatisticsController')->getClubStatistic()["data"]);
        session()->put("next_match",[app('App\Http\Controllers\Client\GameController')->getCLubNextMatch()["data"],'league']);

        return view("client.statistics.statistics");
    }

    public function leagueMatch()
    {
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("league_standing",app('App\Http\Controllers\Client\LeagueCupController')->getLeagueStanding()["data"]);
        session()->put("results_and_fixtures",app('App\Http\Controllers\Client\LeagueCupController')->getLeagueGames()["data"]);
        session()->put("scorers",app('App\Http\Controllers\Client\LeagueCupController')->getTopScorers()["data"]);
        session()->put("next_match",[app('App\Http\Controllers\Client\GameController')->getCLubNextMatch()["data"],'league']);

        return view("client.match.league-match");
    }

    public function cup()
    {
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("league_games",app('App\Http\Controllers\Client\LeagueCupController')->getLeagueGames()["data"]);
        dd(session()->get("league_games"));
    }

    public function activeTournaments()
    {
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("active_challenges",app('App\Http\Controllers\Client\ChallengesController')->getActiveChallenges()["data"]);
        return view("client.match.challenges-active");
    }

    public function tournament($id)
    {
        $tournamentId = $id;
        session()->put("tournament_id", $tournamentId);
        session()->put("challenge_matches",app('App\Http\Controllers\Client\ChallengesController')->getChallengesAllMatches($tournamentId)["data"]);
        session()->put("next_match",[app('App\Http\Controllers\Client\ChallengesController')->getChallengesNextMatch($tournamentId)["data"],'tournament',$tournamentId]);
        return view("client.match.challenge", compact('id'));
    }

    public function tournamentMatch($id)
    {
        $tournamentId = $id;
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("next_match",[app('App\Http\Controllers\Client\ChallengesController')->getChallengesNextMatch($tournamentId)["data"],'tournament',$tournamentId]);
        return view("client.match.challenge-match");
    }

    public function friendly()
    {
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("friendly_matches",app('App\Http\Controllers\Client\FriendlyController')->getFriendlyAllMatch()["data"]);
        session()->put("next_match",[app('App\Http\Controllers\Client\FriendlyController')->getNextMatch()["data"],'friendly']);
        return view("client.match.friendly");
    }

    public function friendlyMatch()
    {
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("next_match",[app('App\Http\Controllers\Client\FriendlyController')->getNextMatch()["data"],'friendly']);
        return view("client.match.friendly-match");
    }

    public function coins()
    {
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("coins",app('App\Http\Controllers\Client\FinancesController')->getUserBudgetCoins()["data"]);
        session()->put("next_match",[app('App\Http\Controllers\Client\GameController')->getCLubNextMatch()["data"],'league']);

        return view("client.finance.coins");
    }

    public function budget()
    {
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("budget",app('App\Http\Controllers\Client\FinancesController')->getUserBudgetMoney()["data"]);
        session()->put("next_match",[app('App\Http\Controllers\Client\GameController')->getCLubNextMatch()["data"],'league']);

        return view("client.finance.budget");
    }

    public function marketing()
    {
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("marketings",app('App\Http\Controllers\Client\MarketingController')->getAllMarketingByUserId()["data"]);
        session()->put("next_match",[app('App\Http\Controllers\Client\GameController')->getCLubNextMatch()["data"],'league']);

        $marketingFalseSize = 0;

        foreach(session()->get("marketings") as $marketing){
            if($marketing["selected"] == false){
                $marketingFalseSize++;
            }
        }
        session()->get("marketing_false_size", $marketingFalseSize);
        return view("client.finance.marketing");
    }

    public function globalRanking()
    {
        session()->put("global_ranks",app('App\Http\Controllers\Client\RankingController')->getGlobalRankings()["data"]);
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("next_match",[app('App\Http\Controllers\Client\GameController')->getCLubNextMatch()["data"],'league']);

        return view("client.statistics.global-ranking");
    }

    public function localRanking()
    {
        session()->put("local_ranks",app('App\Http\Controllers\Client\RankingController')->getLocalRankings()["data"]);
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("next_match",[app('App\Http\Controllers\Client\GameController')->getCLubNextMatch()["data"],'league']);

        return view("client.statistics.global-ranking");
    }

    public function settings()
    {
        session()->put("next_match",[app('App\Http\Controllers\Client\GameController')->getCLubNextMatch()["data"],'league']);
        return view("client.user.setting");
    }


    public function edit(Request $request)
    {
//        dd(session()->get('user'));
        session()->put("next_match",[app('App\Http\Controllers\Client\GameController')->getCLubNextMatch()["data"],'league']);
        $sessionUser = session()->get('user');

        if (isset($request->avatar)){
            $image = $request->file('avatar');
            $imageName = Str::slug($sessionUser['fullName']).'-'.time().$image->getClientOriginalName();
            $image->move(public_path('uploads'),$imageName);

        }


        if ($request->isMethod('post')){
            $validated = $request->validate([
                'fullName' => 'required|max:155',
                'password' => 'max:155',
                'newPassword' => 'max:155'
            ]);
            $data = [
                'fullName' => $validated['fullName']
            ];
            if ($request->password && $request->newPassword){
                $data =[
                    'email' => $sessionUser['email'],
                    'password' => $request->password
                ];
                $uri = "$this->apiURL/login/byEmail";
                $res = apiPostRequest($uri,$data);
                $res = json_decode($res,true);

                if ($res['code'] == 102){
                    return  back()->withErrors(['password' => 'Current password is wrong']);
                }
                if ($res['code'] == 100 && $res['message'] == 'success'){
                    session()->put('authenticated',true);
                    session()->put('user', $res['data']);
                }
                $data['newPassword']  = $request->newPassword;
            }

            $uri = "$this->apiURL/update/".$sessionUser['token'];
            $res = apiPostRequest($uri,$data);
            $res = json_decode($res,true);

            if ($res['code'] == 100 && $res['message'] == 'success'){
                session()->put('authenticated',true);
                session()->put('user', $res['data']);
            }

            return back();
        }

        return view("client.user.edit");
    }


    public function myProfile()
    {
        session()->put("finance",app('App\Http\Controllers\Client\FinancesController')->getUserTotalBudgets()["data"]);
        session()->put("profile_data",app('App\Http\Controllers\Client\StatisticsController')->getProfileStatistic()["data"]);
        session()->put("next_match",[app('App\Http\Controllers\Client\GameController')->getCLubNextMatch()["data"],'league']);

        return view("client.user.myprofile");
//        dd( session()->get("profile_data") );
    }
}
