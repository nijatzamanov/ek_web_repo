<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PrivateTournamentController extends Controller
{
    private $apiURL = 'private/tournaments';

    public function applyTournament()
    {

    }

    public function createTournament()
    {

    }

    public function getPrivateTournamentMatrix()
    {

    }

    public function getMyJoinedTournament()
    {

    }

    public function searchTournament()
    {

    }

}
