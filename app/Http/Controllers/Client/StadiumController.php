<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StadiumController extends Controller
{
    private $apiURL = 'stadium';

    public function getAllStadium()
    {
        $uri = "$this->apiURL/all/".getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function getCurrentStadium()
    {
        $uri = "$this->apiURL/all/".getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            foreach($res["data"] as $stadium){
                if($stadium["selected"] == true){
                    return ['data' => $stadium];
                }
            }
        }
    }

    public function statisticUserStadium()
    {
        $uri = "$this->apiURL/statistic/".getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function updateUserStadium(Request $request)
    {
        $uri = "$this->apiURL/update/".getUserToken().'/'.intval($request->level);
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

}
