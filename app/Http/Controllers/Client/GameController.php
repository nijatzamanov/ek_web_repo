<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use function Couchbase\defaultDecoder;

class GameController extends Controller
{
    private $apiURL = 'game';

    public function reBuildSeason()
    {
        $uri = "$this->apiURL/build/season/" . getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res, true);

        if ($res['code'] == 100) {
            return ['data' => $res["data"]];
        }
    }

    public function getCLubNextMatch()
    {
        $uri = "$this->apiURL/next/match/" . getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res, true);

        if ($res['code'] == 100) {
            return ['data' => $res["data"]];
        } else {
            return ['data' => null];
        }
    }

    public function playMatch()
    {
        $uri = "$this->apiURL/play/match/" . getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res, true);

        if ($res['code'] == 100) {
            return ['data' => $res["data"]];
        }

        return response()->json([
            'message' => $res['message'],
            'data'    => $res['data'],
            'error'   => 1,
            'code' => $res['code']
        ],503);

    }

}
