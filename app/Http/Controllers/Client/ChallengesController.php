<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ChallengesController extends Controller
{
    private $apiURL = 'challenge';


    public function getChallengesAllMatches($tournamentId)
    {
        $uri = "$this->apiURL/all/matches/".getUserToken()."/".intval($tournamentId);
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function buildChallengeSeason(Request $request)
    {
        $uri = "$this->apiURL/build/challenge/".getUserToken()."/".intval($request->tournamentId);
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function getActiveChallenges()
    {
        $uri = "$this->apiURL/list/".getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function getChallengesNextMatch($tournamentId)
    {
        $uri = "$this->apiURL/next/matche/".getUserToken().'/'.$tournamentId;
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function playChallengeMatch($tournamentId)
    {
        $uri = "$this->apiURL/play/match/".getUserToken().'/'.$tournamentId;
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

}
