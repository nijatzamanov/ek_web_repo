<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    private $apiURL = 'account';

    public function register(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|email',
            'password' => 'required|max:155',
            'clubName' => 'required|max:155',
            'fullName' => 'required|max:155',
            'countryId' => 'required|max:155',
        ]);
        $data["sysLangId"] = 0;
        $uri = "$this->apiURL/reg/byEmail";
        $res = apiPostRequest($uri,$data);
        return ['data' => $res];
    }

    public function verify(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|email',
            'key' => 'required|max:155',
        ]);
        $uri = "$this->apiURL/reg/active/".$data['key']."/".$data['email'];
        $res = apiPostRequestUrlFields($uri);
        $res = json_decode($res,true);
        if ($res['code'] == 100){
            return ['data' => $res];
        }
    }
}
