<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use MongoDB\Driver\Session;

class ClubController extends Controller
{

    private $apiURL = 'club';

    public function getClubDetails()
    {
        $uri = "$this->apiURL/details/".getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function getClubDetailsById()
    {


    }

    public function getClubPlayers()
    {
        $uri = "$this->apiURL/players/".getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function updateClubFormation(Request $request)
    {
        $uri = "$this->apiURL/update/formation/".getUserToken()."/".$request->formationId;
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function updateClubTactics(Request $request)
    {
        $uri = "$this->apiURL/update/tactics/".getUserToken();

        $clubTactics = [
            "gameStyleId" => $request->clubTactics[0],
            "passingStyleId" => $request->clubTactics[1],
            "pressingStyleId" => $request->clubTactics[2],

        ];

        $res = apiPostRequest($uri,$clubTactics);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }

    }


}
