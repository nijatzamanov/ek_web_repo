<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TrainingController extends Controller
{

    private $apiURL = 'training';

    public function doTrainingCamp(Request $request)
    {
        $uri = "$this->apiURL/camp/do/".getUserToken()."/".$request->campId;
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function trainingCampList()
    {
        $uri = "$this->apiURL/camp/list";
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function getTrainingCoinsLimit(Request $request)
    {
        $uri = "$this->apiURL/coins/limit/".$request->trainingStyleId;

        $playerIds = explode(",", $request->checkedList);

        $res = apiPostRequest($uri,$playerIds);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }

    }

    public function doTraining(Request $request)
    {
        $uri = "$this->apiURL/do/".getUserToken()."/".$request->trainingStyleId;

        $playerIds = explode(",", $request->checkedList);

        $res = apiPostRequest($uri,$playerIds);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

}
