<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    private $apiURL = 'team';

    public function autoSelectTeamPlayers()
    {
        $uri = "$this->apiURL/lineup/auto-select/".getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function getNewTeam()
    {
        $uri = "$this->apiURL/lineup/clr/main/p/".getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function removePlayerFromMainList(Request $request)
    {
        $uri = "$this->apiURL/lineup/remove/player/".getUserToken()."/".$request->playerId;
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function saveMainListPlayer(Request $request)
    {
        $uri = "$this->apiURL/lineup/save/main/p/".getUserToken();


        $res = apiPostRequest($uri,$request->playerIds);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function recoveryPlayer(Request $request)
    {
        $uri = "$this->apiURL/recovery/player/".getUserToken().'/'.intval($request->playerIds);


        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

//        var_dump($playerIds);
        if($res['code'] == 100){
            return ['data' => $res["data"]];
//            dump($res["data"]);
        }

//        dd($res);

    }

    public function refillEnergyForMainPlayers()
    {
        $uri = "$this->apiURL/refill/".getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function refillEnergyForOnePlayer(Request $request)
    {
        $uri = "$this->apiURL/refill/".getUserToken()."/".$request->playerId;
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function refillEnergyForOnePlayerLimitCoin(Request $request)
    {
        $uri = "$this->apiURL/refill/coin/limit/one/".getUserToken()."/".$request->playerId;
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function refillEnergyCoinsLimit()
    {
        $uri = "$this->apiURL/refill/coins/limit/".getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }
}
