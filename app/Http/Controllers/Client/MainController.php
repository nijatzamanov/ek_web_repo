<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class MainController extends Controller
{
    public function index()
    {
        if(Session::has('authenticated')){
            return redirect("/home");
        }else{
            return view('client.index.index');
        }
    }
}
