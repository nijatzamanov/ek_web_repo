<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TransferController extends Controller
{
    private $apiURL = 'transfer';

    public function getAllTransfers()
    {
        $uri = "$this->apiURL/all/".getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function buildNewSalePlayerList()
    {
        $uri = "$this->apiURL/build/inSales/".getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function buyNewPlayer(Request $request)
    {
        $uri = "$this->apiURL/buy/".getUserToken()."/".$request->playerCode."/".$request->price;
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function buySpacialPlayer()
    {

    }

    public function disablePlayer()
    {

    }

    public function sellPlayer(Request $request)
    {
        $uri = "$this->apiURL/sell/".getUserToken()."/".$request->playerId;
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);

        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function getSpacialPlayers()
    {

    }
}
