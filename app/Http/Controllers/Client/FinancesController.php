<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FinancesController extends Controller
{

    private $apiURL = 'finance';

    public function ambassador()
    {

    }

    public function getUserBudgetCoins()
    {
        $uri = "$this->apiURL/budget/coins/".getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);
        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function getUserBudgetMoney()
    {
        $uri = "$this->apiURL/budget/money/".getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);
        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function getUserTotalBudgets()
    {
//        dd(session()->get('user'));
        $uri = "$this->apiURL/budget/total/".getUserToken();
        $res = apiGetRequest($uri);
        $res = json_decode($res,true);
        if($res['code'] == 100){
            return ['data' => $res["data"]];
        }
    }

    public function changeCoinsToMoney()
    {

    }

    public function changeCoinsToMoneyByMatrix()
    {

    }


    public function getDailyGiftCoins()
    {

    }

    public function purchaseCoins()
    {

    }

    public function getTimeDailyGiftCoins()
    {

    }

    public function useCoupon()
    {

    }

}
