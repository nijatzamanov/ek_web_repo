<?php

use App\Model\Setting;
use App\Model\Translation;
use App\Model\Message;
use GuzzleHttp\Client;

if (!function_exists('getSetting')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function getSetting($name)
    {
        $setting = Setting::getSetting($name);
        if (isset($setting))
            return $setting->value;
        else {
            $setting = Setting::create([
                'name' => $name,
                'value' => 0
            ]);
            return $setting->value;
        }
    }
}




if (!function_exists('getTranslation')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function getTranslation($name, $lang = null)
    {
        $translation = Translation::with('translation')->where('name',$name)->first();

        if (!isset($lang))
            $lang = app()->getLocale();

        if (isset($translation)){
            if (isset($translation->translation->first()->value))
                return $translation->translation->first()->value;
            else{
                $message = Message::create([
                    'translation_id' => $translation->id,
                    'lang' => $lang,
                    'value' => $name
                ]);

                return $message->value;
            }
        }else{
            $translation = Translation::create([
                'name' => $name
            ]);
            $message = Message::create([
                'translation_id' => $translation->id,
                'lang' => $lang,
                'value' => $name
            ]);

            return $message->value;
        }
    }
}


if (!function_exists('getAllLanguages')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function getAllLanguages()
    {
      $langs = \App\Model\Language::getAllLanguages();
      return $langs;
    }
}

if (!function_exists('apiPostRequest')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function apiPostRequest(string $uri, array $data)
    {
        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ];

        $baseAPI = env('BASE_APIURL');
        $data = json_encode($data);
        $uri = "$baseAPI/$uri";
        $client = new Client([
            'headers' => $headers,
        ]);
        $client = $client->request('POST',"$uri",['body' => $data, 'auth' => [env('BASE_API_USERNAME'), env('BASE_API_PASSWORD')]]);
        return $client->getBody()->getContents();
    }
}

if (!function_exists('getUserToken')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function getUserToken()
    {
        return session()->get("user")["token"];
    }
}


if (!function_exists('apiGetRequest')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function apiGetRequest(string $uri)
    {
        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ];

        $baseAPI = env('BASE_APIURL');
        $uri = "$baseAPI/$uri";
        $client = new Client([
            'headers' => $headers,
        ]);
        $client = $client->request('GET',"$uri",['auth' => [env('BASE_API_USERNAME'), env('BASE_API_PASSWORD')]]);
        return $client->getBody()->getContents();
    }
}

if (!function_exists('apiPostRequestUrlFields')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function apiPostRequestUrlFields(string $uri)
    {
        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ];

        $baseAPI = env('BASE_APIURL');
        $uri = "$baseAPI/$uri";
        $client = new Client([
            'headers' => $headers,
        ]);
        $client = $client->request('POST',"$uri",['auth' => [env('BASE_API_USERNAME'), env('BASE_API_PASSWORD')]]);
        return $client->getBody()->getContents();
    }
}
