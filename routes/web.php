<?php

use Illuminate\Support\Facades\Route;
//session()->flush();

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Route::get('/translate', 'TrController@index');

Route::group(['prefix' => 'language_change'], function (){
    Route::get('/{code}/', 'LanguageController@changeLanguage')->name('change_language');
});

Route::get('social', function(){
   return redirect("/");
});
/**
 *
 */

Route::group(['namespace' => 'Client'], function (){

    /**
     * Register and Sign Routes
     */



    Route::post('register', 'RegisterController@register');
    Route::post('verify', 'RegisterController@verify');
    Route::get('/signin', 'HomeController@login')->name('login');
    Route::get("/getCountries","StaticDataController@getCountries")->name("getCountries");
    Route::get('/logout', 'LoginController@logout')->name('client.logout');
    Route::post('acc/login', 'LoginController@login');

    Route::group(['prefix' => 'account'], function (){
        Route::post('/forget/pwd', 'LoginController@forget');
        Route::post('/verify/pwd/code', 'LoginController@verifyForget');
        Route::post('/reset/pwd', 'LoginController@resetPwd');
        Route::post("/resend/otp",'LoginController@resendOTP');
    });

});



Route::group(['middleware' => 'apiauth'], function (){

    /**
     * all routes required authentication will be here
     */

//    Route::get('/home', 'Client\MyController@home')->name('home'); //+

    Route::get('lang/{locale}/{id}', 'Client\AccountController@lang');

    Route::get("/squad",'Client\MyController@squad')->name('squad');
    Route::get('/home', 'Client\ProfileController@home')->name('home'); //+
    Route::get("/squad",'Client\ProfileController@squad')->name('squad'); //+ - Refill qalib

    Route::get("/lineup",'Client\ProfileController@lineup')->name('lineup'); //+

    Route::get("/daily",'Client\ProfileController@daily')->name('daily');
    Route::get("/daily-camp",'Client\ProfileController@dailyCamp')->name('dailyCamp');

    Route::get("/training",'Client\ProfileController@training')->name('training');
    Route::get("/training-camp",'Client\ProfileController@trainingCamp')->name('trainingCamp');

    Route::get("/transfer",'Client\ProfileController@transfer')->name('transfer');
    Route::get("/statistics",'Client\ProfileController@statistics')->name('statistics');
    Route::get("/stadium",'Client\ProfileController@stadium')->name('stadium');
    Route::get("/league-match",'Client\ProfileController@leagueMatch')->name('leagueMatch');
    Route::get("/cup",'Client\ProfileController@cup')->name('leaugeCup');
    Route::get("/play-match",'Client\ProfileController@playMatch')->name('playMatch');


    Route::get("/tournaments",'Client\ProfileController@activeTournaments')->name('tournaments');
    Route::get("/tournament/{id}", 'Client\ProfileController@tournament')->name('tournament');
    Route::get("/tournament-match/{tournamentId}",'Client\ProfileController@tournamentMatch')->name('tournamentMatch');

    Route::get("/friendly",'Client\ProfileController@friendly')->name('friendly');
    Route::get("/friendly-match",'Client\ProfileController@friendlyMatch')->name('friendlyMatch');

    Route::get('/coins', 'Client\ProfileController@coins')->name('coins');
    Route::get("/budget",'Client\ProfileController@budget')->name('budget');
    Route::get("/marketing",'Client\ProfileController@marketing')->name('marketing');
    Route::get("/global-ranking",'Client\ProfileController@globalRanking')->name('gRanking');
    Route::get("/local-ranking",'Client\ProfileController@localRanking')->name('lRanking');


    Route::get("/settings",'Client\ProfileController@settings')->name('settings');


    Route::any("/edit",'Client\ProfileController@edit')->name('edit');
    Route::get("/my-profile",'Client\ProfileController@myProfile')->name('myProfile');

    //Ajax requests
        //ClubController
        Route::get('/getClubData','Client\ClubController@getClubDetails');
        Route::get('/player/u','Client\ClubController@getClubPlayers');
        Route::post('/club/update/formation/','Client\ClubController@updateClubFormation');
        Route::post('/club/update/tactics','Client\ClubController@updateClubTactics');

        //FriendlyController
        Route::get("/friendly/all/matches","Client\FriendlyController@getFriendlyAllMatch");
        Route::get("/friendly/next/match","Client\FriendlyController@getNextMatch");
        Route::get("/friendly/play/next","Client\FriendlyController@playNextMatch");
        Route::get("/friendly/random/club","Client\FriendlyController@buildRandomClubForFriendlyGame");
        Route::get("/friendly/delete/match","Client\FriendlyController@declineMatch");

        //FinanceController
        Route::get('/getUserBudget','Client\FinancesController@getUserBudgetCoins');
        Route::get('/getUserTotalBudgets','Client\FinancesController@getUserTotalBudgets');


    //ChallengeController
        Route::get('/getAllChallengeMatches','Client\ChallengesController@getChallengesAllMatches');
        Route::get("/createNewSession","Client\ChallengesController@buildChallengeSeason");
        Route::get("/getAllChallengeMatches/{tournamentId}","Client\ChallengesController@getChallengesNextMatch");
        Route::get("/playNextChallengeMatch/{tournamentId}","Client\ChallengesController@playChallengeMatch");

        //PlayerController
        Route::get("/player","Client\PlayerController@getPlayerInfo");

        //TeamController
        Route::get("/team/lineup/auto-select","Client\TeamController@autoSelectTeamPlayers");
        Route::get("/team/lineup/clr/main/p","Client\TeamController@getNewTeam");
        Route::post("/team/lineup/remove/player","Client\TeamController@removePlayerFromMainList");
        Route::post("/team/lineup/saveMainList","Client\TeamController@saveMainListPlayer");
        Route::post("/team/recovery/player","Client\TeamController@recoveryPlayer");

        Route::get("/team/refill/coins/limit","Client\TeamController@refillEnergyCoinsLimit");
        Route::get("/team/refill/one/cost","Client\TeamController@refillEnergyForOnePlayerLimitCoin");
        Route::get("/team/refill/one","Client\TeamController@refillEnergyForOnePlayer");
        Route::get("/team/refill","Client\TeamController@refillEnergyForMainPlayers");

        //GamesController
        Route::get("/build/new","Client\GameController@reBuildSeason");
        Route::get("/games","Client\GameController@playMatch");
        Route::get("game/next/match","Client\GameController@getCLubNextMatch");

        //TrainingController
        Route::post("/getTrainingCost","Client\TrainingController@getTrainingCoinsLimit");
        Route::post("/start/training","Client\TrainingController@doTraining");
        Route::get("/start/camp","Client\TrainingController@doTrainingCamp");

        //StadiumController
        Route::get("/stadium-upgrade","Client\StadiumController@updateUserStadium");

        //TransferController
        Route::get("/transfer/all","Client\TransferController@getAllTransfers");
        Route::get("/transfer/buildNewList","Client\TransferController@buildNewSalePlayerList");
        Route::post("/transfer/sellPlayer","Client\TransferController@sellPlayer");
        Route::post("/transfer/buyPlayer","Client\TransferController@buyNewPlayer");

        //MarketingController
        Route::get("/marketing/choose","Client\MarketingController@setMarketing");
        Route::get("/marketing/cancel","Client\MarketingController@removeUserSelectedMarketing");

});


Route::get('/', 'Client\MainController@index')->name('index');
Route::get('/overview', 'Client\HomeController@overview')->name('overview');



Route::post('login/{provider}/callback', 'Client\Auth\SocialLoginController@handleCallback');


Route::get('privacy.html', function (){
    return view('client.staticPages.privacy');
});
