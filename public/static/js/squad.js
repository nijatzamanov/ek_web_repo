$(document).ready(function () {


    $('body').on('click', '.ek--squad--table--body--player--opener', function (e) {

        var playerId = $(this).attr("data-id");


        console.log("Player id:" + playerId);
        $.ajax({
            url: '/player',
            type: 'get',
            data: {
                playerId: playerId
            },
            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    console.log("!data");
                }
                else if (res.hasOwnProperty("data")) {

                    // $.each(Object.keys(res.data), function (i, o) {
                    //
                    // })
                    console.log("country:" + res.data.country.code);
                    console.log("name:" + res.data.name);


                    $('#refillOne').attr('data-id', playerId);


                    $('#ek--notification #playerName').html(res.data.name);

                    $('#mCountry').html('<img src="static/img/Flags/' + res.data.country.code + '.svg" alt="">\n' +
                        '<span class="text--white ek--size--20--400">' + res.data.country.code + '</span>');


                    if (res.data.positionId == 1) {

                        var variable = '<div class="ek--squad--modals--head--about--position bg--orange--dark text--navy--900 ek--size--16--500" >Goalkeeper</div>';

                    }

                    if (res.data.positionId == 2) {

                        var variable = '<div class="ek--squad--modals--head--about--position bg--yellow text--navy--900 ek--size--16--500" >Defender</div>\n';

                    }

                    if (res.data.positionId == 3) {

                        var variable = '<div class="ek--squad--modals--head--about--position bg--cyan--two text--navy--900 ek--size--16--500">Midfielder</div>\n';

                    }


                    if (res.data.positionId == 4) {

                        var variable = '<div class="ek--squad--modals--head--about--position bg--purple text--navy--900 ek--size--16--500" >Forward</div>';

                    }

                    if(res.data.salary < 1000){
                        $('#mSalary').html(Math.ceil(res.data.salary));
                    }

                    else if(res.data.salary >= 1000 && res.data.salary < 1000000){
                        $('#mSalary').html(parseInt(res.data.salary/1000)+"K");
                    }

                    else if(res.data.salary >= 1000000 && res.data.salary < 1000000000){
                        $('#mSalary').html(parseInt(res.data.salary/1000)+"M");
                    }

                    else if(res.data.salary >= 1000000000){
                        $('#mSalary').html(parseInt(res.data.salary/1000)+"B");
                    }


                    $('#mplayerName').html(res.data.name);
                    $('#mPosition').html(variable);
                    $('#mPlayerAbility').html(Math.round(res.data.ability));
                    $('#mPlayerAge').html(res.data.age);
                    $('#mPlayerEnergy').html(res.data.energy);
                    $('#mRedCard').html(res.data.redCard);
                    $('#mYellowCard').html(res.data.yellowCard);
                    $('#mGoals').html(res.data.goals);
                    $('#mAppearance').html(res.data.appearance);


                    if (res.data.value < 1000) {

                        $('#value').html('<p class="ek--size--28--500 text--white" >' + res.data.value + '<span class="ek--squad--table--body--value text--white " ></span></p>\n');
                    }

                    else if (res.data.value >= 1000 && res.data.value < 1000000) {
                        var money = (res.data.value) / 1000;


                        $('#value').html('<p class="ek--size--28--500 text--white" >' + parseInt(money) + '<span class="ek--squad--table--body--value text--white ">K</span></p>\n');

                    }

                    else if (res.data.value >= 1000000 && res.data.value < 1000000000) {

                        var money = (res.data.value) / 1000000;
                        console.log(money);
                        console.log(res.data.value);

                        $('#value').html('<p class="ek--size--28--500 text--white" >' + parseInt(money) + '<span class="ek--squad--table--body--value text--white ">M</span></p>\n');

                    }

                    else if (res.data.value >= 1000000000) {

                        var money = res.data.value / 1000000000;
                        console.log(money);

                        $('#value').html('<p class="ek--size--28--500 text--white" >' + parseInt(money) + '<span class="ek--squad--table--body--value text--white ">B</span></p>\n');

                    }

                    $('#value').append('<h4 class="ek--size--16 text--navy--200">Value</h4>');


                    if (res.data.energy <= 100 && res.data.energy > 90) {

                        var progress = '<div class="progress-bar bg--green--dark--two" role="progressbar" style="width:' + res.data.energy + '%" aria-valuemin="0" aria-valuemax="100"></div>\n';
                    }
                    else if (res.data.energy <= 90 && res.data.energy > 55) {

                        var progress = '<div class="progress-bar bg--green--two" role="progressbar" style="width:' + res.data.energy + '%" aria-valuemin="0" aria-valuemax="100"></div>\n';
                    }


                    else if (res.data.energy <= 55 && res.data.energy > 25) {

                        var progress = '<div class="progress-bar bg--yellow--two" role="progressbar" style="width:' + res.data.energy + '%"   aria-valuemin="0" aria-valuemax="100"></div>\n';


                    }


                    else if (res.data.energy <= 25) {
                        var progress = '<div class="progress-bar bg--orange" role="progressbar" style="width:' + res.data.energy + '%"   aria-valuemin="0" aria-valuemax="100"></div>';

                    }


                    $('#mProgress').html(progress);


                    if (res.data.energy != 100) {
                        var button = ' <button class="ek--squad--modals--inner--refill ek--squad--modals--inner--refill--energy bg--blue text--white" id="refillOneCost" data-id="">Refill Energy</button>';


                    }

                    else if (res.data.energy == 100) {

                        var button = '<button class="ek--squad--modals--inner--refill  btn-secondary "  " disabled >Refill Energy</button>';
                    }

                    $('#mButtons').html(button);

                    $('#refillOneCost').attr('data-id', playerId);


                    $('#ek--squad--modals--player').css('display', 'flex');

                }


            },
        });


        e.preventDefault()
    });


    $('body').on('click', '#refillOneCost', function (e) {

        var playerId = $(this).attr("data-id");
        console.log("Player id:" + playerId);
        $.ajax({
            url: '/team/refill/one/cost',
            type: 'get',
            data: {
                playerId: playerId
            },
            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    console.log("!data");
                }
                else if (res.hasOwnProperty("data")) {

                    $('.ek--squad--modals--player').parent().hide();
                    $('.ek--squad--modals--player').hide();
                    $('.ek--squad--modals--refill--head--want--amount #cost').html('<span id="userCoins" class="text--white ek--size--16">' + res.data + '</span>');


                    $('#ek--squad--modals--refill--buy').show();
                    console.log("success:");
                }


            },
        });


        e.preventDefault()
    });


    $('body').on('click', '#refillOne', function (e) {

        var playerId = $(this).attr("data-id");
        console.log("Player id:" + playerId);

        $.ajax({
            url: '/getUserTotalBudgets',
            type: 'get',

            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    $('#ek--soft--notenough').modal('show');
                }
                else if (res.hasOwnProperty("data")) {

                    console.log(res.data)
                    var coins = $('#userCoins').text();
                    console.log("userCoins:" + coins);
                    console.log("res.data.coins:" + res.data.coins);
                    if (parseInt(res.data.coins) < parseInt(coins)) {

                        $('#ek--soft--notenough').modal('show');
                        console.log("Not enough");
                    }
                    else {
                        // console.log(res.data.coins + " "+ coins)
                        console.log("Enough");

                        $.ajax({
                            url: '/team/refill/one',
                            type: 'get',
                            data: {
                                playerId: playerId
                            },
                            success: function (res) {
                                if (!res.hasOwnProperty("data")) {
                                    $('#k--soft--notenough').modal('show');
                                }
                                else if (res.hasOwnProperty("data")) {


                                    $.ajax({
                                        url: '/getUserBudget',
                                        type: 'get',
                                        success: function (res) {
                                            if (!res.hasOwnProperty("data")) {
                                                console.log("!data");
                                            }
                                            else if (res.hasOwnProperty("data")) {

                                                if (res.data.money < 1000) {

                                                    $('.ek--body--head--money #money').html('<span class="text--white ek--size--16">' + res.data.money + '</span>');
                                                }

                                                else if (res.data.money >= 1000 && res.data.money < 1000000) {

                                                    var money = res.data.money / 1000;

                                                    $('.ek--body--head--money #money').html('<span class="text--white ek--size--16">' + parseInt(money) + '</span><span class="text--white ek--size--16">K</span>');

                                                }

                                                else if (res.data.money >= 1000000 && res.data.money < 1000000000) {

                                                    var money = (res.data.money) / 1000000;
                                                    console.log(money);
                                                    console.log(res.data.money);

                                                    $('.ek--body--head--money #money').html('<span class="text--white ek--size--16">' + parseInt(money) + '</span><span class="text--white ek--size--16">M</span>');

                                                }

                                                else if (res.data.money >= 1000000000) {

                                                    var money = res.data.money / 1000000000;
                                                    console.log(money);

                                                    $('.ek--body--head--money #money').html('<span class="text--white ek--size--16">' + parseInt(money) + '</span><span class="text--white ek--size--16">B</span>');

                                                }
                                                $('.ek--body--head--money #coins').html('<span class="text--white ek--size--16">' + res.data.coins + '</span>');

                                                console.log("success:");
                                            }


                                        },
                                    });

                                    $('#ek--squad--modals--refill--buy').hide();

                                    $('#ek--notification').css('display', 'flex');


                                    console.log("notification");


                                    e.preventDefault();


                                }


                            },
                        });
                    }


                }


            },
        });


    });


    $('body').on('click', '#squadClose', function (e) {


        $('#ek--notification').css('display', 'none');

        $('#loading').css('display', 'flex');

        // function load() {
        //     $('#loading').show();
        // }
        // window.onload=load();
        //
        location.reload();

    })


});
