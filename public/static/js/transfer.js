$(document).ready(function () {

    getAllTransfers();
    getPlayers();
    getBudget();


    $('body').on('click', '#buyPlayerModalLink', function (e) {

        var playerCode = $(this).attr("data-id");

        console.log("playerCode" + playerCode);

        $('#ek--transfer--modal' + playerCode).modal('show');


        e.preventDefault()
    });

    $('body').on('click', '#openSecBuyModal', function (e) {

        var playerCode = $(this).attr("data-id");

        console.log("playerCode" + playerCode);
        $('#ek--transfer--modal' + playerCode).modal('hide');
        $('#ek--transfer--buy' + playerCode).modal('show');


        e.preventDefault()
    });


    $('body').on('click', '#historyLink', function (e) {

        var count = $(this).attr("data-id");

        console.log("count: " + count);

        $('#ek--transfer--history' + count).modal('show');


        e.preventDefault()
    });


    $('body').on('click', '#openSecSellModal', function (e) {

        var playerId = $(this).attr("data-id");

        console.log("playerId" + playerId);

        $('#ek--transfer--sell' + playerId).modal('show');


        e.preventDefault()
    });


    $('body').on('click', '#sellPlayerModalLink', function (e) {

        var playerId = $(this).attr("data-id");

        console.log("playerCode" + playerId);

        $('#ek--transfer--sell--modal' + playerId).modal('show');


        e.preventDefault()
    });




    $('body').on('click', '#buyPlayer', function (e) {

        var playerCode = $(this).attr("data-id");

        console.log("playerCode" + playerCode);

        /*$('#ek--transfer--buy' + playerCode).modal('show');*/


        $.ajax({
            url: '/getUserBudget',
            type: 'get',

            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    $('#ek--soft--notenough').modal('show');
                }
                else if (res.hasOwnProperty("data")) {
                    $('#ek--transfer--modal' + playerCode).modal('show');
                    var coins = $('#ek--transfer--modal' + playerCode + ' #userMoney').text();
                    console.log("userMoney" + coins);
                    var number = $('#ek--transfer--modal' + playerCode + ' #amount').text();
                    console.log("num:" + number);

                    if (number == 'K') {
                        var resultCoin = parseInt(coins) * 1000;
                    }
                    else if (number == 'M') {
                        var resultCoin = parseInt(coins) * 1000000;
                    }

                    else if (number == 'B') {
                        var resultCoin = parseInt(coins) * 1000000000;
                    }

                    else if (number == '') {
                        var resultCoin = parseInt(coins);
                    }
                    console.log("resultCoin:" + parseInt(resultCoin));
                    if (parseInt(res.data.money) < parseInt(resultCoin)) {

                        $('#ek--soft--notenough').modal('show');
                    }
                    else {

                        $.ajax({
                            url: '/transfer/buyPlayer',
                            type: 'post',
                            data: {
                                "_token": $('meta[name="csrf"]').attr('content'),
                                price: resultCoin,
                                playerCode: playerCode
                            },
                            success: function (res) {
                                if (!res.hasOwnProperty("data")) {
                                    // $('#ek--soft--notenough').modal('show');

                                    $('#ek--soft--state #text').html(res.state);

                                    $('#ek--soft--state').modal('show');

                                    console.log("!data");
                                }
                                else if (res.hasOwnProperty("data")) {

                                    // window.location.pathname = '/transfer';
                                    $('#ek--transfer--buy' + playerCode).modal('hide');
                                    $('#ek--transfer--succes' +playerCode).modal('show');


                                     $('#buyList').html('');
                                     $('#historyList').html('');
                                     $('#sellList').html('');

                                     getAllTransfers();
                                     getPlayers();
                                     getBudget();
                                    // $('#ek--transfer--modal' + playerCode).modal('hide');

                                    console.log("success:");
                                }

                            },
                        });
                    }


                }


            },
        });


        e.preventDefault()
    });


    $('body').on('click', '#sellPlayer', function (e) {


        var playerId = $(this).attr("data-id");
        console.log("playerId" + playerId);


        // $('#ek--transfer--sell--modal' + playerId).modal('show');


        $.ajax({
            url: '/transfer/sellPlayer',
            type: 'post',
            data: {
                "_token": $('meta[name="csrf"]').attr('content'),
                playerId: playerId
            },
            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    // $('#ek--soft--notenough').modal('show');
                    $('#ek--soft--state #text').html(res.state);

                    $('#ek--soft--state').modal('show');
                }
                else if (res.hasOwnProperty("data")) {

                    $('#ek--transfer--sell' + playerId).modal('hide');
                    $('#ek--transfer--sell--success').modal('show');
                    $('#sellList').html('');
                    $('#buyList').html('');
                    $('#historyList').html('');


                    getAllTransfers();
                    getPlayers();
                    getBudget();


                    console.log("success:");
                }

            },
        });


        e.preventDefault()
    });


    // players to buy and history
    function getAllTransfers() {

        $.ajax({
            url: '/game/next/match',
            type: 'get',
            success: function (res) {
                // console.log(res)
                if(res.data.round == 2 || res.data.round == 11 || res.data.round == 21){
                    $.ajax({
                        url: '/transfer/buildNewList',
                        type: 'get',
                        success: function (res) {
                            console.log(res)
                        }
                    })
                }

                $.ajax({
                    url: '/transfer/all',
                    type: 'get',

                    success: function (res) {
                        if (!res.hasOwnProperty("data")) {
                            console.log("!data");
                        }
                        else if (res.hasOwnProperty("data")) {

                            console.log("data:" + res.data);
                              var count=0;

                            $.each(res.data.histories, function (i, o) {

                                count++;

                                if(o.transferStatus==2){
                                    var svgDiv='<svg  class="icon icon-arrow_drop_up fill--green"><use xlink:href="static/img/icons.svg#icon-arrow_drop_up"></use></svg>';


                                }

                                if(o.transferStatus!=2){
                                    var svgDiv='<svg class="icon icon-arrow_drop_up fill--red"><use xlink:href="static/img/icons.svg#icon-arrow_drop_down"></use></svg>';


                                }



                                if(o.value <1000){

                                    var historyModalValueDiv='<span class="ek--size--28--500 text--white" id="userMoney" >'+ o.value +'<span class="ek--size--28--500 text--white" id="amount"></span></span>' ;
                                    var historyValueDiv='<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >'+ o.value +'<span class="ek--size--16 text--white"></span></span>' ;

                                }

                                if(o.value >=1000 && o.value < 1000000){

                                    var historyModalValueDiv='<span class="ek--size--28--500 text--white" ><span class="ek--size--28--500 text--white" id="userMoney">'+ parseInt(o.value/1000) +'</span> <span class="ek--size--28--500 text--white" id="amount">K</span></span>';
                                    var historyValueDiv='<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >'+ parseInt(o.value/1000) +'<span class="ek--size--16 text--white">K</span></span>' ;

                                }
                                if(o.value >=1000000 && o.value < 1000000000){

                                    var historyModalValueDiv='<span class="ek--size--28--500 text--white"> <span class="ek--size--28--500 text--white" id="userMoney"> '+ parseInt(o.value/1000000) +'</span><span class="ek--size--28--500 text--white" id="amount">M</span></span>';
                                    var historyValueDiv='<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >'+ parseInt(o.value/1000000) +'<span class="ek--size--16 text--white">M</span></span>' ;
                                }
                                if(o.value  >=1000000000){

                                    var historyModalValueDiv='<span class="ek--size--28--500 text--white"  ><span class="ek--size--28--500 text--white" id="userMoney"  > '+ parseInt(o.value/1000000000) +'</span> <span class="ek--size--28--500 text--white" id="amount">B</span></span>';
                                    var historyValueDiv='<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >'+ parseInt(o.value/1000000000) +'<span class="ek--size--16 text--white">B</span></span>' ;


                                }

                                 if(o.positionId==1){
                                    var posDiv='<div class="ek--squad--modals--head--about--position bg--orange--dark text--navy--900 ek--size--16--500">'+ o.position + '</div>';
                                 }

                                if(o.positionId==2){
                                    var posDiv='<div class="ek--squad--modals--head--about--position bg--yellow text--navy--900 ek--size--16--500">'+ o.position + '</div>';
                                }

                                if(o.positionId==3){
                                    var posDiv='<div class="ek--squad--modals--head--about--position bg--cyan--two text--navy--900 ek--size--16--500">'+ o.position + '</div>';
                                }

                                if(o.positionId==4){
                                    var posDiv='<div class="ek--squad--modals--head--about--position bg--purple text--navy--900 ek--size--16--500">'+ o.position + '</div>';
                                }


                                $('#historyList').append('<div class="ek--transfer--body--tabs--inner bg--navy--600" id="historyLink" data-id="'+ count +'">\n' +
                                    '                        <div class="ek--transfer--body--tabs--inner--pos">\n' +
                                    '                            <div class="ek--transfer--body--tabs--inner--pos--type ek--size--16 text--white">' + svgDiv +
                                    '                            </div>\n' +
                                    '                        </div>\n' +
                                    '                        <div class="ek--transfer--body--tabs--inner--player ek--size--16 text--navy--200">\n' +
                                    '                            <!--<img src="static/img/Flags/ek&#45;&#45;aze.svg" alt="ek&#45;&#45;aze" />-->\n' +
                                    '                            <img src="static/img/Flags/'+ o.country.code +'.svg" alt="">\n' +
                                    '                            <a class="ek--size--16 text--white" >'+ o.playerName +'</a>\n' +
                                    '                            <!-- Ek Transfer Player Modal -->\n' +
                                    '                            <div id="ek--transfer--history' + count +'"  class="ek--squad--modals--player bg--transparent modal" style="box-shadow: unset">\n' +
                                    '                                <div class="ek--close">\n' +
                                    '                                    <a class="ek--close--icon bg--navy--700" href="#" rel="modal:close">\n' +
                                    '                                        <svg class="icon icon-ek--close--icon fill--navy--200"><use xlink:href="static/img/icons.svg#icon-ek--close--icon"></use></svg>\n' +
                                    '                                    </a>\n' +
                                    '                                </div>\n' +
                                    '                                <div class="ek--transfer--body--tabs--inner--main bg--navy--700">\n' +
                                    '                                    <!-- Ek Body Squad Player Modal Head -->\n' +
                                    '                                <div class="ek--squad--modals--head">\n' +
                                    '                                    <!-- Ek Body Squad Player Modal Head Image -->\n' +
                                    '                                    <div class="ek--squad--modals--head--image bg--navy--900">\n' +
                                    '                                        <img src="static/img/Other/ek--null--image.png" alt="">\n' +
                                    '                                    </div>\n' +
                                    '                                    <!-- End Ek Body Squad Player Modal Head Image -->\n' +
                                    '                                    <!-- Ek Body Squad Player Modal Head About -->\n' +
                                    '                                    <div class="ek--squad--modals--head--about">\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Head About Name -->\n' +
                                    '                                        <h3 class="ek--size--20--400 text--white" >'+ o.playerName +'</h3>\n' +
                                    '                                        <!-- End Ek Body Squad Player Modal Head About Name -->\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Head About Country -->\n' +
                                    '                                        <div class="ek--squad--modals--head--about--country">\n' +
                                    '                                            <img src="static/img/Flags/'+ o.country.code +'.svg" alt="">\n' +
                                    '                                            <span class="text--white ek--size--20--400" >'+ o.country.code +'</span>\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Player Modal Head About Country -->\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Head About Position -->\n' + posDiv +
                                    '                                        <!-- End Ek Body Squad Player Modal Head About Position -->\n' +
                                    '                                    </div>\n' +
                                    '                                    <!-- End Ek Body Squad Player Modal Head About -->\n' +
                                    '                                </div>\n' +
                                    '                                <!-- End Ek Body Squad Player Modal Head -->\n' +
                                    '                                <!-- Ek Body Squad Player Modal Inner -->\n' +
                                    '                                <div class="ek--squad--modals--inner">\n' +
                                    '                                    <!-- Ek Body Squad Player Modal Inner Feature -->\n' +
                                    '                                    <div class="ek--squad--modals--inner--feature">\n' +
                                    '                                        <div class="ek--squad--modals--inner--feature--age">\n' +
                                    '                                            <p class="ek--size--28--500 text--white" >'+ o.age +'</p>\n' +
                                    '                                            <h4 class="ek--size--16 text--navy--200">Age</h4>\n' +
                                    '                                        </div>\n' +
                                    '                                        <div class="ek--squad--modals--inner--feature--abl">\n' +
                                    '                                            <p class="ek--size--28--500 text--white">'+ Math.ceil(o.ability) +'</p>\n' +
                                    '                                            <h4 class="ek--size--16 text--navy--200">Abl</h4>\n' +
                                    '                                        </div>\n' +
                                    '                                        <div class="ek--squad--modals--inner--feature--value">\n' +
                                    '                                            <p class="ek--size--28--500 text--white">\n' +
                                    '                                                <span class="ek--size--28--500 text--white">$</span>\n' + historyModalValueDiv+
                                    '                                            </p>\n' +
                                    '                                            <h4 class="ek--size--16 text--navy--200">Value</h4>\n' +
                                    '                                        </div>\n' +
                                    '                                    </div>\n' +
                                    '                                    <!-- End Ek Body Squad Player Modal Inner Feature -->\n' +
                                    '                                    <!-- Ek Body Squad Player Modal Inner Values -->\n' +
                                    '                                    <div class="ek--squad--modals--inner--values">\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Inner Values Sections Energy -->\n' +
                                    '                                        <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                            <div class="ek--squad--modals--inner--values--section--left">\n' +
                                    '                                                <svg class="icon icon-ek--player--country fill--green"><use xlink:href="static/img/icons.svg#icon-ek--player--country"></use></svg>\n' +
                                    '                                                <span class="ek--size--16 text--navy--200">Country</span>\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Inner Values Sections Right -->\n' +
                                    '                                            <div class="ek--squad--modals--inner--values--section--right">\n' +
                                    '                                                <img src="static/img/Flags/'+ o.country.code +'.svg" alt="">\n' +
                                    '                                                <span class="text--white ek--size--16--500" >'+ o.country.code +'</span>\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections Right -->\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Player Modal Inner Values Sections Energy -->\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Inner Values Sections Goals -->\n' +
                                    '                                        <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                            <div class="ek--squad--modals--inner--values--section--left">\n' +
                                    '                                                <svg class="icon icon-ek--ball fill--green"><use xlink:href="static/img/icons.svg#icon-ek--ball"></use></svg>\n' +
                                    '                                                <span class="ek--size--16 text--navy--200">Goals</span>\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                            <span class="text--white ek--size--16--500 ">'+ o.goals +'</span>\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Player Modal Inner Values Sections Goals -->\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Inner Values Sections Played Match -->\n' +
                                    '                                        <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                            <div class="ek--squad--modals--inner--values--section--left">\n' +
                                    '                                                <svg class="icon icon-ek--stadium fill--green"><use xlink:href="static/img/icons.svg#icon-ek--stadium"></use></svg>\n' +
                                    '                                                <span class="ek--size--16 text--navy--200">Played Match</span>\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                            <span class="text--white ek--size--16--500" >'+ o.appearance +'</span>\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Player Modal Inner Values Sections Played Match -->\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Inner Values Sections Red Card -->\n' +
                                    '                                        <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                            <div class="ek--squad--modals--inner--values--section--left">\n' +
                                    '                                                <svg class="icon icon-ek--card fill--red"><use xlink:href="static/img/icons.svg#icon-ek--card"></use></svg>\n' +
                                    '                                                <span class="ek--size--16 text--navy--200">Red Card</span>\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                            <span class="text--white ek--size--16--500">'+ o.redCards +'</span>\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Player Modal Inner Values Sections Red Card  -->\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Inner Values Sections Red Card -->\n' +
                                    '                                        <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                            <div class="ek--squad--modals--inner--values--section--left">\n' +
                                    '                                                <svg class="icon icon-ek--card fill--yellow"><use xlink:href="static/img/icons.svg#icon-ek--card"></use></svg>\n' +
                                    '                                                <span class="ek--size--16 text--navy--200">Yellow Card</span>\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                            <span class="text--white ek--size--16--500 ">' + o.yellowCards  +'</span>\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Player Modal Inner Values Sections Red Card  -->\n' +
                                    '                                        \n' +
                                    '                                    </div>\n' +
                                    '                                    <!-- End Ek Body Squad Player Modal Inner Values -->\n' +
                                    '                                </div>\n' +
                                    '                                <!-- End Ek Body Squad Player Modal Inner -->\n' +
                                    '                                </div>\n' +
                                    '                                <!-- Ek Transfer History Modal -->\n' +
                                    '                                <div class="ek--transfer--body--tabs--inner--history bg--navy--700">\n' +
                                    '                                    <div class="ek--transfer--body--tabs--inner--history--left">\n' +
                                    '                                        <h4 class="ek--size--16 text--navy--200">Transfer to:</h4>\n' +
                                    '                                        <p class="ek--size--18 text--white" >'+ o.toClub +'<span class="ek--size--16"> <img src="static/img/Flags/'+ o.country.code +'.svg "/>'+ o.country.code + '</span></p>\n' +
                                    '                                    </div>\n' +
                                    '                                    <div class="ek--transfer--body--tabs--inner--history--right">\n' + svgDiv+
                                    '                                        <p class="text--navy--200 ek--size--16" >'+ o.transferDate.substr(0,10)  + '</p>\n' +
                                    '\n' +
                                    '                                    </div>\n' +
                                    '                                </div>\n' +
                                    '                                <!-- End Ek Transfer History Modal -->\n' +
                                    '                            </div>\n' +
                                    '                            <!-- End Ek Transfer Player Modal -->\n' +
                                    '                        </div>\n' +
                                    '                        <div class="ek--transfer--body--tabs--inner--age ek--size--16 text--white" th:text="${history.age} ">18</div>\n' +
                                    '                        <div class="ek--transfer--body--tabs--inner--abl ek--size--16 text--white" th:text="${#numbers.formatDecimal(history.ability,0,0)} ">80</div>\n' +
                                    '                        <div class="ek--transfer--body--tabs--inner--price ek--size--16 text--white" >\n' +
                                    '                            <span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white">$</span>'+ historyValueDiv+
                                    '\n' +
                                    '                        </div>\n' +
                                    '                    </div>');

                            });

                            console.log(res.data)
                            if(res.data.buyList.length != 0) {
                                $.each(res.data.buyList, function (i, o) {


                                    if (o.positionId == 1) {

                                        var posDiv = '<div class="ek--squad--modals--head--about--position bg--orange--dark text--navy--900 ek--size--16--500">' + o.position + '</div>';
                                        var positionDiv = '<div class="ek--transfer--body--tabs--inner--pos--type bg--orange--dark ek--size--16 text--white" >G</div>';

                                    }
                                    if (o.positionId == 2) {

                                        var posDiv = '<div class="ek--squad--modals--head--about--position bg--yellow text--navy--900 ek--size--16--500">' + o.position + '</div>';
                                        var positionDiv = '<div class="ek--transfer--body--tabs--inner--pos--type bg--yellow ek--size--16 text--white">D</div>';

                                    }

                                    if (o.positionId == 3) {

                                        var posDiv = '<div class="ek--squad--modals--head--about--position bg--cyan--two text--navy--900 ek--size--16--500">' + o.position + '</div>';
                                        var positionDiv = '<div class="ek--transfer--body--tabs--inner--pos--type bg--cyan--two ek--size--16 text--white" >M</div>';

                                    }
                                    if (o.positionId == 4) {

                                        var posDiv = '<div class="ek--squad--modals--head--about--position bg--purple text--navy--900 ek--size--16--500">' + o.position + '</div>';
                                        var positionDiv = '<div class="ek--transfer--body--tabs--inner--pos--type bg--purple ek--size--16 text--white" th:if="${buyPlayer.positionId == 4}">F</div>'

                                    }


                                    if (o.value < 1000) {

                                        var modalValueDiv = '<span class="ek--size--28--500 text--white" id="userMoney" >' + o.value + '<span class="ek--size--28--500 text--white" id="amount"></span></span>';
                                        var valueDiv = '<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >' + o.value + '<span class="ek--size--16 text--white"></span></span>';

                                    }

                                    if (o.value >= 1000 && o.value < 1000000) {

                                        var modalValueDiv = '<span class="ek--size--28--500 text--white" ><span class="ek--size--28--500 text--white" id="userMoney">' + parseInt(o.value / 1000) + '</span> <span class="ek--size--28--500 text--white" id="amount">K</span></span>';
                                        var valueDiv = '<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >' + parseInt(o.value / 1000) + '<span class="ek--size--16 text--white">K</span></span>';

                                    }
                                    if (o.value >= 1000000 && o.value < 1000000000) {

                                        var modalValueDiv = '<span class="ek--size--28--500 text--white"> <span class="ek--size--28--500 text--white" id="userMoney"> ' + parseInt(o.value / 1000000) + '</span><span class="ek--size--28--500 text--white" id="amount">M</span></span>';
                                        var valueDiv = '<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >' + parseInt(o.value / 1000000) + '<span class="ek--size--16 text--white">M</span></span>';
                                    }
                                    if (o.value >= 1000000000) {

                                        var modalValueDiv = '<span class="ek--size--28--500 text--white"  ><span class="ek--size--28--500 text--white" id="userMoney"  > ' + parseInt(o.value / 1000000000) + '</span> <span class="ek--size--28--500 text--white" id="amount">B</span></span>';
                                        var valueDiv = '<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >' + parseInt(o.value / 1000000000) + '<span class="ek--size--16 text--white">B</span></span>';


                                    }


                                    $('#buyList').append('<div class="ek--transfer--body--tabs--inner bg--navy--600" id="buyPlayerModalLink" data-id="' + o.playerCode + '" >' +
                                        '<div class="ek--transfer--body--tabs--inner--pos">' + positionDiv + '</div>' +
                                        '<div class="ek--transfer--body--tabs--inner--player ek--size--16 text--navy--200">' +
                                        '<img src="static/img/Flags/' + o.country.code + '.svg" alt="">' +
                                        '<a class="ek--size--16 text--white">' + o.playerName + '</a>' +
                                        '<div  id="ek--transfer--modal' + o.playerCode + '" class="ek--squad--modals--player bg--navy--700 modal">' +
                                        '<div class="ek--close">' +
                                        '<a class="ek--close--icon bg--navy--700" href="#" rel="modal:close">' +
                                        '<svg class="icon icon-ek--close--icon fill--navy--200"><use xlink:href="static/img/icons.svg#icon-ek--close--icon"></use></svg></a></div>' +
                                        '<div class="ek--squad--modals--head">' +
                                        '<div class="ek--squad--modals--head--image bg--navy--900">' +
                                        '<img src="static/img/Other/ek--null--image.png" alt=""></div>' +
                                        '<div class="ek--squad--modals--head--about">' +
                                        '<h3 class="ek--size--20--400 text--white" >' + o.playerName + '</h3>\n' +
                                        '<div class="ek--squad--modals--head--about--country">\n' +
                                        '<img src="static/img/Flags/' + o.country.code + '.svg" alt="">\n' +
                                        '<span class="text--white ek--size--20--400" >' + o.country.code + '</span></div>\n' + posDiv +
                                        '</div></div>' +
                                        '<div class="ek--squad--modals--inner">' +
                                        '<div class="ek--squad--modals--inner--feature">' +
                                        '<div class="ek--squad--modals--inner--feature--age">' +
                                        '<p class="ek--size--28--500 text--white">' + o.age + '</p>' +
                                        '<h4 class="ek--size--16 text--navy--200">Age</h4></div>' +
                                        '<div class="ek--squad--modals--inner--feature--abl">' +
                                        '<p class="ek--size--28--500 text--white" >' + Math.ceil(o.ability) + '</p>\n' +
                                        '<h4 class="ek--size--16 text--navy--200">Abl</h4></div>' +
                                        '<div class="ek--squad--modals--inner--feature--value">' +
                                        '<p class="ek--size--28--500 text--white"  >' +
                                        '<span class="ek--size--28--500 text--white">$</span>' + modalValueDiv +
                                        '</p><h4 class="ek--size--16 text--navy--200">Value</h4></div></div>' +
                                        '<div class="ek--squad--modals--inner--values">' +
                                        '<div class="ek--squad--modals--inner--values--section bg--navy--600">' +
                                        '<div class="ek--squad--modals--inner--values--section--left">' +
                                        '<svg class="icon icon-ek--player--country fill--green"><use xlink:href="static/img/icons.svg#icon-ek--player--country"></use></svg>\n' +
                                        '<span class="ek--size--16 text--navy--200">Country</span></div>' +
                                        '<div class="ek--squad--modals--inner--values--section--right">' +
                                        '<img src="static/img/Flags/' + o.country.code + '.svg" alt="">' +
                                        '<span class="text--white ek--size--16--500" >' + o.country.code + '</span>' +
                                        '</div></div>' +

                                        '<div class="ek--squad--modals--inner--values--section bg--navy--600">' +
                                        '<div class="ek--squad--modals--inner--values--section--left">' +
                                        '<svg class="icon icon-ek--ball fill--green"><use xlink:href="static/img/icons.svg#icon-ek--ball"></use></svg>' +
                                        '<span class="ek--size--16 text--navy--200">Goals</span></div>' +
                                        '<span class="text--white ek--size--16--500" >' + o.goals + '</span></div>' +
                                        '<div class="ek--squad--modals--inner--values--section bg--navy--600"><div class="ek--squad--modals--inner--values--section--left">\n' +
                                        '<svg class="icon icon-ek--stadium fill--green"><use xlink:href="static/img/icons.svg#icon-ek--stadium"></use></svg>\n' +
                                        '<span class="ek--size--16 text--navy--200">Played Match</span></div>\n' +
                                        '<span class="text--white ek--size--16--500">' + o.appearance + '</span>\n' +
                                        '</div>\n' +
                                        '<div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                        '<div class="ek--squad--modals--inner--values--section--left">\n' +
                                        '<svg class="icon icon-ek--card fill--red"><use xlink:href="static/img/icons.svg#icon-ek--card"></use></svg>\n' +
                                        '<span class="ek--size--16 text--navy--200">Red Card</span></div>\n' +
                                        '<span class="text--white ek--size--16--500" >' + o.redCards + '</span></div>\n' +
                                        '<div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                        '<div class="ek--squad--modals--inner--values--section--left">\n' +
                                        '<svg class="icon icon-ek--card fill--yellow"><use xlink:href="static/img/icons.svg#icon-ek--card"></use></svg>\n' +
                                        '<span class="ek--size--16 text--navy--200" >Yellow Card</span></div>' +


                                        '<span class="text--white ek--size--16--500" >' + o.yellowCards + '</span></div>' +
                                        '</div>' +
                                        '<a class="ek--squad--modals--inner--refill bg--blue text--white" data-id="' + o.playerCode + '"  href="#" id="openSecBuyModal">Buy player</a>' +
                                        '</div></div>' +
                                        '<div id="ek--transfer--buy' + o.playerCode + '" class="ek--squad--modals--refill bg--navy--700 modal">' +
                                        '<div class="ek--squad--modals--refill--head">' +
                                        '<div class="ek--squad--modals--refill--head--energy bg--navy--600">' +
                                        '<img src="static/img/Other/ek--null--image.png" alt="null">' +
                                        '</div>\n' +
                                        '<div class="ek--squad--modals--refill--head--want">' +
                                        '<h4 class="text--white ek--size--20">Buy player</h4>\n' +
                                        '<p>Do you want to buy <span>' + o.playerName + '</span>?</p></div></div>\n' +
                                        '<div class="ek--squad--modals--refill--buttons">\n' +
                                        '<a class="ek--squad--modals--refill--buttons--cancel text--white ek--size--16 bg--navy--600" href="#ek--squad--modals--player" rel="modal:open">Cancel</a>\n' +
                                        '<input type="hidden" />\n' +
                                        '<!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;energy text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;blue" href="#ek&#45;&#45;transfer&#45;&#45;succes" rel="modal:open">Buy player</a>-->\n' +
                                        '<button class="ek--squad--modals--refill--buttons--energy text--white ek--size--16 bg--blue" id="buyPlayer"  data-id="' + o.playerCode + '">Buy player</button>\n' +
                                        '</div>\n' +
                                        '<a href="#close-modal" rel="modal:close" class="close-modal ">Close</a></div>\n' +
                                        '<div id="ek--transfer--succes' + o.playerCode + '" class="ek--squad--modals--refill ek--modal--border--left--green--dark--two bg--navy--700 modal">\n' +
                                        '<div class="ek--squad--modals--refill--head">\n' +
                                        '<div class="ek--squad--modals--refill--head--energy bg--green--dark--two">\n' +
                                        '<svg class="icon icon-ek--correct fill--white"><use xlink:href="static/img/icons.svg#icon-ek--correct"></use></svg>\n' +
                                        '</div>\n' +
                                        '<div class="ek--squad--modals--refill--head--want">\n' +
                                        '<h4 class="text--white ek--size--20">You bought the player</h4>\n' +
                                        '<p><span> ' + o.playerName + '</span> successfully added to the team</p></div></div>\n' +
                                        '<div class="ek--squad--modals--refill--buttons">\n' +
                                        '<a class="ek--squad--modals--refill--buttons--cancel text--white ek--size--16 bg--navy--600" href="#ek--squad--modals--player" rel="modal:open">Cancel</a>\n' +
                                        '</div>\n' +
                                        '<a href="#close-modal" rel="modal:close" class="close-modal ">Close</a>' +
                                        '</div></div>' +
                                        '<div class="ek--transfer--body--tabs--inner--age ek--size--16 text--white" >' + o.age + '</div>\n' +
                                        '<div class="ek--transfer--body--tabs--inner--abl ek--size--16 text--white" >' + Math.ceil(o.ability) + '</div>\n' +
                                        // '<div class="ek--transfer--body--tabs--inner--matches ek--size--16 text--white" >'+ o.appearance +'</div>\n' +
                                        '<div class="ek--transfer--body--tabs--inner--goals ek--size--16 text--white" >' + o.goals + '</div>' +
                                        '<div class="ek--transfer--body--tabs--inner--price ek--size--16 text--white">\n' +
                                        '<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white">$</span>' + valueDiv +
                                        '</div></div>');


                                })
                            }else{
                                $(".ek--transfer--body--tabs--buy--head").html("");
                                $('#buyList').append(`<h3 class="text--white text--center">The transfer window is not currently open.</h3>`)
                            }
                        }


                    },
                });

            }
        })




        // e.preventDefault();

    }

    // players to sell
    function getPlayers() {

        $.ajax({
            url: '/player/u',
            type: 'get',

            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    console.log("!data");
                }
                else if (res.hasOwnProperty("data")) {
                    $.each(res.data, function (i, o) {

                        if(o.positionId==1) {
                            if (o.value < 1000) {

                                var sellModalValueDiv = '<span class="ek--size--28--500 text--white" id="userMoney" >' + o.value + '<span class="ek--size--28--500 text--white" id="amount"></span></span>';
                                var sellValueDiv = '<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >' + o.value + '<span class="ek--size--16 text--white"></span></span>';

                            }

                            if (o.value >= 1000 && o.value < 1000000) {

                                var sellModalValueDiv = '<span class="ek--size--28--500 text--white" ><span class="ek--size--28--500 text--white" id="userMoney">' + parseInt(o.value / 1000) + '</span> <span class="ek--size--28--500 text--white" id="amount">K</span></span>';
                                var sellValueDiv = '<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >' + parseInt(o.value / 1000) + '<span class="ek--size--16 text--white">K</span></span>';

                            }
                            if (o.value >= 1000000 && o.value < 1000000000) {

                                var sellModalValueDiv = '<span class="ek--size--28--500 text--white"> <span class="ek--size--28--500 text--white" id="userMoney"> ' + parseInt(o.value / 1000000) + '</span><span class="ek--size--28--500 text--white" id="amount">M</span></span>';
                                var sellValueDiv = '<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >' + parseInt(o.value / 1000000) + '<span class="ek--size--16 text--white">M</span></span>';
                            }
                            if (o.value >= 1000000000) {

                                var sellModalValueDiv = '<span class="ek--size--28--500 text--white"  ><span class="ek--size--28--500 text--white" id="userMoney"  > ' + parseInt(o.value / 1000000000) + '</span> <span class="ek--size--28--500 text--white" id="amount">B</span></span>';
                                var sellValueDiv = '<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >' + parseInt(o.value / 1000000000) + '<span class="ek--size--16 text--white">B</span></span>';


                            }


                            $('#sellList').append('<div class="ek--transfer--body--tabs--inner bg--navy--600"  id="sellPlayerModalLink" data-id="'+o.id+'"   >\n' +
                                '                            <div class="ek--transfer--body--tabs--inner--pos">\n' +
                                '                                <div class="ek--transfer--body--tabs--inner--pos--type bg--orange--dark ek--size--16 text--white" >G</div>\n' +
                                '\n' +
                                '                            </div>\n' +
                                '                            <div class="ek--transfer--body--tabs--inner--player ek--size--16 text--navy--200">\n' +
                                '                                <!--<img src="static/img/Flags/ek&#45;&#45;aze.svg" alt="ek&#45;&#45;aze" />-->\n' +
                                '                                <img src="static/img/Flags/' + o.country.code + '.svg" alt="">\n' +
                                '                                <a class="ek--size--16 text--white">' + o.name + '</a>\n' +
                                '                                <!-- Ek Transfer Player Modal -->\n' +
                                '                                <div id="ek--transfer--sell--modal' + o.id + '" class="ek--squad--modals--player bg--navy--700 modal">\n' +
                                '                                    <div class="ek--close">\n' +
                                '                                        <a class="ek--close--icon bg--navy--700" href="#" rel="modal:close">\n' +
                                '                                            <svg class="icon icon-ek--close--icon fill--navy--200"><use xlink:href="static/img/icons.svg#icon-ek--close--icon"></use></svg>\n' +
                                '                                        </a>\n' +
                                '                                    </div>\n' +
                                '                                    <!-- Ek Body Squad Player Modal Head -->\n' +
                                '                                    <div class="ek--squad--modals--head">\n' +
                                '                                        <!-- Ek Body Squad Player Modal Head Image -->\n' +
                                '                                        <div class="ek--squad--modals--head--image bg--navy--900">\n' +
                                '                                            <img src="static/img/Other/ek--null--image.png" alt="">\n' +
                                '                                        </div>\n' +
                                '                                        <!-- End Ek Body Squad Player Modal Head Image -->\n' +
                                '                                        <!-- Ek Body Squad Player Modal Head About -->\n' +
                                '                                        <div class="ek--squad--modals--head--about">\n' +
                                '                                            <!-- Ek Body Squad Player Modal Head About Name -->\n' +
                                '                                            <h3 class="ek--size--20--400 text--white">' + o.name + '</h3>\n' +
                                '                                            <!-- End Ek Body Squad Player Modal Head About Name -->\n' +
                                '                                            <!-- Ek Body Squad Player Modal Head About Country -->\n' +
                                '                                            <div class="ek--squad--modals--head--about--country">\n' +
                                '                                                <img src="static/img/Flags/' + o.country.code + '.svg" alt="">\n' +
                                '                                                <span class="text--white ek--size--20--400" >' + o.country.code + '</span>\n' +
                                '                                            </div>\n' +
                                '                                            <!-- End Ek Body Squad Player Modal Head About Country -->\n' +
                                '                                            <!-- Ek Body Squad Player Modal Head About Position -->\n' +
                                '                                            <div class="ek--squad--modals--head--about--position bg--orange--dark text--navy--900 ek--size--16--500">Goalkeeper</div>\n' +
                                '                                            <!-- End Ek Body Squad Player Modal Head About Position -->\n' +
                                '                                        </div>\n' +
                                '                                        <!-- End Ek Body Squad Player Modal Head About -->\n' +
                                '                                    </div>\n' +
                                '                                    <!-- End Ek Body Squad Player Modal Head -->\n' +
                                '                                    <!-- Ek Body Squad Player Modal Inner -->\n' +
                                '                                    <div class="ek--squad--modals--inner">\n' +
                                '                                        <!-- Ek Body Squad Player Modal Inner Feature -->\n' +
                                '                                        <div class="ek--squad--modals--inner--feature">\n' +
                                '                                            <div class="ek--squad--modals--inner--feature--age">\n' +
                                '                                                <p class="ek--size--28--500 text--white" >' + o.age + '</p>\n' +
                                '                                                <h4 class="ek--size--16 text--navy--200">Age</h4>\n' +
                                '                                            </div>\n' +
                                '                                            <div class="ek--squad--modals--inner--feature--abl">\n' +
                                '                                                <p class="ek--size--28--500 text--white" >' + Math.ceil(o.ability) + '</p>\n' +
                                '                                                <h4 class="ek--size--16 text--navy--200">Abl</h4>\n' +
                                '                                            </div>\n' +
                                '                                            <div class="ek--squad--modals--inner--feature--value">\n' +
                                '                                                <p class="ek--size--28--500 text--white">\n' +
                                '                                                    <span class="ek--size--28--500 text--white">$</span>\n' + sellModalValueDiv +
                                '                                                </p>\n' +
                                '                                                <h4 class="ek--size--16 text--navy--200">Value</h4>\n' +
                                '                                            </div>\n' +
                                '                                        </div>\n' +
                                '                                        <!-- End Ek Body Squad Player Modal Inner Feature -->\n' +
                                '                                        <!-- Ek Body Squad Player Modal Inner Values -->\n' +
                                '                                        <div class="ek--squad--modals--inner--values">\n' +
                                '                                            <!-- Ek Body Squad Player Modal Inner Values Sections Energy -->\n' +
                                '                                            <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                '                                                <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                '                                                <div class="ek--squad--modals--inner--values--section--left">\n' +
                                '                                                    <svg class="icon icon-ek--player--country fill--green"><use xlink:href="static/img/icons.svg#icon-ek--player--country"></use></svg>\n' +
                                '                                                    <span class="ek--size--16 text--navy--200">Country</span>\n' +
                                '                                                </div>\n' +
                                '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                '                                                <!-- Ek Body Squad Player Modal Inner Values Sections Right -->\n' +
                                '                                                <div class="ek--squad--modals--inner--values--section--right">\n' +
                                '                                                    <img src="static/img/Flags/' + o.country.code + '.svg" alt="">\n' +
                                '                                                    <span class="text--white ek--size--16--500" >' + o.country.code + '</span>\n' +
                                '                                                </div>\n' +
                                '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections Right -->\n' +
                                '                                            </div>\n' +
                                '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections Energy -->\n' +
                                '                                            <!-- Ek Body Squad Player Modal Inner Values Sections Goals -->\n' +
                                '                                            <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                '                                                <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                '                                                <div class="ek--squad--modals--inner--values--section--left">\n' +
                                '                                                    <svg class="icon icon-ek--ball fill--green"><use xlink:href="static/img/icons.svg#icon-ek--ball"></use></svg>\n' +
                                '                                                    <span class="ek--size--16 text--navy--200">Goals</span>\n' +
                                '                                                </div>\n' +
                                '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                '                                                <span class="text--white ek--size--16--500"  >' + o.goals + '</span>\n' +
                                '                                            </div>\n' +
                                '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections Goals -->\n' +
                                '                                            <!-- Ek Body Squad Player Modal Inner Values Sections Played Match -->\n' +
                                '                                            <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                '                                                <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                '                                                <div class="ek--squad--modals--inner--values--section--left">\n' +
                                '                                                    <svg class="icon icon-ek--stadium fill--green"><use xlink:href="static/img/icons.svg#icon-ek--stadium"></use></svg>\n' +
                                '                                                    <span class="ek--size--16 text--navy--200">Played Match</span>\n' +
                                '                                                </div>\n' +
                                '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                '                                                <span class="text--white ek--size--16--500" >' + o.appearance + '</span>\n' +
                                '                                            </div>\n' +
                                '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections Played Match -->\n' +
                                '                                            <!-- Ek Body Squad Player Modal Inner Values Sections Red Card -->\n' +
                                '                                            <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                '                                                <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                '                                                <div class="ek--squad--modals--inner--values--section--left">\n' +
                                '                                                    <svg class="icon icon-ek--card fill--red"><use xlink:href="static/img/icons.svg#icon-ek--card"></use></svg>\n' +
                                '                                                    <span class="ek--size--16 text--navy--200">Red Card</span>\n' +
                                '                                                </div>\n' +
                                '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                '                                                <span class="text--white ek--size--16--500" >' + o.redCard + '</span>\n' +
                                '                                            </div>\n' +
                                '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections Red Card  -->\n' +
                                '                                            <!-- Ek Body Squad Player Modal Inner Values Sections Red Card -->\n' +
                                '                                            <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                '                                                <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                '                                                <div class="ek--squad--modals--inner--values--section--left">\n' +
                                '                                                    <svg class="icon icon-ek--card fill--yellow"><use xlink:href="static/img/icons.svg#icon-ek--card"></use></svg>\n' +
                                '                                                    <span class="ek--size--16 text--navy--200" >Yellow Card</span>\n' +
                                '                                                </div>\n' +
                                '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                '                                                <span class="text--white ek--size--16--500" >' + o.yellowCard + '</span>\n' +
                                '                                            </div>\n' +
                                '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections Red Card  -->\n' +
                                '                                        </div>\n' +
                                '                                        <!-- End Ek Body Squad Player Modal Inner Values -->\n' +
                                '                                        <!-- Ek Body Squad Player Modal Inner Refill Energy -->\n' +
                                '                                        <a class="ek--squad--modals--inner--refill bg--blue text--white" data-id="'+ o.id +'" href="#" id="openSecSellModal" >Sell player</a>\n' +
                                '                                        <!-- End Ek Body Squad Player Modal Inner Refill Energy -->\n' +
                                '                                    </div>\n' +
                                '                                    <!-- End Ek Body Squad Player Modal Inner -->\n' +
                                '                                </div>\n' +
                                '                                <!-- End Ek Transfer Player Modal -->\n' +
                                '                                <!-- Ek Transfer Player Buy Modal -->\n' +
                                '                                <div  id="ek--transfer--sell' + o.id + '" class="ek--squad--modals--refill bg--navy--700 modal">\n' +
                                '                                    <!-- Ek Body Squad Refill Modal Head -->\n' +
                                '                                    <div class="ek--squad--modals--refill--head">\n' +
                                '                                        <!-- Ek Body Squad Refill Modal Head  Energy -->\n' +
                                '                                        <div class="ek--squad--modals--refill--head--energy bg--navy--600">\n' +
                                '                                            <img src="static/img/Other/ek--null--image.png" alt="null">\n' +
                                '                                        </div>\n' +
                                '                                        <div class="ek--squad--modals--refill--head--want">\n' +
                                '                                            <h4 class="text--white ek--size--20">Sell player</h4>\n' +
                                '                                            <!-- Ek Body Squad Refill Modal Head  Energy Player Name -->\n' +
                                '                                            <p>Do you want to sell <span >' + o.name + '</span>?</p>\n' +
                                '                                            <!-- End Ek Body Squad Refill Modal Head  Energy Player Name -->\n' +
                                '                                        </div>\n' +
                                '                                        <!-- End Ek Body Squad Refill Modal Head  Energy -->\n' +
                                '                                    </div>\n' +
                                '                                    <!-- End Ek Body Squad Refill Modal Head -->\n' +
                                '                                    <!-- Ek Body Squad Refill Modal Head  Buttons -->\n' +
                                '                                    <div class="ek--squad--modals--refill--buttons">\n' +
                                '                                        <!-- Ek Body Squad Refill Modal Head  Buttons Cancel -->\n' +
                                '                                        <a class="ek--squad--modals--refill--buttons--cancel text--white ek--size--16 bg--navy--600" href="#ek--transfer--sell--modal" rel="modal:open">Cancel</a>\n' +
                                '                                        <!-- End Ek Body Squad Refill Modal Head  Buttons Cancel -->\n' +
                                '                                        <!-- Ek Body Squad Refill Modal Head  Buttons Energy -->\n' +
                                '                                        <!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;energy text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;blue" href="#ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;succes" rel="modal:open">Sell player</a>-->\n' +
                                '                                        <!-- End Ek Body Squad Refill Modal Head  Buttons Energy -->\n' +
                                '                                        <button class="ek--squad--modals--refill--buttons--energy text--white ek--size--16 bg--blue"id="sellPlayer"  data-id="'+o.id+'">Sell Player</button>\n' +
                                '                                    </div>\n' +
                                '                                        <!-- End Ek Body Squad Refill Modal Head  Buttons -->\n' +
                                '                                    <a href="#close-modal" rel="modal:close" class="close-modal">Close</a>\n' +
                                '                                </div>\n' +
                                '                                <!-- End Ek Transfer Player Buy Modal -->\n' +
                                '                                <!-- Ek Transfer Player Buy Modal -->\n' +
                                '                                <div id="ek--transfer--sell--succes" class="ek--squad--modals--refill ek--modal--border--left--green--dark--two bg--navy--700 modal">\n' +
                                '                                    <!-- Ek Body Squad Refill Modal Head -->\n' +
                                '                                    <div class="ek--squad--modals--refill--head">\n' +
                                '                                        <!-- Ek Body Squad Refill Modal Head  Energy -->\n' +
                                '                                        <div class="ek--squad--modals--refill--head--energy bg--green--dark--two">\n' +
                                '                                            <svg class="icon icon-ek--correct fill--white"><use xlink:href="static/img/icons.svg#icon-ek--correct"></use></svg>\n' +
                                '                                        </div>\n' +
                                '                                        <div class="ek--squad--modals--refill--head--want">\n' +
                                '                                            <h4 class="text--white ek--size--20">You sold the player</h4>\n' +
                                '                                            <!-- Ek Body Squad Refill Modal Head  Energy Player Name -->\n' +
                                '                                            <p><span >' + o.name + '</span> successfully sold</p>\n' +
                                '                                            <!-- End Ek Body Squad Refill Modal Head  Energy Player Name -->\n' +
                                '                                        </div>\n' +
                                '                                        <!-- End Ek Body Squad Refill Modal Head  Energy -->\n' +
                                '                                    </div>\n' +
                                '                                    <!-- End Ek Body Squad Refill Modal Head -->\n' +
                                '                                    <!-- Ek Body Squad Refill Modal Head  Buttons -->\n' +
                                '                                    <div class="ek--squad--modals--refill--buttons">\n' +
                                '                                        <!-- Ek Body Squad Refill Modal Head  Buttons Cancel -->\n' +
                                '                                        <a class="ek--squad--modals--refill--buttons--cancel text--white ek--size--16 bg--navy--600" href="#ek--transfer--sell--succes" rel="modal:close">Cancel</a>\n' +
                                '                                        <!-- End Ek Body Squad Refill Modal Head  Buttons Cancel -->\n' +
                                '                                    </div>\n' +
                                '                                        <!-- End Ek Body Squad Refill Modal Head  Buttons -->\n' +
                                '                                </div>\n' +
                                '                                <!-- End Ek Transfer Player Buy Modal -->\n' +
                                '                            </div>\n' +
                                '                            <div class="ek--transfer--body--tabs--inner--age ek--size--16 text--white" >' + o.age + '</div>\n' +
                                '                            <div class="ek--transfer--body--tabs--inner--abl ek--size--16 text--white" >' + Math.ceil(o.ability) + '</div>\n' +
                                // '                            <div class="ek--transfer--body--tabs--inner--matches ek--size--16 text--white" >' + o.matches + '</div>\n' +
                                '                            <div class="ek--transfer--body--tabs--inner--goals ek--size--16 text--white" >' + o.goals + '</div>\n' +
                                // '                            <div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white">'+ o.value +'</div>\n' +
                                '\n' +
                                '                                <span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white">$'+sellValueDiv+'</span>\n' +
                                '\n' +
                                '                        </div>');

                        }


                    });


                    $.each(res.data, function (i, o) {

                        if (o.positionId == 2) {


                            if (o.value < 1000) {

                                var sellModalValueDiv = '<span class="ek--size--28--500 text--white" id="userMoney" >' + o.value + '<span class="ek--size--28--500 text--white" id="amount"></span></span>';
                                var sellValueDiv = '<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >' + o.value + '<span class="ek--size--16 text--white"></span></span>';

                            }

                            if (o.value >= 1000 && o.value < 1000000) {

                                var sellModalValueDiv = '<span class="ek--size--28--500 text--white" ><span class="ek--size--28--500 text--white" id="userMoney">' + parseInt(o.value / 1000) + '</span> <span class="ek--size--28--500 text--white" id="amount">K</span></span>';
                                var sellValueDiv = '<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >' + parseInt(o.value / 1000) + '<span class="ek--size--16 text--white">K</span></span>';

                            }
                            if (o.value >= 1000000 && o.value < 1000000000) {

                                var sellModalValueDiv = '<span class="ek--size--28--500 text--white"> <span class="ek--size--28--500 text--white" id="userMoney"> ' + parseInt(o.value / 1000000) + '</span><span class="ek--size--28--500 text--white" id="amount">M</span></span>';
                                var sellValueDiv = '<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >' + parseInt(o.value / 1000000) + '<span class="ek--size--16 text--white">M</span></span>';
                            }
                            if (o.value >= 1000000000) {

                                var sellModalValueDiv = '<span class="ek--size--28--500 text--white"  ><span class="ek--size--28--500 text--white" id="userMoney"  > ' + parseInt(o.value / 1000000000) + '</span> <span class="ek--size--28--500 text--white" id="amount">B</span></span>';
                                var sellValueDiv = '<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >' + parseInt(o.value / 1000000000) + '<span class="ek--size--16 text--white">B</span></span>';


                            }


                                $('#sellList').append('<div class="ek--transfer--body--tabs--inner bg--navy--600" id="sellPlayerModalLink" data-id="'+ o.id +'"   >\n' +
                                    '                            <div class="ek--transfer--body--tabs--inner--pos">\n' +
                                    '                                <div class="ek--transfer--body--tabs--inner--pos--type bg--yellow ek--size--16 text--white" >D</div>' +
                                    '\n' +
                                    '                            </div>\n' +
                                    '                            <div class="ek--transfer--body--tabs--inner--player ek--size--16 text--navy--200">\n' +
                                    '                                <!--<img src="static/img/Flags/ek&#45;&#45;aze.svg" alt="ek&#45;&#45;aze" />-->\n' +
                                    '                                <img src="static/img/Flags/'+ o.country.code +'.svg" alt="">\n' +
                                    '                                <a class="ek--size--16 text--white" >'+ o.name +'</a>\n' +
                                    '                                <!-- Ek Transfer Player Modal -->\n' +
                                    '                                <div id="ek--transfer--sell--modal' + o.id + '" class="ek--squad--modals--player bg--navy--700 modal">\n' +
                                    '                                    <div class="ek--close">\n' +
                                    '                                        <a class="ek--close--icon bg--navy--700" href="#" rel="modal:close">\n' +
                                    '                                            <svg class="icon icon-ek--close--icon fill--navy--200"><use xlink:href="static/img/icons.svg#icon-ek--close--icon"></use></svg>\n' +
                                    '                                        </a>\n' +
                                    '                                    </div>\n' +
                                    '                                    <!-- Ek Body Squad Player Modal Head -->\n' +
                                    '                                    <div class="ek--squad--modals--head">\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Head Image -->\n' +
                                    '                                        <div class="ek--squad--modals--head--image bg--navy--900">\n' +
                                    '                                            <img src="static/img/Other/ek--null--image.png" alt="">\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Player Modal Head Image -->\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Head About -->\n' +
                                    '                                        <div class="ek--squad--modals--head--about">\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Head About Name -->\n' +
                                    '                                            <h3 class="ek--size--20--400 text--white">'+ o.name +'</h3>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Head About Name -->\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Head About Country -->\n' +
                                    '                                            <div class="ek--squad--modals--head--about--country">\n' +
                                    '                                                <img src="static/img/Flags/'+ o.country.code +'.svg" alt="">\n' +
                                    '                                                <span class="text--white ek--size--20--400" >'+ o.country.code +'</span>\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Head About Country -->\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Head About Position -->\n' +
                                    '                                            <div class="ek--squad--modals--head--about--position bg--yellow text--navy--900 ek--size--16--500" >Defender</div>' +
                                    '                                            <!-- End Ek Body Squad Player Modal Head About Position -->\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Player Modal Head About -->\n' +
                                    '                                    </div>\n' +
                                    '                                    <!-- End Ek Body Squad Player Modal Head -->\n' +
                                    '                                    <!-- Ek Body Squad Player Modal Inner -->\n' +
                                    '                                    <div class="ek--squad--modals--inner">\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Inner Feature -->\n' +
                                    '                                        <div class="ek--squad--modals--inner--feature">\n' +
                                    '                                            <div class="ek--squad--modals--inner--feature--age">\n' +
                                    '                                                <p class="ek--size--28--500 text--white" >'+ o.age +'</p>\n' +
                                    '                                                <h4 class="ek--size--16 text--navy--200">Age</h4>\n' +
                                    '                                            </div>\n' +
                                    '                                            <div class="ek--squad--modals--inner--feature--abl">\n' +
                                    '                                                <p class="ek--size--28--500 text--white" >'+ Math.ceil(o.ability)+'</p>\n' +
                                    '                                                <h4 class="ek--size--16 text--navy--200">Abl</h4>\n' +
                                    '                                            </div>\n' +
                                    '                                            <div class="ek--squad--modals--inner--feature--value">\n' +
                                    '                                                <p class="ek--size--28--500 text--white">\n' +
                                    '                                                    <span class="ek--size--28--500 text--white">$</span>\n' + sellModalValueDiv +
                                    '                                                </p>\n' +
                                    '                                                <h4 class="ek--size--16 text--navy--200">Value</h4>\n' +
                                    '                                            </div>\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Player Modal Inner Feature -->\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Inner Values -->\n' +
                                    '                                        <div class="ek--squad--modals--inner--values">\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Inner Values Sections Energy -->\n' +
                                    '                                            <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                    '                                                <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <div class="ek--squad--modals--inner--values--section--left">\n' +
                                    '                                                    <svg class="icon icon-ek--player--country fill--green"><use xlink:href="static/img/icons.svg#icon-ek--player--country"></use></svg>\n' +
                                    '                                                    <span class="ek--size--16 text--navy--200">Country</span>\n' +
                                    '                                                </div>\n' +
                                    '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <!-- Ek Body Squad Player Modal Inner Values Sections Right -->\n' +
                                    '                                                <div class="ek--squad--modals--inner--values--section--right">\n' +
                                    '                                                    <img src="static/img/Flags/'+ o.country.code +'.svg" alt="">\n' +
                                    '                                                    <span class="text--white ek--size--16--500" >'+ o.country.code +'</span>\n' +
                                    '                                                </div>\n' +
                                    '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections Right -->\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections Energy -->\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Inner Values Sections Goals -->\n' +
                                    '                                            <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                    '                                                <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <div class="ek--squad--modals--inner--values--section--left">\n' +
                                    '                                                    <svg class="icon icon-ek--ball fill--green"><use xlink:href="static/img/icons.svg#icon-ek--ball"></use></svg>\n' +
                                    '                                                    <span class="ek--size--16 text--navy--200">Goals</span>\n' +
                                    '                                                </div>\n' +
                                    '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <span class="text--white ek--size--16--500"  >'+ o.goals +'</span>\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections Goals -->\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Inner Values Sections Played Match -->\n' +
                                    '                                            <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                    '                                                <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <div class="ek--squad--modals--inner--values--section--left">\n' +
                                    '                                                    <svg class="icon icon-ek--stadium fill--green"><use xlink:href="static/img/icons.svg#icon-ek--stadium"></use></svg>\n' +
                                    '                                                    <span class="ek--size--16 text--navy--200">Played Match</span>\n' +
                                    '                                                </div>\n' +
                                    '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <span class="text--white ek--size--16--500" >'+ o.appearance +'</span>\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections Played Match -->\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Inner Values Sections Red Card -->\n' +
                                    '                                            <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                    '                                                <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <div class="ek--squad--modals--inner--values--section--left">\n' +
                                    '                                                    <svg class="icon icon-ek--card fill--red"><use xlink:href="static/img/icons.svg#icon-ek--card"></use></svg>\n' +
                                    '                                                    <span class="ek--size--16 text--navy--200">Red Card</span>\n' +
                                    '                                                </div>\n' +
                                    '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <span class="text--white ek--size--16--500" >'+ o.redCard +'</span>\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections Red Card  -->\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Inner Values Sections Red Card -->\n' +
                                    '                                            <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                    '                                                <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <div class="ek--squad--modals--inner--values--section--left">\n' +
                                    '                                                    <svg class="icon icon-ek--card fill--yellow"><use xlink:href="static/img/icons.svg#icon-ek--card"></use></svg>\n' +
                                    '                                                    <span class="ek--size--16 text--navy--200" >Yellow Card</span>\n' +
                                    '                                                </div>\n' +
                                    '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <span class="text--white ek--size--16--500" >'+ o.yellowCard +'</span>\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections Red Card  -->\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Player Modal Inner Values -->\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Inner Refill Energy -->\n' +
                                    '                                        <a class="ek--squad--modals--inner--refill bg--blue text--white" data-id="'+ o.id +'" id="openSecSellModal" >Sell player</a>\n' +
                                    '                                        <!-- End Ek Body Squad Player Modal Inner Refill Energy -->\n' +
                                    '                                    </div>\n' +
                                    '                                    <!-- End Ek Body Squad Player Modal Inner -->\n' +
                                    '                                </div>\n' +
                                    '                                <!-- End Ek Transfer Player Modal -->\n' +
                                    '                                <!-- Ek Transfer Player Buy Modal -->\n' +
                                    '                                <div  id="ek--transfer--sell' + o.id +'" class="ek--squad--modals--refill bg--navy--700 modal">\n' +
                                    '                                    <!-- Ek Body Squad Refill Modal Head -->\n' +
                                    '                                    <div class="ek--squad--modals--refill--head">\n' +
                                    '                                        <!-- Ek Body Squad Refill Modal Head  Energy -->\n' +
                                    '                                        <div class="ek--squad--modals--refill--head--energy bg--navy--600">\n' +
                                    '                                            <img src="static/img/Other/ek--null--image.png" alt="null">\n' +
                                    '                                        </div>\n' +
                                    '                                        <div class="ek--squad--modals--refill--head--want">\n' +
                                    '                                            <h4 class="text--white ek--size--20">Sell player</h4>\n' +
                                    '                                            <!-- Ek Body Squad Refill Modal Head  Energy Player Name -->\n' +
                                    '                                            <p>Do you want to sell <span >'+ o.name +'</span>?</p>\n' +
                                    '                                            <!-- End Ek Body Squad Refill Modal Head  Energy Player Name -->\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Refill Modal Head  Energy -->\n' +
                                    '                                    </div>\n' +
                                    '                                    <!-- End Ek Body Squad Refill Modal Head -->\n' +
                                    '                                    <!-- Ek Body Squad Refill Modal Head  Buttons -->\n' +
                                    '                                    <div class="ek--squad--modals--refill--buttons">\n' +
                                    '                                        <!-- Ek Body Squad Refill Modal Head  Buttons Cancel -->\n' +
                                    '                                        <a class="ek--squad--modals--refill--buttons--cancel text--white ek--size--16 bg--navy--600" href="#ek--transfer--sell--modal" rel="modal:open">Cancel</a>\n' +
                                    '                                        <!-- End Ek Body Squad Refill Modal Head  Buttons Cancel -->\n' +
                                    '                                        <!-- Ek Body Squad Refill Modal Head  Buttons Energy -->\n' +
                                    '                                        <!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;energy text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;blue" href="#ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;succes" rel="modal:open">Sell player</a>-->\n' +
                                    '                                        <!-- End Ek Body Squad Refill Modal Head  Buttons Energy -->\n' +
                                    '                                        <button class="ek--squad--modals--refill--buttons--energy text--white ek--size--16 bg--blue"id="sellPlayer"  data-id="'+ o.id+'">Sell Player</button>\n' +
                                    '                                    </div>\n' +
                                    '                                        <!-- End Ek Body Squad Refill Modal Head  Buttons -->\n' +
                                    '                                    <a href="#close-modal" rel="modal:close" class="close-modal">Close</a>\n' +
                                    '                                </div>\n' +
                                    '                                <!-- End Ek Transfer Player Buy Modal -->\n' +
                                    '                                <!-- Ek Transfer Player Buy Modal -->\n' +
                                    '                                <div id="ek--transfer--sell--succes" class="ek--squad--modals--refill ek--modal--border--left--green--dark--two bg--navy--700 modal">\n' +
                                    '                                    <!-- Ek Body Squad Refill Modal Head -->\n' +
                                    '                                    <div class="ek--squad--modals--refill--head">\n' +
                                    '                                        <!-- Ek Body Squad Refill Modal Head  Energy -->\n' +
                                    '                                        <div class="ek--squad--modals--refill--head--energy bg--green--dark--two">\n' +
                                    '                                            <svg class="icon icon-ek--correct fill--white"><use xlink:href="static/img/icons.svg#icon-ek--correct"></use></svg>\n' +
                                    '                                        </div>\n' +
                                    '                                        <div class="ek--squad--modals--refill--head--want">\n' +
                                    '                                            <h4 class="text--white ek--size--20">You sold the player</h4>\n' +
                                    '                                            <!-- Ek Body Squad Refill Modal Head  Energy Player Name -->\n' +
                                    '                                            <p><span >'+o.name +'</span> successfully sold</p>\n' +
                                    '                                            <!-- End Ek Body Squad Refill Modal Head  Energy Player Name -->\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Refill Modal Head  Energy -->\n' +
                                    '                                    </div>\n' +
                                    '                                    <!-- End Ek Body Squad Refill Modal Head -->\n' +
                                    '                                    <!-- Ek Body Squad Refill Modal Head  Buttons -->\n' +
                                    '                                    <div class="ek--squad--modals--refill--buttons">\n' +
                                    '                                        <!-- Ek Body Squad Refill Modal Head  Buttons Cancel -->\n' +
                                    '                                        <a class="ek--squad--modals--refill--buttons--cancel text--white ek--size--16 bg--navy--600" href="#ek--transfer--sell--succes" rel="modal:close">Cancel</a>\n' +
                                    '                                        <!-- End Ek Body Squad Refill Modal Head  Buttons Cancel -->\n' +
                                    '                                    </div>\n' +
                                    '                                        <!-- End Ek Body Squad Refill Modal Head  Buttons -->\n' +
                                    '                                </div>\n' +
                                    '                                <!-- End Ek Transfer Player Buy Modal -->\n' +
                                    '                            </div>\n' +
                                    '                            <div class="ek--transfer--body--tabs--inner--age ek--size--16 text--white" >'+ o.age +'</div>\n' +
                                    '                            <div class="ek--transfer--body--tabs--inner--abl ek--size--16 text--white" >'+ Math.ceil(o.ability)+'</div>\n' +
                                    // '                            <div class="ek--transfer--body--tabs--inner--matches ek--size--16 text--white" >'+ o.appearance +'</div>\n' +
                                    '                            <div class="ek--transfer--body--tabs--inner--goals ek--size--16 text--white" >'+ o.goals +'</div>\n' +
                                    '                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${player.value} ">$45M</div>-->\n' +
                                    '\n' +
                                    '                            <span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white">$'+sellValueDiv+'</span>\n' +

                            '\n' +
                                    '                        </div>');
                            }

                    });



                    $.each(res.data, function (i, o) {

                        if (o.positionId == 3) {

                            if (o.value < 1000) {

                                var sellModalValueDiv = '<span class="ek--size--28--500 text--white" id="userMoney" >' + o.value + '<span class="ek--size--28--500 text--white" id="amount"></span></span>';
                                var sellValueDiv = '<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >' + o.value + '<span class="ek--size--16 text--white"></span></span>';

                            }

                            if (o.value >= 1000 && o.value < 1000000) {

                                var sellModalValueDiv = '<span class="ek--size--28--500 text--white" ><span class="ek--size--28--500 text--white" id="userMoney">' + parseInt(o.value / 1000) + '</span> <span class="ek--size--28--500 text--white" id="amount">K</span></span>';
                                var sellValueDiv = '<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >' + parseInt(o.value / 1000) + '<span class="ek--size--16 text--white">K</span></span>';

                            }
                            if (o.value >= 1000000 && o.value < 1000000000) {

                                var sellModalValueDiv = '<span class="ek--size--28--500 text--white"> <span class="ek--size--28--500 text--white" id="userMoney"> ' + parseInt(o.value / 1000000) + '</span><span class="ek--size--28--500 text--white" id="amount">M</span></span>';
                                var sellValueDiv = '<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >' + parseInt(o.value / 1000000) + '<span class="ek--size--16 text--white">M</span></span>';
                            }
                            if (o.value >= 1000000000) {

                                var sellModalValueDiv = '<span class="ek--size--28--500 text--white"  ><span class="ek--size--28--500 text--white" id="userMoney"  > ' + parseInt(o.value / 1000000000) + '</span> <span class="ek--size--28--500 text--white" id="amount">B</span></span>';
                                var sellValueDiv = '<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >' + parseInt(o.value / 1000000000) + '<span class="ek--size--16 text--white">B</span></span>';


                            }



                                $('#sellList').append('<div class="ek--transfer--body--tabs--inner bg--navy--600" id="sellPlayerModalLink" data-id="'+o.id+'"   >\n' +
                                    '                            <div class="ek--transfer--body--tabs--inner--pos">\n' +
                                    '                                <div class="ek--transfer--body--tabs--inner--pos--type bg--cyan--two ek--size--16 text--white" >M</div>' +
                                    '\n' +
                                    '                            </div>\n' +
                                    '                            <div class="ek--transfer--body--tabs--inner--player ek--size--16 text--navy--200">\n' +
                                    '                                <!--<img src="static/img/Flags/ek&#45;&#45;aze.svg" alt="ek&#45;&#45;aze" />-->\n' +
                                    '                                <img src="static/img/Flags/'+ o.country.code +'.svg" alt="">\n' +
                                    '                                <a class="ek--size--16 text--white" >'+ o.name +'</a>\n' +
                                    '                                <!-- Ek Transfer Player Modal -->\n' +
                                    '                                <div id="ek--transfer--sell--modal' + o.id + '" class="ek--squad--modals--player bg--navy--700 modal">\n' +
                                    '                                    <div class="ek--close">\n' +
                                    '                                        <a class="ek--close--icon bg--navy--700" href="#" rel="modal:close">\n' +
                                    '                                            <svg class="icon icon-ek--close--icon fill--navy--200"><use xlink:href="static/img/icons.svg#icon-ek--close--icon"></use></svg>\n' +
                                    '                                        </a>\n' +
                                    '                                    </div>\n' +
                                    '                                    <!-- Ek Body Squad Player Modal Head -->\n' +
                                    '                                    <div class="ek--squad--modals--head">\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Head Image -->\n' +
                                    '                                        <div class="ek--squad--modals--head--image bg--navy--900">\n' +
                                    '                                            <img src="static/img/Other/ek--null--image.png" alt="">\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Player Modal Head Image -->\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Head About -->\n' +
                                    '                                        <div class="ek--squad--modals--head--about">\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Head About Name -->\n' +
                                    '                                            <h3 class="ek--size--20--400 text--white">'+ o.name +'</h3>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Head About Name -->\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Head About Country -->\n' +
                                    '                                            <div class="ek--squad--modals--head--about--country">\n' +
                                    '                                                <img src="static/img/Flags/'+ o.country.code +'.svg" alt="">\n' +
                                    '                                                <span class="text--white ek--size--20--400" >'+ o.country.code +'</span>\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Head About Country -->\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Head About Position -->\n' +
                                    '                                            <div class="ek--squad--modals--head--about--position bg--cyan--two text--navy--900 ek--size--16--500" >Midfielder</div>' +
                                    '                                            <!-- End Ek Body Squad Player Modal Head About Position -->\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Player Modal Head About -->\n' +
                                    '                                    </div>\n' +
                                    '                                    <!-- End Ek Body Squad Player Modal Head -->\n' +
                                    '                                    <!-- Ek Body Squad Player Modal Inner -->\n' +
                                    '                                    <div class="ek--squad--modals--inner">\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Inner Feature -->\n' +
                                    '                                        <div class="ek--squad--modals--inner--feature">\n' +
                                    '                                            <div class="ek--squad--modals--inner--feature--age">\n' +
                                    '                                                <p class="ek--size--28--500 text--white" >'+ o.age +'</p>\n' +
                                    '                                                <h4 class="ek--size--16 text--navy--200">Age</h4>\n' +
                                    '                                            </div>\n' +
                                    '                                            <div class="ek--squad--modals--inner--feature--abl">\n' +
                                    '                                                <p class="ek--size--28--500 text--white" >'+ Math.ceil(o.ability)+'</p>\n' +
                                    '                                                <h4 class="ek--size--16 text--navy--200">Abl</h4>\n' +
                                    '                                            </div>\n' +
                                    '                                            <div class="ek--squad--modals--inner--feature--value">\n' +
                                    '                                                <p class="ek--size--28--500 text--white">\n' +
                                    '                                                    <span class="ek--size--28--500 text--white">$</span>\n' + sellModalValueDiv +
                                    '                                                </p>\n' +
                                    '                                                <h4 class="ek--size--16 text--navy--200">Value</h4>\n' +
                                    '                                            </div>\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Player Modal Inner Feature -->\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Inner Values -->\n' +
                                    '                                        <div class="ek--squad--modals--inner--values">\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Inner Values Sections Energy -->\n' +
                                    '                                            <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                    '                                                <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <div class="ek--squad--modals--inner--values--section--left">\n' +
                                    '                                                    <svg class="icon icon-ek--player--country fill--green"><use xlink:href="static/img/icons.svg#icon-ek--player--country"></use></svg>\n' +
                                    '                                                    <span class="ek--size--16 text--navy--200">Country</span>\n' +
                                    '                                                </div>\n' +
                                    '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <!-- Ek Body Squad Player Modal Inner Values Sections Right -->\n' +
                                    '                                                <div class="ek--squad--modals--inner--values--section--right">\n' +
                                    '                                                    <img src="static/img/Flags/'+ o.country.code +'.svg" alt="">\n' +
                                    '                                                    <span class="text--white ek--size--16--500" >'+ o.country.code +'</span>\n' +
                                    '                                                </div>\n' +
                                    '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections Right -->\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections Energy -->\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Inner Values Sections Goals -->\n' +
                                    '                                            <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                    '                                                <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <div class="ek--squad--modals--inner--values--section--left">\n' +
                                    '                                                    <svg class="icon icon-ek--ball fill--green"><use xlink:href="static/img/icons.svg#icon-ek--ball"></use></svg>\n' +
                                    '                                                    <span class="ek--size--16 text--navy--200">Goals</span>\n' +
                                    '                                                </div>\n' +
                                    '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <span class="text--white ek--size--16--500"  >'+ o.goals +'</span>\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections Goals -->\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Inner Values Sections Played Match -->\n' +
                                    '                                            <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                    '                                                <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <div class="ek--squad--modals--inner--values--section--left">\n' +
                                    '                                                    <svg class="icon icon-ek--stadium fill--green"><use xlink:href="static/img/icons.svg#icon-ek--stadium"></use></svg>\n' +
                                    '                                                    <span class="ek--size--16 text--navy--200">Played Match</span>\n' +
                                    '                                                </div>\n' +
                                    '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <span class="text--white ek--size--16--500" >'+ o.appearance +'</span>\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections Played Match -->\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Inner Values Sections Red Card -->\n' +
                                    '                                            <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                    '                                                <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <div class="ek--squad--modals--inner--values--section--left">\n' +
                                    '                                                    <svg class="icon icon-ek--card fill--red"><use xlink:href="static/img/icons.svg#icon-ek--card"></use></svg>\n' +
                                    '                                                    <span class="ek--size--16 text--navy--200">Red Card</span>\n' +
                                    '                                                </div>\n' +
                                    '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <span class="text--white ek--size--16--500" >'+ o.redCard +'</span>\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections Red Card  -->\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Inner Values Sections Red Card -->\n' +
                                    '                                            <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                    '                                                <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <div class="ek--squad--modals--inner--values--section--left">\n' +
                                    '                                                    <svg class="icon icon-ek--card fill--yellow"><use xlink:href="static/img/icons.svg#icon-ek--card"></use></svg>\n' +
                                    '                                                    <span class="ek--size--16 text--navy--200" >Yellow Card</span>\n' +
                                    '                                                </div>\n' +
                                    '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <span class="text--white ek--size--16--500" >'+ o.yellowCard +'</span>\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections Red Card  -->\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Player Modal Inner Values -->\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Inner Refill Energy -->\n' +
                                    '                                        <a class="ek--squad--modals--inner--refill bg--blue text--white" data-id="'+ o.id +'" id="openSecSellModal" >Sell player</a>\n' +
                                    '                                        <!-- End Ek Body Squad Player Modal Inner Refill Energy -->\n' +
                                    '                                    </div>\n' +
                                    '                                    <!-- End Ek Body Squad Player Modal Inner -->\n' +
                                    '                                </div>\n' +
                                    '                                <!-- End Ek Transfer Player Modal -->\n' +
                                    '                                <!-- Ek Transfer Player Buy Modal -->\n' +
                                    '                                <div  id="ek--transfer--sell' + o.id +'" class="ek--squad--modals--refill bg--navy--700 modal">\n' +
                                    '                                    <!-- Ek Body Squad Refill Modal Head -->\n' +
                                    '                                    <div class="ek--squad--modals--refill--head">\n' +
                                    '                                        <!-- Ek Body Squad Refill Modal Head  Energy -->\n' +
                                    '                                        <div class="ek--squad--modals--refill--head--energy bg--navy--600">\n' +
                                    '                                            <img src="static/img/Other/ek--null--image.png" alt="null">\n' +
                                    '                                        </div>\n' +
                                    '                                        <div class="ek--squad--modals--refill--head--want">\n' +
                                    '                                            <h4 class="text--white ek--size--20">Sell player</h4>\n' +
                                    '                                            <!-- Ek Body Squad Refill Modal Head  Energy Player Name -->\n' +
                                    '                                            <p>Do you want to sell <span >'+ o.name +'</span>?</p>\n' +
                                    '                                            <!-- End Ek Body Squad Refill Modal Head  Energy Player Name -->\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Refill Modal Head  Energy -->\n' +
                                    '                                    </div>\n' +
                                    '                                    <!-- End Ek Body Squad Refill Modal Head -->\n' +
                                    '                                    <!-- Ek Body Squad Refill Modal Head  Buttons -->\n' +
                                    '                                    <div class="ek--squad--modals--refill--buttons">\n' +
                                    '                                        <!-- Ek Body Squad Refill Modal Head  Buttons Cancel -->\n' +
                                    '                                        <a class="ek--squad--modals--refill--buttons--cancel text--white ek--size--16 bg--navy--600" href="#ek--transfer--sell--modal" rel="modal:open">Cancel</a>\n' +
                                    '                                        <!-- End Ek Body Squad Refill Modal Head  Buttons Cancel -->\n' +
                                    '                                        <!-- Ek Body Squad Refill Modal Head  Buttons Energy -->\n' +
                                    '                                        <!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;energy text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;blue" href="#ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;succes" rel="modal:open">Sell player</a>-->\n' +
                                    '                                        <!-- End Ek Body Squad Refill Modal Head  Buttons Energy -->\n' +
                                    '                                        <button class="ek--squad--modals--refill--buttons--energy text--white ek--size--16 bg--blue"id="sellPlayer"  data-id="'+ o.id +'">Sell Player</button>\n' +
                                    '                                    </div>\n' +
                                    '                                        <!-- End Ek Body Squad Refill Modal Head  Buttons -->\n' +
                                    '                                    <a href="#close-modal" rel="modal:close" class="close-modal">Close</a>\n' +
                                    '                                </div>\n' +
                                    '                                <!-- End Ek Transfer Player Buy Modal -->\n' +
                                    '                                <!-- Ek Transfer Player Buy Modal -->\n' +
                                    '                                <div id="ek--transfer--sell--succes" class="ek--squad--modals--refill ek--modal--border--left--green--dark--two bg--navy--700 modal">\n' +
                                    '                                    <!-- Ek Body Squad Refill Modal Head -->\n' +
                                    '                                    <div class="ek--squad--modals--refill--head">\n' +
                                    '                                        <!-- Ek Body Squad Refill Modal Head  Energy -->\n' +
                                    '                                        <div class="ek--squad--modals--refill--head--energy bg--green--dark--two">\n' +
                                    '                                            <svg class="icon icon-ek--correct fill--white"><use xlink:href="static/img/icons.svg#icon-ek--correct"></use></svg>\n' +
                                    '                                        </div>\n' +
                                    '                                        <div class="ek--squad--modals--refill--head--want">\n' +
                                    '                                            <h4 class="text--white ek--size--20">You sold the player</h4>\n' +
                                    '                                            <!-- Ek Body Squad Refill Modal Head  Energy Player Name -->\n' +
                                    '                                            <p><span >'+o.name +'</span> successfully sold</p>\n' +
                                    '                                            <!-- End Ek Body Squad Refill Modal Head  Energy Player Name -->\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Refill Modal Head  Energy -->\n' +
                                    '                                    </div>\n' +
                                    '                                    <!-- End Ek Body Squad Refill Modal Head -->\n' +
                                    '                                    <!-- Ek Body Squad Refill Modal Head  Buttons -->\n' +
                                    '                                    <div class="ek--squad--modals--refill--buttons">\n' +
                                    '                                        <!-- Ek Body Squad Refill Modal Head  Buttons Cancel -->\n' +
                                    '                                        <a class="ek--squad--modals--refill--buttons--cancel text--white ek--size--16 bg--navy--600" href="#ek--transfer--sell--succes" rel="modal:close">Cancel</a>\n' +
                                    '                                        <!-- End Ek Body Squad Refill Modal Head  Buttons Cancel -->\n' +
                                    '                                    </div>\n' +
                                    '                                        <!-- End Ek Body Squad Refill Modal Head  Buttons -->\n' +
                                    '                                </div>\n' +
                                    '                                <!-- End Ek Transfer Player Buy Modal -->\n' +
                                    '                            </div>\n' +
                                    '                            <div class="ek--transfer--body--tabs--inner--age ek--size--16 text--white" >'+ o.age +'</div>\n' +
                                    '                            <div class="ek--transfer--body--tabs--inner--abl ek--size--16 text--white" >'+ Math.ceil(o.ability)+'</div>\n' +
                                    // '                            <div class="ek--transfer--body--tabs--inner--matches ek--size--16 text--white" >'+ o.appearance +'</div>\n' +
                                    '                            <div class="ek--transfer--body--tabs--inner--goals ek--size--16 text--white" >'+ o.goals +'</div>\n' +
                                    '                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${player.value} ">$45M</div>-->\n' +
                                    '\n' +
                                    '                             <span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white">$'+sellValueDiv+'</span>\n' +
                                    '\n' +
                                    '                        </div>');
                            }

                    });


                    $.each(res.data, function (i, o) {

                        if (o.positionId == 4) {

                            if (o.value < 1000) {

                                var sellModalValueDiv = '<span class="ek--size--28--500 text--white" id="userMoney" >' + o.value + '<span class="ek--size--28--500 text--white" id="amount"></span></span>';
                                var sellValueDiv = '<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >' + o.value + '<span class="ek--size--16 text--white"></span></span>';

                            }

                            if (o.value >= 1000 && o.value < 1000000) {

                                var sellModalValueDiv = '<span class="ek--size--28--500 text--white" ><span class="ek--size--28--500 text--white" id="userMoney">' + parseInt(o.value / 1000) + '</span> <span class="ek--size--28--500 text--white" id="amount">K</span></span>';
                                var sellValueDiv = '<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >' + parseInt(o.value / 1000) + '<span class="ek--size--16 text--white">K</span></span>';

                            }
                            if (o.value >= 1000000 && o.value < 1000000000) {

                                var sellModalValueDiv = '<span class="ek--size--28--500 text--white"> <span class="ek--size--28--500 text--white" id="userMoney"> ' + parseInt(o.value / 1000000) + '</span><span class="ek--size--28--500 text--white" id="amount">M</span></span>';
                                var sellValueDiv = '<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >' + parseInt(o.value / 1000000) + '<span class="ek--size--16 text--white">M</span></span>';
                            }
                            if (o.value >= 1000000000) {

                                var sellModalValueDiv = '<span class="ek--size--28--500 text--white"  ><span class="ek--size--28--500 text--white" id="userMoney"  > ' + parseInt(o.value / 1000000000) + '</span> <span class="ek--size--28--500 text--white" id="amount">B</span></span>';
                                var sellValueDiv = '<span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white"  >' + parseInt(o.value / 1000000000) + '<span class="ek--size--16 text--white">B</span></span>';


                            }



                                $('#sellList').append('<div class="ek--transfer--body--tabs--inner bg--navy--600" id="sellPlayerModalLink" data-id="'+ o.id +'"   >\n' +
                                    '                            <div class="ek--transfer--body--tabs--inner--pos">\n' +
                                    '                                <div class="ek--transfer--body--tabs--inner--pos--type bg--purple ek--size--16 text--white">F</div>' +
                                    '\n' +
                                    '                            </div>\n' +
                                    '                            <div class="ek--transfer--body--tabs--inner--player ek--size--16 text--navy--200">\n' +
                                    '                                <!--<img src="static/img/Flags/ek&#45;&#45;aze.svg" alt="ek&#45;&#45;aze" />-->\n' +
                                    '                                <img src="static/img/Flags/'+ o.country.code +'.svg" alt="">\n' +
                                    '                                <a class="ek--size--16 text--white" >'+ o.name +'</a>\n' +
                                    '                                <!-- Ek Transfer Player Modal -->\n' +
                                    '                                <div id="ek--transfer--sell--modal' + o.id + '" class="ek--squad--modals--player bg--navy--700 modal">\n' +
                                    '                                    <div class="ek--close">\n' +
                                    '                                        <a class="ek--close--icon bg--navy--700" href="#" rel="modal:close">\n' +
                                    '                                            <svg class="icon icon-ek--close--icon fill--navy--200"><use xlink:href="static/img/icons.svg#icon-ek--close--icon"></use></svg>\n' +
                                    '                                        </a>\n' +
                                    '                                    </div>\n' +
                                    '                                    <!-- Ek Body Squad Player Modal Head -->\n' +
                                    '                                    <div class="ek--squad--modals--head">\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Head Image -->\n' +
                                    '                                        <div class="ek--squad--modals--head--image bg--navy--900">\n' +
                                    '                                            <img src="static/img/Other/ek--null--image.png" alt="">\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Player Modal Head Image -->\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Head About -->\n' +
                                    '                                        <div class="ek--squad--modals--head--about">\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Head About Name -->\n' +
                                    '                                            <h3 class="ek--size--20--400 text--white">'+ o.name +'</h3>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Head About Name -->\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Head About Country -->\n' +
                                    '                                            <div class="ek--squad--modals--head--about--country">\n' +
                                    '                                                <img src="static/img/Flags/'+ o.country.code +'.svg" alt="">\n' +
                                    '                                                <span class="text--white ek--size--20--400" >'+ o.country.code +'</span>\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Head About Country -->\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Head About Position -->\n' +
                                    '                                            <div class="ek--squad--modals--head--about--position bg--purple text--navy--900 ek--size--16--500" >Forward</div>' +
                                    '                                            <!-- End Ek Body Squad Player Modal Head About Position -->\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Player Modal Head About -->\n' +
                                    '                                    </div>\n' +
                                    '                                    <!-- End Ek Body Squad Player Modal Head -->\n' +
                                    '                                    <!-- Ek Body Squad Player Modal Inner -->\n' +
                                    '                                    <div class="ek--squad--modals--inner">\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Inner Feature -->\n' +
                                    '                                        <div class="ek--squad--modals--inner--feature">\n' +
                                    '                                            <div class="ek--squad--modals--inner--feature--age">\n' +
                                    '                                                <p class="ek--size--28--500 text--white" >'+ o.age +'</p>\n' +
                                    '                                                <h4 class="ek--size--16 text--navy--200">Age</h4>\n' +
                                    '                                            </div>\n' +
                                    '                                            <div class="ek--squad--modals--inner--feature--abl">\n' +
                                    '                                                <p class="ek--size--28--500 text--white" >'+ Math.ceil(o.ability)+'</p>\n' +
                                    '                                                <h4 class="ek--size--16 text--navy--200">Abl</h4>\n' +
                                    '                                            </div>\n' +
                                    '                                            <div class="ek--squad--modals--inner--feature--value">\n' +
                                    '                                                <p class="ek--size--28--500 text--white">\n' +
                                    '                                                    <span class="ek--size--28--500 text--white">$</span>\n' + sellModalValueDiv +
                                    '                                                </p>\n' +
                                    '                                                <h4 class="ek--size--16 text--navy--200">Value</h4>\n' +
                                    '                                            </div>\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Player Modal Inner Feature -->\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Inner Values -->\n' +
                                    '                                        <div class="ek--squad--modals--inner--values">\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Inner Values Sections Energy -->\n' +
                                    '                                            <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                    '                                                <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <div class="ek--squad--modals--inner--values--section--left">\n' +
                                    '                                                    <svg class="icon icon-ek--player--country fill--green"><use xlink:href="static/img/icons.svg#icon-ek--player--country"></use></svg>\n' +
                                    '                                                    <span class="ek--size--16 text--navy--200">Country</span>\n' +
                                    '                                                </div>\n' +
                                    '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <!-- Ek Body Squad Player Modal Inner Values Sections Right -->\n' +
                                    '                                                <div class="ek--squad--modals--inner--values--section--right">\n' +
                                    '                                                    <img src="static/img/Flags/'+ o.country.code +'.svg" alt="">\n' +
                                    '                                                    <span class="text--white ek--size--16--500" >'+ o.country.code +'</span>\n' +
                                    '                                                </div>\n' +
                                    '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections Right -->\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections Energy -->\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Inner Values Sections Goals -->\n' +
                                    '                                            <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                    '                                                <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <div class="ek--squad--modals--inner--values--section--left">\n' +
                                    '                                                    <svg class="icon icon-ek--ball fill--green"><use xlink:href="static/img/icons.svg#icon-ek--ball"></use></svg>\n' +
                                    '                                                    <span class="ek--size--16 text--navy--200">Goals</span>\n' +
                                    '                                                </div>\n' +
                                    '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <span class="text--white ek--size--16--500"  >'+ o.goals +'</span>\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections Goals -->\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Inner Values Sections Played Match -->\n' +
                                    '                                            <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                    '                                                <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <div class="ek--squad--modals--inner--values--section--left">\n' +
                                    '                                                    <svg class="icon icon-ek--stadium fill--green"><use xlink:href="static/img/icons.svg#icon-ek--stadium"></use></svg>\n' +
                                    '                                                    <span class="ek--size--16 text--navy--200">Played Match</span>\n' +
                                    '                                                </div>\n' +
                                    '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <span class="text--white ek--size--16--500" >'+ o.appearance +'</span>\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections Played Match -->\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Inner Values Sections Red Card -->\n' +
                                    '                                            <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                    '                                                <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <div class="ek--squad--modals--inner--values--section--left">\n' +
                                    '                                                    <svg class="icon icon-ek--card fill--red"><use xlink:href="static/img/icons.svg#icon-ek--card"></use></svg>\n' +
                                    '                                                    <span class="ek--size--16 text--navy--200">Red Card</span>\n' +
                                    '                                                </div>\n' +
                                    '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <span class="text--white ek--size--16--500" >'+ o.redCard +'</span>\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections Red Card  -->\n' +
                                    '                                            <!-- Ek Body Squad Player Modal Inner Values Sections Red Card -->\n' +
                                    '                                            <div class="ek--squad--modals--inner--values--section bg--navy--600">\n' +
                                    '                                                <!-- Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <div class="ek--squad--modals--inner--values--section--left">\n' +
                                    '                                                    <svg class="icon icon-ek--card fill--yellow"><use xlink:href="static/img/icons.svg#icon-ek--card"></use></svg>\n' +
                                    '                                                    <span class="ek--size--16 text--navy--200" >Yellow Card</span>\n' +
                                    '                                                </div>\n' +
                                    '                                                <!-- End Ek Body Squad Player Modal Inner Values Sections left -->\n' +
                                    '                                                <span class="text--white ek--size--16--500" >'+ o.yellowCard +'</span>\n' +
                                    '                                            </div>\n' +
                                    '                                            <!-- End Ek Body Squad Player Modal Inner Values Sections Red Card  -->\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Player Modal Inner Values -->\n' +
                                    '                                        <!-- Ek Body Squad Player Modal Inner Refill Energy -->\n' +
                                    '                                        <a class="ek--squad--modals--inner--refill bg--blue text--white" data-id="'+ o.id +'" id="openSecSellModal" >Sell player</a>\n' +
                                    '                                        <!-- End Ek Body Squad Player Modal Inner Refill Energy -->\n' +
                                    '                                    </div>\n' +
                                    '                                    <!-- End Ek Body Squad Player Modal Inner -->\n' +
                                    '                                </div>\n' +
                                    '                                <!-- End Ek Transfer Player Modal -->\n' +
                                    '                                <!-- Ek Transfer Player Buy Modal -->\n' +
                                    '                                <div  id="ek--transfer--sell' + o.id +'" class="ek--squad--modals--refill bg--navy--700 modal">\n' +
                                    '                                    <!-- Ek Body Squad Refill Modal Head -->\n' +
                                    '                                    <div class="ek--squad--modals--refill--head">\n' +
                                    '                                        <!-- Ek Body Squad Refill Modal Head  Energy -->\n' +
                                    '                                        <div class="ek--squad--modals--refill--head--energy bg--navy--600">\n' +
                                    '                                            <img src="static/img/Other/ek--null--image.png" alt="null">\n' +
                                    '                                        </div>\n' +
                                    '                                        <div class="ek--squad--modals--refill--head--want">\n' +
                                    '                                            <h4 class="text--white ek--size--20">Sell player</h4>\n' +
                                    '                                            <!-- Ek Body Squad Refill Modal Head  Energy Player Name -->\n' +
                                    '                                            <p>Do you want to sell <span >'+ o.name +'</span>?</p>\n' +
                                    '                                            <!-- End Ek Body Squad Refill Modal Head  Energy Player Name -->\n' +
                                    '                                        </div>\n' +
                                    '                                        <!-- End Ek Body Squad Refill Modal Head  Energy -->\n' +
                                    '                                    </div>\n' +
                                    '                                    <!-- End Ek Body Squad Refill Modal Head -->\n' +
                                    '                                    <!-- Ek Body Squad Refill Modal Head  Buttons -->\n' +
                                    '                                    <div class="ek--squad--modals--refill--buttons">\n' +
                                    '                                        <!-- Ek Body Squad Refill Modal Head  Buttons Cancel -->\n' +
                                    '                                        <a class="ek--squad--modals--refill--buttons--cancel text--white ek--size--16 bg--navy--600" href="#ek--transfer--sell--modal" rel="modal:open">Cancel</a>\n' +
                                    '                                        <!-- End Ek Body Squad Refill Modal Head  Buttons Cancel -->\n' +
                                    '                                        <!-- Ek Body Squad Refill Modal Head  Buttons Energy -->\n' +
                                    '                                        <!--<a class="ek&#45;&#45;squad&#45;&#45;modals&#45;&#45;refill&#45;&#45;buttons&#45;&#45;energy text&#45;&#45;white ek&#45;&#45;size&#45;&#45;16 bg&#45;&#45;blue" href="#ek&#45;&#45;transfer&#45;&#45;sell&#45;&#45;succes" rel="modal:open">Sell player</a>-->\n' +
                                    '                                        <!-- End Ek Body Squad Refill Modal Head  Buttons Energy -->\n' +
                                    '                                        <button class="ek--squad--modals--refill--buttons--energy text--white ek--size--16 bg--blue"id="sellPlayer"  data-id="'+ o.id +'">Sell Player</button>\n' +
                                    '                                    </div>\n' +
                                    '                                        <!-- End Ek Body Squad Refill Modal Head  Buttons -->\n' +
                                    '                                    <a href="#close-modal" rel="modal:close" class="close-modal">Close</a>\n' +
                                    '                                </div>\n' +
                                    '                            </div>\n' +
                                    '                            <div class="ek--transfer--body--tabs--inner--age ek--size--16 text--white" >'+ o.age +'</div>\n' +
                                    '                            <div class="ek--transfer--body--tabs--inner--abl ek--size--16 text--white" >'+ Math.ceil(o.ability)+'</div>\n' +
                                    // '                            <div class="ek--transfer--body--tabs--inner--matches ek--size--16 text--white" >'+ o.appearance +'</div>\n' +
                                    '                            <div class="ek--transfer--body--tabs--inner--goals ek--size--16 text--white" >'+ o.goals +'</div>\n' +
                                    '                            <!--<div class="ek&#45;&#45;transfer&#45;&#45;body&#45;&#45;tabs&#45;&#45;inner&#45;&#45;price ek&#45;&#45;size&#45;&#45;16 text&#45;&#45;white" th:text="${player.value} ">$45M</div>-->\n' +
                                    '\n' +
                                    '                             <span class="ek--transfer--body--tabs--inner--price ek--size--16 text--white">$'+sellValueDiv+'</span>\n' +
                                    '\n' +
                                    '                        </div>');

                            }


                    });



                }


            },
        });

    }

    function getBudget(){

        $.ajax({
            url:  '/getUserBudget',
            type: 'get',
            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    console.log("!data");
                }
                else if (res.hasOwnProperty("data")) {

                    if(res.data.money < 1000) {

                        $('.ek--body--head--money #money').html('<span class="text--white ek--size--16">' + res.data.money + '</span>');
                    }

                    else if(res.data.money >=1000 && res.data.money < 1000000) {

                        var money= res.data.money/1000;

                        $('.ek--body--head--money #money').html('<span class="text--white ek--size--16">' + parseInt(money) + '</span><span class="text--white ek--size--16">K</span>');

                    }

                    else if(res.data.money >=1000000 && res.data.money < 1000000000) {

                        var money= (res.data.money)/1000000;
                        console.log(money);
                        console.log(res.data.money);

                        $('.ek--body--head--money #money').html('<span class="text--white ek--size--16">' + parseInt(money) + '</span><span class="text--white ek--size--16">M</span>');

                    }

                    else if(res.data.money >=1000000000) {

                        var money= res.data.money/1000000000;
                        console.log(money);

                        $('.ek--body--head--money #money').html('<span class="text--white ek--size--16">' + parseInt(money) + '</span><span class="text--white ek--size--16">B</span>');

                    }
                    $('.ek--body--head--money #coins').html('<span class="text--white ek--size--16">'+ res.data.coins +'</span>');

                    console.log("success:");
                }


            },
        });

    }
});
