$(document).ready(function () {


    var checkedArray = [];



    $('.ek--daily--body--players--inner--check input').click(function() {
        console.log("1:");
        var checked;
        if ($(this).is(':checked')) {
            console.log("2:");
            checked = ($(this).attr("data-id"));
            console.log("Checked:" + checked);
            checkedArray.push(checked);

        } else {
            console.log(33);

            checked = ($(this).attr("data-id"));
             checkedArray = jQuery.grep(checkedArray, function(value) {
                return value != checked;
            });
            console.log("resultArray:" + checkedArray);

        }


        console.log("checkedArray:" + checkedArray);


    });




    $("#all-check").click(function() {

        if($(this).prop('checked')) {
            $('.ek--daily--body--players--inner--check input').not('.disable').each(function () {
                var id = $(this).attr('data-id');
                checkedArray.push(id);
            });
        } else {
            var leng = checkedArray.length;
            checkedArray.splice(0 , leng);
        }
        console.log("checkedArray:" + checkedArray);


    });



    $('body').on('click', '#trainingCostButton', function (e) {
        var checkedList = checkedArray.toString();
        console.log("checkedList:" + checkedList);
        var trainingStyleId = $(this).attr("data-id");
        console.log("style id:" + trainingStyleId);

        $.ajax({
            url:  '/getTrainingCost',
            type: 'post',
            data: {
                "_token": $('meta[name="csrf"]').attr('content'),
                trainingStyleId: trainingStyleId,
                checkedList: checkedList
            },
            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    console.log("!data");
                }
                else if (res.hasOwnProperty("data")) {
                    if(trainingStyleId==1) {
                        $('#ek--soft #training1').html('<span id="userCoins" class="ek--modal--body--coins--right text--white ek--size--16">' + res.data + '</span><img src="static/img/Dashboard/ek--want--coins.svg" />');
                        $('#ek--soft').modal('show');
                        console.log("success:");
                    }
                    else if(trainingStyleId==2){
                        $('#ek--normal #training1').html('<span id="userCoins" class="ek--modal--body--coins--right text--white ek--size--16">' + res.data + '</span><img src="static/img/Dashboard/ek--want--coins.svg" />');
                        $('#ek--normal').modal('show');
                        console.log("success:");
                    }
                    else if(trainingStyleId==3){
                        $('#ek--high #training1').html('<span  id="userCoins" class="ek--modal--body--coins--right text--white ek--size--16">' + res.data + '</span><img src="static/img/Dashboard/ek--want--coins.svg" />');
                        $('#ek--high').modal('show');
                        console.log("success:");

                    }
                }

            },
        });


        e.preventDefault()
    });

    $('body').on('click', '#trainingButton', function (e) {
        var checkedList = checkedArray.toString();
        console.log("checkedList:" + checkedList);
        var trainingStyleId = $(this).attr("data-id");
        console.log("style id:" + trainingStyleId);



        $.ajax({
            url:  '/getUserTotalBudgets',
            type: 'get',

            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    $('#ek--soft--notenough').modal('show');
                }
                else if (res.hasOwnProperty("data")) {

                    var coins= $('#userCoins').text();
                    console.log("userCoins" + coins);
                    if(parseInt(res.data.coins)< parseInt(coins)){

                        $('#ek--soft--notenough').modal('show');
                        $('.ek--modal--loading').fadeOut('slow');
                    }
                    else{

                        $.ajax({
                            url:  '/start/training',
                            type: 'post',
                            data: {
                                "_token": $('meta[name="csrf"]').attr('content'),
                                trainingStyleId: trainingStyleId,
                                checkedList: checkedList
                            },
                            success: function (res) {
                                if (!res.hasOwnProperty("data")) {
                                    $('#ek--soft--notenough').modal('show');
                                    console.log("!data");
                                }
                                else if (res.hasOwnProperty("data")) {

                                    $('.ek--modal').hide();
                                    // $('.jquery-modal').hide();
                                    // $("#ek--complete").css("display","block");
                                    window.location.pathname = '/daily-camp';
                                    console.log("success:");
                                }

                            },
                        });
                    }


                }


            },
        });




        e.preventDefault()
    });


});
