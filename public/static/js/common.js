$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});

// Functions

var lineUpList = function () {
  $(".ek--lineup--stadium--position--player--empty").click(function () {
    $(".ek--lineup--stadium--position--player--empty").css(
      "background-color",
      "rgba(55, 138, 83, 0.8)"
    );
    $(this)
      .parent()
      .find(".ek--lineup--stadium--position--player--empty")
      .css("background-color", "#2e7546");
  });

  $(".forwardStadium").click(function () {
    var forward = $(".ek--lineup--body--tabs--substitutions--scroll .forward");
    $(".ek--lineup--body--tabs--substitutions--inner").css("order", "unset");
    for (var i = 0; i < forward.length; i++) {
      var elementIndex = -(i + 1);
      for (var j = 0; j < forward.length; j++) {
        forward[j].style.order = elementIndex;
      }
    }
    // $('.ek--lineup--stadium--position--player').css({
    //    opacity: '1',
    //    'pointer-events': 'auto'
    // })
    // $('.ek--lineup--stadium--position--player').not('.forwardStadium').css({
    //    opacity: '0.5',
    //    'pointer-events': 'none'
    // })
    $(".ek--lineup--body--tabs--substitutions--inner").css({
      opacity: "1",
      "pointer-events": "auto",
    });
    $(".ek--lineup--body--tabs--substitutions--inner").not(".forward").css({
      opacity: "0.5",
      "pointer-events": "none",
    });
  });
  $(".midfielderStadium").click(function () {
    var midfielder = $(
      ".ek--lineup--body--tabs--substitutions--scroll .midfielder"
    );
    $(".ek--lineup--body--tabs--substitutions--inner").css("order", "unset");
    for (var i = 0; i < midfielder.length; i++) {
      var elementIndex = -(i + 1);
      for (var j = 0; j < midfielder.length; j++) {
        midfielder[j].style.order = elementIndex;
      }
    }
    $(".ek--lineup--body--tabs--substitutions--inner").css({
      opacity: "1",
      "pointer-events": "auto",
    });
    $(".ek--lineup--body--tabs--substitutions--inner").not(".midfielder").css({
      opacity: "0.5",
      "pointer-events": "none",
    });
  });
  $(".defenderStadium").click(function () {
    var defender = $(
      ".ek--lineup--body--tabs--substitutions--scroll .defender"
    );
    $(".ek--lineup--body--tabs--substitutions--inner").css("order", "unset");
    for (var i = 0; i < defender.length; i++) {
      var elementIndex = -(i + 1);
      for (var j = 0; j < defender.length; j++) {
        defender[j].style.order = elementIndex;
      }
    }
    $(".ek--lineup--body--tabs--substitutions--inner").css({
      opacity: "1",
      "pointer-events": "auto",
    });
    $(".ek--lineup--body--tabs--substitutions--inner").not(".defender").css({
      opacity: "0.5",
      "pointer-events": "none",
    });
  });
  $(".goalkeepStadium").click(function () {
    var goalkeep = $(
      ".ek--lineup--body--tabs--substitutions--scroll .goalkeep"
    );
    $(".ek--lineup--body--tabs--substitutions--inner").css("order", "unset");
    for (var i = 0; i < goalkeep.length; i++) {
      var elementIndex = -(i + 1);
      for (var j = 0; j < goalkeep.length; j++) {
        goalkeep[j].style.order = elementIndex;
      }
    }
    $(".ek--lineup--body--tabs--substitutions--inner").css({
      opacity: "1",
      "pointer-events": "auto",
    });
    $(".ek--lineup--body--tabs--substitutions--inner").not(".goalkeep").css({
      opacity: "0.5",
      "pointer-events": "none",
    });
  });
};
var headerMobileMenu = function () {
  $(".ek--body--head--mobile--icon").click(function () {
    var winWidth = $(window).width();
    if (winWidth < 1200) {
      $(".ek--header").css({
        width: "100%",
        left: "0px",
      });
      $("html , body").css({
        overflow: "hidden",
        height: "100%",
      });
    } else {
      $(".ek--header").css({
        width: "260px",
        left: "0px",
      });
      $("html , body").css({
        overflow: "hidden",
        height: "100%",
      });
    }
  });

  $(".ek--header--logo--close , .ek--header--item").click(function () {
    var winWidth = $(window).width();
    if (winWidth < 1366) {
      $(".ek--header").css({
        width: "260px",
        left: "-260px",
      });
      $("html , body").removeAttr("style");
    }
  });

  $(".ek--header--item--promotion").click(function () {
    $(this).find(".ek--header--link").addClass("ek--header--link--active");
    $(".ek--promotion--modal").fadeIn();
  });
  $(".ek--promotion--modal--close--icon").click(function () {
    $(".ek--promotion--modal").fadeOut();
    $(".ek--promotion--success").fadeOut();
    $(".ek--header--item--promotion")
      .find(".ek--header--link")
      .removeClass("ek--header--link--active");
  });
  $(".ek--ranking--table--button").click(function () {
    $(".ek--ranking--table--button").removeClass("active");
    $(this).addClass("active");
    var id = $(this).attr("id");
    $(".ek--ranking--table--content").hide();
    $("." + id).show();
  });
};
var landingScroll = function () {
  $(".ek--landing--body--scroll").click(function (e) {
    $("html,body").animate(
      {
        scrollTop: $("#ek--feature").offset().top,
      },
      1000
    );
  });
};
var nextMatch = function () {
  $(
    ".ek--body--head--left--match--next , .ek--body--head--left--match--start"
  ).hover(function () {
    $(".ek--body--head--left--match--table").fadeIn("slow");
  });
  $(".ek--body--head--left--match--table").mouseleave(function () {
    $(this).fadeOut("slow");
  });
};
var userDropdown = function () {
  $(".ek--body--head--inner").click(function () {
    var display = $(".ek--body--user").css("display");
    if (display === "none") {
      $(".ek--body--user").fadeIn("slow");
    } else {
      $(".ek--body--user").fadeOut("slow");
    }
  });
};
var leagueTabActive = function () {
  $(".ek--league--tab--button").click(function () {
    $(".ek--league--tab--button").removeClass("button--active");
    $(this).addClass("button--active");
    var id = $(this).attr("id");
    $("." + id).show();
    $(".ek--league--tab--table")
      .not("." + id)
      .hide();
  });
};
var profileTabActive = function () {
  $(".ek--profile--tab--head--button").click(function () {
    $(".ek--profile--tab--head--button").removeClass("button--active");
    $(this).addClass("button--active");
    var id = $(this).attr("id");
    $("." + id).show();
    $(".ek--profile--tab--table")
      .not("." + id)
      .hide();
  });
};
var settingLanguage = function () {
  $(".ek--setting--game--language--select").click(function () {
    $(".ek--setting--game--language--content").toggle();
  });
};
var coinsActive = function () {
  $(".ek--coins--tab--button").click(function () {
    $(".ek--coins--tab--button").removeClass("button--active");
    $(this).addClass("button--active");
    var id = $(this).attr("id");
    $("." + id).show();
    $(".ek--coins--tab--table")
      .not("." + id)
      .hide();
  });
};
var arrangeListActive = function () {
  $(".ek--challenge--friendly--arrange--input").click(function () {
    $(".ek--challenge--friendly--arrange--content").fadeIn("slow");
  });
  $(".ek--challenge--friendly--arrange--item").click(function () {
    $(".ek--challenge--friendly--arrange--item").removeClass("list--active");
    $(this).addClass("list--active");
    var val = $(this).find(".ek--challenge--friendly--arrange--league").text();
    $(".ek--challenge--friendly--arrange--input").attr("value", val);
    $(".ek--challenge--friendly--arrange--arrow label").fadeIn("slow");
    $(".ek--challenge--friendly--arrange--content").fadeOut("slow");
  });
  // $('.ek--challenge--friendly--arrange--item').click(function () {
  //    $('.ek--challenge--friendly--opponent').show();
  // });

  $(".ek--challenge--friendly--opponent--decline").click(function () {
    $(".ek--challenge--friendly--opponent").hide();
    $(".ek--challenge--friendly--arrange--arrow label").hide();
    $(".ek--challenge--friendly--arrange--input").attr("value", "");
  });
  $(".ek--challenge--friendly--opponent--accept").click(function () {
    $(".ek--challenge--tabs--semi").hide();
    $("#ek--challenge--tabs--semi").removeClass("button--active");
    $(".ek--challenge--tabs--eight").show();
    $("#ek--challenge--tabs--eight").addClass("button--active");
    $(".ek--challenge--friendly--opponent").hide();
    $(".ek--challenge--friendly--arrange--input").attr("value", "");
    $(".ek--challenge--friendly--arrange--arrow label").hide();
  });
};
var challengeTabsActive = function () {
  $(".ek--challenge--tabs--button").click(function () {
    $(".ek--challenge--tabs--button").removeClass("button--active");
    $(this).addClass("button--active");
    var id = $(this).attr("id");
    $("." + id).show();
    $(".ek--challenge--tabs--table")
      .not("." + id)
      .hide();
  });
};
var statisticsTabActive = function () {
  $(".ek--statistics--tabs--button").click(function () {
    $(".ek--statistics--tabs--button").removeClass("button--active");
    $(this).addClass("button--active");
    var id = $(this).attr("id");
    $("." + id).show();
    $(".ek--statistics--tabs--table")
      .not("." + id)
      .hide();
  });
};
var transferTabActive = function () {
  $(".ek--transfer--body--tabs--button").click(function () {
    $(".ek--transfer--body--tabs--button").removeClass("button--active");
    $(this).addClass("button--active");
    var id = $(this).attr("id");
    $("." + id).show();
    $(".content--active")
      .not("." + id)
      .hide();
  });

  // $('.ek--transfer--body--tabs--inner--player>a').click(function () {
  //     // $('.ek--transfer--background').show();
  //     $('#ek--transfer--modal').fadeIn();
  // });
  // $('.ek--squad--table--body--player').click(function () {
  //     $(this).find('#ek--squad--modals--player').css('display' , 'flex');
  // });
  $(".ek--close--icon").click(function () {
    $("#ek--transfer--modal").fadeOut();
    $(".ek--squad--background").fadeOut();
  });

  /*$('.ek--squad--modals--inner--refill--energy').click(function () {
        $(this).parents('.ek--squad--table--body--player').find('#ek--squad--modals--player').fadeOut();
        $(this).parents('.ek--squad--table--body--player').find('#ek--squad--modals--refill--buy').fadeIn();
    });*/
};
var openStartTrainign = function () {
  $(".ek--start--training").click(function () {
    $(".ek--modal--loading").fadeIn("slow");

    //    setTimeout(function() {
    //       $('.ek--modal--loading').fadeOut('slow');
    //       // $('.ek--training').hide();
    //       // $('.ek--complete').fadeIn('slow');
    //    // }, 1000);
  });

  // $('.ek--start--training--camp').click(function() {
  //    $('.ek--training--loading').fadeIn('slow');
  //
  //    setTimeout(function() {
  //       $('.ek--training--loading').fadeOut('slow');
  //       $('.ek--training--success').fadeIn('slow');
  //    }, 1000);
  // });

  $(".ek--training--success--cancel").click(function () {
    $(".ek--training--success").fadeOut("slow");
    $(".ek--camp--body--choose").hide();
    $(".ek--camp--complete").fadeIn("slow");
  });
  /*$('.ek--transfer--body--tabs--inner--player').click(function () {
       $(this).find('#ek--transfer--modal').fadeIn();
   });
   $('.ek--squad--modals--inner--refill').click(function () {
       $(this).parents('.ek--transfer--body--tabs--inner--player').find('#ek--transfer--modal').fadeIn();
       $(this).parents('.ek--transfer--body--tabs--inner--player').find('#ek--transfer--buy').fadeIn();
   });*/
};
var allCheck = function () {
  // $('.ek--daily--body--players--count--training').prop('disabled' , true);
  $(".ek--daily--body--players--count--training").addClass("btn-is-disabled");
  // $('.ek--mobile--play--continue').prop('disabled' , true);
  $(".ek--mobile--play--continue").addClass("btn-is-disabled");
  $(".ek--daily--body--players--inner--check .disable").prop("disabled", true);
  $(".ek--daily--body--players--head--check").click(function () {
    var checked = $(this).find("input").prop("checked");
    var disCheck = $(".ek--daily--body--players--inner--check")
      .find("input")
      .hasClass("disable");
    if (checked) {
      $(".ek--daily--body--players--inner--check")
        .find("input")
        .prop("checked", true);
      $(".ek--daily--body--players--count--training")
        .removeClass("btn-is-disabled")
        .addClass("training--active");
      $(".ek--mobile--play--continue")
        .removeClass("btn-is-disabled")
        .css("background-color", "#2979FF");
      // $('.ek--daily--body--players--count--amount span').text(inputCheck.length - disableCheck.length);
      if (disCheck) {
        $(".ek--daily--body--players--inner--check .disable").prop(
          "checked",
          false
        );
        $(".ek--daily--body--players--inner--check .disable").prop(
          "disabled",
          true
        );
      }
      var int = $(".ek--daily--body--players--inner--check input:checked")
        .length;
      $(".ek--daily--body--players--count--amount span").text(int);
    } else {
      $(".ek--daily--body--players--inner--check")
        .find("input")
        .prop("checked", false);
      $(".ek--daily--body--players--count--training")
        .addClass("btn-is-disabled")
        .removeClass("training--active");
      $(".ek--mobile--play--continue")
        .addClass("btn-is-disabled")
        .css("background-color", "#495468");
      $(".ek--daily--body--players--count--amount span").text(0);
    }
  });
  $(
    ".ek--daily--body--players--count--training , .ek--mobile--play--continue"
  ).click(function () {
    $(".ek--daily--body").hide();
    $(".ek--training").show();
    $(".ek--mobile--play--continue").hide();
  });
  $(".ek--daily--body--players--inner--check input").click(function () {
    var checked = $(this).prop("checked");
    var int = $(".ek--daily--body--players--inner--check input:checked").length;
    if (checked) {
      $(".ek--daily--body--players--count--training")
        .removeClass("btn-is-disabled")
        .addClass("training--active");
      $(".ek--mobile--play--continue")
        .removeClass("btn-is-disabled")
        .css("background-color", "#2979FF");
      $(".ek--daily--body--players--count--amount span").text(int);
    } else {
      int - 1 + 1;
      $(".ek--daily--body--players--count--amount span").text(int);
      if (int == 0) {
        $(".ek--daily--body--players--head--check input").prop(
          "checked",
          false
        );
        $(".ek--daily--body--players--count--training")
          .addClass("btn-is-disabled")
          .removeClass("training--active");
        $(".ek--mobile--play--continue")
          .addClass("btn-is-disabled")
          .css("background-color", "#495468");
      }
    }
  });
  // $('.ek--training--type--head--back').click(function() {
  //     $('.ek--training').hide();
  //     $('.ek--daily--body').show();
  // });
};
// var rangeSlider = function() {
//    $('#gamestyle , #passingstyle , #pressingstyle').rangeslider({
//       polyfill: false
//
//
//   }).on('change', function() {
//    var values = $(this).val();
//    var numValue = Number(values);
//    $(this).attr('value', numValue);
//    if(numValue === 1) {
//       $(this).parent().find('.soft').addClass('range--active').nextAll().removeClass('range--active');
//    }
//    if (numValue === 2) {
//       $(this).parent().find('.normal').addClass('range--active');
//       $(this).parent().find('.normal').prev().removeClass('range--active');
//       $(this).parent().find('.normal').next().removeClass('range--active');
//    }
//    if (numValue === 3) {
//       $(this).parent().find('.aggresive').addClass('range--active').prevAll().removeClass('range--active');
//    }
//    });
//
// }

var lineupTabs = function () {
  $(".ek--lineup--body--tabs--button").click(function () {
    $(".ek--lineup--body--tabs--button").removeClass("button--active");
    $(this).addClass("button--active");
    var id = $(this).attr("id");
    $("." + id).show();
    $(".ek--lineup--body--tabs--content--table")
      .not("." + id)
      .hide();
  });

  $(".ek--lineup--stadium--position--player").click(function () {
    var winWIdth = $(window).width();
    if (winWIdth <= 992) {
      $(".ek--lineup--body--tabs--substitutions").css({
        position: "fixed",
        top: 0,
        left: 0,
        "z-index": 1100,
        height: "100%",
        "background-color": "#323B4C",
        padding: "24px 15px",
      });
      $(".ek--lineup--body--tabs--substitutions--scroll").css(
        "max-height",
        "initial"
      );
      $(".ek--lineup--body--tabs--substitutions--inner").click(function () {
        $(".ek--lineup--body--tabs--substitutions").removeAttr("style");
        $(".ek--lineup--body--tabs--substitutions--scroll").removeAttr("style");
      });
    }
  });
};
var matchTabs = function () {
  $(".ek--match--body--stats--tabs--head--button").click(function () {
    $(".ek--match--body--stats--tabs--head--button").removeClass(
      "button--active"
    );
    $(this).addClass("button--active");
    var id = $(this).attr("id");

    $("." + id).show();
    $(".ek--match--body--stats--tabs--filter")
      .not("." + id)
      .hide();
  });
};
var checkInput = function () {
  $(".ek--footer--body--form--button").click(function (e) {
    var inputVal = $(".ek--input").val();
    var textareaVal = $(".ek--footer--body--form--textarea").val();
    if (
      (!inputVal.includes("@" && ".") || inputVal === "") &&
      textareaVal === ""
    ) {
      var i = inputVal;
      var t = textareaVal;
      if (!i) {
        $(".ek--input")
          .css({
            border: "2px solid var(--Red)",
          })
          .next()
          .css("display", "flex");
      }

      if (!t) {
        $(".ek--footer--body--form--textarea")
          .css({
            border: "2px solid var(--Red)",
          })
          .next()
          .css("display", "flex");
      }
    } else {
      $(".ek--footer--body--form").css({
        display: "none",
      });

      $(".ek--footer--body--form--success").css({
        display: "block",
        visibility: "visible",
        opacity: "1",
      });
    }

    e.preventDefault();
  });
};

// ----------------------------------- New Label Function --------------------------------------
var label = function () {
  $(".ek--footer--body--form--group input").keyup(function () {
    var create = $(this).parents(".ek--create--div").length;
    var verify = $(this).parents(".ek--create--verify").length;
    var formId = $(this).parents(".ek--parent");
    var formCreateInput = $(this).hasClass("ek--form--create");
    var formIdEmail = formId.find(".ek--manager--email").length;
    var formIdPass = formId.find(".ek--manager--password").length;
    // let formIdSelect = formId.find('.ek--create--search').val(' ');
    if (create) {
      $(this)
        .parents(".ek--create--div")
        .find(".ek--form--button a , .ek--form--button")
        .css({
          backgroundColor: "var(--Green)",
          color: "var(--text--navy--900)",
        });
    } else {
      if (formIdEmail >= 1 && formIdPass >= 1) {
        var emailTxt = formId.find(".ek--manager--email").val();
        var passTxt = formId.find(".ek--manager--password").val().length;
        if (emailTxt.includes("@") && emailTxt.includes(".") && passTxt >= 8) {
          checkInput(formId);
        } else {
          formId.find(".ek--form--button a , .ek--form--button").css({
            backgroundColor: "var(--Navy--Blue--500)",
            color: "var(--text--navy--300)",
          });
        }
      } else {
        checkInput(formId);
      }
    }
  });
  $(".ek--footer--body--form--group input").each(function () {
    if ($(this).val().length >= 1) {
      $(this).parent().find("label").css({
        top: "5px",
        "font-size": "13px",
      });
    } else {
      $(this).parent().find("label").css({
        top: "20px",
        "font-size": "16px",
      });
    }

  });

  $(".ek--footer--body--form--group").click(function () {
    $(this).find("input").focus();
    $(this).find("label").css({
      top: "5px",
      "font-size": "13px",
    });
  });

  $(".ek--footer--body--form--group input").blur(function () {
    if ($(this).val().length >= 1) {
      $(this).parent().find("label").css({
        top: "5px",
        "font-size": "13px",
      });
    } else {
      $(this).parent().find("label").css({
        top: "20px",
        "font-size": "16px",
      });
    }
  });

  $(".ek--footer--body--form--group input").change(function () {
    var val = $(this).val().length;
    if (val >= 1) {
      $(this).parent().find("label").css({
        top: "5px",
        "font-size": "13px",
      });
    } else {
      $(this).parent().find("label").css({
        top: "20px",
        "font-size": "16px",
      });
    }
  });

  $(".ek--manager--email").keyup(function () {
    var valText = $(this).val();
    if (valText.includes("@") && valText.includes(".")) {
      $(this).css({
        borderColor: "#495468",
      });
    } else {
      $(this).css({
        borderColor: "#f03d3e",
      });
    }
  });

  $(".ek--manager--password").keyup(function () {
    var valLength = $(this).val().length;
    if (valLength >= 8) {
      $(this).css({
        borderColor: "#495468",
      });
    } else {
      $(this).css({
        borderColor: "#f03d3e",
      });
    }
  });

  function checkInput(formId) {
    var $this = formId;
    const $thisInputLength = $this.find(".ek--footer--body--form--group input")
      .length;
    var count = 0;

    $this.find(".ek--footer--body--form--group input").each(function () {
      if ($(this).val().length >= 1) {
        count++;
        if ($thisInputLength === count) {
          $this.find(".ek--form--button a , .ek--form--button").css({
            backgroundColor: "var(--Green)",
            color: "var(--text--navy--900)",
          });
        } else {
          $this.find(".ek--form--button a , .ek--form--button").css({
            backgroundColor: "var(--Navy--Blue--500)",
            color: "var(--text--navy--300)",
          });
        }
      }
    });
  }

  // $('.ek--create--select').focus(function () {
  //    $(this).parent().find('.ek--create--content').css('display', 'block');
  // });

  $(document).on("mouseup", function (e) {
    var item = e.target.classList.contains("ek--create--select");
    if (!item) {
      $(".ek--create--content").hide();
    }
  });
};

var searchInput = function () {

  $(".ek--create--search").keyup(function () {
    var input, filter, ul, li, a, i;
    input = $("#selectedCountry");
    // console.log(input.val())
    filter = input.val().toUpperCase();
    ul = document.getElementById("ek--create--content--nav");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
      a = li[i].getElementsByTagName("a")[0];
      if (a.getElementsByTagName("span")[0].innerHTML.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display = "";
      } else {
        li[i].style.display = "none";
      }
    }
  });
};



var createActive = function () {
  $(".ek--create--content--item").click(function () {
    $(".ek--create--content--item").removeClass("ek--create--active");
    $(this).addClass("ek--create--active");
    $(".ek--create--select").attr(
      "value",
      $(this).find(".ek--create--content--link").text()
    );
    $(".ek--create--select").attr(
      "data-id",
      $(this).find(".ek--create--content--link").attr("data-id")
    );
    $(".ek--create--content").fadeOut();
  });
  $(".ek--create--button a").click(function () {
    var checkInput = $(
      "#create-club .ek--footer--body--form--group input"
    ).val();
    if (checkInput != "") {
      $("#create-club").hide();
      $("#ek--manager").fadeIn("slow");
      $("#create-club-social").hide();
    } else {
      $("#create-club .ek--footer--body--form--group:first-child input").css(
        "border",
        "2px solid red"
      );
    }
  });
  $(".ek--manager--back").click(function () {
    var id = $(this).attr("id");
    if (id === "registerBack") {
      $("#ek--create--verifycode input").val("");
      $("#ek--create--verifycode").hide();
      $("#ek--manager").show();
    } else if (id === "forgetPasswordEmailBack") {
      $("#ek--create--forget").hide();
      $("#ek--create--sign").show();
    } else if (id === "sendemailback") {
      $("#ek--create--send").hide();
      $("#ek--create--forget").show();
    } else if (id === "forgetPassBack") {
      $("#ek--create--verifycode input").val("");
      $("#ek--create--verifycode").hide();
      $("#ek--create--forget").show();
    } else if (id === "createNewPassBack") {
      $("#ek--create--change input").val("");
      $("#ek--create--verifycode input").val("");

      $("#ek--create--change").hide();
      $("#ek--create--forget").show();
    } else {
      $("#ek--manager").hide();
      $("#create-club").fadeIn("slow");
    }
  });
  // $('.bg--facebook , .bg--google').click(function (e) {
  //   $('#ek--manager').hide();
  //   $('#create-club-social').fadeIn('slow');
  //   $('#ek--create--sign').hide();
  //   e.preventDefault();
  // });
  $(".ek--sign--button").click(function () {
    $("#ek--manager , #create-club , #create-club-social").hide();
    $("#ek--create--sign").fadeIn("slow");
  });
  $(".ek--sign--button--up").click(function () {
    $("#ek--create--sign").hide();
    $("#create-club").fadeIn("slow");
  });

  $(".ek--squad--open").click(function () {
    $(this).parent().find(".ek--modal--background").css("display", "flex");
    $(this).parent().find(".ek--squad--modals--player").css({
      display: "block",
      margin: "auto",
    });
  });

  // $('.ek--notification--right--cancel a').click(function () {
  //     $('.ek--modal--background').hide();
  // });
  // $('.ek--close').click(function () {
  //     $(this).parent().parent().hide();
  // });
  // $('.ek--squad--modals--inner--refill').click(function () {
  //     $(this).parent().parent().hide();
  //     $(this).parent().parent().parent().find('#ek--squad--modals--refill').css({
  //         display : 'block',
  //         margin: "auto"
  //     });
  // });
  // $('.ek--squad--modals--refill--buttons--cancel').click(function () {
  //     $(this).parent().parent().hide();
  //     $(this).parent().parent().parent().parent().find('.ek--squad--modals--player').css({
  //         display : 'block',
  //         margin: "auto"
  //     });
  // });
  // $('.ek--squad--modals--refill--buttons--energy').click(function () {
  //     $(this).parent().parent().hide();
  //     $(this).parent().parent().parent().parent().find('#ek--squad--modals--refill--buy').css({
  //         display : 'block',
  //         margin: "auto"
  //     });
  // });
  // $('.ek--squad--modals--refill--buttons--cancel2').click(function () {
  //     $(this).parent().parent().hide();
  //     $(this).parent().parent().parent().find('.ek--squad--modals--refill').css({
  //         display : 'block',
  //         margin: "auto"
  //     });
  // });;
  // $('.ek--notification--right--cancel').click(function () {
  //     $(this).parent().parent().parent().hide();
  //     $(this).parent().parent().parent().parent().hide();
  // });
  // $('.ek--squad--modals--refill--buttons--energy2').click(function () {
  //     // $(this).parent().parent().hide();
  //     // $(this).parent().parent().parent().parent().find('#ek--notification').css({
  //     //     display : 'block',
  //     //     margin: "auto"
  //     // });
  // });
};
var createContent = function () {
  $(".ek--create--select").click(function () {
    $(".ek--create--content").toggle();
  });

  $(".ek--create--content--item").click(function () {
    $(".ek--create--content").hide();
    $(".ek--create--search").attr("value", "");
  });

  $(".ek--create--button").click(function (e) {
    e.preventDefault();
  });

  $(".ek--create--forget").click(function () {
    $("#ek--create--sign").hide();
    $("#ek--create--forget").fadeIn();
  });
  $(".ek--create--signin a").click(function () {
    $("#ek--create--forget").hide();
  });

  var inputQuantity = [];

  $(function () {
    $(".quantity").each(function (i) {
      inputQuantity[i] = this.defaultValue;
      $(this).data("idx", i); // save this field's index to access later
    });
    $(".quantity").on("keyup", function (e) {
      $(this).addClass("ok");
      var $field = $(this),
        val = this.value,
        $thisIndex = parseInt($field.data("idx"), 10);
      if (e.which !== 8) {
        $field.parent().next().children("input").focus();
        var okLength = $(".quantity.ok").length;
        var quaLength = $(".quantity").length;
        if (okLength == quaLength) {
          $("#ek--create--verifycode #confirmCode")
            .removeClass("button--navy--500")
            .addClass("button--green");
        } else {
          $("#ek--create--verifycode #confirmCode")
            .removeClass("button--green")
            .addClass("button--navy--500");
        }
      }else if(e.which === 8){
          $field.parent().prev().children("input").focus();
      } else {
        $field.removeClass("ok");
        var okLength = $(".quantity.ok").length;
        var quaLength = $(".quantity").length;
        if (okLength == quaLength) {
          $("#ek--create--verifycode #confirmCode")
            .removeClass("button--navy--500")
            .addClass("button--green");
        } else {
          $("#ek--create--verifycode #confirmCode")
            .removeClass("button--green")
            .addClass("button--navy--500");
        }
      }
      // retrieve the index
      //        window.console && console.log($field.is(":invalid"));
      //  $field.is(":invalid") is for Safari, it must be the last to not error in IE8
      if (
        (this.validity && this.validity.badInput) ||
        isNaN(val) ||
        $field.is(":invalid")
      ) {
        this.value = inputQuantity[$thisIndex];
        return;
      }
      if (val.length > Number($field.attr("maxlength"))) {
        val = val.slice(0, 5);
        $field.val(val);
      }
      inputQuantity[$thisIndex] = val;
    });
  });
};
var ekLanguageContent = function () {
  $(".ek--landing--language").click(function () {
    var contentCss = $(".ek--landing--language--content").css("display");
    if (contentCss === "none") {
      $(".ek--landing--language--content").show().css("opacity", "1");
    }
    if (contentCss === "block") {
      $(".ek--landing--language--content").css("opacity", "0").hide();
    }
  });
};
var accordion = function () {
  $(document).ready(function () {
    function initCollapsibleWithjQuery(startOpen) {
      var collapsibleButtons$ = $(".collapsible");
      collapsibleButtons$.each(function (index, ele) {
        var collapsible$ = $(ele),
          content$ = collapsible$.next();
        collapsible$.click(function () {
          collapsible$.toggleClass("active");
          if (!collapsible$.hasClass("active")) {
            content$.slideUp();
          } else {
            content$.slideDown();
          }
        });
      });
      if (startOpen) {
        collapsibleButtons$.click();
      }
    }

    initCollapsibleWithjQuery(false);
    // initCollapsibleWithVanillaJs();
  });
};
var stadiumAllJS = function () {
  $(".ek--stadium--right--head button").click(function () {
    $(".ek--stadium--right--head button").removeClass("active");
    $(this).addClass("active");
    var id = $(this).attr("id");
    $(".ek--stadium--right--table").hide();
    $("." + id).show();
  });
  $(".ek--stadium--right--inner--buy").click(function () {
    $(this)
      .parents(".ek--stadium--right--inner--about")
      .find(".ek--stadium--right--modal")
      .fadeIn();
  });
  $(".ek--stadium--modal--close").click(function () {
    $(".ek--stadium--right--modal").fadeOut();
  });
  $(".ek--stadium--modal--button").click(function () {
    $(".ek--stadium--right--modal").hide();
    $(this)
      .parents(".ek--stadium--right--inner--about")
      .find(".ek--stadium--right--upgrade")
      .fadeIn();
  });
  $(".ek--stadium--upgrade--cancel").click(function () {
    $(".ek--stadium--right--upgrade").fadeOut();
  });
};

var marketingChoose = function () {
  var _this;
  $(".ek--marketing--agency--button button").click(function () {
    $(this).toggleClass("choose");
    var cls = $(this).hasClass("choose");
    $(".ek--marketing--agency--button button").removeClass("bg--blue");
    $(".ek--marketing--agency--button button").addClass("bg--navy--500");
    $(".ek--marketing--agency--button button").addClass("btn-is-disabled");

    // $('.ek--marketing--agency--button button').attr("id", "choose");

    _this = $(this);
    _this.removeClass("bg--navy--500");
    _this.removeClass("btn-is-disabled");
    _this.addClass("bg--blue");
    if (cls) {
      _this.text("Cancel");
      /*_this.attr('id' , 'one');*/
      // _this.attr("id", "cancel");
    } else {
      _this.text("Choose");
      // _this.attr('id' , 'choose');
      $(".ek--marketing--agency--button button").addClass("bg--blue");
      $(".ek--marketing--agency--button button").removeClass("bg--navy--500");
      $(".ek--marketing--agency--button button").removeClass("btn-is-disabled");
    }
  });
};

/*var skip = function () {*/

$("#skip1").click(function () {
  $(this).attr("id", "skip2");

  /*$('#skip2').click(function () {
        $('.ek--match--body--game').fadeOut(500);
        $('.ek--match--body--stats').fadeIn(1000);
        });
    });*/
});

/*$('.toggle-control').click(function () {
        $(this).toggleClass('active');
        var toggleClass = $(this).hasClass('active');
        if(toggleClass) {
            $('.ek--setting--game--sound--disabled').text('enabled');
        } else {
            $('.ek--setting--game--sound--disabled').text('disabled');
        }
    });*/

// Functions Init
var init = function () {
  ekLanguageContent();
  accordion();
  label();
  checkInput();
  searchInput();
  createActive();
  createContent();
  matchTabs();
  lineupTabs();
  // rangeSlider();
  allCheck();
  openStartTrainign();
  transferTabActive();
  statisticsTabActive();
  challengeTabsActive();
  arrangeListActive();
  coinsActive();
  settingLanguage();
  profileTabActive();
  leagueTabActive();
  userDropdown();
  nextMatch();
  lineUpList();
  marketingChoose();
  landingScroll();
  headerMobileMenu();
  stadiumAllJS();
};

// OK. Finished
$(function () {
  init();
});

$(".ek--setting--game--sound--right").click(function () {
  var chLength = $(this).find("input:checked").length;

  if (chLength > 0) {
    $(".ek--setting--game--sound--disabled").text("Enabled");
  } else {
    $(".ek--setting--game--sound--disabled").text("Disabled");
  }
});

$(document).on("click", ".ek--lineup--stadium--position--player", function () {
  $(".ek--lineup--body--tabs--substitutions--scroll").animate(
    {
      scrollTop: 0,
    },
    400
  );
});

// ------------------------------------------- New Javascript Update -----------------------------------------------

$(document).ready(function () {
  $(".ek--footer--body--form--group input").each(function () {
    if ($(this).val().length >= 1) {
      $(this).parent().find("label").css({
        top: "5px",
        "font-size": "13px",
      });
    } else {
      $(this).parent().find("label").css({
        top: "20px",
        "font-size": "16px",
      });
    }

    $(this).on("keyup",function(){
        $(this).parent().find("label").css({
            top: "5px",
            "font-size": "13px",
        });
    })
  });

});

$(document).on("click", ".ek--password--eye", function (e) {

  let input = $(this).parents(".ek--password").find(".ek--manager--password");
  let type = $(this)
    .parents(".ek--password")
    .find(".ek--manager--password")
    .attr("type");
  if (type === "password") {
    input.attr("type", "text");
    $(this).find(".icon-eye").hide();
    $(this).find(".icon-eye-2").show();
  } else {
    input.attr("type", "password");
    $(this).find(".icon-eye").show();
    $(this).find(".icon-eye-2").hide();
  }
});

function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0) {
            timer = 0;
            $(".ek--body--head--left--match--button a").removeClass("button--gray");
            $(".ek--body--head--left--match--button a").removeClass("btn-is-disabled");
            $("#time").hide();
        }
    }, 1000);
}

var display = document.querySelector('#time');
var duration = display.getAttribute("data-time");
startTimer(duration, display);
