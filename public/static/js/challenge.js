$(document).ready(function () {

    $('body').on('click', '#newSeasonBtn', function (e) {


        function load() {
            $('#loading').show();
        }

        window.onload = load();

        $.ajax({
            url: '/createNewSession',
            type: 'get',
            data: {
                tournamentId: $("#tournamentId").val(),
            },
            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    console.log("!data");
                }
                else if (res.hasOwnProperty("data")) {
                    location.reload();


                    // $('#loading').modal('hide');
                    $('#newSeasonSuccess').modal('show');

                    console.log("success:");
                }


            },
        });


        e.preventDefault()
    });


});
