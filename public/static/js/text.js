var _text = {


    a1: "GOAL by [player name]. It is just sensational!",
    a2: "GOAL! [player name] scores! Oh, what an effort!",
    a3: "GOAL! It’s a brilliant header from [player name].",
    a4: "GOAL! Goalkeeper can’t stop [player name] scoring.",
    a5: "GOAL! Superb finish by [player name].",
    a6: "Devastating finish from [player name] and it is GOAL!",
    a7: "GOAL from [player name]. That is special!",
    a8: "GOAL! Superb volley from [player name].",
    a9:" A terrific GOAL by [player name].",
    a10: "Dangerous attack and GOAL! [player name] scores!",
    a11:  " [player name] takes control of the ball. Shot and… GOAL!",
    a12: "Quick counter attack and GOAL! No comment! It’s [player name].",
    a13:"Quick counter attack and GOAL! [player name] scores!",
    a14: "GOAL! Ahahaha. Very funny goal by [player name].",
    a15:"GOAL! [player name] scores a fantastic goal!",
    a16:"Shot and GOAL! It’s was an easy goal for [player name].",
    a17:"Great dribbling from [player name] and great GOAL!",
    a18: "Lovely touch, amazing GOAL by [player name].",
    a19: " [player name] scores! No comment!",
    a20: "GOAL! Bravo, [player name]. That was a great run!"


};


var _textRed ={

    a1: "Very dangerous play and direct RED card to [player name].",
    a2: "[player name] is out now!",
    a3: "It’s RED card! [player name] is out!",
    a4: "[player name] is OFF! Players are nervous.",
    a5: "RED! [player name] is sent off!",
    a6: "[player name] gets red card for protesting to the referee.",
    a7: "Oh! RED card for [player name].",
    a8: "It's RED card for [player name].",
    a9: "RED card for [player name].",
    a10: "Referee sends [player name] OFF!",
    a11:  " Very dangerous play and direct RED card to [player name].",
    a12: "[player name] is out now!",
    a13:"It’s RED card! [player name] is out!",
    a14: "[player name] is OFF! Players are nervous.",
    a15:"RED! [player name] is sent off!",
    a16:"[player name] gets red card for protesting to the referee.",
    a17:"Oh! RED card for [player name].",
    a18: "It's RED card for [player name].",
    a19: "RED card for [player name].",
    a20: "Referee sends [player name] OFF!"


};



var _textYellow ={

    a1: "Dangerous play by [player name] and yellow card!",
    a2: "[player name] is shown the yellow car.",
    a3: "[player name] commits a rough foul and gets yellow card.",
    a4: "[player name] gets himself a booking.",
    a5: "[player name] goes into the book for a trip on his opponent.",
    a6: "[player name] gets booking as he brings down his opponent.",
    a7: "Yellow card for [player name] after he trips up the opponent.",
    a8: "Yellow card for [player name] for his challenge on opponent.",
    a9: "Yellow! [player name] went in very, very strong on the opponent.",
    a10: "It's Yellow! Referee books [player name].",
    a11:  "Yellow! Referee books [player name].",
    a12: "[player name] is booked for a late challenge on his opponent.",
    a13:"[player name] finds himself in the book for catching the opponent.",
    a14: "Yellow! [player name] is booked after sliding in on his opponent.",
    a15:"Yellow! [player name] is penalised for a foul on his opponent.",
    a16:"Yellow for [player name].",
    a17:"Yellow! [player name] played dangerous and referee saw it.",
    a18: "it's foul and it's Yellow card for [player name].",
    a19: "It was dangerous as [player name] saw Yellow!",
    a20: "Yellow! It was difficult for [player name] to stop the opponent."


};


var _textOther ={

    a1: "Player wins a free kick in the defensive half.",
    a2: "Player wins a free kick on the right wing.",
    a3: "Player wins a free kick on the left wing.",
    a4: "Foul by Player.",
    a5: "Offside! It’s Player who is caught offside.",
    a6: "After a VAR review, referee says it’s not a penalty!",
    a7: "It’s foul. Players play aggressive today",
    a8: "The Team is in control of the ball.",
    a9: "Player got the ball and lost it.",
    a10: "Player plays very well today.",
    a11:  "Goal kick for the opponent team.",
    a12: "The referee says it’s a free kick.",
    a13: "Great shot from outside the box, but not a goal.",
    a14: "Shot, but not Goal. It was excellent defending.",
    a15: "Beautiful strike! But no Goal!",
    a16: "Good shot from the free-kick, saved by goalkeeper..",
    a17: "Dangerous free-kick.",
    a18: "Player wins a free kick, but no result.",
    a19: "Player fires a ferocious shot on goal, but no goal.",
    a20: "Chance for attack, but midfielders don't play carefully."


};