$(document).ready(function () {

       getMatches();
       getPlayNextMatch();

       function getPlayNextMatch() {
        $.ajax({
            url: 'friendly/all/matches',
            type: 'get',
            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    console.log("!data");
                }

                else if(res.data==null){
                    $('.ek--body--head--left--match bg--navy--700').css("display", "none");

                }
                else if (res.hasOwnProperty("data") && res.data!= null) {

                    var loop = false;

                    $.each(res.data, function (i, o) {

                        if (o.played == false && loop == false) {

                            $('.ek--body--head--left--match--table--clubs #homeTeam').html('<span class="text--white">' + o.homeTeam + '</span>');
                            $('.ek--body--head--left--match--table--clubs #awayTeam').html('<span class="text--white">' + o.awayTeam + '</span>');
                            $('.ek--body--head--left--match--table--clubs #homeScore').html('<span class="text--navy--900">' + Math.ceil(o.homePower) + '</span>');
                            $('.ek--body--head--left--match--table--clubs #awayScore').html('<span class="text--navy--900">' + Math.ceil(o.awayPower) + '</span>');


                            loop = true;

                        }

                    })
                }


            },

        });
    }


    $('body').on('click', '#random', function (e) {

        var level = $(this).attr("data-id");

        console.log("level:" +level);

        $.ajax({
            url:  '/friendly/random/club',
            type: 'get',
            data:{
                level:level
            },
            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    // $('#ek--soft--notenough').modal('show');
                    console.log("!data");
                }
                else if (res.hasOwnProperty("data") &&  res.data!= null) {
                    console.log(res.data)
                    $('#randomClub #club').html('<span class="ek--size--20 text--white">'+ res.data.awayTeam.clubName +'</span>');
                    $('#randomClub #power').html('<span class="ek--size--20 text--white">'+ Math.ceil(res.data.awayTeam.ability) +'</span>');
                    $('#randomClub #powerr').html(Math.ceil(res.data.awayTeam.ability));
                    $('#randomClub #removeButton').attr("data-id" , res.data.gameCode);
                    console.log("gameCode:" + res.data.gameCode)
                    $('#randomClub').show();

                    console.log("success:");
                }

            },
        });


        e.preventDefault()
    });


    $('body').on('click', '#removeButton', function (e) {

        var gameCode = $(this).attr("data-id");

        console.log("gameCode:" +gameCode);

        $.ajax({
            url:  '/friendly/delete/match',
            type: 'get',
            data:{
                gameCode:gameCode
            },
            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    console.log("!data");
                }
                else if (res.hasOwnProperty("data")) {

                    $('#randomClub').hide();
                    console.log("success:");
                }

            },
        });


        e.preventDefault()
    });


    function getMatches(){

        $.ajax({
            url: 'friendly/all/matches',
            type: 'get',
            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    console.log("!data");
                }

                else if(res.data==null){
                    $('#match-ready').css("display", "none");
                }
                else if (res.hasOwnProperty("data")) {

                    $('#fixture').html('');
                    $.each(res.data, function (i, o) {

                        if (o.played == false ) {

                            $('#fixture').append('<div  class="ek--challenge--tabs--inner bg--navy--600"><div class="ek--challenge--tabs--inner--left ek--size--16 text--yellow" >'+ o.homeTeam +'</div>\n' +
                                '<div class="ek--challenge--tabs--inner--center ek--size--16 text--navy--200">vs</div>' +
                                '<div class="ek--challenge--tabs--inner--right ek--size--16 text--white" >'+ o.awayTeam +'</div></div>');


                        }

                    })
                }


            },

        });


    };

    $('body').on('click', '#accept', function (e) {

        getMatches();
        getPlayNextMatch();
        e.preventDefault();

    });


});
