$(document).ready(function () {
    $.ajaxSetup({
        headers: {"X-CSRF-TOKEN": $('meta[name=csrf]').attr('content')}
    });

    // var selectedCountry = $("#selectCountry");
    //
    // $.ajax({
    //     url: '/getCountries',
    //     type: 'get',
    //     success: function (res) {
    //         if (!res.hasOwnProperty("data")) {
    //             console.log("!data");
    //         }
    //
    //         else if (res.hasOwnProperty("data") && res.data != null) {
    //
    //             var countries = $(".countries");
    //
    //
    //             $.each(res.data, function(i,o){
    //                 // console.log(o)
    //                 countries.append(
    //                     '<li class="ek--create--content--item"> ' +
    //                     '   <a class="text--navy--200" href="javascript:void(0)" >\n' +
    //                     '      <span id="countryId" class="ek--create--content--link text--navy--200" data-id='+o.id+' text='+o.name+'>\n' +
    //                     '\n' +  o.name +
    //                     '      </span>\n' +
    //                     '   </a>' +
    //                     '</li>'
    //                 )
    //             });
    //
    //
    //         }
    //     },
    //
    //
    // });
    //
    // $("a").on("click",function(e){
    //     // selectedCountry.attr("data-id", $(this).attr("data-id"));
    //     // selectedCountry.attr("value", $(this).attr("text"));
    //     console.log('hi')
    // });

    $('body').on('click', '#register', function (e) {

        $('#regMessage').html('');
        $('#regMessage').hide();

        var email= ($('#email').val() || '').trim();
        var fullName=$('#fullName').val();
        var clubName=($('#clubName').val()  || '').trim();
        var password=$('#password').val();
        var countryId=$('#selectCountry').attr("data-id");

          $('#verifyEmail').val(email);


          if(password.length < 8){
              $('#regMessage').html('Password must be at least 8 character!');
              $('#regMessage').css('display','flex');

          }

          else if(email.includes("@")){


              $('#register span').hide();
              $('#register img').show();


              $.ajax({
                  url: '/register',
                  type: 'post',
                  data: {
                      email: email,
                      password: password,
                      fullName: fullName,
                      countryId: countryId,
                      clubName: clubName
                  },
                  success: function (res) {
                      if (!res.hasOwnProperty("data")) {
                          console.log("!data");
                          $('#register img').hide();
                          $('#register span').show();
                          $('#regMessage').html(res.state);
                          $('#regMessage').css("display, flex");

                      }

                      else if (res.hasOwnProperty("data") && res.data != null) {
                          console.log("sucCess");
                          $('#ek--manager').hide();
                          $('#ek--create--verifycode').show();
                          $('#register img').hide();
                          $('#register span').show();

                      }
                  },


              });

          }


        e.preventDefault();

    });

    $('body').on('click', '#confirmCode', function (e) {

        var email= $('#verifyEmail').val();
        var codeArr=[];

        codeArr.push($('#one').val());
        codeArr.push($('#two').val());
        codeArr.push($('#three').val());
        codeArr.push($('#four').val());
        codeArr.push($('#five').val());
        codeArr.push($('#six').val());

        var key= codeArr.join('');


        if(key.length!=6){

            $('#one').css('border','2px solid red');
            $('#two').css('border','2px solid red');
            $('#three').css('border','2px solid red');
            $('#five').css('border','2px solid red');
            $('#four').css('border','2px solid red');
            $('#six').css('border','2px solid red');
        }


        else {

            $('#confirmCode span').hide();
            $('#confirmCode img').show();



            $.ajax({
                url: '/verify',
                type: 'post',
                data: {
                    email: email,
                    key: key
                },
                success: function (res) {
                    if (!res.hasOwnProperty("data")) {
                        console.log("!data");
                        $('#confirmCode img').hide();
                        $('#confirmCode span').show();
                        $('#regVerifyMessage').html(res.state);
                        $('#regVerifyMessage').css('display','flex');
                        $('#ek--create--verifycode input').css('border','2px solid red');


                    }

                    else if (res.hasOwnProperty("data") && res.data != null) {


                        $('#ek--create--verifycode').hide();

                        // $('#ek--soft--notenough').modal('show');

                        $('#ek--notification').css({
                            'display': 'inline-block',
                            'height':'auto',
                            'margin': 'auto'
                        });
                        var password=$('#password').val();
                        $.ajax({
                            url: 'acc/login',
                            type: 'post',
                            data: {
                                email: email,
                                password: password
                            },
                            success: function (res) {
                                console.log("success")
                            }
                        });


                    }


                },
            });
        }

    });

    $('body').on('click', '#close', function (e) {


          window.location.href='/home';

    });

    $('body').on('click', '#resendCodeReg', function (e) {

        var email= ($('#email').val() || '').trim();
        var fullName=$('#fullName').val();
        var clubName=($('#clubName').val()  || '').trim();
        var password=$('#password').val();
        var countryId=$('#countryId').attr("data-id");

        $('#verifyEmail').val(email);




            $('#confirmCode span').hide();
            $('#confirmCode img').show();


            $.ajax({
                url: '/register',
                type: 'post',
                data: {
                    email: email,
                    password: password,
                    fullName: fullName,
                    countryId: countryId,
                    clubName: clubName
                },
                success: function (res) {
                    if (!res.hasOwnProperty("data")) {
                        console.log("!data");
                        $('#register img').hide();
                        $('#register span').show();

                    }

                    else if (res.hasOwnProperty("data") && res.data != null) {

                        $('#confirmCode img').hide();
                        $('#confirmCode span').show();

                    }
                },


            });


        e.preventDefault();

    });


    $('body').on('click', '#signUpFacebook', function (e) {

        $('#registerFaceBookMessage').html('');

        var email= ($('#socialEmail').val() || '').trim();
        var fullName=$('#socialName').val();
        var clubName=($('#socialClubName').val()  || '').trim();
        var password=$('#socialPass').val();
        var countryId=$('#socialCountryId').attr("data-id");

        console.log("email" + email);
        console.log("pass" + password);
        console.log("name" + fullName);
        console.log("clubname" +clubName);
        console.log("contryId"+countryId);

            $.ajax({
                url: '/registerWithFacebook',
                type: 'post',
                data: {
                    email: email,
                    password: password,
                    fullName: fullName,
                    countryId: countryId,
                    clubName: clubName
                },
                success: function (res) {
                    if (res.hasOwnProperty("state")) {
                        console.log("!data");

                        $('#registerFaceBookMessage').html(res.state);
                        $('#registerFaceBookMessage').css('display','flex');

                    }

                    else if (res.hasOwnProperty("data") && res.data != null) {

                        window.location.href='/home';

                    }
                },


            });




        e.preventDefault();

    });


    $('body').on('click', '#signUpWithGoogle', function (e) {

        $('#registerFaceBookMessage').html('');

        var email= ($('#socialEmail').val() || '').trim();
        var fullName=$('#socialName').val();
        var clubName=($('#socialClubName').val()  || '').trim();
        var password=$('#socialPass').val();
        var countryId=$('#socialCountryId').attr("data-id");


        $.ajax({
            url: '/registerWithGoogle',
            type: 'post',
            data: {
                email: email,
                password: password,
                fullName: fullName,
                countryId: countryId,
                clubName: clubName
            },
            success: function (res) {
                if (res.hasOwnProperty("state")) {
                    console.log("!data");

                    $('#registerFaceBookMessage').html(res.state);
                    $('#registerFaceBookMessage').css('display','flex');

                }

                else if (res.hasOwnProperty("data") && res.data != null) {

                    window.location.href='/home';

                }
            },


        });




        e.preventDefault();

    });


});
