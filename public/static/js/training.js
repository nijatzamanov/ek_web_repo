$(document).ready(function () {


    $('body').on('click', '#trainingCamp', function (e) {

        $('#loading').modal('show');


        var campId = $(this).attr("data-id");

        $.ajax({
            url:  '/getUserTotalBudgets',
            type: 'get',

            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    $('#ek--soft--notenough').modal('show');
                }
                else if (res.hasOwnProperty("data")) {

                    var coins= $('#userMoney').text();
                    var number= $('#amount').text();

                    if(number=='K') {
                        var resultCoin =parseInt(coins)*1000;
                    }
                    else if(number=='M'){
                        var resultCoin =parseInt(coins)*1000000;
                    }

                    else if(number=='B') {
                        var resultCoin =parseInt(coins)*1000000000;
                    }

                    else if(number==''){
                        var resultCoin =parseInt(coins);
                    }
                    console.log("userMoney" + coins);
                    if(parseInt(res.data.coins)< parseInt(resultCoin)){

                        $('#ek--soft--notenough').modal('show');
                    }
                    else{

                        $.ajax({
                            url:  '/start/camp',
                            type: 'get',
                            data:{
                                campId:campId
                            },
                            success: function (res) {
                                if (!res.hasOwnProperty("data")) {
                                    $('#ek--camp--exist').modal('show');
                                    console.log("!data");
                                }

                                else if(res.hasOwnProperty("state2")){
                                    $('#training').modal('show');

                                }
                                else if (res.hasOwnProperty("data")) {

                                    // $('#ek--soft').hide();
                                    $('#ek--training').hide();
                                    // $("#ek--complete").css("display","block");
                                    window.location.pathname = '/training-camp';

                                    $('#ek--training--success').modal('show');
                                    console.log("success:");
                                }

                            },
                        });
                    }


                }


            },
        });
        $('#loading').modal('hide');



        e.preventDefault()
    });

    $('body').on('click', '#chooseCamp', function (e) {

        var campId = $(this).attr("data-id");
        console.log("campId:" + campId);

        $('#ek--training' + campId).modal('show');

        e.preventDefault()
    });


});
