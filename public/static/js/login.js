$(document).ready(function () {

    $.ajaxSetup({
        headers: {"X-CSRF-TOKEN": $('meta[name=csrf]').attr('content')}
    });

    $('body').on('click', '#signin', function (e) {

        $('#loginError').hide();


        var email = ($('#loginEmail').val() || '').trim();
        var password = $('#loginPass').val();

        if (email.includes("@")) {

            $('.ek--create--button--play span').hide();
            $('.ek--create--button--play img').show();

            $.ajax({
                url: 'acc/login',
                type: 'post',
                data: {
                    email: email,
                    password: password
                },
                success: function (res) {
                    if (res.hasOwnProperty("state")) {
                        $('#message').html(res.state);
                        $('#loginError').css('display', 'flex');


                    } else if (res.hasOwnProperty("data") && res.data != null) {

                        window.location.href = '/home';

                    }

                    $('.ek--create--button--play img').hide();
                    $('.ek--create--button--play span').show();

                },
            });
        }

    });

    $('body').on('click', '#resendCode', function(e){
        var email = ($('#forgetPwdEmail').val() || '').trim();
        $('#ek--create--verifycode #emaill').html('You have received a new OTP for a password request for ' + '<span id="email">' + email + '</span>');
        $('#ek--create--change #emaill1').html('You have received a new OTP for a password request for ' + '<span id="email1">' + email + '</span>');
        $.ajax({
            url: '/account/resend/otp',
            type: 'post',
            data: {
                email: email
            },
            success: function (res) {
                if (!res.hasOwnProperty("data")) {

                    $('#forgetEmailMessage').html('res.state');
                    $('#forgetEmailMessage').css('display', 'flex');

                } else if (res.hasOwnProperty("data") && res.data != null) {
                    $("#confirmCode img").hide();
                    $("#confirmCode span").show();
                    console.log("success")

                }


            },
        });
    });

    $('body').on('click', '#forgetPwd', function (e) {

        var email = ($('#forgetPwdEmail').val() || '').trim();


        console.log("Email: " + email);
        $('#ek--create--verifycode #emaill').html('You have sent a password reset request for ' + '<span id="email">' + email + '</span>');
        $('#ek--create--change #emaill1').html('You have sent a password reset request for ' + '<span id="email1">' + email + '</span>');


        if (email.includes('@')) {

            $('#forgetPwd span').hide();
            $('#forgetPwd img').show();


            $.ajax({
                url: '/account/forget/pwd',
                type: 'post',
                data: {
                    email: email
                },
                success: function (res) {
                    if (!res.hasOwnProperty("data")) {

                        $('#forgetPwd img').hide();
                        $('#forgetPwd span').show();


                        $('#forgetEmailMessage').html(res.state);
                        $('#forgetEmailMessage').css('display', 'flex');

                    } else if (res.hasOwnProperty("data") && res.data != null) {

                        $('#ek--create--forget').hide();
                        $('#ek--create--send').show();
                        $('#forgetPwd img').hide();
                        $('#forgetPwd span').show();

                    }


                },
            });
        }

    });

    $('body').on('click', '#verifyCode', function (e) {


        $('#ek--create--send').hide();
        $('#ek--create--verifycode').show();
    });

    $('body').on('click', '#confirmCode', function (e) {

        var email = $('#email').text();

        var codeArr = [];

        codeArr.push($('#one').val());
        codeArr.push($('#two').val());
        codeArr.push($('#three').val());
        codeArr.push($('#four').val());
        codeArr.push($('#five').val());
        codeArr.push($('#six').val());


        console.log("codeArr:" + codeArr);
        console.log("EMAIL:" + email);

        var code = codeArr.join('');

        if (code.length != 6) {

            $('#one').css('border', '2px solid red');
            $('#two').css('border', '2px solid red');
            $('#three').css('border', '2px solid red');
            $('#four').css('border', '2px solid red');
            $('#five').css('border', '2px solid red');
            $('#six').css('border', '2px solid red');

        } else {

            $('#confirmCode span').hide();
            $('#confirmCode img').show();

            console.log("codeString:" + code);
            $('#code').val(code);
            $.ajax({
                url: '/account/verify/pwd/code',
                type: 'post',
                data: {
                    email: email,
                    code: code
                },
                success: function (res) {
                    if (!res.hasOwnProperty("data")) {
                        $('#confirmCode img').hide();
                        $('#confirmCode span').show();
                        $('#forgetVerifyMessage').html(res.state);

                        $('#forgetVerifyMessage').css('display', 'flex');
                        $('#ek--create--verifycode input').css('border', '2px solid red');

                    } else if (res.hasOwnProperty("data") && res.data != null) {
                        $('#ek--create--verifycode').hide();
                        $('#ek--create--change').show();

                    }


                },
            });
        }


    });

    $('body').on('click', '#changePass', function (e) {

        var email = $('#email1').text();
        var code = $('#code').val();
        var password = $('#newPass').val();

        if (password.length < 8) {

            $('#changePassMessage').html('Password must be at least 8 character!')
        } else {

            $('#changePass span').hide();
            $('#changePass img').show();


            console.log("code:" + code);
            $.ajax({
                url: '/account/reset/pwd',
                type: 'post',
                data: {
                    email: email,
                    code: code,
                    password: password
                },
                success: function (res) {
                    if (!res.hasOwnProperty("data")) {
                        $('#changePass img').hide();
                        $('#changePass span').show();
                        $('#changePassMessage').html(res.state);
                        $('#changePassMessage').css('display', 'flex');
                    } else if (res.hasOwnProperty("data") && res.data != null) {

                        $('#ek--create--change').hide();
                        $('#ek--create--success').show();

                    }


                },
            });

        }


    });

    // $('body').on('click', '#resendCode', function (e) {
    //
    //     $('.ek--create--button--play span').hide();
    //     $('.ek--create--button--play img').show();
    //
    //
    //     var email = $('#email').val();
    //
    //     $.ajax({
    //         url: '/account/forget/pwd',
    //         type: 'post',
    //         data: {
    //             email: email
    //         },
    //         success: function (res) {
    //             if (!res.hasOwnProperty("data")) {
    //                 // $('#ek--soft--notenough').modal('show');
    //                 console.log("!data");
    //             } else if (res.hasOwnProperty("data") && res.data != null) {
    //
    //                 $('.ek--create--button--play img').hide();
    //
    //                 $('.ek--create--button--play span').show();
    //
    //             }
    //
    //
    //         },
    //     });
    //
    //
    // });


});
