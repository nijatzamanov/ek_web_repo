$(document).ready(function () {

    var table = $("#leagueStanding");
    var row = $(".active--line");

    table.scrollTop( row.offset().top - (table.height()/2) );

    $('body').on('click', '#startNewSeason', function (e) {




        function load() {
            $('#loading').css('display','block');
        }

        window.onload=load();

        $.ajax({
            url:  '/build/new',
            type: 'get',
            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    console.log("!data");
                }
                else if (res.hasOwnProperty("data")) {
                    location.reload();

                    $('#newSeasonSuccess').modal('show');

                    console.log("success:");
                }


            },
        });


        e.preventDefault()
    });


    $('.js--leagueStanding').animate({
            scrollTop: $(".js--currentCommand").offset().top+($(".js--currentCommand").height()*7) },
        'slow');


});
