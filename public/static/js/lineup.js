$(document).ready(function () {

    var mainList = [];
    //
    // $( ".ek--lineup--stadium--position--player").unbind('click')
    // $( ".ek--lineup--body--tabs--substitutions--inner").unbind('click')



    function saveMainPlayers() {
        if(mainList.length == 11){
            $.ajax({
                url: '/team/lineup/saveMainList',
                type: 'post',
                data: {
                    "_token": $('meta[name="csrf"]').attr('content'),
                    playerIds: mainList,
                },
                success: function (res) {
                    if (!res.hasOwnProperty("data")) {
                        console.log("!data");
                    } else if (res.hasOwnProperty("data")) {
                        console.log("ok")
                        location.reload()
                        // playerLineUp()
                    }
                }
            })
        }
        console.log(mainList);
    }

    var playerLineUp = function () {
        var stadiumPlayerType;
        var stadiumPlayerArea;
        var stadiumPlayerClickTrue;
        var checkItem;
        var playerIdStadium;

        $('.ek--lineup--stadium--position--player').click(function () {
            stadiumPlayerType = $(this).parent().attr('playertype');
            stadiumPlayerArea = $(this);
            stadiumPlayerClickTrue = true;
            if($(this).children('.ek--lineup--drag').length == 1) {
                checkItem = true
                var playerIdd = $(this).children('.ek--lineup--drag').attr("data-id");
                playerIdStadium= parseInt(playerIdd);
                console.log("playerIdStadium:" + playerIdStadium);
            }else{
                checkItem = false
            }
            // return stadiumPlayerArea , checkItem;
        });

        $('.ek--lineup--body--tabs--substitutions--inner').click(function () {
            var subPlayerArea = $(this).parent();
            var subPlayerId = $(this).attr('data-id');
            var subPlayerPos = $(this).find('.ek--lineup--body--tabs--substitutions--inner--pos').text();
            var subPlayerName = $(this).find('.ek--lineup--body--tabs--substitutions--inner--player').text();
            var subPlayerAbl = $(this).find('.ek--lineup--body--tabs--substitutions--inner--abl').text();
            var subPlayerProgress = $(this).find('.ek--lineup--body--tabs--substitutions--inner--energy--progress .progress-bar').attr('style').split(':');
            var subPlayerProgress2 = subPlayerProgress[1].split('%')[0];
            var orange = $(this).find('.ek--lineup--body--tabs--substitutions--inner--pos').hasClass('bg--orange');
            var cyan = $(this).find('.ek--lineup--body--tabs--substitutions--inner--pos').hasClass('bg--cyan--two');
            var yellow = $(this).find('.ek--lineup--body--tabs--substitutions--inner--pos').hasClass('bg--yellow');
            var purple = $(this).find('.ek--lineup--body--tabs--substitutions--inner--pos').hasClass('bg--purple');
            var orange2 = $(this).find('.ek--lineup--body--tabs--substitutions--inner--energy--progress .progress-bar').hasClass('bg--orange');
            var green = $(this).find('.ek--lineup--body--tabs--substitutions--inner--energy--progress .progress-bar').hasClass('bg--green--dark--two');
            var green2 = $(this).find('.ek--lineup--body--tabs--substitutions--inner--energy--progress .progress-bar').hasClass('bg--green--two');
            var yellow2 = $(this).find('.ek--lineup--body--tabs--substitutions--inner--energy--progress .progress-bar').hasClass('bg--yellow');
            var yellow3 = $(this).find('.ek--lineup--body--tabs--substitutions--inner--energy--progress .progress-bar').hasClass('bg--yellow--two');
            var subPlayerBColor;
            var subPlayerProgressColor;
            if(orange) {
                subPlayerBColor = 'bg--orange';
            }
            if(cyan) {
                subPlayerBColor = 'bg--cyan--two';
            }
            if(yellow) {
                subPlayerBColor = 'bg--yellow';
            }
            if(purple) {
                subPlayerBColor = 'bg--purple';
            }
            if(orange2) {
                subPlayerProgressColor = 'bg--orange';
            }
            if(green) {
                subPlayerProgressColor = 'bg--green--dark--two';
            }
            if(green2) {
                subPlayerProgressColor = 'bg--green--two';
            }
            if(yellow2) {
                subPlayerProgressColor = 'bg--yellow';
            }
            if(yellow3) {
                subPlayerProgressColor = 'bg--yellow--two';
            }
            var clone = `
         <div data-id="${subPlayerId}" class="ek--lineup--drag">
               <div class="ek--lineup--drop--abl ${subPlayerBColor} text--white">${subPlayerAbl}</div>
               <div class="ek--lineup--drop--progress">
                  <div class="progress">
                     <div class="progress-bar ${subPlayerProgressColor}" role="progressbar" style="width: ${subPlayerProgress2}%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
               </div>
               <div class="ek--lineup--drop--name text--white">${subPlayerName}</div>
         </div>
    `;

            if(checkItem) {
                var changeDiv = stadiumPlayerArea.find('.ek--lineup--drag');
                stadiumPlayerArea.html(' ');
                var stadiumPlayerId = changeDiv.attr('data-id');
                var stadiumPlayerName = changeDiv.find('.ek--lineup--drop--name').text();
                var stadiumPlayerAbl = changeDiv.find('.ek--lineup--drop--abl').text();
                var stadiumPlayerProgress = changeDiv.find('.ek--lineup--drop--progress .progress-bar').attr('style').split(':');
                var stadiumPlayerProgress2 = stadiumPlayerProgress[1].split('%')[0];
                var orange1 = changeDiv.find('.ek--lineup--drop--abl').hasClass('bg--orange');
                var cyan1 = changeDiv.find('.ek--lineup--drop--abl').hasClass('bg--cyan--two');
                var yellow1 = changeDiv.find('.ek--lineup--drop--abl').hasClass('bg--yellow');
                var purple1 = changeDiv.find('.ek--lineup--drop--abl').hasClass('bg--purple');
                var orange3 = changeDiv.find('.ek--lineup--drop--progress .progress-bar').hasClass('bg--orange');
                var green1 = changeDiv.find('.ek--lineup--drop--progress .progress-bar').hasClass('bg--green--dark--two');
                var green3 = changeDiv.find('.ek--lineup--drop--progress .progress-bar').hasClass('bg--green--two');
                var yellow3 = changeDiv.find('.ek--lineup--drop--progress .progress-bar').hasClass('bg--yellow');
                var yellow4 = changeDiv.find('.ek--lineup--drop--progress .progress-bar').hasClass('bg--yellow--two');
                var stadiumAblBColor;
                var stadiumProgressBColor;
                if(orange1) {
                    stadiumAblBColor = 'bg--orange';
                }
                if(cyan1) {
                    stadiumAblBColor = 'bg--cyan--two';
                }
                if(yellow1) {
                    stadiumAblBColor = 'bg--yellow';
                }
                if(purple1) {
                    stadiumAblBColor = 'bg--purple';
                }
                if(orange3) {
                    stadiumProgressBColor = 'bg--orange';
                }
                if(green1) {
                    stadiumProgressBColor = 'bg--green--dark--two';
                }
                if(green3) {
                    stadiumProgressBColor = 'bg--green--two';
                }
                if(yellow3) {
                    stadiumProgressBColor = 'bg--yellow';
                }
                if(yellow4) {
                    stadiumProgressBColor = 'bg--yellow--two';
                }
                // $(this).attr('data-id' , stadiumPlayerId);
                $(this).find('.ek--lineup--body--tabs--substitutions--inner--pos').addClass(stadiumAblBColor);
                $(this).find('.ek--lineup--body--tabs--substitutions--inner--pos').text(subPlayerPos);
                $(this).find('.ek--lineup--body--tabs--substitutions--inner--player').text(stadiumPlayerName);
                $(this).find('.ek--lineup--body--tabs--substitutions--inner--abl').text(stadiumPlayerAbl);
                $(this).find('.ek--lineup--body--tabs--substitutions--inner--energy--progress .progress-bar').css('width' , stadiumPlayerProgress2 + '%');
                $(this).find('.ek--lineup--body--tabs--substitutions--inner--energy--progress .progress-bar').addClass(stadiumProgressBColor);
                $(this).find('.ek--lineup--body--tabs--substitutions--inner--energy--progress').next().text(stadiumPlayerProgress2 + '%');
                // console.log(stadiumPlayerArea.find('.ek--lineup--drag'))
                // stadiumPlayerArea.find('.ek--lineup--drag').remove();
                // subPlayerArea.append(clone2);
                stadiumPlayerArea.append(clone);
                subPlayerId = null;
                subPlayerName = null;
                subPlayerAbl = null;
                subPlayerProgress = null;
                subPlayerBColor = null;
                subPlayerProgressColor = null;
                stadiumPlayerArea = null;
                clone = null;
                // $(this).remove();


                if(stadiumPlayerClickTrue == true && checkItem == true){
                    $.ajax({
                        url:  '/team/lineup/remove/player',
                        type: 'post',
                        data: {
                            "_token": $('meta[name="csrf"]').attr('content'),
                            playerId:playerIdStadium
                        },
                        success: function (res) {
                            if (!res.hasOwnProperty("data")) {
                                console.log("!data");

                            }
                            else if (res.hasOwnProperty("data")) {
                                console.log("success:");

                                index = mainList.indexOf(playerIdStadium)
                                if (index > -1) {
                                    mainList.splice(index, 1);
                                    saveMainPlayers()
                                }
                            }
                        },
                    });
                }


                var playerId = $(this).attr("data-id");

                var playerIds=parseInt(playerId);
                console.log("Player bosh :" + playerIds);
                $.ajax({
                    url:  '/team/recovery/player',
                    type: 'post',
                    data: {
                        "_token": $('meta[name="csrf"]').attr('content'),
                        playerIds:playerIds
                    },
                    success: function (res) {
                        if (!res.hasOwnProperty("data")) {
                            console.log("!data");
                        }
                        else if (res.hasOwnProperty("data")) {
                            console.log("success1:");

                            $('#substitutions.ek--lineup--body--tabs--substitutions--scroll').html('');
                            mainList.push(playerIds)
                            saveMainPlayers()
                            getSubstitutions()
                        }


                    },
                });
            } else {
                stadiumPlayerArea.append(clone);
                subPlayerId = null;
                subPlayerName = null;
                subPlayerAbl = null;
                subPlayerProgress = null;
                subPlayerBColor = null;
                subPlayerProgressColor = null;
                stadiumPlayerArea = null;
                clone = null;
                $(this).remove();
                var playerId = $(this).attr("data-id");

                var playerIds=parseInt(playerId);
                console.log("Player bosh :" + playerId);
                $.ajax({
                    url:  '/team/recovery/player',
                    type: 'post',
                    data: {
                        "_token": $('meta[name="csrf"]').attr('content'),
                        playerIds:playerIds
                    },
                    success: function (res) {
                        if (!res.hasOwnProperty("data")) {
                            console.log("!data");
                        }
                        else if (res.hasOwnProperty("data")) {
                            console.log("success2:");

                            $('#substitutions.ek--lineup--body--tabs--substitutions--scroll').html('');

                            mainList.push(playerIds)
                            saveMainPlayers()
                            getSubstitutions()

                        }


                    },
                });
            }
        });

        mainList = $(".ek--lineup--drag").map(function(){return parseInt($(this).attr("data-id"));}).get();
        mainList = mainList.filter(function (x, i, a) {
            return a.indexOf(x) === i;
        });

    }

    var lineupFormationActive = function() {
        var txt = $('#formationType').text().split('-').join('');

        $('.ek--lineup--stadium--position').each(function () {
            var dataId = $(this).attr('data-id');
            if(dataId === txt) {
                $('.ek--lineup--stadium--position').hide();
                $(this).show();
            }
            /*else {
                $('.ek--lineup--stadium--position').hide();
                $(this).show();
            }*/
        });

        $('.ek--lineup--body--tabs--formation--tactics').click(function() {
            $('.ek--lineup--body--tabs--formation--tactics').removeClass('button--active');
            $(this).addClass('button--active');
            var id = $(this).attr('id');
            $('.ek--lineup--stadium--position').hide();
            $('.' + id).show();

        });
    }

    getSubstitutions();

    lineupFormationActive();

    function getSubstitutions() {
         var forvards = [];
         var goalkepper = [];
         var midfielder = [];
         var defenders = [];


   $.ajax({
        url:  'player/u',
        type: 'get',
        success: function (res) {
            if (!res.hasOwnProperty("data")) {
                console.log("!data");
            }
            else if (res.hasOwnProperty("data")) {


                forvards=[];
                goalkepper=[];
                midfielder=[];
                defenders=[];


                $.each(res.data, function (i, o) {

                        if (o.positionId == 4 && o.playStatus==true) {
                            forvards.push(o);
                        }
                        else if (o.positionId == 3  && o.playStatus==true) {
                            midfielder.push(o);

                        }
                        else if (o.positionId == 2 &&  o.playStatus==true) {
                            defenders.push(o);

                        }
                        else if (o.positionId == 1 && o.playStatus==true) {
                            goalkepper.push(o);

                        }

                });


                console.log("defenders:" +defenders);




                    if(forvards.length>0) {

                    if(parseInt(forvards[0].energy) <= 25){
                      var progressDiv = '<div class="progress-bar bg--orange role="progressbar" style="width: ' + forvards[0].energy + '%" aria-valuenow="'+forvards[0].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                    }

                    else if(parseInt(forvards[0].energy) > 25 && parseInt(forvards[0].energy) <= 55 ){
                        var progressDiv = '<div class="progress-bar bg--yellow--two role="progressbar" style="width: ' + forvards[0].energy + '%" aria-valuenow="'+forvards[0].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                    }
                    else if(parseInt(forvards[0].energy) >55  && parseInt(forvards[0].energy) <= 90){
                        var progressDiv = '<div class="progress-bar bg--green--two role="progressbar" style="width: ' + forvards[0].energy + '%" aria-valuenow="'+forvards[0].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                    }
                    else if(parseInt(forvards[0].energy) >90  && parseInt(forvards[0].energy) <= 100){
                        var progressDiv = '<div class="progress-bar bg--green--dark--two role="progressbar" style="width: ' + forvards[0].energy + '%" aria-valuenow="'+forvards[0].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                    }


                    if($(".forwardOne").find(".ek--lineup--drag").length == 0){
                        $('.forwardOne').append(' <div data-id="' + forvards[0].id + '" class="ek--lineup--drag">' +
                            ' <div class="ek--lineup--drop--abl bg--purple text--white">' + parseInt(forvards[0].ability) + '</div>\n' +
                            '<div class="ek--lineup--drop--progress">\n' +
                            '<div  class="progress">'+ progressDiv +'</div>' +
                            '</div>' +
                            '<div class="ek--lineup--drop--name text--white">' + forvards[0].name + '</div>' +
                            '</div>');
                    }


                }

                    if (forvards.length > 1) {



                        if(parseInt(forvards[1].energy) <= 25){
                            var progressDiv = '<div class="progress-bar bg--orange role="progressbar" style="width: ' + forvards[1].energy + '%" aria-valuenow="'+forvards[1].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }

                        else if(parseInt(forvards[1].energy) > 25 && parseInt(forvards[1].energy) <= 55 ){
                            var progressDiv = '<div class="progress-bar bg--yellow--two role="progressbar" style="width: ' + forvards[1].energy + '%" aria-valuenow="'+forvards[1].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }
                        else if(parseInt(forvards[1].energy) >55  && parseInt(forvards[1].energy) <= 90){
                            var progressDiv = '<div class="progress-bar bg--green--two role="progressbar" style="width: ' + forvards[1].energy + '%" aria-valuenow="'+forvards[1].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }
                        else if(parseInt(forvards[1].energy) >90  && parseInt(forvards[1].energy) <= 100){
                            var progressDiv = '<div class="progress-bar bg--green--dark--two role="progressbar" style="width: ' + forvards[1].energy + '%" aria-valuenow="'+forvards[1].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }



                        if($(".forwardTwo").find(".ek--lineup--drag").length == 0){
                            $('.forwardTwo').append(' <div data-id="' + forvards[1].id + '" class="ek--lineup--drag">' +
                                ' <div class="ek--lineup--drop--abl bg--purple text--white">' + parseInt(forvards[1].ability) + '</div>\n' +
                                '<div class="ek--lineup--drop--progress">\n' +
                                '<div class="progress">' + progressDiv + ' </div>' +
                                '</div>' +
                                '<div class="ek--lineup--drop--name text--white">' + forvards[1].name + '</div>' +
                                '</div>');

                        }


                    }

                    if (forvards.length > 2) {


                        if(parseInt(forvards[2].energy) <= 25){
                            var progressDiv = '<div class="progress-bar bg--orange role="progressbar" style="width: ' + forvards[2].energy + '%" aria-valuenow="'+forvards[2].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }

                        else if(parseInt(forvards[2].energy) > 25 && parseInt(forvards[2].energy) <= 55 ){
                            var progressDiv = '<div class="progress-bar bg--yellow--two role="progressbar" style="width: ' + forvards[2].energy + '%" aria-valuenow="'+forvards[2].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }
                        else if(parseInt(forvards[2].energy) >55  && parseInt(forvards[2].energy) <= 90){
                            var progressDiv = '<div class="progress-bar bg--green--two role="progressbar" style="width: ' + forvards[2].energy + '%" aria-valuenow="'+forvards[2].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }
                        else if(parseInt(forvards[2].energy) >90  && parseInt(forvards[2].energy) <= 100){
                            var progressDiv = '<div class="progress-bar bg--green--dark--two role="progressbar" style="width: ' + forvards[2].energy + '%" aria-valuenow="'+forvards[2].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }


                        if($(".forwardThree").find(".ek--lineup--drag").length == 0) {
                            $('.forwardThree').append(' <div data-id="' + forvards[2].id + '" class="ek--lineup--drag">' +
                                ' <div class="ek--lineup--drop--abl bg--purple text--white">' + parseInt(forvards[2].ability) + '</div>\n' +
                                '<div class="ek--lineup--drop--progress">\n' +
                                '<div  class="progress">' + progressDiv + ' </div>' +
                                '</div>' +
                                '<div class="ek--lineup--drop--name text--white">' + forvards[2].name + '</div>' +
                                '</div>');
                        }

                    }

                    if(midfielder.length>0) {


                    if(parseInt(midfielder[0].energy) <= 25){
                        var progressDiv = '<div class="progress-bar bg--orange role="progressbar" style="width: ' + midfielder[0].energy + '%" aria-valuenow="'+midfielder[0].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                    }

                    else if(parseInt(midfielder[0].energy) > 25 && parseInt(midfielder[0].energy) <= 55 ){
                        var progressDiv = '<div class="progress-bar bg--yellow--two role="progressbar" style="width: ' + midfielder[0].energy + '%" aria-valuenow="'+midfielder[0].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                    }
                    else if(parseInt(midfielder[0].energy) >55  && parseInt(midfielder[0].energy) <= 90){
                        var progressDiv = '<div class="progress-bar bg--green--two role="progressbar" style="width: ' + midfielder[0].energy + '%" aria-valuenow="'+midfielder[0].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                    }
                    else if(parseInt(midfielder[0].energy) >90  && parseInt(midfielder[0].energy) <= 100){
                        var progressDiv = '<div class="progress-bar bg--green--dark--two role="progressbar" style="width: ' + midfielder[0].energy + '%" aria-valuenow="'+midfielder[0].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                    }



                        if($(".midfielderOne").find(".ek--lineup--drag").length == 0) {

                            $('.midfielderOne ').append(' <div data-id="' + midfielder[0].id + '" class="ek--lineup--drag">' +
                                ' <div class="ek--lineup--drop--abl bg--cyan--two text--white">' + parseInt(midfielder[0].ability) + '</div>\n' +
                                '<div class="ek--lineup--drop--progress">\n' +
                                '<div class="progress">' + progressDiv + ' </div>' +
                                '</div>' +
                                '<div class="ek--lineup--drop--name text--white">' + midfielder[0].name + '</div>' +
                                '</div>');

                        }


                }

                    if (midfielder.length > 1) {



                        if(parseInt(midfielder[1].energy) <= 25){
                            var progressDiv = '<div class="progress-bar bg--orange role="progressbar" style="width: ' + midfielder[1].energy + '%" aria-valuenow="'+midfielder[0].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }

                        else if(parseInt(midfielder[1].energy) > 25 && parseInt(midfielder[1].energy) <= 55 ){
                            var progressDiv = '<div class="progress-bar bg--yellow--two role="progressbar" style="width: ' + midfielder[1].energy + '%" aria-valuenow="'+midfielder[0].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }
                        else if(parseInt(midfielder[1].energy) >55  && parseInt(midfielder[1].energy) <= 90){
                            var progressDiv = '<div class="progress-bar bg--green--two role="progressbar" style="width: ' + midfielder[1].energy + '%" aria-valuenow="'+midfielder[0].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }
                        else if(parseInt(midfielder[1].energy) >90  && parseInt(midfielder[1].energy) <= 100){
                            var progressDiv = '<div class="progress-bar bg--green--dark--two role="progressbar" style="width: ' + midfielder[1].energy + '%" aria-valuenow="'+midfielder[0].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }


                        if($(".midfielderTwo").find(".ek--lineup--drag").length == 0) {

                            $('.midfielderTwo ').append(' <div data-id="' + midfielder[1].id + '" class="ek--lineup--drag">' +
                                ' <div class="ek--lineup--drop--abl bg--cyan--two text--white">' + parseInt(midfielder[1].ability) + '</div>\n' +
                                '<div class="ek--lineup--drop--progress">\n' +
                                '<div  class="progress">' + progressDiv + ' </div>' +
                                '</div>' +
                                '<div class="ek--lineup--drop--name text--white">' + midfielder[1].name + '</div>' +
                                '</div>');

                        }
                    }

                    if(midfielder.length>2) {


                        if(parseInt(midfielder[2].energy) <= 25){
                            var progressDiv = '<div class="progress-bar bg--orange role="progressbar" style="width: ' + midfielder[2].energy + '%" aria-valuenow="'+midfielder[2].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }

                        else if(parseInt(midfielder[2].energy) > 25 && parseInt(midfielder[2].energy) <= 55 ){
                            var progressDiv = '<div class="progress-bar bg--yellow--two role="progressbar" style="width: ' + midfielder[2].energy + '%" aria-valuenow="'+midfielder[2].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }
                        else if(parseInt(midfielder[2].energy) >55  && parseInt(midfielder[2].energy) <= 90){
                            var progressDiv = '<div class="progress-bar bg--green--two role="progressbar" style="width: ' + midfielder[2].energy + '%" aria-valuenow="'+midfielder[2].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }
                        else if(parseInt(midfielder[2].energy) >90  && parseInt(midfielder[2].energy) <= 100){
                            var progressDiv = '<div class="progress-bar bg--green--dark--two role="progressbar" style="width: ' + midfielder[2].energy + '%" aria-valuenow="'+midfielder[2].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }


                        if($(".midfielderThree").find(".ek--lineup--drag").length == 0) {

                            $('.midfielderThree').append(' <div data-id="' + midfielder[2].id + '" class="ek--lineup--drag">' +
                                ' <div class="ek--lineup--drop--abl bg--cyan--two text--white">' + parseInt(midfielder[2].ability) + '</div>\n' +
                                '<div class="ek--lineup--drop--progress">\n' +
                                '<div class="progress">' + progressDiv + ' </div>' +
                                '</div>' +
                                '<div class="ek--lineup--drop--name text--white">' + midfielder[2].name + '</div>' +
                                '</div>');
                        }

                    }

                    if(midfielder.length>3) {


                        if(parseInt(midfielder[3].energy) <= 25){
                            var progressDiv = '<div class="progress-bar bg--orange role="progressbar" style="width: ' + midfielder[3].energy + '%" aria-valuenow="'+midfielder[3].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }

                        else if(parseInt(midfielder[3].energy) > 25 && parseInt(midfielder[3].energy) <= 55 ){
                            var progressDiv = '<div class="progress-bar bg--yellow--two role="progressbar" style="width: ' + midfielder[3].energy + '%" aria-valuenow="'+midfielder[3].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }
                        else if(parseInt(midfielder[3].energy) >55  && parseInt(midfielder[3].energy) <= 90){
                            var progressDiv = '<div class="progress-bar bg--green--two role="progressbar" style="width: ' + midfielder[3].energy + '%" aria-valuenow="'+midfielder[3].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }
                        else if(parseInt(midfielder[3].energy) >90  && parseInt(midfielder[3].energy) <= 100){
                            var progressDiv = '<div class="progress-bar bg--green--dark--two role="progressbar" style="width: ' + midfielder[3].energy + '%" aria-valuenow="'+midfielder[3].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }


                        if($(".midfielderFour").find(".ek--lineup--drag").length == 0) {

                            $('.midfielderFour').append(' <div data-id="' + midfielder[3].id + '" class="ek--lineup--drag">' +
                                ' <div class="ek--lineup--drop--abl bg--cyan--two text--white">' + parseInt(midfielder[3].ability) + '</div>\n' +
                                '<div class="ek--lineup--drop--progress">\n' +
                                '<div id="progress7" class="progress">' + progressDiv + ' </div>' +
                                '</div>' +
                                '<div class="ek--lineup--drop--name text--white">' + midfielder[3].name + '</div>' +
                                '</div>');
                        }
                    }

                    if(midfielder.length>4) {


                if(parseInt(midfielder[4].energy) <= 25){
                    var progressDiv = '<div class="progress-bar bg--orange role="progressbar" style="width: ' + midfielder[4].energy + '%" aria-valuenow="'+midfielder[4].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                }

                else if(parseInt(midfielder[4].energy) > 25 && parseInt(midfielder[4].energy) <= 55 ){
                    var progressDiv = '<div class="progress-bar bg--yellow--two role="progressbar" style="width: ' + midfielder[4].energy + '%" aria-valuenow="'+midfielder[4].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                }
                else if(parseInt(midfielder[4].energy) >55  && parseInt(midfielder[4].energy) <= 90){
                    var progressDiv = '<div class="progress-bar bg--green--two role="progressbar" style="width: ' + midfielder[4].energy + '%" aria-valuenow="'+midfielder[4].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                }
                else if(parseInt(midfielder[4].energy) >90  && parseInt(midfielder[4].energy) <= 100){
                    var progressDiv = '<div class="progress-bar bg--green--dark--two role="progressbar" style="width: ' + midfielder[4].energy + '%" aria-valuenow="'+midfielder[4].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                }

                        if($(".midfielderFive").find(".ek--lineup--drag").length == 0) {

                            $('.midfielderFive').append(' <div data-id="' + midfielder[4].id + '" class="ek--lineup--drag">' +
                                ' <div class="ek--lineup--drop--abl bg--cyan--two text--white">' + parseInt(midfielder[4].ability) + '</div>\n' +
                                '<div class="ek--lineup--drop--progress">\n' +
                                '<div class="progress">' + progressDiv + ' </div>' +
                                '</div>' +
                                '<div class="ek--lineup--drop--name text--white">' + midfielder[4].name + '</div>' +
                                '</div>');
                        }


                    }

                    if(defenders.length>0) {



                    if(parseInt(defenders[0].energy) <= 25){
                        var progressDiv = '<div class="progress-bar bg--orange role="progressbar" style="width: ' + defenders[0].energy + '%" aria-valuenow="'+defenders[0].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                    }

                    else if(parseInt(defenders[0].energy) > 25 && parseInt(defenders[0].energy) <= 55 ){
                        var progressDiv = '<div class="progress-bar bg--yellow--two role="progressbar" style="width: ' + defenders[0].energy + '%" aria-valuenow="'+defenders[0].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                    }
                    else if(parseInt(defenders[0].energy) >55  && parseInt(defenders[0].energy) <= 90){
                        var progressDiv = '<div class="progress-bar bg--green--two role="progressbar" style="width: ' + defenders[0].energy + '%" aria-valuenow="'+defenders[0].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                    }
                    else if(parseInt(defenders[0].energy) >90  && parseInt(defenders[0].energy) <= 100){
                        var progressDiv = '<div class="progress-bar bg--green--dark--two role="progressbar" style="width: ' + defenders[0].energy + '%" aria-valuenow="'+defenders[0].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                    }



                        if($(".defenderOne").find(".ek--lineup--drag").length == 0) {

                            $('.defenderOne').append(' <div data-id="' + defenders[0].id + '" class="ek--lineup--drag">' +
                                ' <div class="ek--lineup--drop--abl bg--yellow text--white">' + parseInt(defenders[0].ability) + '</div>\n' +
                                '<div class="ek--lineup--drop--progress">\n' +
                                '<div  class="progress">' + progressDiv + ' </div>' +
                                '</div>' +
                                '<div class="ek--lineup--drop--name text--white">' + defenders[0].name + '</div>' +
                                '</div>');
                        }

                }

                    if(defenders.length>1) {


                        if(parseInt(defenders[1].energy) <= 25){
                            var progressDiv = '<div class="progress-bar bg--orange role="progressbar" style="width: ' + defenders[1].energy + '%" aria-valuenow="'+defenders[1].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }

                        else if(parseInt(defenders[1].energy) > 25 && parseInt(defenders[1].energy) <= 55 ){
                            var progressDiv = '<div class="progress-bar bg--yellow--two role="progressbar" style="width: ' + defenders[1].energy + '%" aria-valuenow="'+defenders[1].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }
                        else if(parseInt(defenders[1].energy) >55  && parseInt(defenders[1].energy) <= 90){
                            var progressDiv = '<div class="progress-bar bg--green--two role="progressbar" style="width: ' + defenders[1].energy + '%" aria-valuenow="'+defenders[1].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }
                        else if(parseInt(defenders[1].energy) >90  && parseInt(defenders[1].energy) <= 100){
                            var progressDiv = '<div class="progress-bar bg--green--dark--two role="progressbar" style="width: ' + defenders[1].energy + '%" aria-valuenow="'+defenders[1].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }


                        if($(".defenderTwo").find(".ek--lineup--drag").length == 0) {

                            $('.defenderTwo ').append(' <div data-id="' + defenders[1].id + '" class="ek--lineup--drag">' +
                                ' <div class="ek--lineup--drop--abl bg--yellow text--white">' + parseInt(defenders[1].ability) + '</div>\n' +
                                '<div class="ek--lineup--drop--progress">\n' +
                                '<div class="progress">' + progressDiv + ' </div>' +
                                '</div>' +
                                '<div class="ek--lineup--drop--name text--white">' + defenders[1].name + '</div>' +
                                '</div>');

                        }

                    }

                    if(defenders.length>2) {


                        if(parseInt(defenders[2].energy) <= 25){
                            var progressDiv = '<div class="progress-bar bg--orange role="progressbar" style="width: ' + defenders[2].energy + '%" aria-valuenow="'+defenders[2].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }

                        else if(parseInt(defenders[2].energy) > 25 && parseInt(defenders[2].energy) <= 55 ){
                            var progressDiv = '<div class="progress-bar bg--yellow--two role="progressbar" style="width: ' + defenders[2].energy + '%" aria-valuenow="'+defenders[2].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }
                        else if(parseInt(defenders[2].energy) >55  && parseInt(defenders[2].energy) <= 90){
                            var progressDiv = '<div class="progress-bar bg--green--two role="progressbar" style="width: ' + defenders[2].energy + '%" aria-valuenow="'+defenders[2].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }
                        else if(parseInt(defenders[2].energy) >90  && parseInt(defenders[2].energy) <= 100){
                            var progressDiv = '<div class="progress-bar bg--green--dark--two role="progressbar" style="width: ' + defenders[2].energy + '%" aria-valuenow="'+defenders[2].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }


                        if($(".defenderThree").find(".ek--lineup--drag").length == 0) {

                            $('.defenderThree').append(' <div data-id="' + defenders[2].id + '" class="ek--lineup--drag">' +
                                ' <div class="ek--lineup--drop--abl bg--yellow text--white">' + parseInt(defenders[2].ability) + '</div>\n' +
                                '<div class="ek--lineup--drop--progress">\n' +
                                '<div  class="progress">' + progressDiv + ' </div>' +
                                '</div>' +
                                '<div class="ek--lineup--drop--name text--white">' + defenders[2].name + '</div>' +
                                '</div>');
                        }

                    }

                    if(defenders.length>3) {



                        if(parseInt(defenders[3].energy) <= 25){
                            var progressDiv = '<div class="progress-bar bg--orange role="progressbar" style="width: ' + defenders[3].energy + '%" aria-valuenow="'+defenders[3].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }

                        else if(parseInt(defenders[3].energy) > 25 && parseInt(defenders[3].energy) <= 55 ){
                            var progressDiv = '<div class="progress-bar bg--yellow--two role="progressbar" style="width: ' + defenders[3].energy + '%" aria-valuenow="'+defenders[3].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }
                        else if(parseInt(defenders[3].energy) >55  && parseInt(defenders[3].energy) <= 90){
                            var progressDiv = '<div class="progress-bar bg--green--two role="progressbar" style="width: ' + defenders[3].energy + '%" aria-valuenow="'+defenders[3].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }
                        else if(parseInt(defenders[3].energy) >90  && parseInt(defenders[3].energy) <= 100){
                            var progressDiv = '<div class="progress-bar bg--green--dark--two role="progressbar" style="width: ' + defenders[3].energy + '%" aria-valuenow="'+defenders[3].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }


                        if($(".defenderFour").find(".ek--lineup--drag").length == 0) {

                            $('.defenderFour').append(' <div data-id="' + defenders[3].id + '" class="ek--lineup--drag">' +
                                ' <div class="ek--lineup--drop--abl bg--yellow text--white">' + parseInt(defenders[3].ability) + '</div>\n' +
                                '<div class="ek--lineup--drop--progress">\n' +
                                '<div class="progress">' + progressDiv + ' </div>' +
                                '</div>' +
                                '<div class="ek--lineup--drop--name text--white">' + defenders[3].name + '</div>' +
                                '</div>');
                        }


                    }

                    if(defenders.length>4) {


                        if(parseInt(defenders[4].energy) <= 25){
                            var progressDiv = '<div class="progress-bar bg--orange role="progressbar" style="width: ' + defenders[4].energy + '%" aria-valuenow="'+defenders[4].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }

                        else if(parseInt(defenders[4].energy) > 25 && parseInt(defenders[4].energy) <= 55 ){
                            var progressDiv = '<div class="progress-bar bg--yellow--two role="progressbar" style="width: ' + defenders[4].energy + '%" aria-valuenow="'+defenders[4].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }
                        else if(parseInt(defenders[4].energy) >55  && parseInt(defenders[4].energy) <= 90){
                            var progressDiv = '<div class="progress-bar bg--green--two role="progressbar" style="width: ' + defenders[4].energy + '%" aria-valuenow="'+defenders[4].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }
                        else if(parseInt(defenders[4].energy) >90  && parseInt(defenders[4].energy) <= 100){
                            var progressDiv = '<div class="progress-bar bg--green--dark--two role="progressbar" style="width: ' + defenders[4].energy + '%" aria-valuenow="'+defenders[4].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                        }


                        if($(".defenderFive").find(".ek--lineup--drag").length == 0) {

                            $('.defenderFive').append(' <div data-id="' + defenders[4].id + '" class="ek--lineup--drag">' +
                                ' <div class="ek--lineup--drop--abl bg--yellow text--white">' + parseInt(defenders[4].ability) + '</div>\n' +
                                '<div class="ek--lineup--drop--progress">\n' +
                                '<div  class="progress">' + progressDiv + ' </div>' +
                                '</div>' +
                                '<div class="ek--lineup--drop--name text--white">' + defenders[4].name + '</div>' +
                                '</div>');
                        }


                    }

                    if(goalkepper.length>0) {



                    if(parseInt(goalkepper[0].energy) <= 25){
                        var progressDiv = '<div class="progress-bar bg--orange role="progressbar" style="width: ' + goalkepper[0].energy + '%" aria-valuenow="'+goalkepper[0].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                    }

                    else if(parseInt(goalkepper[0].energy) > 25 && parseInt(goalkepper[0].energy) <= 55 ){
                        var progressDiv = '<div class="progress-bar bg--yellow--two role="progressbar" style="width: ' + goalkepper[0].energy + '%" aria-valuenow="'+goalkepper[0].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                    }
                    else if(parseInt(goalkepper[0].energy) >55  && parseInt(goalkepper[0].energy) <= 90){
                        var progressDiv = '<div class="progress-bar bg--green--two role="progressbar" style="width: ' + goalkepper[0].energy + '%" aria-valuenow="'+goalkepper[0].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                    }
                    else if(parseInt(goalkepper[0].energy) >90  && parseInt(goalkepper[0].energy) <= 100){
                        var progressDiv = '<div class="progress-bar bg--green--dark--two role="progressbar" style="width: ' + goalkepper[0].energy + '%" aria-valuenow="'+goalkepper[0].energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                    }



                        if($(".goalkeepOne").find(".ek--lineup--drag").length == 0) {

                            $('.goalkeepOne').append(' <div data-id="' + goalkepper[0].id + '" class="ek--lineup--drag">' +
                                ' <div class="ek--lineup--drop--abl bg--orange text--white">' + parseInt(goalkepper[0].ability) + '</div>\n' +
                                '<div class="ek--lineup--drop--progress">\n' +
                                '<div  class="progress">' + progressDiv + ' </div>' +
                                '</div>' +
                                '<div class="ek--lineup--drop--name text--white">' + goalkepper[0].name + '</div>' +
                                '</div>');
                        }


                }

                $.each(res.data, function (i, o) {

                    if(o.positionId==1 && o.playStatus==false){



                        if(parseInt(o.energy) <= 25){

                            var progressDiv = '<div class="progress-bar bg--orange role="progressbar" style="width: ' + o.energy + '%" aria-valuenow="'+o.energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                            var energyDiv='<span class="text--orange ek--size--16">'+ o.energy +'%</span>'
                        }

                        else if(parseInt(o.energy) > 25 && parseInt(o.energy) <= 55 ){

                            var progressDiv = '<div class="progress-bar bg--yellow--two role="progressbar" style="width: ' + o.energy + '%" aria-valuenow="'+o.energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                            var energyDiv='<span class="text--yellow--two ek--size--16">'+ o.energy +'%</span>'


                        }
                        else if(parseInt(o.energy) >55  && parseInt(o.energy) <= 90){

                            var progressDiv = '<div class="progress-bar bg--green--two role="progressbar" style="width: ' + o.energy + '%" aria-valuenow="'+o.energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                            var energyDiv='<span class="text--green--two ek--size--16">'+ o.energy +'%</span>'

                        }
                        else if(parseInt(o.energy) >90  && parseInt(o.energy) <= 100){

                            var progressDiv = '<div class="progress-bar bg--green--dark--two role="progressbar" style="width: ' + o.energy + '%" aria-valuenow="'+o.energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                            var energyDiv='<span class="text--green--dark--two ek--size--16">'+ o.energy +'%</span>'

                        }




                        var div = '<div data-id='+o.id +' class="ek--lineup--body--tabs--substitutions--inner goalkeep bg--navy--600" playertype="gk" draggable="true">' +
                            '<div class="ek--lineup--body--tabs--substitutions--inner--pos bg--orange text--white">G</div>' +
                            '<div class="ek--lineup--body--tabs--substitutions--inner--player ek--size--16 text--white">'+o.name+'</div>' +
                            '<div class="ek--lineup--body--tabs--substitutions--inner--abl ek--size--16 text--white">'+ parseInt(o.ability) +'</div>' +
                            '<div class="ek--lineup--body--tabs--substitutions--inner--energy ek--size--16 text--navy--200">' +
                            '<div class="ek--lineup--body--tabs--substitutions--inner--energy--progress bg--navy--500">' +
                            '<div  class="progress">' +progressDiv + '</div>' +
                            '</div>' + energyDiv +
                            '</div>' +
                            '</div>';



                        $('#substitutions.ek--lineup--body--tabs--substitutions--scroll').append(div);


                    }
                    else if(o.positionId==2 && o.playStatus==false){




                        if(parseInt(o.energy) <= 25){
                            var progressDiv = '<div class="progress-bar bg--orange role="progressbar" style="width: ' + o.energy + '%" aria-valuenow="'+o.energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                            var energyDiv='<span class="text--orange ek--size--16">'+ o.energy +'%</span>'

                        }

                        else if(parseInt(o.energy) > 25 && parseInt(o.energy) <= 55 ){
                            var progressDiv = '<div class="progress-bar bg--yellow--two role="progressbar" style="width: ' + o.energy + '%" aria-valuenow="'+o.energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                            var energyDiv='<span class="text--yellow--two ek--size--16">'+ o.energy +'%</span>'

                        }
                        else if(parseInt(o.energy) >55  && parseInt(o.energy) <= 90){
                            var progressDiv = '<div class="progress-bar bg--green--two role="progressbar" style="width: ' + o.energy + '%" aria-valuenow="'+o.energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                            var energyDiv='<span class="text--green--two ek--size--16">'+ o.energy +'%</span>'

                        }
                        else if(parseInt(o.energy) >90  && parseInt(o.energy) <= 100){
                            var progressDiv = '<div class="progress-bar bg--green--dark--two role="progressbar" style="width: ' + o.energy + '%" aria-valuenow="'+o.energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                            var energyDiv='<span class="text--green--dark--two ek--size--16">'+ o.energy +'%</span>'

                        }


                        $('#substitutions.ek--lineup--body--tabs--substitutions--scroll').append('<div data-id='+o.id +' class="ek--lineup--body--tabs--substitutions--inner defender bg--navy--600" playertype="df" draggable="true">\n' +
                            '<div class="ek--lineup--body--tabs--substitutions--inner--pos bg--yellow text--white">d</div>' +
                            '<div class="ek--lineup--body--tabs--substitutions--inner--player ek--size--16 text--white">'+o.name+'</div>' +
                            '<div class="ek--lineup--body--tabs--substitutions--inner--abl ek--size--16 text--white">'+ parseInt(o.ability) +'</div>' +
                            '<div class="ek--lineup--body--tabs--substitutions--inner--energy ek--size--16 text--navy--200">' +
                            '<div class="ek--lineup--body--tabs--substitutions--inner--energy--progress bg--navy--500">' +
                            '<div  class="progress">' +progressDiv +'</div>' +
                            '</div>' + energyDiv+
                            '</div>' +
                            '</div>');

                    }
                    else if(o.positionId==3 && o.playStatus==false){


                        if(parseInt(o.energy) <= 25){
                            var progressDiv = '<div class="progress-bar bg--orange role="progressbar" style="width: ' + o.energy + '%" aria-valuenow="'+o.energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                            var energyDiv='<span class="text--orange ek--size--16">'+ o.energy +'%</span>'

                        }

                        else if(parseInt(o.energy) > 25 && parseInt(o.energy) <= 55 ){
                            var progressDiv = '<div class="progress-bar bg--yellow--two role="progressbar" style="width: ' + o.energy + '%" aria-valuenow="'+o.energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                            var energyDiv='<span class="text--yellow--two ek--size--16">'+ o.energy +'%</span>'

                        }
                        else if(parseInt(o.energy) >55  && parseInt(o.energy) <= 90){
                            var progressDiv = '<div class="progress-bar bg--green--two role="progressbar" style="width: ' + o.energy + '%" aria-valuenow="'+o.energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                            var energyDiv='<span class="text--green--two ek--size--16">'+ o.energy +'%</span>'

                        }
                        else if(parseInt(o.energy) >90  && parseInt(o.energy) <= 100){
                            var progressDiv = '<div class="progress-bar bg--green--dark--two role="progressbar" style="width: ' + o.energy + '%" aria-valuenow="'+o.energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                            var energyDiv='<span class="text--green--dark--two ek--size--16">'+ o.energy +'%</span>'

                        }

                        $('#substitutions.ek--lineup--body--tabs--substitutions--scroll').append('<div data-id='+o.id +' class="ek--lineup--body--tabs--substitutions--inner midfielder bg--navy--600" playertype="mf" draggable="true">' +
                            '<div class="ek--lineup--body--tabs--substitutions--inner--pos bg--cyan--two text--white">m</div>' +
                            '<div class="ek--lineup--body--tabs--substitutions--inner--player ek--size--16 text--white">'+o.name+'</div>' +
                            '<div class="ek--lineup--body--tabs--substitutions--inner--abl ek--size--16 text--white">'+ parseInt(o.ability) +'</div>' +
                            '<div class="ek--lineup--body--tabs--substitutions--inner--energy ek--size--16 text--navy--200">' +
                            '<div class="ek--lineup--body--tabs--substitutions--inner--energy--progress bg--navy--500">' +
                            '<div  class="progress">'+ progressDiv +'</div>' +
                            '</div>' + energyDiv +
                            '</div>' +
                            '</div>');



                    }
                    else if(o.positionId==4 && o.playStatus==false){

                        if(parseInt(o.energy) <= 25){
                            var progressDiv = '<div class="progress-bar bg--orange role="progressbar" style="width: ' + o.energy + '%" aria-valuenow="'+o.energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                            var energyDiv='<span class="text--orange ek--size--16">'+ o.energy +'%</span>'

                        }

                        else if(parseInt(o.energy) > 25 && parseInt(o.energy) <= 55 ){
                            var progressDiv = '<div class="progress-bar bg--yellow--two role="progressbar" style="width: ' + o.energy + '%" aria-valuenow="'+o.energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                            var energyDiv='<span class="text--yellow--two ek--size--16">'+ o.energy +'%</span>'

                        }
                        else if(parseInt(o.energy) >55  && parseInt(o.energy) <= 90){
                            var progressDiv = '<div class="progress-bar bg--green--two role="progressbar" style="width: ' + o.energy + '%" aria-valuenow="'+o.energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                            var energyDiv='<span class="text--green--two ek--size--16">'+ o.energy +'%</span>'

                        }
                        else if(parseInt(o.energy) >90  && parseInt(o.energy) <= 100){
                            var progressDiv = '<div class="progress-bar bg--green--dark--two role="progressbar" style="width: ' + o.energy + '%" aria-valuenow="'+o.energy+'" aria-valuemin="0" aria-valuemax="100"></div>';

                            var energyDiv='<span class="text--green--dark--two ek--size--16">'+ o.energy +'%</span>'

                        }



                        $('#substitutions.ek--lineup--body--tabs--substitutions--scroll').append('<div data-id='+o.id +' class="ek--lineup--body--tabs--substitutions--inner forward bg--navy--600" playertype="fd" draggable="true">' +
                            '<div class="ek--lineup--body--tabs--substitutions--inner--pos bg--purple text--white">f</div>' +
                            '<div class="ek--lineup--body--tabs--substitutions--inner--player ek--size--16 text--white">'+o.name+'</div>' +
                            '<div class="ek--lineup--body--tabs--substitutions--inner--abl ek--size--16 text--white">'+ parseInt(o.ability) +'</div>' +
                            '<div class="ek--lineup--body--tabs--substitutions--inner--energy ek--size--16 text--navy--200">' +
                            '<div class="ek--lineup--body--tabs--substitutions--inner--energy--progress bg--navy--500">' +
                            '<div  class="progress">'+ progressDiv +'</div>' +
                            '</div>' + energyDiv +
                            '</div>' +
                            '</div>');


                    }




});
playerLineUp();


}


},

});


    }


    getGameStyle();

    $('body').on('click', '#refillTeam', function (e) {

        $.ajax({
            url:  '/team/refill/coins/limit',
            type: 'get',

            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    console.log("!data");
                }
                else if (res.hasOwnProperty("data")) {

                    $('.ek--squad--modals--refill--head--want--amount #cost').html('<span id="userCoins" class="text--white ek--size--16">'+ res.data +'</span>');

                    $('#ek--squad--modals--refill--buy').modal('show');
                    console.log("success:");
                }


            },
        });


        e.preventDefault()
    });



    $('body').on('click', '#refill', function (e) {



        $.ajax({
            url:  '/getUserTotalBudgets',
            type: 'get',

            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    $('#ek--soft--notenough').modal('show');
                }
                else if (res.hasOwnProperty("data")) {

                    var coins= $('#userCoins').text();
                    console.log("userCoins" + coins);
                    if(parseInt(res.data.coins)< parseInt(coins)){

                        $('#ek--soft--notenough').modal('show');
                    }
                    else{

                        $.ajax({
                            url:  '/team/refill',
                            type: 'get',

                            success: function (res) {
                                if (!res.hasOwnProperty("data")) {
                                    $('#ek--soft--notenough').modal('show');
                                }
                                else if (res.hasOwnProperty("data")) {
                                    $('#ek--notification').modal('show');


                                    $.ajax({
                                        url:  '/getUserBudget',
                                        type: 'get',
                                        success: function (res) {
                                            if (!res.hasOwnProperty("data")) {
                                                console.log("!data");
                                            }
                                            else if (res.hasOwnProperty("data")) {

                                                if(res.data.money < 1000) {

                                                    $('.ek--body--head--money #money').html('<span class="text--white ek--size--16">' + res.data.money + '</span>');
                                                }

                                                else if(res.data.money >=1000 && res.data.money < 1000000) {

                                                    var money= res.data.money/1000;

                                                    $('.ek--body--head--money #money').html('<span class="text--white ek--size--16">' + parseInt(money) + '</span><span class="text--white ek--size--16">K</span>');

                                                }

                                                else if(res.data.money >=1000000 && res.data.money < 1000000000) {

                                                    var money= (res.data.money)/1000000;
                                                    console.log(money);
                                                    console.log(res.data.money);

                                                    $('.ek--body--head--money #money').html('<span class="text--white ek--size--16">' + parseInt(money) + '</span><span class="text--white ek--size--16">M</span>');

                                                }

                                                else if(res.data.money >=1000000000) {

                                                    var money= res.data.money/1000000000;
                                                    console.log(money);

                                                    $('.ek--body--head--money #money').html('<span class="text--white ek--size--16">' + parseInt(money) + '</span><span class="text--white ek--size--16">B</span>');

                                                }
                                                $('.ek--body--head--money #coins').html('<span class="text--white ek--size--16">'+ res.data.coins +'</span>');

                                                console.log("success:");
                                            }

                                            $('#substitutions.ek--lineup--body--tabs--substitutions--scroll').html('');

                                            // getSubstitutions();

                                            location.reload();


                                        },
                                    });


                                    e.preventDefault()



                                }


                            },
                        });
                    }


                }


            },
        });








        e.preventDefault()
    });


    $('body').on('click', '#reset', function (e) {

        $('#substitutions.ek--lineup--body--tabs--substitutions--scroll').html('');


        $.ajax({
            url:  '/team/lineup/clr/main/p',
            type: 'Get',
            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    console.log("!data");
                }
                else if (res.hasOwnProperty("data")) {

                    $('.ek--lineup--drag').each(function () {
                        $(this).remove();
                    });
                    $('.ek--lineup--body--tabs--substitutions--inner').css({
                        'order': 'unset',
                        'pointer-events': 'auto',
                        'opacity': '1'
                    });
                    $('.ek--lineup--stadium--position--player--empty').removeAttr('style');

                    getSubstitutions();


                    console.log("success:");
                }


            },
        });


        e.preventDefault()
    });


    $('body').on('click', '#random', function (e) {



        var forvards = [];
        var goalkepper = [];
        var midfielder = [];
        var defenders = [];

        forvards.length=0;
        goalkepper.length=0;
        midfielder.length=0;
        defenders.length=0;


        $.ajax({
            url:  '/team/lineup/auto-select',
            type: 'get',

            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    console.log("!data");
                }
                else if (res.hasOwnProperty("data")) {

                    console.log("success:");


                    // $('#substitutions.ek--lineup--body--tabs--substitutions--scroll').html('');

                        // getSubstitutions();
                    location.reload();


                }


            },
        });


        e.preventDefault()

    });


    $('body').on('click', '.formation', function (e) {

        $('#loading').css('display' , 'block');

        var formationId = $(this).attr("data-id");
        var formatId = $(this).attr("format-id");
        var formatList = formatId.split('-');
        console.log("FormationId:"+ formationId);
        console.log("FormatList:"+ formatList);


        var ListF=[];
        var ListM=[];
        var ListD=[];
        var ListG=[];
        var f=formatList[2];
        var m=formatList[1];
        var d=formatList[0];


        $.ajax({
            url:  'player/u',
            type: 'get',
            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    console.log("!data");
                }
                else if (res.hasOwnProperty("data")) {


                    ListF = [];
                    ListM = [];
                    ListD = [];
                    ListG = [];


                    $.each(res.data, function (i, o) {


                        if (o.positionId == 4 && o.playStatus == true) {
                            ListF.push(o.id);
                        }
                        else if (o.positionId == 3 && o.playStatus == true) {
                            ListM.push(o.id);

                        }
                        else if (o.positionId == 2 && o.playStatus == true) {
                            ListD.push(o.id);

                        }
                        else if (o.positionId == 1 && o.playStatus == true) {
                            ListG.push(o.id);

                        }

                    });

                    console.log("before D:" + ListD);
                    console.log("before M:" + ListM);
                    console.log("before F:" + ListF);

                    var i;
                    var x = parseInt(ListF.length) - parseInt(f);
                    console.log("ArrayF Length:" + ListF.length);
                    console.log("f:"+f);
                    console.log("x:" + x);
                    if (x > 0) {

                        for (ii = 0; ii < x; ii++) {
                            var id = ListF[ii];
                            console.log(id + "id");
                            $.ajax({
                                url: '/team/lineup/remove/player',
                                type: 'post',
                                data: {
                                    "_token": $('meta[name="csrf"]').attr('content'),
                                    playerId: id
                                },
                                success: function (res) {
                                    if (!res.hasOwnProperty("data")) {
                                        console.log("!data");

                                    }
                                    else if (res.hasOwnProperty("data")) {

                                        console.log("success:");

                                        ListF = jQuery.grep(ListF, function (value) {
                                            return value != id;

                                        });


                                    }
                                },
                            });
                        }
                    }
                    var j;
                    var y = parseInt(ListM.length) - parseInt(m);

                    console.log("ArrayM Length:" + ListM.length);
                    console.log("m:"+m);
                    console.log("y:" + y);

                    if (y >0) {
                        for (j = 0; j < y; j++) {

                            var id = ListM[j];

                            $.ajax({
                                url: '/team/lineup/remove/player',
                                type: 'post',
                                data: {
                                    "_token": $('meta[name="csrf"]').attr('content'),
                                    playerId: id
                                },
                                success: function (res) {
                                    if (!res.hasOwnProperty("data")) {
                                        console.log("!data");

                                    }
                                    else if (res.hasOwnProperty("data")) {

                                        console.log("success:");

                                        ListM = jQuery.grep(ListM, function (value) {
                                            return value != id;

                                        });

                                    }


                                },
                            });
                        }
                    }
                    var k;
                    var z = parseInt(ListD.length) - parseInt(d);

                    console.log("ArrayD Length:" + ListD.length);
                    console.log("d:"+d);
                    console.log("z:" + z);

                    if (z > 0) {
                        for (k = 0; k < z; k++) {

                            var id = ListD[k];
                            $.ajax({
                                url: '/team/lineup/remove/player',
                                type: 'post',
                                data: {
                                    "_token": $('meta[name="csrf"]').attr('content'),
                                    playerId: id
                                },
                                success: function (res) {
                                    if (!res.hasOwnProperty("data")) {
                                        console.log("!data");

                                    }
                                    else if (res.hasOwnProperty("data")) {

                                        console.log("success:");

                                        ListD = jQuery.grep(ListD, function (value) {
                                            return value != id;

                                        });


                                        console.log("id:" + id);
                                    }


                                },
                            });


                        }
                    }


                        console.log("After D:" + ListD);
                        console.log("After M:" + ListM);
                        console.log("After F:" + ListF);



                }
                },
            });





        $.ajax({
            url:  '/club/update/formation/',
            type: 'post',
            data: {
             "_token": $('meta[name="csrf"]').attr('content'),
             formationId:formationId
            },
            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    console.log("!data");
                }
                else if (res.hasOwnProperty("data")) {

                   // location.reload();


                    $('.forwardOne .ek--lineup--drag').remove();
                    $('.forwardTwo .ek--lineup--drag').remove();
                    $('.forwardThree .ek--lineup--drag').remove();

                    $('.midfielderOne .ek--lineup--drag').remove();
                    $('.midfielderTwo .ek--lineup--drag').remove();
                    $('.midfielderThree .ek--lineup--drag').remove();
                    $('.midfielderFour .ek--lineup--drag').remove();
                    $('.midfielderFive .ek--lineup--drag').remove();

                    $('.defenderOne .ek--lineup--drag').remove();
                    $('.defenderTwo .ek--lineup--drag').remove();
                    $('.defenderThree .ek--lineup--drag').remove();
                    $('.defenderFour .ek--lineup--drag').remove();
                    $('.defenderFive .ek--lineup--drag').remove();

                    $('.goalkeepOne .ek--lineup--drag').remove();


                    $('.ek--lineup--body--tabs--substitutions--scroll#substitutions').html('');


                    getSubstitutions();

                    console.log("success:");

                    $('#loading').hide();

                    location.reload()

                }


            },
        });




    });


    $('.ek--lineup--body--tabs--tactics--inner--type .soft').click(function () {
        $(this).parent().find('span').removeClass('range--active');
        $(this).addClass('range--active');
        var value = 1;
        $(this).parents('form').find('input').val(value).change();
    });
    $('.ek--lineup--body--tabs--tactics--inner--type .normal').click(function () {
        $(this).parent().find('span').removeClass('range--active');
        $(this).addClass('range--active');
        var value = 2;
        $(this).parents('form').find('input').val(value).change();
    });
    $('.ek--lineup--body--tabs--tactics--inner--type .aggresive').click(function () {
        $(this).parent().find('span').removeClass('range--active');
        $(this).addClass('range--active');
        var value = 3;
        $(this).parents('form').find('input').val(value).change();
    });


    function getGameStyle() {

        $.ajax({
            url:  '/getClubData',
            type: 'get',
            success: function (res) {
                if (!res.hasOwnProperty("data")) {
                    console.log("!data");
                }
                else if (res.hasOwnProperty("data")) {

                   $('#gamestyle').attr('value',res.data.gameStyle.id);
                    $('#passingstyle').attr('value',res.data.passingStyle.id);
                    $('#pressingstyle').attr('value',res.data.pressingStyle.id);

                    $('#gamestyle , #passingstyle , #pressingstyle ').rangeslider({
                        polyfill: false

                    }).on('change', function() {

                        var game =[];

                        var gameId = $('#gamestyle').val();
                        var passId=$('#passingstyle').val();
                        var pressId =$('#pressingstyle').val();
                        game=[gameId,passId,pressId];
                        console.log("Array:"+ game);

                        var list =game;

                        $.ajax({
                            url:  '/club/update/tactics',
                            type: 'post',
                            data: {
                                "_token": $('meta[name="csrf"]').attr('content'),
                                clubTactics: list
                            },
                            success: function (res) {
                                if (!res.hasOwnProperty("data")) {
                                    console.log("!data");
                                }
                                else if (res.hasOwnProperty("data")) {

                                    // window.location.pathname = '/lineup';

                                    getGameStyle();
                                    console.log("success:");



                                }


                            },
                        });


                        var values = $(this).val();
                        var numValue = Number(values);
                        $(this).attr('value', numValue);
                        if(numValue === 1) {
                            $(this).parent().find('.soft').addClass('range--active').nextAll().removeClass('range--active');
                        }
                        if (numValue === 2) {
                            $(this).parent().find('.normal').addClass('range--active');
                            $(this).parent().find('.normal').prev().removeClass('range--active');
                            $(this).parent().find('.normal').next().removeClass('range--active');
                        }
                        if (numValue === 3) {
                            $(this).parent().find('.aggresive').addClass('range--active').prevAll().removeClass('range--active');
                        }



                    });

                    /*var $('#gamestyle , #passingstyle , #pressingstyle ')*/

                    var gamestyleVal = $('#gamestyle').val();
                    if(Number(gamestyleVal) === 1) {
                        $('#gamestyle').parent().find('.soft').addClass('range--active').nextAll().removeClass('range--active');
                    }
                    if (Number(gamestyleVal) === 2) {
                        $('#gamestyle').parent().find('.normal').addClass('range--active');
                        $('#gamestyle').parent().find('.normal').prev().removeClass('range--active');
                        $('#gamestyle').parent().find('.normal').next().removeClass('range--active');
                    }
                    if (Number(gamestyleVal) === 3) {
                        $('#gamestyle').parent().find('.aggresive').addClass('range--active').prevAll().removeClass('range--active');
                    }

                    var passingstyleVal = $('#passingstyle').val();
                    if(Number(passingstyleVal) === 1) {
                        $('#passingstyle').parent().find('.soft').addClass('range--active').nextAll().removeClass('range--active');
                    }
                    if (Number(passingstyleVal) === 2) {
                        $('#passingstyle').parent().find('.normal').addClass('range--active');
                        $('#passingstyle').parent().find('.normal').prev().removeClass('range--active');
                        $('#passingstyle').parent().find('.normal').next().removeClass('range--active');
                    }
                    if (Number(passingstyleVal) === 3) {
                        $('#passingstyle').parent().find('.aggresive').addClass('range--active').prevAll().removeClass('range--active');
                    }

                    var pressingstyleVal = $('#pressingstyle').val();
                    if(Number(pressingstyleVal) === 1) {
                        $('#pressingstyle').parent().find('.soft').addClass('range--active').nextAll().removeClass('range--active');
                    }
                    if (Number(pressingstyleVal) === 2) {
                        $('#pressingstyle').parent().find('.normal').addClass('range--active');
                        $('#pressingstyle').parent().find('.normal').prev().removeClass('range--active');
                        $('#pressingstyle').parent().find('.normal').next().removeClass('range--active');
                    }
                    if (Number(pressingstyleVal) === 3) {
                        $('#pressingstyle').parent().find('.aggresive').addClass('range--active').prevAll().removeClass('range--active');
                    }


                }


            },
        });

    }


});





